<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\State;
use Illuminate\Http\Request;
use View;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Rap2hpoutre\FastExcel\FastExcel;

class DistrictController extends Controller
{

    public function importDistricts(Request $request)

    {
        if($request->file('districtExcel') == null)
        {
            return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        }

        /* Import dealerloyees */
        $districts = (new FastExcel)->import($request->file('districtExcel')->getRealPath(), function ($line) {
                        $district = new District();
                        $district->id = $line['id'];
                        $district->state_id = $line['state_id'];
                        $district->name = $line['name'];
                        $district->zone = $line['zone'];
                        $district->save();
        });

        return back();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $items = $request->items ?? 5;
        $districts = District::where('status','=',1)->with('state')->paginate($items);
        $itemArr = array();   
        $itemArr[] = $districts;
        return view('district.index')->with(compact('districts','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $items = State::all(['id', 'name']);
        return view('district.create', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $district = new District();
            $district->name = $request->name;
            $district->code = $request->code;
            $district->state_id = $request->state;

        $itemArr = array();   
        $itemArr[] = $request->state;

            $district->save();
            return redirect()->route("district.index")->with(['result'=>'New district added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("district.index")->with(['result'=>'Error adding district','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
          $district = District::find($district->id);
          $id = $district->id;
          $items = State::all(['id', 'name']);
          return view('district.edit')->with(compact('district','id','items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, District $district)
    {
        try{
         $district = District::find($district->id);
         $district->name = $request->name;
         $district->code = $request->code;
         $district->state_id = $request->state;
         $district->save();
         return redirect()->route('district.index')->with(['result'=>'district updated successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("district.index")->with(['result'=>'Error updating district','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $districts = District::find($district->id);
        $districts->status = 0;
        $districts->save();
        return redirect()->route('district.index')->with(['result'=>'district deleted successfully','color'=>'bg-green']);
    }

    public function FilterRows(Request $request)
    {
        $districts = District::where('status','=',1)->with('state')->where('name', 'LIKE', '%' . $request->queryFilter . '%')->where('status','=',1)->paginate(5);
        return $districts;
    }
}
