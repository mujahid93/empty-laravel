<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dealer;
use App\Models\District;
use App\Models\DealerVisit;
use App\Models\Employee;
use App\Models\Feedback;
use App\Models\FeedbackAnswer;
use App\Models\DemoCategory;
use Illuminate\Support\Facades\Input;
use DB;
use Log;
use Carbon\Carbon;
use Rap2hpoutre\FastExcel\FastExcel;
use Rap2hpoutre\FastExcel\SheetCollection;

class FeedbackController extends Controller{
    //Save Feedback
    public function saveFeedback(){

    	$data=Input::all();
    	//Log::info($data);
    	if($data==null){
    		return;
    	}
		
		// $test=$data;
        // $test['checkin_image']="";
        // $test['checkout_image']="";
        // Log::info($test);

    	DB::beginTransaction();
         try{
         	$feedback = new Feedback();
         	$feedback->emp_id=$data['emp_id'];
         	$feedback->check_in=$data['checkin_time'];
         	$feedback->check_out=$data['checkout_time'];
         	$feedback->cat_id=$data['cat_id'];
         	$feedback->subcat_id=$data['subcat_id'];
         	$time=Carbon::now();
	        $time=strtotime($time);
         	$filenameFront=$feedback->emp_id."_".$time."_Check_IN.jpg";
        	$filenameInside=$feedback->emp_id."_".$time."_Check_OUT.jpg";
         	$checkin=$this->base64ToImage($data['checkin_image'],$filenameFront);
        	$checkout=$this->base64ToImage($data['checkout_image'],$filenameInside);
        	$feedback->check_in_img=$checkin;
        	$feedback->check_out_img=$checkout;
        	$feedback->save();
        	foreach ($data['answers'] as $key => $value) {
        		$answer=new FeedbackAnswer();
        		$answer->feedback_id=$feedback->id;
        		$answer->question_id=$value['question_id'];
        		$answer->answer=$value['answer'];
        		$answer->save();
        	}
         }
         catch(\Exeption $e){
            DB::rollback();
            return array('status'=>false);
         }
         //app('App\Http\Controllers\NotifController')->sendDemoNotification($emp_id,$token);
         DB::commit();
    	return array('status'=>true);
    }

    function base64ToImage($base64_string, $output_file) {
        $decoded=base64_decode($base64_string); 
        file_put_contents("images/".$output_file,$decoded);
        return "images/".$output_file;
    }

    public function getDemoFeedbackReports(){
        $category=DB::table('demo_categories')->where('status',1)->groupBy('catName')->orderBy('cat_id')->get();
        $subcat=DB::table('demo_categories')->where('status',1)->get();
        return view('feedback.feedback_report')->with(compact('category','subcat'));
    }
	
	 public function getDemoFeedbackList(){
        $items = $request->items ?? 10;
        $feedback=Feedback::orderBy('id','DESC')->paginate($items);
        return view('feedback.feedback_list')->with(compact('feedback'));
    }

    public function exportFeedbackReport(Request $req,$fromDate,$toDate,$subcat_id){
		$subcats=Feedback::select('subcat_id')
				->where('check_in','>=',$fromDate.' 00:00:00')
				->where('check_out','<=',$toDate.' 23:59:59')
				->distinct()
				->get();
				
		$sheets="";
		$index=1;
		
				
		foreach($subcats as $key => $val){
			$questions=DB::table('feedback_questions')->where('subcat_id',$val['subcat_id'])->get();

			$select="emp_id,employees.name as empName,check_in,check_out";
			foreach ($questions as $key => $value) {
				$select=$select.",MAX(CASE WHEN feedback_answer.question_id=$value->id THEN feedback_answer.answer END) as '$value->question'";
			}

			${"feed" . $index}=Feedback::leftJoin('feedback_answer',function($join){
				$join->on('feedback_answer.feedback_id','=','demo_feedback.id');
			})->leftJoin('feedback_questions',function($join){
				$join->on('feedback_answer.question_id','=','feedback_questions.id');
			})->leftJoin('employees',function($join){
				$join->on('demo_feedback.emp_id','=','employees.id');
			})
			->select(DB::raw($select))
			->where('demo_feedback.check_in','>=',$fromDate.' 00:00:00')
			->where('demo_feedback.check_in','<=',$toDate.' 23:59:59')
			->where('demo_feedback.subcat_id',$val['subcat_id'])
			->groupBy('demo_feedback.id')
			->get();
			
			
			
			$sheetName=DB::table('demo_categories')->where('id',$val['subcat_id'])->value('subCatName');
			$sheetName = str_replace(' ', '-', $sheetName);
			$sheetName=preg_replace('/[^A-Za-z0-9\-]/', '', $sheetName);
			$sheets=$sheets."'$sheetName'=>".'$feed'.$index.',';
			$index++;
		}
		$sheets=substr($sheets, 0, -1);

		$sheets=eval("return array(".$sheets.");");
		$coll = new SheetCollection($sheets);
		
		return (new FastExcel($coll))->download('feedback_report.xlsx');
	
        
       // return (new FastExcel($feed))->download('file.xlsx');
    }
	
	
	
	public function exportSingleFeedbackReport(Request $req,$feedback_id){
        $feed=Feedback::leftJoin('feedback_answer',function($join){
            $join->on('feedback_answer.feedback_id','=','demo_feedback.id');
        })->leftJoin('feedback_questions',function($join){
            $join->on('feedback_answer.question_id','=','feedback_questions.id');
        })->leftJoin('employees',function($join){
            $join->on('demo_feedback.emp_id','=','employees.id');
        })
		->select(DB::raw("emp_id,employees.name as empName,feedback_questions.question as question,feedback_answer.answer"))
        ->where('demo_feedback.id',$feedback_id)
        ->get();
        return (new FastExcel($feed))->download($feed[0]['empName'].' feedback.xlsx');
    }
}
