<?php



namespace App\Http\Controllers;



use App\Models\AccessoriesSku;

use App\Models\AccSubsidy;

use App\Models\Version;

use Illuminate\Http\Request;

use DB;

use Redirect;

use Rap2hpoutre\FastExcel\FastExcel;



class AccessoriesSkuController extends Controller

{

    public function importAccessories(Request $request)

    {

        //Update mrp wherever 0
        // (new FastExcel)->import($request->file('accExcel')->getRealPath(), function ($line) {
        //     AccessoriesSku::where('sku',$line['sku'])->update(['mrp'=>$line['mrp']]);
        // });


        /* Import Accessory Sku */
        if($request->file('accExcel') == null)
        {
            return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        }
        $users = (new FastExcel)->import($request->file('accExcel')->getRealPath(), function ($line) {
            $acc = new AccessoriesSku();
            $acc->item = $line['items'];
            $acc->grp = $line['grp'];
            $acc->subgrp = $line['subgrp'];
            $acc->sku = $line['sku'];
            // $acc->hsn = $line['hsn'];
            $acc->gst = $line['gst'];
            $acc->dp = $line['dp'];
            $acc->mrp = $line['mrp'];
            if($line['subsidy'] > 0)
                $acc->subsidy = 1;
            $acc->save();
        });
        Version::where('tbl_name','acc_sku')->update(['version'=> DB::raw('version+1')]);


        // /* Import Accessories Subsidy */

        // $users = (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {

        //     if($line['subsidy'] == 0)

        //         $temp = 0;

        //     else if($line['subsidy'] > 0)

        //     {

        //         $accId = AccessoriesSku::where('sku',$line['sku'])->value('id');

        //         for ($i=1; $i < 3; $i++) { 

        //             $accSubsidy = new AccSubsidy();

        //             $accSubsidy->acc_id = $accId;

        //             $accSubsidy->branch_id = $i;

        //             $accSubsidy->subsidy_price = $line['subsidy'];

        //             $accSubsidy->save();

        //         }

        //     }

        // });

        // Version::where('tbl_name','acc_subsidy')->update(['version'=> DB::raw('version+1')]);

        return back();

    }



    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $items = $request->items ?? 5;

        $accSkus = AccessoriesSku::where('status','=',1)->paginate($items);

        return view('accSku.index')->with(compact('accSkus','items'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $accGroups = AccessoriesSku::distinct('grp')->pluck('grp');

        $accSubgroups = AccessoriesSku::distinct('subgrp')->pluck('subgrp');

        return view('accSku.create')->with(compact('accGroups','accSubgroups'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        try{

            $accSku = new AccessoriesSku();

            $accSku->grp = $request->accGroup;

            $accSku->subgrp = $request->accSubgroup;

            $accSku->sku = $request->sku;

            $accSku->hsn = $request->hsn;

            $accSku->gst = $request->gst;

            $accSku->dp = $request->dp;

            $accSku->mrp = $request->mrp;

            $accSku->subsidy = $request->subsidy;

            $accSku->bulk_qty = $request->bulkQty;

            if($accSku->save())

            Version::where('tbl_name','acc_sku')->update(['version'=> DB::raw('version+1')]);

            return redirect()->route("accSku.index")->with(['result'=>'New Accessories Sku added','color'=>'bg-green']);

        }

        catch(\Exception $e){

            return redirect()->route("accSku.index")->with(['result'=>'Error adding Accessories Sku','color'=>'alert-warning']);

        }

    }



    /**

     * Display the specified resource.

     *

     * @param  \App\Models\AccessoriesSku  $accSku

     * @return \Illuminate\Http\Response

     */

    public function show(AccessoriesSku $accSku)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Models\AccessoriesSku  $accSku

     * @return \Illuminate\Http\Response

     */

    public function edit(AccessoriesSku $accSku)

    {   

        // Log::info($accSku->id);



        $accGroups = AccessoriesSku::distinct('grp')->pluck('grp');

        $accSubgroups = AccessoriesSku::distinct('grp')->pluck('subgrp');

        $accSku = AccessoriesSku::find($accSku->id);

        return view('accSku.edit')->with(compact('accSku','accGroups','accSubgroups'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Models\AccessoriesSku  $accSku

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, AccessoriesSku $accSku)

    {

            $accSku = AccessoriesSku::find($accSku->id);

            $accSku->grp = $request->accGroup;

            $accSku->subgrp = $request->accSubgroup;

            $accSku->sku = $request->sku;

            $accSku->hsn = $request->hsn;

            $accSku->gst = $request->gst;

            $accSku->dp = $request->dp;

            $accSku->mrp = $request->mrp;

            $accSku->subsidy = $request->subsidy;

            $accSku->bulk_qty = $request->bulkQty;

        if($accSku->save())

            Version::where('tbl_name','acc_sku')->update(['version'=> DB::raw('version+1')]);

        return Redirect::route('accSku.index',array('items' => 5));

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Models\AccessoriesSku  $accSku

     * @return \Illuminate\Http\Response

     */

    public function destroy(AccessoriesSku $accSku)

    {

        AccessoriesSku::where('id',$accSku->id)->update(['status'=>0]);

        Version::where('tbl_name','acc_sku')->update(['version'=> DB::raw('version+1')]);

        return Redirect::route('accSku.index',array('items' => 5));

    }



    public function FetchSkuByGroup(Request $r)        

    {

        $subgrp = AccessoriesSku::where('grp',$r->grp)->distinct('subgrp')->pluck('subgrp');

        return $subgrp;

    }

}

