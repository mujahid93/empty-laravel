<?php

namespace App\Http\Controllers;

use App\Models\Tax;
use Illuminate\Http\Request;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $request->items ?? 5;
        $taxes = Tax::paginate($items);
        return view('tax.index')->with(compact('taxes','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $tax = new Tax();
            $tax->hsn = $request->hsn;
            $tax->gst = $request->gst;
            if($tax->save())
                Version::where('tbl_name','tax')->update(['version'=> DB::raw('version+1')]);
            return redirect()->route("tax.index")->with(['result'=>'New tax added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("tax.index")->with(['result'=>'Error adding tax','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function show(Tax $tax)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function edit(Tax $tax)
    {
          $taxes = Tax::find($tax->id);
          $id = $tax->id;
          return view('tax.edit')->with(compact('taxes','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tax $tax)
    {
         try{
         $tax = Tax::find($tax->id);
         $tax->hsn = $request->hsn;
         $tax->gst = $request->gst;
         if($tax->save())
                Version::where('tbl_name','tax')->update(['version'=> DB::raw('version+1')]);
         return redirect()->route('tax.index')->with(['result'=>'tax updated successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("tax.index")->with(['result'=>'Error updating tax','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tax $tax)
    {
        
    }

    public function FilterRows(Request $request)
    {
        $taxes = Tax::where('name', 'LIKE', '%' . $request->queryFilter . '%')->paginate(5);
        return $taxes;
    }
}
