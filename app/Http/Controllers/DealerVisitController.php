<?php

namespace App\Http\Controllers;

use App\Models\Dispatch;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Order;
use App\Models\Dealer;
use App\Models\District;
use App\Models\DealerVisit;
use App\Models\Employee;
use App\Exports\DealerVisitExport;
use Illuminate\Support\Facades\Auth;
use Session;
use Log;
use Redirect;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use ZipArchive;

class DealerVisitController extends Controller
{
    //
    public function index()
    {

        //From Date
        if(Session::has('dealerVisit_fromDate'))
            $fromDate = Session::get('dealerVisit_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');

        //To Date
        if(Session::has('dealerVisit_toDate'))
            $toDate = Session::get('dealerVisit_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');

        //Employee Data
        if(Session::has('dealerVisitEmp')){
            $emp_id = Session::get('dealerVisitEmp');   
            $emp_id = array_values($emp_id);         
        }
        else
            $emp_id ="";



        $employees=Employee::where('status',1)->orderBy('name','ASC')->get();

        $items = $request->items ?? 10;

        if($emp_id==""){
            $dealerVisits=DealerVisit::where('created_at','>=',$fromDate.' 00:00:00')
                ->where('created_at','<=',$toDate.' 23:59:59')
                ->where('is_deleted',0)
                ->paginate($items);
        }
        else{
            $dealerVisits=DealerVisit::where('created_at','>=',$fromDate.' 00:00:00')
                ->where('created_at','<=',$toDate.' 23:59:59')
                ->where('is_deleted',0)
                ->whereIn('emp_id',$emp_id)
                ->paginate($items);
        }

        return view('dealer.dealer_visit')->with(compact('dealerVisits','items','employees'));
    }

    public function saveDealerVisit(){
        $data=Input::all();
        //Log::info($data);
        /*$test=$data;
        $test['img_url1']="";
        $test['img_url2']="";
        Log::info($test);*/
        
        if($data==null){
            $emp=Auth::user()->employee;
            Log::info("NULL DATA FROM: ".$emp->name);
            return array('success'=>false);
        }
        if (!array_key_exists("is_new",$data)){
            $data['is_new']=1;
        }
        
        
        $dealerVisit = new DealerVisit();
        $dealerVisit->is_new=$data['is_new'];
        $dealerVisit->emp_id=Auth::user()->employee->id;
        if($data['dealer_id']==0)
            $dealerVisit->dealer_id=null;
        else
            $dealerVisit->dealer_id=$data['dealer_id'];
        if($data['district_id']==0)
            $dealerVisit->district_id=null;
        else
            $dealerVisit->district_id=$data['district_id'];
        $dealerVisit->dealer_name=$data['dealer_name'];
        $dealerVisit->district_name=$data['district_name'];
        $dealerVisit->segments=$data['segments'];
        $dealerVisit->competition_products=$data['competitions_products'];
        $dealerVisit->feedback=$data['feedback'];
        $dealerVisit->address=$data['address'];
        $dealerVisit->lat=$data['lat'];
        $dealerVisit->lng=$data['lng'];
        if(array_key_exists("check_in",$data)){
            $dealerVisit->check_in=$data['check_in'];
            $dealerVisit->check_out=$data['check_out'];
            $dealerVisit->taluk=$data['taluk'];
        }
        if (array_key_exists("seed",$data))
            $dealerVisit->seed=$data['seed'];

        if (array_key_exists("fertilizer",$data))
            $dealerVisit->fertlizer=$data['fertilizer'];

        if (array_key_exists("pesticides",$data))
            $dealerVisit->pesticide=$data['pesticides'];

        if (array_key_exists("none",$data))
            $dealerVisit->none=$data['none'];

        $time=Carbon::now();
        $time=strtotime($time);
        $dealerName=preg_replace("/[^a-zA-Z0-9 ]/", "", $dealerVisit->dealer_name);
        $filenameFront=$dealerVisit->district_name."_".$dealerVisit->emp_id."_".$dealerName.$time."_Front.jpg";
        $filenameInside=$dealerVisit->district_name."_".$dealerVisit->emp_id."_".$dealerName.$time."_Inside.jpg";
        $path1=$this->base64ToImage($data['img_url1'],$filenameFront);
        $path2=$this->base64ToImage($data['img_url2'],$filenameInside);
        $dealerVisit->image_url1=$path1;
        $dealerVisit->image_url2=$path2;
        if($dealerVisit->save()){
            return array('success'=>true);
        }
        else
            return array('success'=>false);
    }

    function base64ToImage($base64_string, $output_file) {
        $decoded=base64_decode($base64_string); 
        file_put_contents("images/".$output_file,$decoded);
        return "images/".$output_file;
    }

    public function RemoveImageFromFolder(Request $req){
        $visit=DealerVisit::where('id',$req->id)->first();
        if(file_exists($visit->image_url1))
            unlink($visit->image_url1);
        if(file_exists($visit->image_url2))
            unlink($visit->image_url2);
        DealerVisit::where('id',$req->id)->update(['is_deleted'=>1]);
        return array("success"=>true);
    }


    public function excel(Request $req) {

        //From Date
        if(Session::has('dealerVisit_fromDate'))
            $fromDate = Session::get('dealerVisit_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');

        //To Date
        if(Session::has('dealerVisit_toDate'))
            $toDate = Session::get('dealerVisit_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');

        //Employee Data
        if(Session::has('dealerVisitEmp')){
            $emp_id = Session::get('dealerVisitEmp');   
            $emp_id = array_values($emp_id);         
        }
        else
            $emp_id ="";

        if($emp_id==""){
            $data=DealerVisit::leftJoin('employees', function($join) {
                          $join->on('employees.id', '=', 'dealer_visit.emp_id');
                        })
						->leftJoin('districts', function($join) {
                          $join->on('districts.id', '=', 'dealer_visit.district_id');
                        })
						->leftJoin('states', function($join) {
                          $join->on('states.id', '=', 'districts.state_id');
                        })
                        ->select(DB::raw("DATE_FORMAT(dealer_visit.created_at,'%d-%M-%Y') AS date,DATE_FORMAT(dealer_visit.check_in,'%d-%M-%Y %l:%i %p') AS check_in,DATE_FORMAT(dealer_visit.check_out,'%d-%M-%Y %l:%i %p') AS check_out,DATE_FORMAT(dealer_visit.created_at,'%l:%i %p') AS Servertime,DAYNAME(dealer_visit.created_at) as day,employees.name as employee,(CASE WHEN is_new=1 THEN 'YES' ELSE 'NO' END) as 'New Dealer',dealer_name as 'Dealer Name',states.name as state,district_name as 'District',segments,competition_products,feedback,address,lat,lng,(CASE WHEN seed=1 THEN 'YES' ELSE 'NO' END) as seed,(CASE WHEN fertlizer=1 THEN 'YES' ELSE 'NO' END) as fertilizer,(CASE WHEN pesticide=1 THEN 'YES' ELSE 'NO' END) as pesticide,(CASE WHEN none=1 THEN 'YES' ELSE 'NO' END) as none"))
                        ->where('dealer_visit.created_at','>=',$fromDate.' 00:00:00')
                        ->where('dealer_visit.created_at','<=',$toDate.' 23:59:59')
                        ->get();
        }
        else{
            $data=DealerVisit::leftJoin('employees', function($join) {
                          $join->on('employees.id', '=', 'dealer_visit.emp_id');
                        })
                        ->select(DB::raw("DATE_FORMAT(dealer_visit.created_at,'%d-%M-%Y') AS date,DATE_FORMAT(dealer_visit.check_in,'%d-%M-%Y %l:%i %p') AS check_in,DATE_FORMAT(dealer_visit.check_out,'%d-%M-%Y %l:%i %p') AS check_out,DATE_FORMAT(dealer_visit.created_at,'%l:%i %p') AS Servertime,DAYNAME(dealer_visit.created_at) as day,name as employee,(CASE WHEN is_new=1 THEN 'YES' ELSE 'NO' END) as 'New Dealer',dealer_name as 'Dealer Name',district_name as 'District',segments,competition_products,feedback,address,lat,lng,(CASE WHEN seed=1 THEN 'YES' ELSE 'NO' END) as seed,(CASE WHEN fertlizer=1 THEN 'YES' ELSE 'NO' END) as fertilizer,(CASE WHEN pesticide=1 THEN 'YES' ELSE 'NO' END) as pesticide,(CASE WHEN none=1 THEN 'YES' ELSE 'NO' END) as none"))
                        ->where('dealer_visit.created_at','>=',$fromDate.' 00:00:00')
                        ->where('dealer_visit.created_at','<=',$toDate.' 23:59:59')
                        ->whereIn('emp_id',$emp_id)
                        ->get();
						
        }
        //Log::info($data[0]->name);
        return (new FastExcel($data))->download('file.xlsx');
    }


    public function zipFile(Request $req){
        $headers = ["Content-Type"=>"application/zip"];
        $fileName = "test2.zip"; // name of zip
        $dealerVisits=DealerVisit::where('is_deleted',1)->get();
        foreach ($dealerVisits as $key => $visit) {
            \Zipper::make('public/test2.zip') //file path for zip file
                ->add(glob('public/'.$visit->image_url1))
                ->add(glob('public/'.$visit->image_url2));
                break;

        }
        \Zipper::close(); //files to be zipped

        return response()->download(public_path('public/'.$fileName),$fileName, $headers);
    }
}
