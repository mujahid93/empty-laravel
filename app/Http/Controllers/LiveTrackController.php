<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dealer;
use App\Models\LiveTrack;
use Illuminate\Support\Facades\Auth;
use Log;
use Hash;
use DB;
use Illuminate\Support\Facades\Input;
use App\Models\Employee;
use Session;

class LiveTrackController extends Controller
{
    //
    public function saveLiveTrack(Request $request)
    {
    	$data=Input::all();
        try{
        	for ($i=0; $i < COUNT($data); $i++) { 
        		$live = new LiveTrack();
	            $live->emp_id = $data[$i]['emp_id'];
	            $live->lat = $data[$i]['lat'];
	            $live->lng = $data[$i]['lng'];
	            $live->time = $data[$i]['date'];
	            $live->save();
        	}
        	return array('status' => true );
            
        }
        catch(\Exception $e){
           // do task when error
		   return array('status' => false );
        }
    }

    public function gmaps()
    {
        //From Date
        if(Session::has('tracking_date'))
            $date = Session::get('tracking_date');            
        else
            $date = \Carbon\Carbon::now()->format('Y-m-d');

        $empList=Employee::where('status',1)->orderBy('name','ASC')->get();

        //Employee Data
        if(Session::has('tracking_emp'))
            $emp_id = Session::get('tracking_emp');      
        else
            $emp_id =$empList[0]['id'];


    	$locations = DB::table('live_track')->whereDate('time',$date)->where('emp_id',$emp_id)->get();
    	return view('tracking.gmaps',compact('locations','empList'));
    }
}
