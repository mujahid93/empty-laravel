<?php

namespace App\Http\Controllers;

use App\Models\Dispatch;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Order;
use App\Models\Dealer;
use App\Models\District;
use Illuminate\Support\Facades\Auth;
use Session;
use Log;
use Redirect;

class DispatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $states = State::all();
        $districtList=array();
        $dealerList=array();

        //From Date
        if(Session::has('dispatch_fromDate'))
            $fromDate = Session::get('dispatch_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');

        //To Date
        if(Session::has('dispatch_toDate'))
            $toDate = Session::get('dispatch_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');



        //State
        if(Session::has('dispatch_state')){
            $state=Session::get('dispatch_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";

        //District
        if(Session::has('dispatch_district')){
            $districts = Session::get('dispatch_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";


        //Dealer Name
        if(Session::has('dispatch_dealer')){
            $dealer = Session::get('dispatch_dealer');
        }
        else
            $dealer = "%";


        //Tracking No
        if(Session::has('dispatch_tracking'))
            $tracking_no=Session::get('dispatch_tracking');
        else
            $tracking_no="%";

        $user=Auth::user()->employee;
        $emp_id=$user->id;
        if($user->role_id==4 || $user->role_id==14)
            $emp_id="%";

        $items = $request->items ?? 5;
                $dispatches = Dispatch::whereHas('OrderDetail', function ($stmt) use($dealer,$districts,$state,$tracking_no) {
                    $stmt->where('branch',\Auth::user()->employee->branch_id)
                    ->whereHas('Order',function($query) use($dealer,$districts,$state,$tracking_no){
                        $query->where('dealer_id','LIKE',$dealer)
                              ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                              ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    });
                    });
                })
                ->where('created_at','>=', $fromDate)->where('created_at','<=',$toDate)
                ->orderBy('created_at','desc')
                ->paginate($items);
        return view('dispatch.index')->with(compact('dispatches','items','states','districtList','dealerList'));
    }


    public function NewDispatch(Request $r)
    {
        $order = $r;
        return view('dispatch.create')->with(compact('order'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        // Log::info($r->h_purchaseNo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $AOrder = OrderDetail::where('branch_tracking_no',decrypt($request->tracking_no))->get();
            $dispatch = new Dispatch();
            $dispatch->date = $request->date;
            $dispatch->lr_number = $request->lr_number;
            $dispatch->transport = $request->transport;
            $dispatch->noc = $request->noc;
            $dispatch->freight = $request->freight;
            $dispatch->destination = $request->destination;
            $dispatch->emp_id = $AOrder[0]->order->emp_id;
            $dispatch->order_detail_id = $AOrder[0]->id;
            if($dispatch->save())
            {
                if($dispatch->destination <> $dispatch->OrderDetail->destination)
                    app('App\Http\Controllers\OrderController')->sendNotificationToMgmt("Destination Changed during dispatch",decrypt($request->tracking_no));
                OrderDetail::where('branch_tracking_no',decrypt($request->tracking_no))->update(['order_status'=>6]);
            }
            app('App\Http\Controllers\OrderController')->sendNotificationToExecutive(decrypt($request->tracking_no),6);
             app('App\Http\Controllers\NotifController')->sendNotification($AOrder[0]->tracking_no);
            return redirect()->route("dispatch.index")->with(['result'=>'New Dispatch Created','color'=>'bg-green']);
       // try{

       //  }
       //  catch(\Exception $e){
       //      return $e->getMessage();
       //      // return redirect()->route("accGroup.index")->with(['result'=>'Error adding Machine Group','color'=>'alert-warning']);
       //  }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dispatch  $dispatch
     * @return \Illuminate\Http\Response
     */
    public function show(Dispatch $dispatch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dispatch  $dispatch
     * @return \Illuminate\Http\Response
     */
    public function edit(Dispatch $dispatch)
    {
        $dispatch = Dispatch::find($dispatch->id);
        return view('dispatch.edit')->with(compact('dispatch'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dispatch  $dispatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $dispatchId)
    {
        $dispatch = Dispatch::find($dispatchId);
        $dispatch->dNumber = $request->number;
        $dispatch->dBoxes = $request->boxes;
        $dispatch->dNote = $request->note;
        $dispatch->save();
        return Redirect::route('dispatch.index',array('items' => 5));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dispatch  $dispatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dispatch $dispatch)
    {
        // Dispatch::where('id',$dispatch->id)->delete();
        return Redirect::route('dispatch.index',array('items' => 5));
    }
}
