<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Log;

class FilterController extends Controller
{
    //This controller set the filter session.
    

    //Controller to set Filter for Executive__Placed_orders web route All status
    public function FilterExePlacedOrders(Request $request)
    {
        Session::put('e_mo_fromDate',$_GET['fromDate']);

        Session::put('e_mo_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('e_mo_dealer',$request->dealer);
        else
            Session::forget('e_mo_dealer');

        if($request->state>0)
            Session::put('e_mo_state',$request->state);
        else
            Session::forget('e_mo_state');

        if($request->district>0)
            Session::put('e_mo_district',$request->district);
        else
            Session::forget('e_mo_district');            

        if($request->order_status>0)
            Session::put('e_mo_orderStatus',$request->order_status);
        else
            Session::forget('e_mo_orderStatus');  

        if($request->tracking_no!="")              
            Session::put('e_mo_tracking',$request->tracking_no);
        else
            Session::forget('e_mo_tracking');              
    }


    public function FilterMyRegionOrders(Request $request){
        Session::put('region_fromDate',$_GET['fromDate']);

        Session::put('region_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('region_dealer',$request->dealer);
        else
            Session::forget('region_dealer');

        if($request->order_status>0)
            Session::put('region_orderStatus',$request->order_status);
        else
            Session::forget('region_orderStatus');  
    }



    //Filter For PayApprovedOrders
    public function FilterPayApprovedOrders(Request $request)
    {

        Session::put('payapproved_fromDate',$_GET['fromDate']);

        Session::put('payapproved_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('payapproved_dealer',$request->dealer);
        else
            Session::forget('payapproved_dealer');

        if($request->state>0)
            Session::put('payapproved_state',$request->state);
        else
            Session::forget('payapproved_state');

        if($request->district>0)
            Session::put('payapproved_district',$request->district);
        else
            Session::forget('payapproved_district');            
        
        if($request->tracking_no!="")              
            Session::put('payapproved_tracking',$request->tracking_no);
        else
            Session::forget('payapproved_tracking');              
    }

    //Filter For PendingPartsOrder
    public function FilterPendingPartsOrder(Request $request)
    {

        Session::put('pending_parts_fromDate',$_GET['fromDate']);

        Session::put('pending_parts_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('pending_parts_dealer',$request->dealer);
        else
            Session::forget('pending_parts_dealer');

        if($request->state>0)
            Session::put('pending_parts_state',$request->state);
        else
            Session::forget('pending_parts_state');

        if($request->district>0)
            Session::put('pending_parts_district',$request->district);
        else
            Session::forget('pending_parts_district');            
        
        if($request->tracking_no!="")              
            Session::put('pending_parts_tracking',$request->tracking_no);
        else
            Session::forget('pending_parts_tracking');              
    }


    //Filter For RejectedOrder
    public function FilterRejectedOrders(Request $request)
    {

        Session::put('rejected_fromDate',$_GET['fromDate']);

        Session::put('rejected_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('rejected_dealer',$request->dealer);
        else
            Session::forget('rejected_dealer');

        if($request->state>0)
            Session::put('rejected_state',$request->state);
        else
            Session::forget('rejected_state');

        if($request->district>0)
            Session::put('rejected_district',$request->district);
        else
            Session::forget('rejected_district');            

        if($request->tracking_no!="")              
            Session::put('rejected_tracking',$request->tracking_no);
        else
            Session::forget('rejected_tracking');              
    }


    //Filter For Invoices
    public function FilterInvoiceOrders(Request $request)
    {
        Session::put('invoice_fromDate',$_GET['fromDate']);

        Session::put('invoice_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('invoice_dealer',$request->dealer);
        else
            Session::forget('invoice_dealer');

        if($request->state>0)
            Session::put('invoice_state',$request->state);
        else
            Session::forget('invoice_state');

        if($request->district>0)
            Session::put('invoice_district',$request->district);
        else
            Session::forget('invoice_district');            

        if($request->tracking_no!="")              
            Session::put('invoice_tracking',$request->tracking_no);
        else
            Session::forget('invoice_tracking');              
    }


    //Filter For Dispatches
    public function FilterDispatchOrders(Request $request)
    {
        Session::put('dispatch_fromDate',$_GET['fromDate']);

        Session::put('dispatch_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('dispatch_dealer',$request->dealer);
        else
            Session::forget('dispatch_dealer');

        if($request->state>0)
            Session::put('dispatch_state',$request->state);
        else
            Session::forget('dispatch_state');

        if($request->district>0)
            Session::put('dispatch_district',$request->district);
        else
            Session::forget('dispatch_district');            

        if($request->tracking_no!="")              
            Session::put('dispatch_tracking',$request->tracking_no);
        else
            Session::forget('dispatch_tracking');              
    }


     //Filter For Payment Pending Order
    public function FilterPayPendingOrders(Request $request)
    {
        Session::put('exe_placed_fromDate',$_GET['fromDate']);

        Session::put('exe_placed_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('exe_placed_dealer',$request->dealer);
        else
            Session::forget('exe_placed_dealer');

        if($request->state>0)
            Session::put('exe_placed_state',$request->state);
        else
            Session::forget('exe_placed_state');

        if($request->district>0)
            Session::put('exe_placed_district',$request->district);
        else
            Session::forget('exe_placed_district');            

        if($request->tracking_no!="")              
            Session::put('exe_placed_tracking',$request->tracking_no);
        else
            Session::forget('exe_placed_tracking');    

        return array('status'=>"OK");          
    }


    //Filter For Managment Approved Order
    public function FilterMgmtApprovedOrders(Request $request)
    {
        Session::put('mgmt_appoved_fromDate',$_GET['fromDate']);

        Session::put('mgmt_appoved_toDate',$_GET['toDate']);

        if($request->dealer>0)
            Session::put('mgmt_appoved_dealer',$request->dealer);
        else
            Session::forget('mgmt_appoved_dealer');

        if($request->state>0)
            Session::put('mgmt_appoved_state',$request->state);
        else
            Session::forget('mgmt_appoved_state');

        if($request->district>0)
            Session::put('mgmt_appoved_district',$request->district);
        else
            Session::forget('mgmt_appoved_district');            

        if($request->tracking_no!="")              
            Session::put('mgmt_appoved_tracking',$request->tracking_no);
        else
            Session::forget('mgmt_appoved_tracking');    
  
    }

    public function FilterDealerVisited(Request $request)
    {
        Session::put('dealerVisit_fromDate',$_GET['fromDate']);

        Session::put('dealerVisit_toDate',$_GET['toDate']);


        if($request->emp_id[0]>0)
            Session::put('dealerVisitEmp',$request->emp_id);
        else
            Session::forget('dealerVisitEmp'); 

    }


    public function FilterExpenses(Request $request)
    {
        Session::put('expenseFromDate',$_GET['fromDate']);

        Session::put('expenseToDate',$_GET['toDate']);


        if($request->emp_id>0)
            Session::put('expenseEmp',$request->emp_id);
        else
            Session::forget('expenseEmp'); 

    }

    public function FilterEmployeeTracking(Request $request)
    {
        Session::put('tracking_date',$_GET['date']);
        Session::put('tracking_emp',$_GET['emp_id']);
    }






}
