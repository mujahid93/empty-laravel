<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Version;
use DB;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $request->items ?? 5;
        $branches = Branch::where('status','=',1)->paginate($items);
        return view('branch.index')->with(compact('branches','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('branch.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $branch = new Branch();
            $branch->name = $request->name;
            if($branch->save())
            Version::where('tbl_name','branch')->update(['version'=> DB::raw('version+1')]);
            return redirect()->route("branch.index")->with(['result'=>'New branch added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return $e->getMessage();
            // return redirect()->route("branch.index")->with(['result'=>'Error adding branch','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branch $branch)
    {
          $branch = Branch::find($branch->id);
          $id = $branch->id;
          return view('branch.edit')->with(compact('branch','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        try{
         $branch = Branch::find($branch->id);
         $branch->name = $request->name;
         if($branch->save())
            Version::where('tbl_name','branch')->update(['version'=> DB::raw('version+1')]);
         return redirect()->route('branch.index')->with(['result'=>'branch updated successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("branch.index")->with(['result'=>'Error updating branch','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branches = Branch::find($branch->id);
        $branches->status = 0;
        if($branch->save())
            Version::where('tbl_name','branch')->update(['version'=> DB::raw('version+1')]);
        return redirect()->route('branch.index')->with(['result'=>'branch deleted successfully','color'=>'bg-green']);
    }

    public function FilterRows(Request $request)
    {
        $branches = Branch::where('name', 'LIKE', '%' . $request->queryFilter . '%')->where('status','=',1)->paginate(5);
        return $branches;
    }
}
