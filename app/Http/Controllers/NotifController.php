<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\Dealer;
use App\Models\Employee;
use App\Models\Email;
use Carbon\Carbon;
use App\Models\Invoice;
use App\Models\Tour;
use App\Models\DemoPlan;
use App\Models\EmpDistrict;
use App\Models\Expense;
use DB;
use Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Notification as Notify;
use Illuminate\Support\Facades\Input;

class NotifController extends Controller
{


    function sendNotification($tracking_no){
        $status=DB::select("select MAX(order_status) as status FROM order_details WHERE tracking_no='$tracking_no'");
        //Log::info($status[0]->status);

        $isPartsOrder=DB::select("select * FROM order_details WHERE tracking_no='$tracking_no' LIMIT 1");
        $order=Order::where("tracking_no",$tracking_no)->first();

        $isMgmt=false;
        $isExecutive=false;
        $isDealer=false;
        $order=Order::where('tracking_no',$tracking_no)->first();
        switch ($status[0]->status) {
            case 1:
            $dealer=Dealer::where('id',$order->dealer_id)->first();
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Order is placed by dealer-> '.$dealer->name,
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'1',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>1);
            $isExecutive=true;    
                break;
            
            case 2:
            if($isPartsOrder[0]->classification="Parts"){
                $data= array('tracking_no' =>$tracking_no,
                         'message'=>'Order Approved By Parts Admin and sent to payment approval',
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'name'=>'',
                         'fileurl'=>'',
                         'status'=>'2',
                         'is_dirty'=>$order->is_dirty,
                         'messagetype'=>2);
                $isExecutive=true;
                $isDealer=true;
            }
            else{
                $data= array('tracking_no' =>$tracking_no,
                         'message'=>'Order is accepted by dealer',
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'name'=>'',
                         'fileurl'=>'',
                         'status'=>'2',
                         'is_dirty'=>$order->is_dirty,
                         'messagetype'=>2);
                $isExecutive=true;
            }
                break;

            case 3:
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Payment is approved for the order.',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'3',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>3);

            $mgmtData= array('tracking_no' =>$tracking_no,
                     'message'=>'Order is submitted for management approval.',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'3',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>3);
            $isMgmt=true;
            $isExecutive=true;
            $isDealer=true;
                break;

            case 4:
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'KisanKraft management approved the order.',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'4',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>4);
            $isExecutive=true;
            $isDealer=true;
                break;



            case 5:
            $file=DB::select("SELECT iPdf FROM `invoices` LEFT JOIN order_details ON order_details.id=order_detail_id WHERE order_details.tracking_no='$tracking_no'  ORDER BY invoices.updated_at DESC LIMIT 1");

            $branchTracking=DB::select("SELECT branch_tracking_no FROM order_details WHERE tracking_no='$tracking_no' ORDER BY updated_at DESC LIMIT 1");

            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Invoice is generated for the order',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$branchTracking[0]->branch_tracking_no,
                     'fileurl'=>"https://ekisan.kisancraft.com/storage".$file[0]->iPdf,
                     'status'=>'5',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>5);
            $isExecutive=true;
            $isDealer=true;
                break;


            case 6:
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Order dispatched successfully',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'6',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>6);
            $isExecutive=true;
            $isDealer=true;
                break;


            case 7:
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Order is rejected',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'7',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>7);

            $mgmtData= array('tracking_no' =>$tracking_no,
                     'message'=>'Order is rejected',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'7',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>7);

            $isMgmt=true;
            $isExecutive=true;
            $isDealer=true;
                break;


            case 8:
            $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Your order is processed by KisanKraft executive, Please review your order for confirmation',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'8',
                     'is_dirty'=>$order->is_dirty,
                     'messagetype'=>9);
            $isDealer=true;
                break;
        }

        Log::info($data);

        if($isExecutive){
            if($order->emp_id==null){
                $emp_id=EmpDistrict::where('district_id',$dealer->district_id)->pluck('emp_id')->toArray();
                $fcm_tokens=Email::whereIn('id',Employee::whereIn('id',$emp_id)->whereNotIn('role_id',[17,18])->get(['email_id']))->pluck('fcm_mobile')->toArray();
                notifyUser($fcm_tokens,$data); 
            }
            else
            {
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->whereNotIn('role_id',[17,18])->get(['email_id']))->get();
                notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);
            }
        }

        if($isDealer){
            $fcm_tokens = Dealer::where('id', Order::where('tracking_no',$tracking_no)->value('dealer_id'))->get(['fcm_mobile']);
            notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);  
        }

        if($isMgmt){
            $this->notifyManagement($mgmtData);
        }
    }

    public function sendTourNotification($emp_id,$token){
        $tour=Tour::where('emp_id',$emp_id)->where('token',$token)->first();
        //Check Status
        switch ($tour->status) {
            case 1:
                //Send Notification to Manager
                $data= array('tracking_no' =>$token,
                     'message'=>'Tour planning scheduled by '.$tour->employee->name,
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'1',
                     'messagetype'=>18);

                Log::info($data);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('manager_id'))->get(['email_id']))->first();
                if($fcm_tokens!=null)
                    notifyUser(array($fcm_tokens->fcm_mobile),$data);

                break;

            case 2:
                    $data= array('tracking_no' =>$token,
                     'message'=>'Tour approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'2',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;

            case 3:
                 $data= array('tracking_no' =>$token,
                     'message'=>'Tour Rejected',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'3',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);
                break;


            case 4:
                    $data= array('tracking_no' =>$token,
                     'message'=>'Tour is updated and approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'priority'=>$tour->is_updated,
                     'status'=>'4',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;
            
            default:
                # code...
                break;
        }
    }


    public function sendDemoPlanNotification($emp_id,$token){
        $tour=DemoPlan::where('emp_id',$emp_id)->where('token',$token)->first();
        //Check Status
        switch ($tour->status) {
            case 1:
                //Send Notification to Manager
                $data= array('tracking_no' =>$token,
                     'message'=>'Demo planning scheduled by '.$tour->employee->name,
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'1',
                     'messagetype'=>19);

                Log::info($data);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('manager_id'))->get(['email_id']))->first();
                if($fcm_tokens!=null)
                    notifyUser(array($fcm_tokens->fcm_mobile),$data);

                break;

            case 2:
                    $data= array('tracking_no' =>$token,
                     'message'=>'Demo plan approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'2',
                     'messagetype'=>19);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;

            case 3:
                 $data= array('tracking_no' =>$token,
                     'message'=>'Demo Rejected',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'status'=>'3',
                     'messagetype'=>19);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);
                break;


            case 4:
                    $data= array('tracking_no' =>$token,
                     'message'=>'Demo plan is updated and approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$tour->date,
                     'fileurl'=>'',
                     'priority'=>$tour->is_updated,
                     'status'=>'4',
                     'messagetype'=>19);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$tour->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;
            
            default:
                # code...
                break;
        }
    }

     public function sendExpenseNotification($transaction_no){
        $exp=Expense::where('transaction_code',$transaction_no)->first();
        //Check Status
        switch ($exp->status) {
            case 1:
                //Send Notification to Manager
                $data= array('tracking_no' =>$transaction_no,
                     'message'=>'Final Expense added by '.$exp->employee->name,
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$exp->date,
                     'fileurl'=>'',
                     'status'=>'1',
                     'messagetype'=>18);

                Log::info($data);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$exp->emp_id)->value('manager_id'))->get(['email_id']))->first();
                if($fcm_tokens!=null)
                    notifyUser(array($fcm_tokens->fcm_mobile),$data);

                break;

            case 2:
                    $data= array('tracking_no' =>$transaction_no,
                     'message'=>'Expense approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$exp->date,
                     'fileurl'=>'',
                     'status'=>'2',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$exp->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;

            case 3:
                 $data= array('tracking_no' =>$transaction_no,
                     'message'=>'Expense Rejected',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$exp->date,
                     'fileurl'=>'',
                     'status'=>'3',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$exp->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);
                break;


            case 4:
                    $data= array('tracking_no' =>$transaction_no,
                     'message'=>'Expense is updated and approved',
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>$exp->date,
                     'fileurl'=>'',
                     'priority'=>1,
                     'status'=>'4',
                     'messagetype'=>18);
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Employee::where('id',$exp->emp_id)->value('id'))->get(['email_id']))->first();

            notifyUser(array($fcm_tokens->fcm_mobile),$data);

               break;
            
            default:
                # code...
                break;
        }
    }



    public function orderNotifManager(Request $request){
        $data=Input::all();
        $tracking_no=$data["nameValuePairs"]["tracking_no"];
        $status=DB::select("select MAX(DISTINCT(order_status)) as status FROM order_details WHERE tracking_no='$tracking_no'");
        if($status[0]->status!=1){
            return;
        }
        $emp_id=Auth::user()->employee->id;
        $emp_name=Auth::user()->employee->name;
        $data= array('tracking_no' =>$tracking_no,
                     'message'=>'Order is not processed by '.$emp_name." Please approve the order",
                     'date'=>Carbon::now()->format('d-m-Y g:i a'),
                     'name'=>'',
                     'fileurl'=>'',
                     'status'=>'1',
                     'messagetype'=>1);

        $role=Auth::user()->employee->role_id;
        

        if($role==3){
            $manager=DB::select("SELECT DISTINCT(CASE WHEN aid<>0 THEN aid ELSE CASE WHEN sid<>0 THEN sid ELSE CASE WHEN zid<>0 THEN zid ELSE 0 END END END) as MANAGER FROM `district_mapping` where eid=$emp_id");
            
            if(COUNT($manager)==0){
                $this->sendOrderNotifManagement($tracking_no);
            }
            else if($manager[0]->MANAGER==0){
                //sendToManagement
                $this->sendOrderNotifManagement($tracking_no);
            }
            else{
                $fcm_tokens = Email::whereIn('id',Employee::where('id', $manager[0]->MANAGER)->get(['email_id']))->get();
                notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);
            }

        }
        
        else if($role==8){
            $manager=DB::select("SELECT DISTINCT(CASE WHEN sid<>0 THEN sid ELSE CASE WHEN zid<>0 THEN zid ELSE 0 END END) as MANAGER FROM `district_mapping` where aid=$emp_id");
            
            if(COUNT($manager)==0){
                $this->sendOrderNotifManagement($tracking_no);
            }
            else if($manager[0]->MANAGER==0){
                //sendToManagement
                $this->sendOrderNotifManagement($tracking_no);
            }
            else{
                $fcm_tokens = Email::whereIn('id',Employee::where('id', $manager[0]->MANAGER)->get(['email_id']))->get();
                notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);
            }
        }

        else if($role==9){
            $manager=DB::select("SELECT DISTINCT(CASE WHEN zid<>0 THEN zid ELSE 0 END) as MANAGER FROM `district_mapping` where sid=$emp_id");
            if(COUNT($manager)==0){
                $this->sendOrderNotifManagement($tracking_no);
            }
            else if($manager[0]->MANAGER==0){
                //sendToManagement
                $this->sendOrderNotifManagement($tracking_no);
            }
            else{
                $fcm_tokens = Email::whereIn('id',Employee::where('id', $manager[0]->MANAGER)->get(['email_id']))->get();
                notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);
            }
        }

        else if($role==10){
            //Send To management
            $this->sendOrderNotifManagement($tracking_no);
        }



    }

    
    public function orderNotifManagment(Request $request){
        $data=Input::all();
        $tracking_no=$data["nameValuePairs"]["tracking_no"];
        sendOrderNotifManagement($tracking_no);
    }

    public function sendOrderNotifManagement($tracking_no){
     $notification = 'Dealer Order is not processed by executive with tracking_no '.$tracking_no;
         $notify = new Notify();
        $order = Order::where('tracking_no',$tracking_no)->get();
        $notify->emp_id = $order[0]->emp_id;
        $notify->dealer_id = $order[0]->dealer_id;
        $notify->order_id = $order[0]->id;
        $notify->message = $notification;
        $notify->save();   

        $data= array('tracking_no' =>$tracking_no,
                         'message'=>$notification,
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'fileurl'=>'',
                         'name'=>Auth::user()->employee->name,
                         'status'=>'16',
                         'messagetype'=>16);
    }

    function notifyUser($firebaseIds,$data){
           $notif=array(
                    'title'=>$data['tracking_no'],
                    'body'=>$data['message']
                    );
           $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array (
                        'registration_ids' => $firebaseIds,
                        'priority' => 'high',
                        'notification' => $notif,
                        'data'=>$data
                );
                $fields = json_encode ( $fields );
                $headers = array (
                        'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                        'Content-Type: application/json'
                );
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_URL, $url );
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                $result = curl_exec ( $ch );
                curl_close ( $ch );
    }


    function notifyManagement($data){
           $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array (
                        'to' => '/topics/Management',
                        'data' => $data
                );
                $fields = json_encode ( $fields );
                $headers = array (
                        'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                        'Content-Type: application/json'
                );
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_URL, $url );
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                $result = curl_exec ( $ch );
                curl_close ( $ch );
    }
}
