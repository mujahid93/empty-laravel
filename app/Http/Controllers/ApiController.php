<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Dealer;
use App\Models\MachineSku;
use App\Models\Part;
use App\Models\MachineSubsidy;
use App\Models\AccessoriesSku;
use App\Models\AccSubsidy;
use App\Models\SubsidyDept;
use App\Models\SubsidyDeptState;
use App\Models\User;
use App\Models\Shipping;
use App\Models\Branch;
use App\Models\State;
use App\Models\Cart;
use App\Models\OrderDetail;
use App\Models\Employee;
use App\Models\Email;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use App\Models\Notification as Notify;
use Kreait\Firebase;
use Illuminate\Http\Request;
use Session;
use Log;
use DB;
use PDF;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    //Order Related Controllers

    //Controller for adding new shipping to app
    public function api_newShipping(Request $request)
    {
         $data = Input::all();
         DB::beginTransaction();
         try{
             $shipping = new Shipping();
             $shipping->branch_id = $data['branch_id'];
             $shipping->name = $data['name'];
             $shipping->agency = $data['agency'];
             if($shipping->save()){
                DB::commit();
                return $shipping;
            }
             else
                return null;
         }
         catch(Exeption $e){
            DB::rollback();
            return null;
         }
    }

    //Controller to place order from app
    public function api_placeOrder(Request $request)
    {
        //Data submitted for placing order
        $data = Input::all();
		
		
		
		//Log::info($data);

        //Starting Transaction to ensure order is placed correctly
        DB::beginTransaction();

        //Adding all controlls in try catch block 
        try{

            //First Checking the order is repeating order or new order
            //Gathering variables required to check this condition
            $order_token=$data['nameValuePairs']['order_token'];
            $emp_id=$data['nameValuePairs']['employee'];
            $dealer_id=$data['nameValuePairs']['dealer'];
            $orderObj=Order::where('order_token',$order_token)->where('dealer_id',$dealer_id)->first();
            if($orderObj!=null){
                    //Here order is already placed so no need to add this in database.
                    $details=DB::select("SELECT branch,branch_tracking_no FROM orderDetail WHERE order_id=$orderObj->id");
                    $saleBlock=array();
                    foreach ($details as $key => $value) {
                      $purchaseBlock = array(
                                          'tracking_no'=>$orderObj->tracking_no,
                                          'branch_id'=>$value->branch,
                                          'branch_tracking_no'=>$value->branch_tracking_no);
                      array_push($saleBlock, $purchaseBlock);
                    }
                    DB::commit();
                    app('App\Http\Controllers\NotifController')->sendNotification($orderObj->tracking_no);
                    return array('status' => false,
                                  'isPlaced'=>true,
                                  'placedTracking'=>$orderObj->tracking_no,
                                  'tracking_no'=>$saleBlock,
                                  'date'=>date('Y-m-d'));
            }

            //At this point order is new and need to be placed.

            //First generating tracking no for new order.
            $tracking_no =app('App\Http\Controllers\OrderController')->GenerateTrackingNo();

            //Creating Order object
            $newOrder = new Order();
            $newOrder->tracking_no = $tracking_no;
            if($emp_id==0 && is_numeric(Auth::user()->email))
                $emp_id=-1;
            if($emp_id==-1)
              $emp_id=null;
			if($emp_id==0)
			  $emp_id=Auth::user()->employee->id;
            $newOrder->emp_id = $emp_id;
            $newOrder->dealer_id = $data['nameValuePairs']['dealer'];
            $newOrder->order_token=$order_token;
            if($emp_id==null)
              $newOrder->is_app=1;
            $newOrder->save();

            //Now order is saved is database and need to add it's items in database
            //First need to fetch dealer detail for correct pricing.
            $dealer=Dealer::where('id',$newOrder->dealer_id)->first();
            $purchaseArray=array();
            $branchTrackingNo = array();
            
            //Generating Branch tracking number
            $char = 'A';
            $items_data=$data['nameValuePairs']['cartItems'];
            $type=$items_data[0]['rate_type'];
            $branchList=array();
            foreach ($data['nameValuePairs']['cartItems'] as $key => $cart) {
				
                
                $category_no = $cart['classification'];
                if($category_no=='0'){
					$countMch=MachineSku::where('sku',str_replace('(AGRICULTURE)','',$cart["name"]))->count();
					if($countMch==0){
						DB::rollback();
						$response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
						return $response;
					}
						
                    $itemInfo = MachineSku::where('id','=',$cart['skuId'])->get();
                    $classification="Machinery";
                }
                elseif ($category_no=="1") {
					$countMch=AccessoriesSku::where('sku',$cart["name"])->count();
					if($countMch==0){
						DB::rollback();
						$response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
						return $response;
					}
                    $itemInfo = AccessoriesSku::where('id','=',$cart['skuId'])->get();
                    $classification="Accessories";
                }
                elseif($category_no=="2"){
					/*DB::rollback();
						$response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
						return $response;*/
					$countMch=Part::where('sku',$cart["name"])->count();
					if($countMch==0){
						/*DB::rollback();
						$response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
						return $response;*/
					}
                    $itemInfo = Part::where('id','=',$cart['skuId'])->get();
                    $classification="Parts";
                }
                if($type!=$cart['rate_type']){
                  $char++;
                  $type=$cart['rate_type'];
                  $branchTrackingNo_Code = $char;
                  $branchTrackingNo[$cart['dispBranch']] = $branchTrackingNo_Code;
                }


                if($branchTrackingNo)
                {
                    if (array_key_exists($cart['dispBranch'],$branchTrackingNo))
                    {
                        $branchTrackingNo_Code = $branchTrackingNo[$cart['dispBranch']];
                    }
                    else
                    {
                        $char++;
                        $branchTrackingNo_Code = $char;
                    }
                }
                else
                {
                    $branchTrackingNo_Code = 'A';
                }
                //Now attaching item detail to orders
                $branchTrackingNo[$cart['dispBranch']] = $branchTrackingNo_Code;
                $orderDetail = new OrderDetail();
                $orderDetail->order_date = date('Y-m-d H:i:s');
                $orderDetail->order_id = $newOrder->id;
                $orderDetail->tracking_no = $tracking_no;
                $orderDetail->classification = $classification;
                $orderDetail->branch = $cart['dispBranch'];
                $orderDetail->nav_no=$itemInfo[0]->nav_no;
				
                $orderDetail->branch_tracking_no = $tracking_no."-".$branchTrackingNo_Code;
                if($category_no=="2"){
                    $orderDetail->grp = "Part";
                    $orderDetail->subgrp = "Part";
                }
                else{
                    $orderDetail->grp = $itemInfo[0]->grp;
                    $orderDetail->subgrp = $itemInfo[0]->subgrp;
                }
                
                if($cart['subsidyDeptId']>0)
                    $orderDetail->subsidy_dept_id=$cart['subsidyDeptId'];
                $orderDetail->hsn = $itemInfo[0]->hsn;
                $orderDetail->sku = $itemInfo[0]->sku;
                $orderDetail->qty = $cart['qty'];
                if(array_key_exists('originalQty', $cart))
                  $orderDetail->orignal_qty = $cart['originalQty'];
                else
                  $orderDetail->orignal_qty=$cart['qty'];
                $orderDetail->spl_disc = $cart['spl_disc'];
                
                if($emp_id==null)
                  $orderDetail->order_status = 1;
                elseif($category_no=="2")
                  $orderDetail->order_status = 9;
                else
                  $orderDetail->order_status = 2;
                $orderDetail->bulk_qty = $itemInfo[0]->bulk_qty;
                $orderDetail->rate_type = $cart['rate_type'];
                if($cart['subsidyDeptId']>0)
                    $orderDetail->rate_type=2;
                $orderDetail->gst = $itemInfo[0]->gst;
                $orderDetail->dis_per = $cart['disPerc'];
                //Adding Current price of product from database.
                switch ($orderDetail->rate_type) {
                    case '0':
                        //Dealer price
                        if($classification=="Parts")
                          $orderDetail->item_rate=$itemInfo[0]->dp;
                        else
                          $orderDetail->item_rate = ROUND(Branch::where('id',$orderDetail->branch)->value('rate') * $itemInfo[0]->dp);
                        break;
                    case '1':
                        //Mrp price
                        $orderDetail->item_rate = $itemInfo[0]->mrp;
                        break;
                    case '2':
                        //Subsidy price
                        switch ($orderDetail->classification) {
                            case 'Machinery':
                                $orderDetail->item_rate = MachineSubsidy::where('mch_id',$itemInfo[0]->id)->where('state_id',$dealer->state_id)->where('subsidy_dept_id',$cart['subsidyDeptId'])->where('status',1)->value('subsidy_price');
                                //If rate is not found then take it as dealer price.
                                if($orderDetail->item_rate==0 || $orderDetail->item_rate==null){
                                    $orderDetail->item_rate = ROUND(Branch::where('id',$orderDetail->branch)->value('rate') * $itemInfo[0]->dp);
                                    $orderDetail->rate_type=0;
                                    $orderDetail->subsidy_dept_id=null;
                                }
                                else{
                                    //Rate is correct now we need to give 5% discount to subsidy amount.
                                    $orderDetail->item_rate=($orderDetail->item_rate)-(($orderDetail->item_rate/100)*5);
                                    $orderDetail->dis_per=0;
                                }
                                break;
                            case 'Accessories':
                                $orderDetail->item_rate = AccSubsidy::where('acc_id',$itemInfo[0]->id)->where('state_id',$dealer->state_id)->where('subsidy_dept_id',$cart['subsidyDeptId'])->where('status',1)->value('subsidy_price');
                                //If rate is not found then take it as dealer price.
                                if($orderDetail->item_rate==0 || $orderDetail->item_rate==null){
                                    $orderDetail->item_rate = ROUND(Branch::where('id',$orderDetail->branch)->value('rate') * $itemInfo[0]->dp);
                                    $orderDetail->rate_type=0;
                                    $orderDetail->subsidy_dept_id=null;
                                }
                                else{
                                    //Rate is correct now we need to give 5% discount to subsidy amount.
                                    $orderDetail->item_rate=($orderDetail->item_rate)-(($orderDetail->item_rate/100)*5);
                                    $orderDetail->dis_per=0;
                                }
                                break;

                            case 'Parts':
                                //This statement will not be used for current architecture of app
                                //If subsidy added for parts then implement the following statment to support that.
                                $orderDetail->item_rate=Part::where('id',$itemInfo[0]->id)->value('dp');
                                $orderDetail->rate_type=0;
                                $orderDetail->subsidy_dept_id=null;
                                break;    

                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }

                $orderDetail->destination = $cart['destination'];
                $orderDetail->freight = $cart['freight'];
                $shipName=Shipping::where('id',$cart['shippingId'])->first();
                if($shipName!=null && $shipName->name!="Other")
                	$orderDetail->shipping_id = $cart['shippingId'];
                else
                	$orderDetail->shipping_id=null;
                $orderDetail->other_ship = $cart['other_ship'];
                $orderDetail->save();
                if(!in_array($orderDetail->branch,$branchList)){
                  $purchaseBlock = array('sku' => $cart['name'],
                                          'tracking_no'=>$tracking_no,
                                          'branch_id'=>$orderDetail->branch,
                                          'branch_tracking_no'=>$orderDetail->branch_tracking_no,
                                          'order_id'=>$data['nameValuePairs']['local_order_id']);
                  array_push($purchaseArray, $purchaseBlock);
                  $branchList[]=$orderDetail->branch;
                }

            }
        }catch(Exeption $e){
            //There is some error occured while placing order and need rollback
            DB::rollback();
			Log::info($e);
            $response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
        }
        //Order is successfully added in database and now we can commit to finalize the changes.
        DB::commit();

        if($emp_id==null)
          app('App\Http\Controllers\NotifController')->sendNotification($tracking_no);
        $response=array('status' => true,
                        'isPlaced'=>true,
                        'tracking_no'=>$purchaseArray,
                        'date'=>date('Y-m-d'));
        return json_encode($response);
    }



    //Managment Controllers for orders

    //Controller which apply the discount to particular product in order.
    public function api_applyDiscount(Request $request)
    {
      $client_data = Input::all();
      DB::beginTransaction();
      try{
          $orderDetailId = $client_data['nameValuePairs']['orderDetailId'];
          $disPer = $client_data['nameValuePairs']['disPer'];
          $qty = $client_data['nameValuePairs']['qty'];
          OrderDetail::where('id',$orderDetailId)->update(['dis_per'=>$disPer,'qty'=>$qty]);
          $status = array(
                  'status'     => true,
                  );
        }
        catch(Exeption $e){
            DB::rollback();
            $status = array(
                  'status'     => false,
                  );
        }
        DB::commit();
      return $status;
    }

    //Controller which approve the order
    public function api_approveOrder(Request $request)
    {
      $client_data = Input::all();
      DB::beginTransaction();
      try{
          $orderId = $client_data['nameValuePairs']['orderId'];
          $orderDetail=OrderDetail::where('order_id',$orderId)->first();
          if($orderDetail->order_status!=3){
            DB::commit();
             $status = array(
                  'status'     => false,
                  );
             return $status;
          }
          OrderDetail::where('order_id',$orderId)->update(['order_status'=>4,'mgmt_id'=>Auth::user()->employee->id]);
          $status = array(
                  'status'     => true,
                  );
      }
      catch(Exeption $e){
          DB::rollback();
          $status = array(
                  'status'     => false,
                  );
          return $status;
      }
      DB::commit();

      //Sending Firebase notifications
      $orderDetail = Order::where('id',$orderId)->first();
      app('App\Http\Controllers\NotifController')->sendNotification($orderDetail->tracking_no);
      return $status;
    }

    //Controller which reject the order
    public function api_rejectOrder(Request $request)
    {
        $client_data = Input::all();
        DB::beginTransaction();
        try{
            $orderId = $client_data['nameValuePairs']['orderId'];
            $orderDetail=OrderDetail::where('order_id',$orderId)->first();
            if($orderDetail->order_status!=3){
              DB::commit();
               $status = array(
                    'status'     => false,
                    );
               return $status;
            }
            OrderDetail::where('order_id',$orderId)->update(['order_status'=>7,'mgmt_id'=>Auth::user()->employee->id]);
            $status = array(
                    'status'     => true,
                    );
        }
        catch(Exeption $e){
            DB::rollback();
            $status = array(
                    'status'     => false,
                    );   
            return $status;
        }
        DB::commit();

        //Sending Firebase Notifications
        $orderDetail = Order::where('id',$orderId)->first();
        app('App\Http\Controllers\NotifController')->sendNotification($orderDetail->tracking_no);
        return $status;
    }

    //Controller to get management summary
    public function FetchManagmentSummary()
    {
        $today = \Carbon\Carbon::today()->toDateTimeString();
        $ordersCount = DB::select("SELECT COALESCE(SUM(count),0) as count FROM (SELECT COUNT(*) as count FROM `order_details` WHERE order_status=3 GROUP BY order_id) o_d");
        $empCount = Employee::count();
        $dealerCount = Dealer::count();
        $todayDate = date('Y-m-d');
        $monthlySales = DB::select("SELECT COALESCE(SUM(a.Total), 0) AS total FROM (SELECT (CASE WHEN rate_type=0 THEN ((item_rate * qty - ((item_rate * qty) / 100) * dis_per) + (item_rate * qty - ((item_rate * qty) / 100) * dis_per) / 100 * gst) ELSE (item_rate*qty) END) AS Total FROM order_details WHERE order_date = '$todayDate' ORDER BY sku) a");
        return array('ordersCount'=>$ordersCount[0]->count , 'empCount'=>$empCount, 'dealerCount'=>$dealerCount, 'monthlySales'=>$monthlySales[0]->total);
    }

    public function api_approveDealer(){
      $data=Input::all();
      
    }

}