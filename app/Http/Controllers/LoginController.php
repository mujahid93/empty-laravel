<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\IssuedInTheFuture;
use App\Models\User;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Notification;
use App\Models\Dealer;
use Illuminate\Support\Facades\Auth;
use Log;
use Hash;
use Illuminate\Support\Facades\Input;
use ApiConsumer;
use Lcobucci\JWT\Parser;
use Kreait\Firebase;
use DB;
use App\Models\DealerLogin;	

class LoginController extends Controller
{
    //

    public function getEmailAccessToken(Request $request)
	{
		$data = Input::all();
		Log::info($data);
		//Verify user token with firebase server (if email exists)
		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');
		$firebase = (new Factory)
		    ->withServiceAccount($serviceAccount)
		    ->create();
		$idTokenString = $data[0];
		try {
		    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString,false);
			Log::info("Token Verified");
		} catch (IssuedInTheFuture $e) {
			Log::info("Token Verified due to IssuedInTheFuture Exception");
		    $verifiedIdToken = (new Parser())->parse($idTokenString);
		}
		// Get User Firebase Id (if user token verified with server)
		$uid = $verifiedIdToken->getClaim('sub');
		$user = $firebase->getAuth()->getUser($uid);
		//Check if user password is created
		Log::info($user->email);
		if(Email::where('email',$user->email)->value('isDealer') == 1)
		{
			$reg_user = Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->get();
		}
		else
		{
			$reg_user = Employee::where('email_id', Email::where('email',$user->email)->value('id'))->get();
			User::where('email',$user->email)->update(['password'=>(Hash::make($uid))]);
			if($user->displayName==null)
				$user->displayName=$user->email;
		}
		if (User::where('email',$user->email)->count() == 0){
			try{
		          $user_info = User::create([
		          			'name' => $user->displayName,
		                    'email' => $user->email,
		                    'password' => Hash::make($uid),
		                    ]);
		        }
		        catch(\Exception $e)
		        {
					Log::info($e->getMessage());
		            return $e->getMessage();
		        }
		        if(Email::where('email',$user->email)->value('isDealer') == 1)
				{
					Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$user_info->id]);
				}
				else
				{
					Employee::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$user_info->id]);
				}
		}
		$request->request->add([
              'client_id' => '2',
              'client_secret' => 'yfMBM7dNV1AduHNRaC8soGN9R5lwhXU3CUp7NESA',
              'grant_type' => 'password',
              'username' => $user->email,
              'password' => $uid
            ]);
            $tokenRequest = $request->create('/oauth/token', 'POST', $request->all());
            $token =  \Route::dispatch($tokenRequest);
            $data = $token->getContent();
            $access_token = json_decode($data, true);
        	return [
	            'access_token' => $access_token['access_token'],
	            'reg_userId' => $reg_user[0]->id,
	            'role_id' => $reg_user[0]->role_id
	        ];
	}



	public function getOTPAccessTokenDealer(Request $request){
	    $data = Input::all();
	    Log::info($data);
	    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');
	    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
	    
	    $firebaseToken=$data["nameValuePairs"]['firebase_token'];
	    $token_app=$data["nameValuePairs"]['token_app'];
	    try {
		    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($token_app,false);
			Log::info("Token Verified");
		} catch (IssuedInTheFuture $e) {
			Log::info("Token Verified due to IssuedInTheFuture Exception");
		    $verifiedIdToken = (new Parser())->parse($token_app);

		}
		
		$uid = $verifiedIdToken->getClaim('sub');
		$mobile=$firebase->getAuth()->getUser($uid)->phoneNumber;
		$mobile=str_replace('+91', '', $mobile) ;
	    $dealer=Dealer::where('mobile1',$mobile)->first();
		
		if($dealer==null)
		    return null;
		    
		Dealer::where('mobile1',$mobile)->update(['fcm_mobile'=>$firebaseToken]);
		$random=$this->generateRandomString(8);
		$userObject=User::where('email',$mobile)->first();
		if($userObject==null){
			$userObject=User::create([
							'name'=>$dealer->name,
		          			'email' => $mobile,
		                    'password' =>Hash::make($random)
		                    ]);
				Dealer::where('mobile1',$mobile)->update(['login_id_app'=>$userObject->id]);
			}
			else
			$userObject=User::where('email',$mobile)->update(['password'=>Hash::make($random)]);
		$dealer=Dealer::where('mobile1',$mobile)->first();
		if($dealer->login_id_app==null){
			Dealer::where('mobile1',$mobile)->update(['login_id_app'=>User::where('email',$mobile)->value('id')]);
		}
		
		
		$res=Auth::attempt(['email' => $mobile, 'password' => $random]);
		if($res==true){
		    
		    $http = new \GuzzleHttp\Client;
		    
		    Log::info('Test !!!');
		    $response = $http->post('https://ekisan.kisancraft.com/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => 'yfMBM7dNV1AduHNRaC8soGN9R5lwhXU3CUp7NESA',
                    'username' => $mobile,
                    'password' => $random,
                    'scope' => '',
                ],
            ]);
            
            $token_rec=json_decode((string) $response->getBody(), true);
            $response=array(
                            'id'=>$dealer->id,
                            'branch_id'=>$dealer->branch->branch_name->id,
                            'state_id'=>$dealer->state_id,
                            'branch_rate'=>$dealer->branch->branch_name->rate,
                            'name'=>$dealer->name,
                            'contact'=>$dealer->contact,
                            'address1'=>$dealer->address1,
                            'address2'=>$dealer->address2,
                            'address3'=>$dealer->address3,
                            'mobile'=>$dealer->mobile1,
                            'updated_at'=>'',
                            'access_token'=>$token_rec['access_token']);
            
            Log::info($response);
            
            return $response;
		}
		else
		    return null;
	}


public function getOTPAccessTokenAgronomy(Request $request){
	    $data = Input::all();
	    //Log::info($data);
	    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');
	    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
	    
	    $firebaseToken=$data["nameValuePairs"]['firebase_token'];
	    $token_app=$data["nameValuePairs"]['token_app'];
	    try {
		    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($token_app,false);
			Log::info("Token Verified");
		} catch (IssuedInTheFuture $e) {
			Log::info("Token Verified due to IssuedInTheFuture Exception");
		    $verifiedIdToken = (new Parser())->parse($token_app);

		}
		
		$uid = $verifiedIdToken->getClaim('sub');
		$mobile=$firebase->getAuth()->getUser($uid)->phoneNumber;
		$mobile=str_replace('+91', '', $mobile) ;
	    $emp=Employee::where('mobile',$mobile)->first();

	    Log::info("Mobile number ".$mobile." trying to login");
		
		if($emp==null)
		    return null;
		    

		 Email::where('email',$mobile)->update(['fcm_mobile'=>$firebaseToken]);
		//Dealer::where('mobile1',$mobile)->update(['fcm_mobile'=>$firebaseToken]);
		$random=$this->generateRandomString(8);
		$userObject=User::where('email',$mobile)->first();
		if($userObject==null){
			$userObject=User::create([
							'name'=>$emp->name,
		          			'email' => $mobile,
		                    'password' =>Hash::make($random)
		                    ]);
				Employee::where('mobile',$mobile)->update(['login_id'=>$userObject->id]);
			}
			else
			$userObject=User::where('email',$mobile)->update(['password'=>Hash::make($random)]);
		
		$res=Auth::attempt(['email' => $mobile, 'password' => $random]);
		if($res==true){
		    
		    $http = new \GuzzleHttp\Client;
		    
		    Log::info('Test !!!');
		    $response = $http->post('https://ekisan.kisancraft.com/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => 'yfMBM7dNV1AduHNRaC8soGN9R5lwhXU3CUp7NESA',
                    'username' => $mobile,
                    'password' => $random,
                    'scope' => '',
                ],
            ]);

            Log::info("Mobile number ".$mobile." Logged In");
            
            $token_rec=json_decode((string) $response->getBody(), true);
            return [
	            'access_token' => $token_rec['access_token'],
	            'reg_userId' => $emp->id,
	            'role_id' => $emp->role_id
	        ];
            
		}
		else
		    return null;
	}


	public function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}
