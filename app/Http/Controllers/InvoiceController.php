<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\OrderDetail;
use App\Models\State;
use App\Models\Order;
use App\Models\Dealer;
use App\Models\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;
use Redirect;
use Session;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $states = State::all();
        $districtList=array();
        $dealerList=array();

        //From Date
        if(Session::has('invoice_fromDate'))
            $fromDate = Session::get('invoice_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');

        //To Date
        if(Session::has('invoice_toDate'))
            $toDate = Session::get('invoice_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');



        //State
        if(Session::has('invoice_state')){
            $state=Session::get('invoice_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";

        //District
        if(Session::has('invoice_district')){
            $districts = Session::get('invoice_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";


        //Dealer Name
        if(Session::has('invoice_dealer')){
            $dealer = Session::get('invoice_dealer');
        }
        else
            $dealer = "%";


        //Tracking No
        if(Session::has('invoice_tracking'))
            $tracking_no=Session::get('invoice_tracking');
        else
            $tracking_no="%";

        $user=Auth::user()->employee;
        $branch_id=$user->branch_id;
        if($user->role_id==4 || $user->role_id==6 || $user->role_id==14)
            $branch_id="%";

        $items = $request->items ?? 10;
        $invoices = Invoice::whereHas('OrderDetail', function ($stmt) use($dealer,$districts,$state,$tracking_no,$branch_id) {
                    $stmt->where('branch','LIKE',$branch_id)
                    ->whereHas('Order',function($query) use($dealer,$districts,$state,$tracking_no){
                        $query->where('dealer_id','LIKE',$dealer)
                              ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                              ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    });
                    });
                })
                ->where('created_at','>=', $fromDate)->where('created_at','<=',$toDate)
                ->orderBy('created_at','desc')
                ->paginate($items);
        return view('invoice.index')->with(compact('invoices','items','states','districtList','dealerList'));
    }

    public function NewInvoice(Request $r)
    {
        $order = $r;
        return view('invoice.create')->with(compact('order'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $r)
    {
        // Log::info($r->h_purchaseNo);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Updated Code
       if(\Auth::user()->employee->role_id == 6 || \Auth::user()->employee->role_id == 20)
       {
           try{
                $AOrder = OrderDetail::where('branch_tracking_no','=',decrypt($request->tracking_no))->where('order_status',4)->get();
                $invoice = new Invoice();
                $invoice->iNumber = $request->number;
                $invoice->iAmt = $request->amt;
                $invoice->order_detail_id = $AOrder[0]->id;
                $pdf = $request->pdf;
                $name = time().'.'.$pdf->getClientOriginalExtension();
                $destinationPath = storage_path().'/invoices/';
                $pdf->move($destinationPath, $name);
                $pdfPath = '/invoices/'.$name;
                $invoice->iPdf = $pdfPath;
                if($invoice->save())
                    OrderDetail::where('branch_tracking_no',decrypt($request->tracking_no))->update(['order_status'=>5]);
                Log::info($invoice->OrderDetail->tracking_no);
                app('App\Http\Controllers\OrderController')->sendNotificationToExecutive(decrypt($request->tracking_no),5);
                app('App\Http\Controllers\NotifController')->sendNotification($AOrder[0]->tracking_no);
                return redirect()->route("invoice.index")->with(['result'=>'New Invoice Created','color'=>'bg-green']);
            }
            catch(\Exception $e){
                return $e->getMessage();
                // return redirect()->route("accGroup.index")->with(['result'=>'Error adding Machine Group','color'=>'alert-warning']);
            }
        }
       
       //Old Code - 05 December  
       // if(\Auth::user()->employee->role_id == 6)
       // {
       //     try{
       //          $AOrder = OrderDetail::where('branch_tracking_no','=',$request->tracking_no)->get();
       //          $invoice = new Invoice();
       //          $invoice->iNumber = $request->number;
       //          $invoice->iAmt = $request->amt;
       //          $invoice->order_detail_id = $AOrder[0]->id;
       //          $pdf = $request->pdf;
       //          $name = time().'.'.$pdf->getClientOriginalExtension();
       //          $destinationPath = public_path('/invoices/');
       //          $pdf->move($destinationPath, $name);
       //          $pdfPath = '/invoices/'.$name;
       //          $invoice->iPdf = $pdfPath;
       //          if($invoice->save())
       //              OrderDetail::where('branch_tracking_no',$request->tracking_no)->update(['order_status'=>5]);
       //          Log::info($invoice->OrderDetail->tracking_no);
       //          app('App\Http\Controllers\OrderController')->sendNotificationToExecutive($request->tracking_no,5);
       //          app('App\Http\Controllers\OrderController')->sendInvoiceNotifApp($AOrder[0]->tracking_no,$invoice);
       //          return redirect()->route("invoice.index")->with(['result'=>'New Invoice Created','color'=>'bg-green']);
       //      }
       //      catch(\Exception $e){
       //          return $e->getMessage();
       //          // return redirect()->route("accGroup.index")->with(['result'=>'Error adding Machine Group','color'=>'alert-warning']);
       //      }
       //  }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {   
        $invoice = Invoice::find($invoice->id);
        return view('invoice.edit')->with(compact('invoice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $invoiceId)
    {
        // $invoice = Invoice::find($invoiceId);
        // $invoice->iNumber = $request->number;
        // $invoice->iAmt = $request->amt;
        // if($request->pdf)
        // {
        //     $pdf = $request->pdf;
        //     $name = time().'.'.$pdf->getClientOriginalExtension();
        //     $destinationPath = public_path('/invoices/');
        //     $pdf->move($destinationPath, $name);
        //     $invoice->iPdf = '/invoices/'.$name;
        // }
        // else
        // {
        //     $invoice->iPdf = $request->oldPdf;
        // }
        // $invoice->save();
        return Redirect::route('invoice.index',array('items' => 5));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        // Invoice::where('id',$invoice->id)->delete();
        return Redirect::route('invoice.index',array('items' => 5));
    }

    public function getFile(Request $request)
    {
        if(Auth::user()->employee->role_id == 4 or Auth::user()->employee->role_id == 6 || Auth::user()->employee->role_id == 20 || Auth::user()->employee->role_id == 19)
        {
            $fullpath=decrypt($request->invoicePath);
            return response()->download(storage_path($fullpath), null, [], null);
        }
        else
        if(Auth::user()->employee->branch_id == decrypt($request->branch))
        {
            $fullpath=decrypt($request->invoicePath);
            return response()->download(storage_path($fullpath), null, [], null);
        }
        else
        {
            $items = $request->items ?? 10;
            $invoices = Invoice::whereHas('OrderDetail', function ($stmt) {
                        $stmt->where('branch',\Auth::user()->employee->branch_id);
                    })
                    ->orderBy('created_at','desc')
                    ->paginate($items);
            return view('invoice.index')->with(compact('invoices','items'));
        }
    }
}
