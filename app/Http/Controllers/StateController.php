<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $request->items ?? 5;
        $states = State::where('status','=',1)->paginate($items);
        return view('state.index')->with(compact('states','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('state.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $state = new State();
            $state->name = $request->name;
            $state->code = $request->code;
            $state->save();
            return redirect()->route("state.index")->with(['result'=>'New state added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("state.index")->with(['result'=>'Error adding state','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
          $state = State::find($state->id);
          $id = $state->id;
          return view('state.edit')->with(compact('state','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        try{
         $state = State::find($state->id);
         $state->name = $request->name;
         $state->code = $request->code;
         $state->save();
         return redirect()->route('state.index')->with(['result'=>'state updated successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("state.index")->with(['result'=>'Error updating state','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        $states = State::find($state->id);
        $states->status = 0;
        $states->save();
        return redirect()->route('state.index')->with(['result'=>'state deleted successfully','color'=>'bg-green']);
    }

    public function FilterRows(Request $request)
    {
        $states = State::where('name', 'LIKE', '%' . $request->queryFilter . '%')->where('status','=',1)->paginate(5);
        return $states;
    }
}
