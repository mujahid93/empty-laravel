<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $request->items ?? 5;
        $roles = Role::where('status','=',1)->paginate($items);
        return view('role.index')->with(compact('roles','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $role = new Role();
            $role->name = $request->name;
            $role->code = $request->code;
            $role->save();
            return redirect()->route("role.index")->with(['result'=>'New role added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("role.index")->with(['result'=>'Error adding role','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
          $role = Role::find($role->id);
          return view('role.edit')->with(compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        try{
         $role = Role::find($role->id);
         $role->name = $request->name;
         $role->code = $request->code;
         $role->save();
         return redirect()->route('role.index')->with(['result'=>'Role update successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("role.index")->with(['result'=>'Error updating role','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $roles = Role::find($role->id);
        $roles->status = 0;
        $roles->save();
        return redirect()->route('role.index')->with(['result'=>'Role deleted successfully','color'=>'bg-green']);
    }

    public function FilterRole()
    {
        $query = $_GET['queryFilter'];
        $roles = Role::where('name', 'LIKE', '%' . $query . '%')->where('status','=',1)->paginate(5);
        return $roles;
    }
}
