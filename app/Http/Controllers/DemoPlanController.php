<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DemoPlan;
use App\Models\Dealer;
use App\Models\District;
use App\Models\DealerVisit;
use App\Models\Employee;
use App\Models\Feedback;
use App\Models\FeedbackAnswer;
use Illuminate\Support\Facades\Input;
use DB;
use Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Rap2hpoutre\FastExcel\FastExcel;

class DemoPlanController extends Controller
{

	//Save Demo plan
    public function saveDemoPlan(){

    	$data=Input::all();
    	if(COUNT($data)==0){
    		//No Data Found
    		return array('status'=>false);	
    	}
        $emp_id=0;
        $token="";
    	DB::beginTransaction();
         try{
         	//Adding Demo Plan
            $usrData=$data[0];
            $transaction=$this->generateTransactionNo($usrData['emp_id'],"DP");
         	foreach($data as $key => $value) {
                //Log::info($value);
         		$demo=new DemoPlan();
                $demo->date=$value['date'];
         		$demo->transaction_code=$transaction;
                $demo->days=$value['days'];
         		$demo->emp_id=$value['emp_id'];
         		$demo->cat_id=$value['cat_id'];
         		$demo->subcat_id=$value['subcat_id'];
         		$demo->purpose=$value['purpose'];
         		$demo->state=$value['state'];
         		$demo->district=$value['district'];
         		$demo->taluk=$value['taluk'];
         		$demo->village=$value['village'];
         		$demo->dealerName=$value['dealerName'];
         		$demo->model=$value['model'];
         		$demo->cropName=$value['cropName'];
         		$demo->cropRowGap=$value['cropRowGap'];
         		$demo->cropHeight=$value['cropHeight'];
         		$demo->salesPersonName=$value['salesPersonName'];
         		$demo->technicianName=$value['technicianName'];
         		$demo->vehicleType=$value['vehicleType'];
         		$demo->localExps=$value['local_travel_exp'];
         		$demo->lodgingExps=$value['lodging_exp'];
         		$demo->mealExps=$value['meal_exp'];
         		$demo->miscExps=$value['misc_exp'];
         		$demo->outstationExps=$value['outstation_exp'];
         		$demo->token=$value['token'];
                $token=$demo->token;
                $emp_id=$demo->emp_id;
                $demo->save();
         	}
         }
         catch(\Exeption $e){
            DB::rollback();
            return array('status'=>false);
         }
         app('App\Http\Controllers\NotifController')->sendDemoPlanNotification($emp_id,$token);
         DB::commit();
    	return array('status'=>true);
    }

    public function getPendingDemoPlanList(Request $req,$emp_id){
        $data=DB::select("SELECT employees.id as emp_id,employees.name,MIN(plan.date) as start_date,MAX(plan.date) as end_date,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,token FROM demo_plans plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE plan.status=1 AND plan.emp_id IN (SELECT id FROM employees WHERE manager_id='$emp_id') GROUP BY plan.emp_id,plan.token");
        return $data;
    }

    public function setDemoAction(Request $req,$emp_id,$token,$action){
    	if($action==2){
    		 DemoPlan::where('emp_id',$emp_id)->where('token',$token)->update(['status'=>2,'manager_id'=>Auth::user()->employee->id]);
             app('App\Http\Controllers\NotifController')->sendDemoPlanNotification($emp_id,$token);
        }
    	else if($action==3){
    		 DemoPlan::where('emp_id',$emp_id)->where('token',$token)->update(['status'=>3,'manager_id'=>Auth::user()->employee->id]);
             app('App\Http\Controllers\NotifController')->sendDemoPlanNotification($emp_id,$token);
            }
    	return array('status'=>true);
    }

    public function getDemoPlanDetails(Request $req,$emp_id,$token){
        return DemoPlan::where('emp_id',$emp_id)->where('token',$token)->select('date','days','emp_id','cat_id','subcat_id','purpose','state','district','taluk','village','dealerName','model','cropName','cropRowGap','cropHeight','salesPersonName','technicianName','vehicleType','localExps as local_travel_exp','lodgingExps as lodging_exp','mealExps as meal_exp','miscExps as misc_exp','outstationExps as outstation_exp','token')->get();
    }

    public function getDemoStatus(Request $req, $emp_id){
        $date=Carbon::now()->format('Y-m-d');
        $data=DB::select("SELECT plan.token,plan.status FROM `demo_plans` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id' GROUP BY plan.token");
        //Log::info($data);
        return $data;
    }

     public function getWebPendingDemoList(){
        $items = $request->items ?? 10;
        $user=Auth::user()->employee;
        if($user->role_id==4 || $user->role_id==14){
            $demo=DemoPlan::where('status',1)->whereIn('emp_id',Employee::where('manager_id',null)->whereIn('role_id',[17,18])->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        else if($user->role_id==18){
            $demo=DemoPlan::where('status',1)->whereIn('emp_id',Employee::where('manager_id',null)->whereIn('role_id',[17,18])->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        else{
            $demo=DemoPlan::where('status',1)->whereIn('emp_id',Employee::where('manager_id',$user->id)->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        return view('demo.demolist')->with(compact('demo'));
    }

    public function pendingDemoDetails(Request $req, $emp_id, $token){
        $items = $request->items ?? 10;
        $data=DB::select("SELECT employees.id as emp_id,employees.name,MAX(plan.date) as start_date,MIN(plan.date) as end_date,token,GROUP_CONCAT(DISTINCT(plan.state) SEPARATOR ',') as state,GROUP_CONCAT(DISTINCT(plan.district) SEPARATOR ',') as district,GROUP_CONCAT(DISTINCT(plan.taluk) SEPARATOR ',') as city,COUNT(plan.dealerName) as dealersQty,GROUP_CONCAT(plan.dealerName SEPARATOR ',') as dealers,GROUP_CONCAT(plan.purpose SEPARATOR ',') as purpose,SUM(localExps) as localExps,SUM(lodgingExps) as lodgingExps,SUM(mealExps) as mealExps,SUM(outstationExps) as outstationExps,SUM(miscExps) as miscExps,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,plan.emp_id,plan.token FROM demo_plans plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE employees.id='$emp_id' AND plan.token='$token' GROUP BY plan.emp_id,plan.token");

        $demo=DB::select("SELECT plan.id as demo_id,plan.date,plan.token,plan.state as state,plan.district,plan.taluk,(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expenses  FROM demo_plans plan WHERE emp_id=$emp_id AND token='$token'");
        return view('demo.details')->with(compact('demo','data'));
    }

    public function getAllDemoDetails(Request $req, $emp_id, $token){
        $demo=DemoPlan::where('emp_id',$emp_id)->where('token',$token)->select('emp_id','date','purpose','state','district','taluk','village','dealerName','model','cropName','cropRowGap','cropHeight','salesPersonName','technicianName','vehicleType')->get();
        return view('demo.all_details')->with(compact('demo','emp_id','token'));
    }


     public function demoApprove(Request $req){
        DemoPlan::where('emp_id',$req->emp_id)->where('token',$req->token)->update(["status"=>2,'manager_id'=>Auth::user()->employee->id]);
        app('App\Http\Controllers\NotifController')->sendDemoPlanNotification($req->emp_id,$req->token);
        return redirect()->route('pendingDemo');
    }

    public function demoReject(Request $req){
        DemoPlan::where('emp_id',$req->emp_id)->where('token',$req->token)->update(["status"=>3,'manager_id'=>Auth::user()->employee->id]);
        app('App\Http\Controllers\NotifController')->sendDemoPlanNotification($req->emp_id,$req->token);
        return redirect()->route('pendingDemo');
    }

    public function getWebApprovedDemoList(){
        $items = $request->items ?? 10;
        $emp=Auth::user()->employee;
            $demo=DemoPlan::where('status',2)
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        return view('demo.approved')->with(compact('demo','emp'));
    }

    public function demoPrint(Request $req, $emp_id, $token){
        $demo=DemoPlan::whereIn('status',[2,4])->where('emp_id',$emp_id)->where('token',$token)->get();
        return view('demo.demoPrint')->with(compact('demo'));
    }


    public function excel(Request $req,$emp_id,$token) {

        $demo=DemoPlan::where('emp_id',$emp_id)->where('token',$token)->select('emp_id','date','purpose','state','district','taluk','village','dealerName','model','cropName','cropRowGap','cropHeight','salesPersonName','technicianName','vehicleType')->get();
        return (new FastExcel($demo))->download('Demo'.$emp_id.'.xlsx');
    }


    public function generateTransactionNo($emp_id,$type){
        $tracking=$type.$emp_id.Carbon::now()->month."0";
        $demo=DemoPlan::orderBy('id','DESC')->first();
        if($demo==null)
            $tracking=$tracking."1";
        else
            $tracking=$tracking.($demo->id+1);
        return $tracking;
    }

}
