<?php
 namespace App\Http\Controllers;
 use App\Models\Order;
use App\Models\Dealer;
use App\Models\MachineSku;
use App\Models\MachineSubsidy;
use App\Models\AccessoriesSku;
use App\Models\AccSubsidy;
use App\Models\Part;
use App\Models\SubsidyDept;
use App\Models\SubsidyDeptState;
use App\Models\User;
use App\Models\Shipping;
use App\Models\Branch;
use App\Models\State;
use App\Models\Cart;
use App\Models\OrderDetail;
use App\Models\Employee;
use App\Models\Email;
use App\Models\District;
use App\Models\EmpDistrict;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use App\Models\Notification as Notify;
use Kreait\Firebase;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Log;
use DB;
use PDF;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Exception;
 
 class OrderController extends Controller
{
     /**
     * New Shipping .
     *
     */
    public function DoKeepNewShipping(Request $request)
    {
        Shipping::where('id',decrypt($request->shipping))->update(['is_duplicate'=>0]);
        $shippings = Shipping::where('is_duplicate',1)->paginate(10);
        return view('shipping.new_ship')->with(compact('shippings'));
    }

    public function DoUpdateOrderShipping(Request $request)
    {
        OrderDetail::where('shipping_id',decrypt($request->old_shipping))->update(['shipping_id'=>$request->new_shipping]);
        Shipping::where('id',decrypt($request->old_shipping))->delete();
    }

    public function GetNewShippingDetail(Request $request)
    {
        $shipping = Shipping::where('id',decrypt($request->shipping))->first();
        $ShippingDetails = Shipping::where('branch_id',$shipping->branch_id)->get(['id','agency']);
        return view('shipping.ship_detail')->with(compact('shipping','ShippingDetails'));
    }

    public function GetNewShippingRequest()
    {
        $shippings = Shipping::where('is_duplicate',1)->paginate(10);
        return view('shipping.new_ship')->with(compact('shippings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function PendingOrders(Request $request)
    {
        $orders = Order::with(['dealer','placedBy'])->where('emp_id',Auth::user()->employee->id)->whereHas('OrderDetail', function ($stmt){
                    $stmt->where('order_status',2);
                })->orderBy('created_at', 'desc')->paginate(10);
        return view('order.executive.pending')->with(compact('orders'));
    }
     public function DeleteOrderByExecutive(Request $r)
    {
        $trackingNo = decrypt($r->tracking_no);
        DB::beginTransaction();
        try{
            $id = Order::where('tracking_no',$trackingNo)->where('emp_id',Auth::user()->employee->id)->whereHas('OrderDetail', function ($stmt){
                    $stmt->where('order_status',2);
                })->value('id');
            $order = Order::find($id);
            if($order)
            {
                $destroy = Order::destroy($id);
            }
            Log::info("Delete status :".$destroy);
            if($destroy)
            {
                OrderDetail::where('tracking_no',$trackingNo)->where('order_status',2)->delete();
                $status = OrderDetail::where('tracking_no',$trackingNo)->count();
                Log::info("order count : ".$status);
                if($status)
                {   
                    DB::rollback();
                    $orders = Order::with(['dealer','placedBy'])->where('emp_id',Auth::user()->employee->id)->whereHas('OrderDetail', function ($stmt){
                        $stmt->where('order_status',2);
                    })->orderBy('created_at', 'desc')->paginate(10);
                    return view('order.executive.pending')->with(compact('orders'));
                }
                else
                {
                    Log::info("Sending notification to payment approver");
                    $data= array('tracking_no' =>$trackingNo,
                         'message'=>'Order with tracking no '.$trackingNo." deleted by the executive",
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'fileurl'=>'',
                         'name'=>Auth::user()->employee->name,
                         'status'=>'8',
                         'messagetype'=>8);
                    $this->notifyPaymentApprover($data);
                }
            }
        }
        catch(\Exception $e)
        {
            Log::info($e->getMessage());
            DB::rollback();
        }
        DB::commit();
        $orders = Order::with(['dealer','placedBy'])->where('emp_id',Auth::user()->employee->id)->whereHas('OrderDetail', function ($stmt){
                    $stmt->where('order_status',2);
                })->orderBy('created_at', 'desc')->paginate(10);
        return view('order.executive.pending')->with(compact('orders'));
    }
     public function FetchDealersByState(Request $request)
    {
        $dealers = Dealer::where('state_id',$request->stateId)->orderBy('name')->get(['id','name']);
        $dealerId = 0;
        if(Session::has('current_dealer'))
            $dealerId = Session::get('current_dealer');
        $dealerData = array();
        $dealerData = ['dealerId'=>$dealerId, 'dealers'=>$dealers];
        return $dealerData;
    }
     public function create()
    {
        if(Auth::user()->employee->role_id == 2)
        {
            $dealers = Auth::user()->dealer;
            $todayDate = date('Y-m-d');
            $branch = Branch::where('id',DB::table('state_branches')->where('state_id',$dealers->state_id)->value('branch_id'))->get() ;
            return view('order.dealer.create_by_dealer')->with(compact(['dealers','todayDate','branch']));
        }
        else if(Auth::user()->employee->role_id == 4)
        {
            $dealers = Dealer::where('status',1)->orderBy('name', 'asc')->where('state_id',1)->get(['id','name']);
            $states = State::get(['id','name']);
            $todayDate = date('Y-m-d');
            $branches = Branch::where('status',1)->get(['id','name']);
            if(Session::has('dealer_id'))
            {
                $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches','cartCount','states']));
            }
            else
            {
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches','states']));
            }
        }
        else
        {
            $dealers = Dealer::where('status',1)->whereIn('district_id',Auth::user()->employee->districts->pluck('district_id'))->orderBy('name', 'asc')->get(['id','name']);
            $todayDate = date('Y-m-d');
            $branches = Branch::where('status',1)->get(['id','name']);
            if(Session::has('dealer_id'))
            {
                $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches','cartCount']));
            }
            else
            {
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches']));
            }
        }
    }
     public function GetNotificationCount()
    {
        return Notify::where('emp_id',Auth::user()->employee->id)->where('seen',1)->count();
    }
     public function GetAllNotifications()
    {
        if(Auth::user()->employee->role_id == 4 || Auth::user()->employee->role_id == 14)
        {
            $orders = Notify::join('notifications as m2', function($join)
                                 {
                                   $join->on('notifications.order_id', '=', 'm2.order_id');
                                   $join->on('notifications.id','<','m2.id');
                                 }
                             )
                       ->orderBy('notifications.created_at','desc')
                       ->paginate();
        }
        else if(Auth::user()->employee->role_id == 5)
        {
            $orders = Notify::whereHas('Order', function ($query) {
                    $query->whereHas('OrderDetail', function ($stmt) {
                    $stmt->where('order_status',2);
                });
                })->orderBy('created_at','desc')->paginate();
        }
        else if(Auth::user()->employee->role_id == 6)
        {
            $orders = Notify::whereHas('Order', function ($query) {
                    $query->whereHas('OrderDetail', function ($stmt) {
                    $stmt->where('order_status',4);
                });
                })->orderBy('created_at','desc')->paginate();
        }
        else
        {
            $orders = Notify::where('emp_id',Auth::user()->employee->id)->orderBy('created_at','desc')->paginate(10);
        }
        Notify::where('emp_id',Auth::user()->employee->id)->update(['seen'=>0]);
        return view('notifications')->with(compact('orders'));
    }
     public function GetMchPriceList(Request $request)
    {
        $branch_id= Session::get('mch_branch_id');
        $grp= Session::get('mch_grp_name');
        $subgrp= Session::get('mch_subgrp_name');
        if($grp=="All")
            $grp='';
        if($subgrp=="All")
            $subgrp='';
         $items = MachineSku::with('subsidy_rate')->where('grp','LIKE','%'.$grp.'%')->where('subgrp','LIKE','%'.$subgrp.'%')->paginate(10);
        $branches = Branch::all(['name','id','rate']);
        $groups = MachineSku::distinct('grp')->pluck('grp')->toArray();
        array_unshift($groups , 'All');
        if($grp!=''){
            $subgrp = MachineSku::distinct('subgrp')->where('grp','LIKE','%'.$grp.'%')->pluck('subgrp')->toArray();
            array_unshift($subgrp , 'All');
        }
        else
            $subgrp=[];
        return view('order.executive.mch_price_list')->with(compact('items','branches','groups','subgrp'))->with('sel_branch',Branch::where('id',$branch_id)->value('rate'));
    }

    public function FilterAllByFromAndToDate()
    {
        $fromDate = $_GET['fromDate'];
        $toDate = $_GET['toDate'];
        $orders = Order::with(['dealer','placedBy'])->whereHas('OrderDetail', function ($query)  use($fromDate,$toDate) {
                    $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate);
                })->paginate(20);
        return $orders;
    }
    public function FilterByFromAndToDate()
     {
         $fromDate = $_GET['fromDate'];
         $toDate = $_GET['toDate'];
         // $orders = Order::with(['dealer'])->where('created_at',$fromDate)->where('emp_id',Auth::user()->employee->id)->paginate(5);
         $orders = Order::with(['dealer'])->whereHas('OrderDetail', function ($query)  use($fromDate,$toDate) {
                     $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate);
                 })->where('emp_id',Auth::user()->employee->id)->paginate(5);
         return $orders;
     }
       public function FilterAllByOrderStatus()
    {
        $search_query = $_GET['queryFilter'];
        $orders = Order::with(['dealer','placedBy'])->where('status',1)->where('order_status',$search_query)->paginate(5);
        return $orders;
    }
    /**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
    */
    public function FetchDashboardSummary()
    {
        if(\Auth::user()->employee->role->id == 4)
        {
            $today = \Carbon\Carbon::today()->toDateTimeString();
            $ordersCount = DB::select("SELECT COALESCE(SUM(count),0) as count FROM (SELECT COUNT(*) as count FROM `order_details` WHERE order_status=3 GROUP BY order_id) o_d");
            $empCount = Employee::count();
            $dealerCount = Dealer::count();
            $todayDate = date('Y-m-d');
            $monthlySales = DB::select("SELECT COALESCE(SUM(a.Total),0) as total FROM (SELECT ((item_rate*qty-((item_rate*qty)/100)*dis_per)+ (item_rate*qty-((item_rate*qty)/100)*dis_per)/100*gst) as Total from order_details where order_date='$todayDate' order by sku) a");
            return array('ordersCount'=>$ordersCount[0]->count , 'empCount'=>$empCount, 'dealerCount'=>$dealerCount, 'monthlySales'=>$monthlySales[0]->total);
        }
        else
        {
            $today = \Carbon\Carbon::today()->toDateTimeString();
            $ordersCount = Order::where('created_at','>',$today)->count();
            $empCount = Employee::count();
            $dealerCount = Dealer::count();
            $monthlySales = OrderDetail::where('created_at','>=',\Carbon\Carbon::now()->startOfMonth())->where('created_at','<=',\Carbon\Carbon::now()->endOfMonth())->where('order_id','>',3)->groupBy('order_id')->get(['order_id'])->count();
            return array('ordersCount'=>$ordersCount, 'empCount'=>$empCount, 'dealerCount'=>$dealerCount, 'monthlySales'=>$monthlySales);
        }
    }
     public function FetchManagmentSummary()
    {
        $today = \Carbon\Carbon::today()->toDateTimeString();
        $ordersCount = DB::select("SELECT COALESCE(SUM(count),0) as count FROM (SELECT COUNT(*) as count FROM `order_details` WHERE order_status=3 GROUP BY order_id) o_d");
        $empCount = Employee::count();
        $dealerCount = Dealer::count();
        $todayDate = date('Y-m-d');
        $monthlySales = DB::select("SELECT COALESCE(SUM(a.Total),0) as total FROM (SELECT ((item_rate*qty-((item_rate*qty)/100)*dis_per)+ (item_rate*qty-((item_rate*qty)/100)*dis_per)/100*gst) as Total from order_details where order_date='$todayDate' order by sku) a");
        return array('ordersCount'=>$ordersCount[0]->count , 'empCount'=>$empCount, 'dealerCount'=>$dealerCount, 'monthlySales'=>$monthlySales[0]->total);
    }
       //Pending Orders
    public function index()
    {
        // $dealers = DB::table('dealer_district')->whereIn('district_id',Auth::user()->employee->districts->pluck('district_id'))->pluck('dealer_id');
         // $pendingOrders = Order::with(['dealer','shipping'])->where('order_status','=',1)->whereIn('dealer_id',$dealers)->orderBy('created_at', 'desc')->paginate(10);
         // $pendingOrders = Order::with(['dealer','shipping','placedBy'])->where('order_status','=',1)->orderBy('created_at', 'desc')->paginate(10);
         // return view('order.index')->with(compact('pendingOrders'));
    }
        public function JsonExecutivePlacedOrders()
     {
         $execApprovedOrders = Order::where('order_status','=',2)->where('status',1)->orderBy('created_at', 'desc')->paginate(10);
         return $execApprovedOrders;
     }
        public function LoadShipMode()
     {
        return Shipping::all(['id','name','agency']);
     }
       public function GetDealerOrders()
     {
         $dealerOrders = Order::with(['dealer','placedBy'])->where('dealer_id',Auth::user()->dealer->id)->where('status',1)->orderBy('created_at', 'desc')->paginate(10);
         return view('order.general.dealer_orders')->with(compact('dealerOrders'));
     }
       public function DealerPlacedOrders()
    {
        $emp=Auth::user()->employee;
        if($emp->role_id==4){
            $dealerPlacedOrders = Order::with(['dealer','placedBy'])->whereHas('OrderDetail', function ($query) {
                                             $query->where('order_status',1);
                                         })->where('status',1)->orderBy('created_at', 'desc')->paginate(10);
            return view('order.executive.dealer_placed_orders')->with(compact('dealerPlacedOrders'));
        }
        else{
             $dealerPlacedOrders = Order::with(['dealer','placedBy'])->whereHas('OrderDetail', function ($query) {
                                             $query->where('order_status',1);
                                         })
                                         ->whereHas('dealer',function($query2) use($emp){
                                            $query2->whereIn('district_id',Auth::user()->employee->districts->pluck('district_id'));
                                         })->where('status',1)->orderBy('created_at', 'desc')->paginate(10);
            return view('order.executive.dealer_placed_orders')->with(compact('dealerPlacedOrders'));   
        }
    }
    
    public function cart()
    {
        Session::forget('discountFlag');
        Session::forget('current_dealer');
        Session::forget('orderDate');
        Session::forget('dispatchBranch');
          $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                         $query->select('name');
                     })->get();
          $branches = Cart::distinct()->pluck('dispatch_branch');
          $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
          return view('order.executive.cart')->with(compact('cart','branches','destination'));
    }
     /**
      * Store a newly created resource in storage.
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
    public function store(Request $r)
    {
        // Session::flush();
       
        
         $order_dealer = 0;
         if(Auth::user()->employee->role_id == 4)
        {
            Session::put('current_dealer',$r->m_dealer);
            $order_dealer = $r->m_dealer;
        }
        else
        {
            Session::put('current_dealer',$r->dealer);
            $order_dealer = $r->dealer;
        }

        

         if(!Session::has('state'))
            Session::put('state_id',$r->state);
         Session::put('orderDate',$r->todayDate);
        Session::put('dispatchBranch',$r->dispatchBranch);
        Session::put('branchRatePer',Branch::where('id',$r->dispatchBranch)->value('rate'));
         Log::info("classification : ".$r->classification);
         if($r->classification == 1)
        {
                switch($r->orderSubmit)
                {
                    case 'addToCart':
                         $partCount=Cart::where('classification',3)->where('dealer_id',$order_dealer)->count();
                         if($partCount>0){
                                return back()->with(['msg'=>'Parts And Other Machinery cannot be addeded in same order','classification'=>1]);
                         }
                         $category = $r->classification;
                         $sku = 'Sku'.$category;
                         $qty = 'qty'.$category;
                         $dis_per = 'disPerc'.$category;
                         $rate_type = 'priceLevel'.$category;
                         $subsidy_dept = 'subsidyDept'.$category;
                         $cart = new Cart();
                        $cart->order_date = $r->todayDate;
                        $cart->emp_id = Employee::where('login_id',Auth::user()->id)->value('id');
                        $cart->dealer_id = $order_dealer;
                        $cart->branch = $r->dispatchBranch;
                        $cart->rate_type = $r->$rate_type;
                        $cart->classification = $category;
                        if($r->$rate_type == 2)
                            $cart->subsidy_dept_id = SubsidyDept::where('code',$r->$subsidy_dept)->value('id');
                        $skuId = 0;
                        $gst = null;
                         switch ($category)
                        {
                            case '1':
                                $skuId = MachineSku::where('sku',$r->$sku)->value('id');
                                 switch ($cart->rate_type) {
                                    case '0':
                                        $item_rate = MachineSku::where('sku',$r->$sku)->value('dp') * Branch::where('id',$r->dispatchBranch)->value('rate');
                                        $gst = MachineSku::where('sku',$r->$sku)->value('gst');
                                        if(Session::get("gst") == null)
                                        {
                                            Session::put("gst",MachineSku::where('sku',$r->$sku)->value('gst'));
                                        }
                                        break;
                                    case '1':
                                        $item_rate = MachineSku::where('sku',$r->$sku)->value('mrp');
                                        break;
                                    case '2':
                                        $item_rate = MachineSubsidy::where('mch_id',$skuId)->where('subsidy_dept_id',SubsidyDept::where('code',$r->$subsidy_dept)->value('id'))->where('state_id',Dealer::where('id',$order_dealer)->value('state_id'))->value('subsidy_price');
                                        break;
                                    default:
                                        break;
                                }
                                break;
                             case '2':
                                $skuId = AccessoriesSku::where('sku',$r->$sku)->value('id');
                                switch ($cart->rate_type) {
                                    case '0':
                                        $item_rate = AccessoriesSku::where('sku',$r->$sku)->value('dp') * Branch::where('id',$r->dispatchBranch)->value('rate');
                                        $gst = AccessoriesSku::where('sku',$r->$sku)->value('gst');
                                        if(Session::get("gst") == null)
                                        {
                                            Session::put("gst",AccessoriesSku::where('sku',$r->$sku)->value('gst'));
                                        }
                                        break;
                                    case '1':
                                        $item_rate = AccessoriesSku::where('sku',$r->$sku)->value('mrp');
                                        break;
                                    case '2':
                                        $item_rate = AccSubsidy::where('mch_id',$skuId)->where('branch_id',$r->dispatchBranch)->value('subsidy_price');
                                        break;
                                    default:
                                        return null;
                                        break;
                                }
                                break;
                             default:
                                break;
                        }
                         $IsGstNotInOrder = 0;
                        if(Session::get("gst") != null)
                        {
                            if((Session::get("gst")>0 && $gst!=null && $gst==0) || ($gst>0 && Session::get("gst")==0))
                                $IsGstNotInOrder = 1;
                        }
                        Log::info("GST not in order - gst($gst) ".$IsGstNotInOrder);
                          $cart->sku = $skuId;
                        $cart->item_rate = $item_rate;
                        $cart->qty = $r->$qty;
                         if($r->$dis_per == null)
                            $cart->dis_per = 0;
                        else
                            $cart->dis_per = $r->$dis_per;
                         $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                        
                        //Validation for duplicate items in cart
                        $v_dealer = Session::get('current_dealer');
                        $v_emp_id = Employee::where('login_id',Auth::user()->id)->value('id');
                        $v_branch = $r->dispatchBranch;
                        $v_classification = $category;
                        $v_sku = $skuId;
                         $dealers = Dealer::where('state_id',Session::get('state_id'))->get(['id','name']);
                         if(Cart::where('emp_id',$v_emp_id)->where('dealer_id',$v_dealer)->where('branch',$v_branch)->where('classification',$v_classification)->where('sku',$v_sku)->count() > 0 || $IsGstNotInOrder==1 || $item_rate==0)
                            return back()->with(['cartCount'=>$cartCount,'dealer_id'=>$order_dealer,'orderDate'=>$r->todayDate,'dispatchBranch'=>$r->dispatchBranch,'dealers'=>$dealers]);
                        else
                            $cart->save();
                        return back()->with(['cartCount'=>$cartCount,'dealer_id'=>$order_dealer,'orderDate'=>$r->todayDate,'dispatchBranch'=>$r->dispatchBranch,'dealers'=>$dealers,'classification'=>1]);
                         case 'checkoutCart' :
                             $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                                $query->select('name');
                            })->get();
                            $branches = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->distinct('branch')->pluck('branch');
                            $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
                            DB::commit();
                            return view('order.executive.cart')->with(compact('cart','branches','destination'))->with('dealer_id',Session::get('current_dealer'));
                }
        }
        else if($r->classification == 2)
        {
               // Log::info($r->orderAccSubmit);
                 switch($r->orderAccSubmit)
                {
                    case 'addToCart':
                        $partCount=Cart::where('classification',3)->where('dealer_id',$order_dealer)->count();
                         if($partCount>0){
                                return back()->with(['msg'=>'Parts And Other Machinery cannot be addeded in same order','classification'=>2]);
                         }
                         $category = $r->classification;
                         $sku = 'Sku'.$category;
                         $qty = 'qty'.$category;
                         $dis_per = 'disPerc'.$category;
                         $rate_type = 'priceLevel'.$category;
                         $subsidy_dept = 'subsidyDept'.$category;
                         $cart = new Cart();
                        $cart->order_date = $r->todayDate;
                        $cart->emp_id = Employee::where('login_id',Auth::user()->id)->value('id');
                        $cart->dealer_id = $order_dealer;
                        $cart->branch = $r->dispatchBranch;
                        $cart->rate_type = $r->$rate_type;
                        $cart->classification = $category;
                        if($r->$rate_type == 2)
                            $cart->subsidy_dept_id = SubsidyDept::where('code',$r->$subsidy_dept)->value('id');
                        $skuId = 0;
                        $gst = null;
                         switch ($category)
                        {
                            case '1':
                                $skuId = MachineSku::where('sku',$r->$sku)->value('id');
                                 switch ($cart->rate_type) {
                                    case '0':
                                        $item_rate = MachineSku::where('sku',$r->$sku)->value('dp') * Branch::where('id',$r->dispatchBranch)->value('rate');
                                        $gst = MachineSku::where('sku',$r->$sku)->value('gst');
                                        if(Session::get("gst") == null)
                                        {
                                            Session::put("gst",MachineSku::where('sku',$r->$sku)->value('gst'));
                                        }
                                        break;
                                    case '1':
                                        $item_rate = MachineSku::where('sku',$r->$sku)->value('mrp');
                                        break;
                                    case '2':
                                        $item_rate = MachineSubsidy::where('mch_id',$skuId)->where('subsidy_dept_id',SubsidyDept::where('code',$r->$subsidy_dept)->value('id'))->where('state_id',Dealer::where('id',$order_dealer)->value('state_id'))->value('subsidy_price');
                                        break;
                                    default:
                                        break;
                                }
                                break;
                             case '2':
                                $skuId = AccessoriesSku::where('sku',$r->$sku)->value('id');
                                switch ($cart->rate_type) {
                                    case '0':
                                        $item_rate = AccessoriesSku::where('sku',$r->$sku)->value('dp') * Branch::where('id',$r->dispatchBranch)->value('rate');
                                        $gst = AccessoriesSku::where('sku',$r->$sku)->value('gst');
                                        if(Session::get("gst") == null)
                                        {
                                            Session::put("gst",AccessoriesSku::where('sku',$r->$sku)->value('gst'));
                                        }
                                        break;
                                    case '1':
                                        $item_rate = AccessoriesSku::where('sku',$r->$sku)->value('mrp');
                                        break;
                                    case '2':
                                        $item_rate = AccSubsidy::where('mch_id',$skuId)->where('branch_id',$r->dispatchBranch)->value('subsidy_price');
                                        break;
                                    default:
                                        return null;
                                        break;
                                }
                                break;
                             default:
                                break;
                        }
                         $IsGstNotInOrder = 0;
                        if(Session::get("gst") != null)
                        {
                            if((Session::get("gst")>0 && $gst!=null && $gst==0) || ($gst>0 && Session::get("gst")==0))
                                $IsGstNotInOrder = 1;
                        }
                         $cart->sku = $skuId;
                        $cart->item_rate = $item_rate;
                        $cart->qty = $r->$qty;
                         if($r->$dis_per == null)
                            $cart->dis_per = 0;
                        else
                            $cart->dis_per = $r->$dis_per;
                         $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                        
                        //Validation for duplicate items in cart
                        $v_dealer = Session::get('current_dealer');
                        $v_emp_id = Employee::where('login_id',Auth::user()->id)->value('id');
                        $v_branch = $r->dispatchBranch;
                        $v_classification = $category;
                        $v_sku = $skuId;
                         Log::info("GST not in order - ".$IsGstNotInOrder);
                         $dealers = Dealer::where('state_id',Session::get('state_id'))->get(['id','name']);
                         if(Cart::where('emp_id',$v_emp_id)->where('dealer_id',$v_dealer)->where('branch',$v_branch)->where('classification',$v_classification)->where('sku',$v_sku)->count() > 0 || $IsGstNotInOrder==1 || $item_rate==0)
                            return back()->with(['cartCount'=>$cartCount,'dealer_id'=>$order_dealer,'orderDate'=>$r->todayDate,'dispatchBranch'=>$r->dispatchBranch,'dealers'=>$dealers]);
                        else
                            $cart->save();
                        return back()->with(['cartCount'=>$cartCount,'dealer_id'=>$order_dealer,'orderDate'=>$r->todayDate,'dispatchBranch'=>$r->dispatchBranch,'dealers'=>$dealers,'classification'=>2]);
                         case 'checkoutCart' :
                             $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                                $query->select('name');
                            })->get();
                            $branches = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->distinct('branch')->pluck('branch');
                            $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
                            DB::commit();
                            return view('order.executive.cart')->with(compact('cart','branches','destination'))->with('dealer_id',Session::get('current_dealer'));
                }
        }
        else if($r->classification == 3){
            switch($r->orderPartSubmit){
                case 'addToCart':
                    $mchAccCount=Cart::whereIn('classification',[1,2])->where('dealer_id',$order_dealer)->count();
                         if($mchAccCount>0){
                                return back()->with(['msg'=>'Parts And Other Machinery cannot be addeded in same order','classification'=>3]);
                         }
                    $category = $r->classification;
                    $sku = 'Sku'.$category;
                    $part_id = 'partSku3';
                    $qty = 'qty'.$category;
                    $dis_per = 'disPerc'.$category;
                    $rate_type = 'priceLevel'.$category;
                    $subsidy_dept = 'subsidyDept'.$category;
                    $cart = new Cart();
                    $cart->order_date = $r->todayDate;
                    $cart->emp_id = Employee::where('login_id',Auth::user()->id)->value('id');
                    $cart->dealer_id = $order_dealer;
                    $cart->branch = $r->dispatchBranch;
                    $cart->rate_type = $r->$rate_type;
                    $cart->qty = $r->$qty;
                    $cart->dis_per = $r->$dis_per;
                    $cart->classification = $category;
                    $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                    $skuObj=Part::where('id',$r->$part_id)->first();
                    $cart->sku=$skuObj->id;
                    if($cart->rate_type==0)
                        $cart->item_rate=$skuObj->dp;
                    else if($cart->rate_type==1)
                        $cart->item_rate=$skuObj->mrp;
                    $cartStatus=Cart::where('classification',3)->where('dealer_id',$order_dealer)->count();
                    $dealers = Dealer::where('state_id',Session::get('state_id'))->get(['id','name']);
                    //Check GST validation
                    if($skuObj->gst>0){
                        $isGstNotPresent=Cart::leftJoin('parts','parts.id','=','carts.sku')->where('classification',3)->where('gst','=',0)->first();
                        if($isGstNotPresent!=NULL)
                            return back()->with(['msg'=>'Tax exempted item cannot be added with taxable item','classification'=>3]);
                    }
                    else{
                        $isGstPresent=Cart::leftJoin('parts','parts.id','=','carts.sku')->where('classification',3)->where('gst','>',0)->first();
                        if($isGstPresent!=NULL)
                            return back()->with(['msg'=>'Tax exempted item cannot be added with taxable item','classification'=>3]);
                    }
                    $cart->save();
                    return back()->with(['cartCount'=>$cartCount,'dealer_id'=>$order_dealer,'orderDate'=>$r->todayDate,'dispatchBranch'=>$r->dispatchBranch,'dealers'=>$dealers,'classification'=>3]);

                    break;
                case 'checkoutCart':
                    $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                                $query->select('name');
                            })->get();
                            $branches = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->distinct('branch')->pluck('branch');
                            $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
                            DB::commit();
                            return view('order.executive.cart')->with(compact('cart','branches','destination'))->with('dealer_id',Session::get('current_dealer'));
                    break;
            }
        }
    }


    public function getSearchedParts(Request $req){
        $part=$req->part;
        $partArray=explode(' ', $part);
        $condition="";
        for ($i=0; $i < COUNT($partArray); $i++) { 
            $condition=$condition."sku LIKE '%$partArray[$i]%' AND ";
        }
        $condition=substr_replace($condition, "", -4);
        $query="";
        if(COUNT($partArray)>1)
            $query="SELECT id,sku,mrp,dp,0 as subsidyPrice,gst,0 as dept_id,'2' as type,nav_no FROM parts WHERE ($condition) AND status=1 LIMIT 50";
        else
            $query="SELECT id,sku,mrp,dp,0 as subsidyPrice,gst,0 as dept_id,'2' as type,nav_no FROM parts WHERE $condition or nav_no LIKE '%$part%' AND status=1 LIMIT 50";
        return DB::select($query);
    }



    public function updatePartsOrder(Request $req){
        $data=json_decode(base64_decode($req->order_data),TRUE);
        //Log::info($data[0]["tracking_no"]);
        $order=Order::where('tracking_no',$data[0]["tracking_no"])->first();
        //return $data;
        //Starting Database Transaction for concistency
        DB::beginTransaction();
        //Adding all controlls in try catch block 
        try{

            //If Order is edited update the dirty counter
            if($req->isOrderEdited==true)
                Order::where('tracking_no',$data[0]["tracking_no"])->update(['is_dirty'=>($order->is_dirty+1)]);

            //Since order detail is changed we need to delete current details
            OrderDetail::where("tracking_no",$order->tracking_no)->delete();
            
            //Now we have deleted all items so need to insert new update item list
            //Fetching first branch to update branch tracking_no
            $branch_id=$data[0]["branch"];
            $branch_code="A";
            for ($i=0; $i < COUNT($data); $i++) { 
                if($branch_id!=$data[$i]["branch"]){
                    $branch_code++;
                    $branch_id=$data[$i]["branch"];
                }
                $orderDetail=new OrderDetail();
                $orderDetail->order_id=$order->id;
                $orderDetail->nav_no=$data[$i]["nav_no"];
                $orderDetail->order_date=$order->created_at;
                $orderDetail->tracking_no=$order->tracking_no;
                $orderDetail->branch_tracking_no=$order->tracking_no."-".$branch_code;
                $orderDetail->classification="Parts";
                $orderDetail->grp="Part";
                $orderDetail->subgrp="Part";
                
                //Fetching individual item details for accurate data
                $part=Part::where("nav_no",$data[$i]["nav_no"])->first();
                
                $orderDetail->hsn=$part->hsn;
                $orderDetail->sku=$part->sku;
                $orderDetail->qty=$data[$i]["qty"];
                $orderDetail->orignal_qty=$data[$i]["original_qty"];
                $orderDetail->branch=$data[$i]["branch"];
                $orderDetail->rate_type=$data[$i]["rate_type"];
                $orderDetail->subsidy_dept_id=null;
                if($orderDetail->rate_type==0)
                    $orderDetail->item_rate=$part->dp;
                else if($orderDetail->rate_type==1)
                    $orderDetail->item_rate=$part->mrp;
                else if($orderDetail->rate_type==2)
                    $orderDetail->item_rate=$part->subsidy;
                $orderDetail->dis_per=$data[$i]["discPer"];
                $orderDetail->gst=$part->gst;
                $orderDetail->shipping_id=$data[$i]["shipping_id"];
                $orderDetail->other_ship=htmlspecialchars_decode($data[$i]["other_ship"]);
                $orderDetail->destination=$data[$i]["destination"];
                $orderDetail->freight=$data[$i]["freight"];
                $orderDetail->order_status=2;
                $orderDetail->save();
            }
            $response=array('status' => true,
                            'isPlaced'=>true,
                        'date'=>date('Y-m-d'));
        }
        catch(\Exeption $e){
            //There is some error occured while placing order and need rollback
            DB::rollback();
            $response=array('status' => false,
                            'isPlaced'=>false,
                        'date'=>date('Y-m-d'));
        }
        //Order is successfully added in database and now we can commit to finalize the changes.
        DB::commit();
        app('App\Http\Controllers\NotifController')->sendNotification($order->tracking_no);
        return $response;

    }


     public function ClearCart()
    {
        Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->delete();
        Session::forget('discountFlag');
        Session::forget('current_dealer');
        Session::forget('state_id');
        Session::forget('orderDate');
        Session::forget('dispatchBranch');
        Session::forget('branchRatePer');
        Session::forget('gst');
        return back();
    }
     public function Cart_AmendOrder()
    {
                    $dealer = Session::get('amend_dealer');
                    $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',$dealer)->whereHas('dealer', function ($query) {
                        $query->select('name');
                    })->get();
                    $branches = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',$dealer)->distinct('branch')->pluck('branch');
                    $destination = Dealer::where('id',$dealer)->get(['address1','address2','address3']);
                    return view('order.executive.cart')->with(compact('cart','branches','destination'));
    }
     public function IncompleteOrders()
    {
        $cartItems = Cart::where('emp_id',Auth::user()->employee->id)->select('dealer_id','order_date', DB::raw('count(dealer_id) as items'))->groupBy('dealer_id','order_date')->get();
        return view('order.executive.incomplete')->with(compact('cartItems'));
    }
      public function ViewCheckoutPage(Request $request)
    {
                    $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',$request->dealer_id)->whereHas('dealer', function ($query) {
                        $query->select('name');
                    })->get();
                    $branches = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',$request->dealer_id)->distinct('branch')->pluck('branch');
                    $destination = Dealer::where('id',$request->dealer_id)->get(['address1','address2','address3']);
                    return view('order.executive.cart')->with(compact('cart','branches','destination'));
    }

    public function GetShippingCompanies(Request $request)
    {
        switch ($request->shippingMode) {
             case 'Transport':
                $ShippingDetails = Shipping::where('name',$request->shippingMode)->where('branch_id',$request->dispatchBranch)->orderBy('agency')->get(['id','agency']);
                break;
             case 'Courier':
                $ShippingDetails = Shipping::where('name',$request->shippingMode)->where('branch_id',$request->dispatchBranch)->orderBy('agency')->get(['id','agency']);
                break;

             case 'Other':
                 $ShippingDetails = Shipping::where('name',$request->shippingMode)->where('branch_id',$request->dispatchBranch)->orderBy('agency')->get(['id','agency']);
                break;
             default:
                $ShippingDetails = Shipping::where('name',$request->shippingMode)->orderBy('agency')->get(['id','agency']);
        }
        if($request->tracking_no)
        {
            $tracking_no = decrypt($request->tracking_no);
            OrderDetail::where('tracking_no',$tracking_no)->where('branch',$request->dispatchBranch)->update(['shipping_id'=>$ShippingDetails[0]['id']]);
        }
        return $ShippingDetails;
    }

     public function UpdateOtherShip(Request $request)
    {
        Log::info($request->otherShipping);
        $tracking_no = decrypt($request->tracking_no);
        OrderDetail::where('tracking_no',$tracking_no)->where('branch',$request->branch)->update(['other_ship'=>$request->otherShipping]);
    }

    public function SaveShippingData(Request $request)
    {
        $tracking_no = decrypt($request->tracking_no);
        OrderDetail::where('tracking_no',$tracking_no)->where('branch',$request->dispatchBranch)->update(['shipping_id'=>$request->shipping]);
    }

    public function GetDealerConfirmation(Request $request)
    {
        $tracking_no = decrypt($request->tracking_no);
        $status = OrderDetail::where('tracking_no',$tracking_no)->where('order_status',1)->update(['order_status'=>8]);
        Log::info("Order Update Status of - ".$tracking_no." with ".$status);
        if($status)
        {
            Order::where('tracking_no',$tracking_no)->update(['emp_id'=>Auth::user()->employee->id]);
            //App notification for new order placed
            $data= array('tracking_no' =>$tracking_no,
                             'message'=>'Get order confirmation by dealer with tracking no '.$tracking_no,
                             'date'=>Carbon::now()->format('d-m-Y g:i a'),
                             'fileurl'=>'',
                             'name'=>Auth::user()->name,
                             'status'=>'8',
                             'messagetype'=>8);
            $notification = 'Get order confirmation by dealer with tracking no '.$tracking_no;
            
            $notify = new Notify();
            $order = Order::where('tracking_no',$tracking_no)->get();
            $notify->dealer_id = $order[0]->dealer_id;
            $notify->order_id = $order[0]->id;
            $notify->message = $notification;
            $notify->save();

            $fcm_tokens = DB::table('dealer_logins')->where('id',Dealer::where('id',$order[0]->dealer_id)->value('login_id'))->pluck('fcm_token');
            $data['messagetype']=9;

            $this->notifyManagement($data);
            $this->notifyUser($fcm_tokens,$data);
            return redirect()->route('order.dealerOrderList');
        }
        else
        {
            Log::info("Redirecting without Update");
            return redirect()->route('order.dealerPlaced',array('tracking_no'=>encrypt($tracking_no)));
        }
    }

    public function RemoveItemFromCart(Request $req)
    {
         Cart::where('id',$req->rowId)->delete();
    }

    public function FilterAllByPurchaseNumber()
    {
        $search_query = $_GET['queryFilter'];
        $orders = Order::with(['dealer','placedBy'])->where('tracking_no','LIKE','%' . $search_query . '%')->paginate(5);
        return $orders;
    }

    public function FilterPurchaseNumber()
    {
        $search_query = $_GET['queryFilter'];
        $orders = Order::with(['dealer'])->where('tracking_no','LIKE','%' . $search_query . '%')->where('emp_id',Auth::user()->employee->id)->paginate(5);
        return $orders;
    }

    public function GetAccPriceList()
    {
        $branch_id= Session::get('acc_branch_id');
        $grp= Session::get('acc_grp_name');
        $subgrp= Session::get('acc_subgrp_name');
         if($grp=="All")
            $grp='';
         if($subgrp=="All")
            $subgrp='';
         $items = AccessoriesSku::with('subsidy_rate')->where('grp','LIKE','%'.$grp.'%')->where('subgrp','LIKE','%'.$subgrp.'%')->paginate(10);
        $branches = Branch::all(['name','id','rate']);
        $groups = AccessoriesSku::distinct('grp')->pluck('grp')->toArray();
         array_unshift($groups , 'All');
         if($grp!=''){
            $subgrp = AccessoriesSku::distinct('subgrp')->where('grp','LIKE','%'.$grp.'%')->pluck('subgrp')->toArray();
            array_unshift($subgrp , 'All');
        }
        else
            $subgrp=[];
        return view('order.executive.acc_price_list')->with(compact('items','branches','groups','subgrp'))->with('sel_branch',Branch::where('id',$branch_id)->value('rate'));
    }

     public function FilterAllByDealer()
     {
         $search_query = $_GET['queryFilter'];
         $orders = Order::with(['dealer','placedBy'])->whereHas('dealer', function ($query)  use($search_query) {
                     $query->where('name', 'LIKE', '%' . $search_query . '%');
                 })->paginate(5);
         return $orders;
     }

     public function FilterByDealer()
     {
         $search_query = $_GET['queryFilter'];
         $orders = Order::with(['dealer'])->whereHas('dealer', function ($query)  use($search_query) {
                     $query->where('name', 'LIKE', '%' . $search_query . '%');
                 })->where('emp_id',Auth::user()->employee->id)->paginate(5);
         return $orders;
     }
       public function FilterByGrpName()
     {
         $search_query = $_GET['queryFilter'];
         switch ($_GET['classification']) {
             case '1':
                 $items = MachineSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('grp','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             case '2':
                 $items = AccessoriesSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('grp','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             default:
                 break;
         }
         return array('items'=>$items,'sel_branch'=>Branch::where('id',Session::get('mch_price_list_branch'))->value('rate'));
     }
       public function FilterBySubgrpName()
     {
         $search_query = $_GET['queryFilter'];
         switch ($_GET['classification']) {
             case '1':
                 $items = MachineSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('subgrp','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             case '2':
                 $items = AccessoriesSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('subgrp','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             default:
                 # code...
                 break;
         }
         return array('items'=>$items,'sel_branch'=>Branch::where('id',Session::get('mch_price_list_branch'))->value('rate'));
     }
       public function FilterBySkuName()
     {
         $search_query = $_GET['queryFilter'];
         switch ($_GET['classification']) {
             case '1':
                 $items = MachineSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('sku','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             case '2':
                 $items = AccessoriesSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('sku','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             default:
                 # code...
                 break;
         }
         return array('items'=>$items,'sel_branch'=>Branch::where('id',Session::get('mch_price_list_branch'))->value('rate'));
     }
       public function FilterByHsn()
    {
         $search_query = $_GET['queryFilter'];
         switch ($_GET['classification']) {
             case '1':
                 $items = MachineSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('hsn','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             case '2':
                 $items = AccessoriesSku::select(['grp','subgrp','sku','dp','mrp','hsn','gst'])->where('hsn','LIKE','%'.$search_query.'%')->paginate(10);
                 break;
             default:
                 break;
         }
         return array('items'=>$items,'sel_branch'=>Branch::where('id',Session::get('mch_price_list_branch'))->value('rate'));
     }
       public function FilterByOrderStatus()
    {
        $search_query = $_GET['queryFilter'];
        $orders = Order::with(['dealer'])->where('order_status',$search_query)->where('emp_id',Auth::user()->employee->id)->paginate(5);
        return $orders;
    }
        public function AmendOrder(Request $request)
    {
         $orderDetail = OrderDetail::where('tracking_no',decrypt($request->tracking_no))->get();
         foreach ($orderDetail as $key => $value) {
             $cart = new Cart();
             switch ($value->classification) {
                 case 'Machinery':
                     $cart->order_date = \Carbon\Carbon::now();
                     $cart->emp_id = Auth::user()->employee->id;
                     $cart->dealer_id = $value->order->dealer_id;
                     $cart->branch = $value->branch;
                     $cart->classification = 1;
                     $cart->sku = MachineSku::where('sku',$value->sku)->value('id');
                     $cart->qty = $value->qty;
                     $cart->dis_per = $value->dis_per;
                     $cart->rate_type = $value->rate_type;
                     $cart->item_rate = $value->item_rate;
                     $cart->save();
                     break;
                   case 'Accessory':
                     $cart->order_date = \Carbon\Carbon::now();
                     $cart->emp_id = Auth::user()->employee->id;
                     $cart->dealer_id = $value->order->dealer_id;
                     $cart->branch = $value->branch;
                     $cart->classification = 1;
                     $cart->sku = MachineSku::where('sku',$value->sku)->value('id');
                     $cart->qty = $value->qty;
                     $cart->dis_per = $value->dis_per;
                     $cart->rate_type = $value->rate_type;
                     $cart->item_rate = $value->item_rate;
                     $cart->save();
                     break;
                   default:
                     # code...
                     break;
             }
         }
                     Session::put('current_dealer',$orderDetail[0]->order->dealer_id);
                     Session::put('amend_dealer',$orderDetail[0]->order->dealer_id);
                     $today = \Carbon\Carbon::today()->toDateTimeString();
                     Session::put('orderDate',$today);
                    DB::commit();
                    return redirect()->route('order.cart_amend');
       }
     public function ConfirmOrder(Request $req)
    {
         //Create New Order
        //Starting the transaction
          $cartList = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',$req->dealer)->orderBy('branch','ASC')->orderBy('rate_type','ASC')->pluck('id');
          Log::info($cartList);
         if($cartList->count() == 0)
        {
            $dealers = Dealer::where('status',1)->whereIn('district_id',Auth::user()->employee->districts->pluck('district_id'))->orderBy('name', 'asc')->get(['id','name']);
            $todayDate = date('Y-m-d');
            $branches = Branch::where('status',1)->get(['id','name']);
            $states = State::get(['id','name']);
            if(Session::has('current_dealer'))
            {   
                $cartCount = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->count();
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches','cartCount','states']));
            }
            else
            {
                return view('order.executive.create')->with(compact(['dealers','todayDate','branches','states']));
            }
        }
         $tracking_no = self::GenerateTrackingNo();
         //Save Order in orders table
        $newOrder = new Order();
        $newOrder->tracking_no = $tracking_no;
        $newOrder->emp_id = Auth::user()->employee->id;
        $newOrder->dealer_id = $req->dealer;
        $newOrder->save();
         $branchTrackingNo = array();
        $char = 'A';
        $rateType = 'rateType-'.$cartList[0];
        $type=$req->$rateType;
         foreach ($cartList as $cart) {
             $classification = 'category-'.$cart;
             $sku = 'sku-'.$cart;
             $branch = 'branchId-'.$cart;
             $qty = 'qty-'.$cart;
             $disPer = 'disPer-'.$cart;
             $spl_disc = 'spl_disc-'.$cart;
             $rateType = 'rateType-'.$cart;
             $subsidy_dept_id = 'subsidyDept-'.$cart;
             $shipping_id = 'shippingCompany_'.$req->$branch;
             $otherShip='otherShip_'.$req->$branch;
             $destination = 'destination_'.$req->$branch;
             $new_destination = 'new_dest_'.$req->$branch;
             if($req->$new_destination != null)
                $destination = $new_destination;
             $freight = 'freight_'.$req->$branch;
             if($type!=$req->$rateType){
                $char++;
                $type=$req->$rateType;
                $branchTrackingNo_Code = $char;
                $branchTrackingNo[$req->$branch] = $branchTrackingNo_Code;
             }

             if($branchTrackingNo)
            {
                if (array_key_exists($req->$branch,$branchTrackingNo))
                {
                    $branchTrackingNo_Code = $branchTrackingNo[$req->$branch];
                }
                else
                {
                    $char++;
                    $branchTrackingNo_Code = $char;
                }
            }
            else
            {
                $branchTrackingNo_Code = 'A';
            }
             $branchTrackingNo[$req->$branch] = $branchTrackingNo_Code;
             switch ($req->$classification) {
                case '1':
                    $itemInfo = MachineSku::where('sku',$req->$sku)->get();
                    break;
                 case '2':
                    $itemInfo = AccessoriesSku::where('sku',$req->$sku)->get();
                    break;
                 case '3':
                    $itemInfo = Part::where('sku',$req->$sku)->get();
                    break;
                 default:
                    break;
            }
               // Save Order Details in order_details table
             $orderDetail = new OrderDetail();
             $orderDetail->order_date = Session::get('orderDate');
             $orderDetail->order_id = $newOrder->id;
             $orderDetail->tracking_no = $tracking_no;
             $orderDetail->nav_no=$itemInfo[0]->nav_no;
             switch ($req->$classification) {
                 case '1':
                    $Category = 'Machinery';
                    break;
                 case '2':
                    $Category = 'Accessories';
                    break;
                 case '3':
                    $Category = 'Parts';
                    break;
                 default:
                    break;
             }
             $orderDetail->classification = $Category;
            $orderDetail->branch = $req->$branch;
            $orderDetail->branch_tracking_no = $tracking_no."-".$branchTrackingNo_Code;
            if($req->$classification==3){
                $orderDetail->grp = "Part";
                $orderDetail->subgrp = "Part";
            }
            else{
                $orderDetail->grp = $itemInfo[0]->grp;
                $orderDetail->subgrp = $itemInfo[0]->subgrp;
            }
            $orderDetail->hsn = $itemInfo[0]->hsn;
            $orderDetail->sku = $itemInfo[0]->sku;
            $orderDetail->qty = $req->$qty;
            $orderDetail->spl_disc = $req->$spl_disc;
            $orderDetail->rate_type = $req->$rateType;
            $orderDetail->bulk_qty = $itemInfo[0]->bulk_qty;
             switch ($req->$rateType) {
                 case '0':
                    $orderDetail->item_rate = Branch::where('id',$req->$branch)->value('rate') * $itemInfo[0]->dp;
                    break;
                 case '1':
                    $orderDetail->item_rate = $itemInfo[0]->mrp;
                    break;
                 case '2':
                     switch ($req->$classification) {
                         case '1':
                            $orderDetail->item_rate = MachineSubsidy::where('mch_id',$itemInfo[0]->id)->where('state_id',Dealer::where('id',$req->dealer)->value('state_id'))->where('subsidy_dept_id',$req->$subsidy_dept_id)->value('subsidy_price');
                            $orderDetail->item_rate = $orderDetail->item_rate - $orderDetail->item_rate * 5/100;
                            $orderDetail->subsidy_dept_id = $req->$subsidy_dept_id;
                            break;
                         case '2':
                            $orderDetail->item_rate = AccSubsidy::where('acc_id',$itemInfo[0]->id)->where('state_id',Dealer::where('id',$req->dealer)->value('state_id'))->where('subsidy_dept_id',$req->$subsidy_dept_id)->value('subsidy_price');
                            $orderDetail->item_rate = $orderDetail->item_rate - $orderDetail->item_rate * 5/100;
                            $orderDetail->subsidy_dept_id = $req->$subsidy_dept_id;
                            break;
                         default:
                            break;
                    }
                    break;
                 default:
                    break;
            }
             $orderDetail->gst = $itemInfo[0]->gst;
            $orderDetail->dis_per = $req->$disPer;
            if($req->$classification==3)
                $orderDetail->order_status = 9;
            else
                $orderDetail->order_status = 2;
            $orderDetail->other_ship=$req->$otherShip;
             if(Auth::user()->employee->role_id != 2)
            {
                $orderDetail->shipping_id = $req->$shipping_id;
                $orderDetail->destination = $req->$destination;
                $orderDetail->freight = $req->$freight;
            }
             if($orderDetail->save())
            {
                if(!($orderDetail->order->dealer->address2 == $orderDetail->destination || $orderDetail->order->dealer->address1 == $orderDetail->destination || $orderDetail->order->dealer->address3 == $orderDetail->destination))
                {       
                        Log::info("Destination changed");
                        $this->sendNotification($tracking_no);
                }
                else
                {
                        Log::info("Destination not changed");
                }
                Cart::where('id',$cart)->delete();
            }
        }
         Session::forget('discountFlag');
        session::forget('current_dealer');
        session::forget('state_id');
        Session::forget('orderDate');
         //App notification for new order placed

         $notification = 'New order is placed by '.Auth::user()->employee->name.' with tracking no '.$tracking_no;
         $notify = new Notify();
        $order = Order::where('tracking_no',$tracking_no)->get();
        $notify->emp_id = $order[0]->emp_id;
        $notify->dealer_id = $order[0]->dealer_id;
        $notify->order_id = $order[0]->id;
        $notify->message = $notification;
        $notify->save();

        $data= array('tracking_no' =>$tracking_no,
                         'message'=>'New order is placed by '.Auth::user()->employee->name.' with tracking no '.$tracking_no,
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'fileurl'=>'',
                         'name'=>Auth::user()->employee->name,
                         'status'=>'8',
                         'messagetype'=>8);

         //$this->notifyManagement($data);
         $this->notifyPaymentApprover($data);
        app('App\Http\Controllers\NotifController')->sendNotification($tracking_no);

        return redirect()->route('order.success')->with(['result'=>'Your order has been sent for payment approval']);
    }
     function notifyPaymentApprover($data){
           $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = array (
                        'to' => '/topics/Payment_Approver',
                        'data' => $data
                );
                $fields = json_encode ( $fields );
                $headers = array (
                        'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                        'Content-Type: application/json'
                );
                $ch = curl_init ();
                curl_setopt ( $ch, CURLOPT_URL, $url );
                curl_setopt ( $ch, CURLOPT_POST, true );
                curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                $result = curl_exec ( $ch );
                curl_close ( $ch );
    }
     // public function sendNotificationToMgmt()
     // {
     //     $deviceTokens = array();
     //     $fcm_tokens = Email::whereIn('id',Employee::where('role_id',1)->get(['email_id']))->get(['fcm_token']);
     //     for($i=0; $i<count($fcm_tokens); $i++)
     //     {
     //         $url = 'https://fcm.googleapis.com/fcm/send';
     //         $fields = array (
     //                 'registration_ids' => array($fcm_tokens[$i]->fcm_token)
     //                 ,
     //                 'data' => array (
     //                         "message" => "askjaslkdfjalsdjflka"
     //                 )
     //         );
     //         $fields = json_encode ( $fields );
     //         $headers = array (
     //                 'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
     //                 'Content-Type: application/json'
     //         );
     //         $ch = curl_init ();
     //         curl_setopt ( $ch, CURLOPT_URL, $url );
     //         curl_setopt ( $ch, CURLOPT_POST, true );
     //         curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
     //         curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
     //         curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
     //         $result = curl_exec ( $ch );
     //        // echo $result;
     //         curl_close ( $ch );
     //     }
     // }
       public function ChangeOrderStatus(Request $r)
    {
        $trackingNo = decrypt($r->tracking_no);
        
        //Executive Routes
        if($r->orderStatus == 2 && in_array(Auth::user()->employee->role_id, [3,4,8,9,10,11,19] ))
        {
            $orderDetail=OrderDetail::where('tracking_no',$trackingNo)->first();
            if($orderDetail->classification="Parts"){
                Order::where('tracking_no',$trackingNo)->update([
                'updated_at'=>\Carbon\Carbon::now()
                ]);
                OrderDetail::where('tracking_no',$trackingNo)->update(['order_status'=>$r->orderStatus]);
                app('App\Http\Controllers\NotifController')->sendNotification($trackingNo);
            }
            else{
                Order::where('tracking_no',$trackingNo)->update(['emp_id'=>Auth::user()->employee->id,'updated_at'=>\Carbon\Carbon::now()]);
                $branches = OrderDetail::groupBy('branch')->pluck('branch');
                foreach ($branches as $branch) 
                {
                    $shippingId = 'shippingCompany_'.$branch;
                    $freight = 'freight_'.$branch;
                    $destination = 'destination_'.$branch;
                    OrderDetail::where('tracking_no',$trackingNo)->where('branch',$branch)->update(['shipping_id' => $r->$shippingId, 'destination' => $r->$destination, 'freight' => $r->$freight, 'order_status'=>$r->orderStatus ]);
                }
            }
        }

        //Payment Routes
        if($r->orderStatus == 3 && in_array(Auth::user()->employee->role_id, [5] ))
        {
            $order = Order::where('tracking_no',$trackingNo)->first();
            
            if(!$order)
                return redirect()->route('order.exec_placed')->with(["result"=>"The Order has been deleted by the executive",'color'=>'alert-danger']);
             $emp=Employee::where('id',$order->emp_id)->first();
             //Database Trasaction
            DB::beginTransaction();
            try{
                $update = Order::where('tracking_no',$trackingNo)->update([
                    'p_balance' => $r->balance, 'p_mode'=> $r->p_mode, 'updated_at'=>\Carbon\Carbon::now()
                ]);
                if($update)
                {
                    $update_inner = OrderDetail::where('tracking_no',$trackingNo)->update(['order_status'=>$r->orderStatus]);
                    if($update_inner == 0)
                    {
                        DB::rollback();
                        return redirect()->route('home');
                    }
                }
                else
                {
                    return redirect()->route('home');
                }
            }
            catch(\Exception $e)
            {
                DB::rollback();                
            }
            DB::commit();

            app('App\Http\Controllers\NotifController')->sendNotification($trackingNo);
             return redirect()->route('order.exec_placed')->with(["result"=>'Payment is successfully approved for the order - '.$trackingNo,'color'=>'alert-success']);
        }

        //Approved by management
        if($r->orderStatus == 4 && in_array(Auth::user()->employee->role_id, [4] ) && OrderDetail::where('tracking_no',$trackingNo)->value('order_status')==3)
        {
            $order_detail = OrderDetail::where('tracking_no',$trackingNo)->distinct('branch_tracking_no')->get(['branch_tracking_no']);
            Order::where('tracking_no',$trackingNo)->update([
                'updated_at'=>\Carbon\Carbon::now()
            ]);
            $order = Order::where('tracking_no',$trackingNo)->first();
            $emp=Employee::where('id',$order->emp_id)->first();
             OrderDetail::where('tracking_no',$trackingNo)->update(['order_status' => $r->orderStatus, 'mgmt_id' => Auth::user()->employee['id']]);
            Order::where('tracking_no',$trackingNo)->update([
                'total_amt' => $r->final_amt
            ]);
            OrderDetail::where('tracking_no',$trackingNo)->update(['order_status'=>$r->orderStatus]);
            app('App\Http\Controllers\NotifController')->sendNotification($trackingNo);
         }
        if($r->orderStatus == 7 && in_array(Auth::user()->employee->role_id, [4,19,20]) && (OrderDetail::where('tracking_no',$trackingNo)->value('order_status')==3 || OrderDetail::where('tracking_no',$trackingNo)->value('order_status')==9 || OrderDetail::where('tracking_no',$trackingNo)->value('order_status')==4 ))
        {
            Order::where('tracking_no',$trackingNo)->update([
                'updated_at'=>\Carbon\Carbon::now()
            ]);
            $order_detail = OrderDetail::where('tracking_no',$trackingNo)->distinct('branch_tracking_no')->get(['branch_tracking_no']);
            $order = Order::where('tracking_no',$trackingNo)->first();
            OrderDetail::where('tracking_no',$trackingNo)->update(['order_status'=>$r->orderStatus]);
            app('App\Http\Controllers\NotifController')->sendNotification($trackingNo);
        }
        if(!($r->orderStatus == 2 || $r->orderStatus == 3 || $r->orderStatus == 4 || $r->orderStatus == 7))
            OrderDetail::where('tracking_no',$trackingNo)->update(['order_status'=>$r->orderStatus]);
        
        $this->sendNotificationToExecutive($trackingNo, $r->orderStatus);
         if(Auth::user()->employee->role_id == 5)
            return redirect()->route('order.exec_placed');
        else if(Auth::user()->employee->role_id == 4 || Auth::user()->employee->role_id == 14)
                return redirect()->route('order.pay_verified');
        else if(Auth::user()->employee->role_id == 20 || Auth::user()->employee->role_id == 6){
                return redirect('/MgmtApprovedOrders');
        }
        else
                return redirect()->route('home');
    }

     public function sendNotificationToMgmt($notification,$tracking_no)
    {
        $fcm_tokens = Email::whereIn('id',Employee::Where('role_id',4)->get(['email_id']))->get(['fcm_token']);
         $notify = new Notify();
        $order_detail =OrderDetail::where('branch_tracking_no',$tracking_no)->get();
        $notify->emp_id = $order_detail[0]->order->emp_id;
        $notify->dealer_id = $order_detail[0]->order->dealer_id;
        $notify->order_id = $order_detail[0]->order->id;
        $notify->message = $notification;
        $notify->save();
         for($i=0; $i<count($fcm_tokens); $i++)
        {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array (
                    'registration_ids' => array($fcm_tokens[$i]->fcm_token)
                    ,
                    'data' => array (
                            "message" => $notification
                    )
            );
            $fields = json_encode ( $fields );
            $headers = array (
                    'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                    'Content-Type: application/json'
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $url );
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
        }
    }
     public function sendNotification($tracking_no)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
        $messaging = $firebase->getMessaging();
             $notification = "Order with ".$tracking_no." - Destination changed";
         $notify = new Notify();
        $order = Order::where('tracking_no',$tracking_no)->get();
        $notify->emp_id = $order[0]->emp_id;
        $notify->dealer_id = $order[0]->dealer_id;
        $notify->order_id = $order[0]->id;
        $notify->message = $notification;
        $notify->save();
         $data= array('tracking_no' =>$tracking_no,
                         'message'=>$notification,
                         'date'=>Carbon::now()->format('d-m-Y g:i a'),
                         'fileurl'=>'',
                         'name'=>Auth::user()->employee->name,
                         'status'=>'8',
                         'messagetype'=>"8");
         $condition = "'Management' in topics";
        $message = CloudMessage::withTarget('condition', $condition)
            ->withData($data)
        ;
        $messaging->send($message);
         $condition = "'Payment_Approver' in topics";
        $message = CloudMessage::withTarget('condition', $condition)
            ->withData($data)
        ;
        $messaging->send($message);        
    }
     public function sendNotificationToExecutive($tracking_no, $orderStatus)
    {
         $deviceTokens = array();
         $notify = new Notify();
         switch ($orderStatus) {
             case 2:
                $message = "Placed by Sales Executive";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',$tracking_no)->get();
                break;
             case 3:
                $message = "Payment Verified";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',$tracking_no)->get();
                break;
             case 4:
                $message = "Management Approved";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',$tracking_no)->get();
                break;
             case 5:
                $message = "Invoice Raised";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',OrderDetail::where('branch_tracking_no',$tracking_no)->value('tracking_no'))->get();
                $notify->split_id = OrderDetail::where('branch_tracking_no',$tracking_no)->value('id');
                break;
             case 6:
                $message = "Order Dispatched";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',OrderDetail::where('branch_tracking_no',$tracking_no)->value('tracking_no'))->get();
                $notify->split_id = OrderDetail::where('branch_tracking_no',$tracking_no)->value('id');
                break;
             case 7:
                $message = "Order Rejected";
                $fcm_tokens = Email::whereIn('id',Employee::where('id', Order::where('tracking_no',$tracking_no)->value('emp_id'))->orWhere('role_id',4)->orWhere('role_id',5)->get(['email_id']))->get(['fcm_token']);
                $order = Order::where('tracking_no',$tracking_no)->get();
                break;
             default:
                $message = "Pending";
                break;
         }
         $notification = "Tracking ".$tracking_no." - ".$message;
        $notify->emp_id = $order[0]->emp_id;
        $notify->dealer_id = $order[0]->dealer_id;
        $notify->order_id = $order[0]->id;
        $notify->message = $notification;
        $notify->save();
         for($i=0; $i<count($fcm_tokens); $i++)
        {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array (
                    'registration_ids' => array($fcm_tokens[$i]->fcm_token)
                    ,
                    'data' => array (
                            "message" => $notification
                    )
            );
            $fields = json_encode ( $fields );
            $headers = array (
                    'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                    'Content-Type: application/json'
            );
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $url );
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
            $result = curl_exec ( $ch );
            curl_close ( $ch );
        }
    }
     public function FetchMchPriceList()
    {
        $branch_id = $_GET['branch_id'];
        $grp = $_GET['grp'];
        $subgrp = $_GET['subgrp'];
        Session::put('mch_branch_id',$branch_id);
        Session::put('mch_grp_name',$grp);
        Session::put('mch_subgrp_name',$subgrp);
        return array();
    }
     public function FetchAccPriceList()
    {
        $branch_id = $_GET['branch_id'];
        $grp = $_GET['grp'];
        $subgrp = $_GET['subgrp'];
        Session::put('acc_branch_id',$branch_id);
        Session::put('acc_grp_name',$grp);
        Session::put('acc_subgrp_name',$subgrp);
        return array();
    }
     public function ShowSuccessPage()
    {
        if(Auth::user()->employee->role_id == 2)
        {
            return view('order.dealer.dealer_order_placed');
        }
        else
        {
            return view('order.general.success');
        }
    }
     public function FetchPriceRateByBranch(Request $request)
    {
        $priceRate = Branch::where('id',$request->branchId)->value('rate');
        Session::put('branchRatePer',$priceRate);
        return $priceRate;
    }

    public function ChangeItemQtyByExec(Request $r)
    {
        $order = OrderDetail::find($r->orderId);
        $order->qty = $r->qty;
        $order->save();

        $finalRate_NC = 0;

        switch ($order->rate_type) {
             
             case '0':
                $rate_type = "Dealer Price";
                $price = $order->item_rate;
                $price = $price - $order->dis_per/100 * $price;
                $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                $finalPrice = number_format((float)($price * $order->qty), 2, '.', '');
                $finalRate_NC = ($finalPrice*$order->gst/100) + $finalPrice ;
                break;
             
             case '1':
                $rate_type = "MRP";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                $finalRate_NC = $finalPrice;
                break;
             
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                $finalRate_NC = $finalPrice;
                break;
             
             default:
                break;
        }
         
         $totalPrice = 0;
         $order_detail = OrderDetail::where('branch_tracking_no',$order->branch_tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                 $rate_type = "Dealer Price";
                 $price = $o->item_rate;
                 $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                 $finalRate = number_format((float)($rate * $o->qty), 2, '.', '');
                 break;
             case '1':
                 $rate_type = "MRP";
                 $price = $o->item_rate;
                 $dis_rate = $o->item_rate;
                 $finalRate = $o->item_rate * $o->qty;
                 break;
             case '2':
                 $rate_type = "Subsidy";
                 $price = $order->item_rate;
                 $dis_rate = $order->item_rate;
                 $finalRate = $order->item_rate * $order->qty;
                 break;
             default:
                 break;
             }
             $totalPrice += $finalRate; 
         }
           $T_Amount = 0;
         $order_detail = OrderDetail::where('tracking_no',$order->tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                $rate_type = "Dealer Price";
                $price = $o->item_rate;
                $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                $amount = number_format((float)($rate * $o->qty), 2, '.', '');
                break;
             case '1':
                $rate_type = "MRP";
                $price = $o->item_rate;
                $dis_rate = $o->item_rate;
                $amount = $o->item_rate * $o->qty;
                break;
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $amount = $order->item_rate * $order->qty;
                break;
             default:
                break;
             }
            $T_Amount += $amount; 
        }
         $T_Freight = 0;
         $freight_detail = OrderDetail::where('tracking_no',$order->tracking_no)->groupBy('branch_tracking_no')->get();
         foreach ($freight_detail as $o) {
                 $freight = $o->freight;
                 $T_Freight += $freight; 
         }
         $tax = 0;
        $taxableAmt = 0;
        $orders_table_id = $order_detail[0]->order_id;
        $branch = $order->branch;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branch' Group by branch");
            $branchTotal = collect($branchTotal);
            foreach ($branchTotal as $value) {
                $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branch and order_id = $orders_table_id) total_rate) a) a";
                Log::info("TAXABLE QUERY _ ".$query);
                $data=DB::select($query);
                foreach ($data as $item) {
                    $tax+=$item->totalTax;
                    $taxableAmt += $item->taxable;
                }
            }
        Log::info("Taxable Amt - ".$taxableAmt);

        $purchaseList = OrderDetail::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->pluck('id');
        
        $finalAmount = 0;
        $branches = OrderDetail::where('order_id',$orders_table_id)->distinct('branch')->get(['branch']);

        foreach ($branches as $branche) {
            $tax_total = 0;
            $taxable_total = 0;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,IFNULL(freight,0) as freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch");
            
            $query = "SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,IFNULL(freight,0) FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch";

            $branchTotal = collect($branchTotal);
            $freight = 0;
            foreach ($branchTotal as $value) {
                    $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM((((IFNULL($value->freight,0)/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branche->branch and order_id = $orders_table_id) total_rate) a) a";
                    Log::info("Query - ".json_encode($value));
                    $data=DB::select($query);
                    foreach ($data as $item) {
                        $tax_total+=$item->totalTax;
                        $taxable_total += $item->taxable;
                    }
                    $freight = $value->freight;
            }
            $finalAmount += $tax_total + $taxable_total + $freight;
        }
        
        Log::info("FINAL AMT - ".$finalAmount);
        Log::info("taxableAmt AMT - ".$taxableAmt." - order - ".$order->freight." - Tax - ".$tax);

        $item = array(
                'orderId'   => $order->id,
                'price'     => $price,
                'finalPrice'=> $finalRate_NC,
                'purchaseList' => $purchaseList,
                'branch'    => $order->branch,
                'totalPrice'=> $taxableAmt+$order->freight+$tax,
                'tfinal'    => $finalAmount
                );


        $ordr=Order::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->first();
        Order::where('tracking_no',$ordr->tracking_no)->update(['is_dirty'=>$ordr->is_dirty+1]);

        return $item;
    }

    public function ChangeBranchFreight(Request $r)
    {
        OrderDetail::where('tracking_no',decrypt($r->tracking_no))->where('branch',$r->branch)->update(['freight'=>$r->freight]);
        
        $order = OrderDetail::where('tracking_no',decrypt($r->tracking_no))->where('branch',$r->branch)->first();

        switch ($order->rate_type) {
             
             case '0':
                $rate_type = "Dealer Price";
                $price = $order->item_rate;
                $price = $price - $order->dis_per/100 * $price;
                $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                $finalPrice = number_format((float)($price * $order->qty), 2, '.', '');
                break;
             
             case '1':
                $rate_type = "MRP";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             
             default:
                break;
        }
         
         $totalPrice = 0;
         $order_detail = OrderDetail::where('branch_tracking_no',$order->branch_tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                 $rate_type = "Dealer Price";
                 $price = $o->item_rate;
                 $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                 $finalRate = number_format((float)($rate * $o->qty), 2, '.', '');
                 break;
             case '1':
                 $rate_type = "MRP";
                 $price = $o->item_rate;
                 $dis_rate = $o->item_rate;
                 $finalRate = $o->item_rate * $o->qty;
                 break;
             case '2':
                 $rate_type = "Subsidy";
                 $price = $order->item_rate;
                 $dis_rate = $order->item_rate;
                 $finalRate = $order->item_rate * $order->qty;
                 break;
             default:
                 break;
             }
             $totalPrice += $finalRate; 
         }
           $T_Amount = 0;
         $order_detail = OrderDetail::where('tracking_no',$order->tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                $rate_type = "Dealer Price";
                $price = $o->item_rate;
                $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                $amount = number_format((float)($rate * $o->qty), 2, '.', '');
                break;
             case '1':
                $rate_type = "MRP";
                $price = $o->item_rate;
                $dis_rate = $o->item_rate;
                $amount = $o->item_rate * $o->qty;
                break;
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $amount = $order->item_rate * $order->qty;
                break;
             default:
                break;
             }
            $T_Amount += $amount; 
        }
         $T_Freight = 0;
         $freight_detail = OrderDetail::where('tracking_no',$order->tracking_no)->groupBy('branch_tracking_no')->get();
         foreach ($freight_detail as $o) {
                 $freight = $o->freight;
                 $T_Freight += $freight; 
         }
         $tax = 0;
        $taxableAmt = 0;
        $orders_table_id = $order_detail[0]->order_id;
        $branch = $order->branch;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branch' Group by branch");
            $branchTotal = collect($branchTotal);
            foreach ($branchTotal as $value) {
                $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branch and order_id = $orders_table_id) total_rate) a) a";
                $data=DB::select($query);
                foreach ($data as $item) {
                    $tax+=$item->totalTax;
                    $taxableAmt += $item->taxable;
                }
            }
        $purchaseList = OrderDetail::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->pluck('id');
        
        $finalAmount = 0;
        $branches = OrderDetail::where('order_id',$orders_table_id)->distinct('branch')->get(['branch']);

        foreach ($branches as $branche) {
            $tax_total = 0;
            $taxable_total = 0;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch");
            
            $query = "SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch";

            $branchTotal = collect($branchTotal);
            $freight = 0;
            foreach ($branchTotal as $value) {
                    $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branche->branch and order_id = $orders_table_id) total_rate) a) a";
                    Log::info("Query Available - ".$query);
                    $data=DB::select($query);
                    foreach ($data as $item) {
                        $tax_total+=$item->totalTax;
                        $taxable_total += $item->taxable;
                        Log::info("Tax Amount by branch - ".$item->totalTax);
                    }
                    $freight = $value->freight;
            }
            $finalAmount += $tax_total + $taxable_total + $freight;
        }
        
        Log::info($finalAmount);

        $item = array(
                'orderId'   => $order->id,
                'price'     => $price,
                'finalPrice'=> $finalPrice,
                'purchaseList' => $purchaseList,
                'branch'    => $order->branch,
                'totalPrice'=> $taxableAmt+$order->freight+$tax,
                'tfinal'    => $finalAmount
                );
        return $item;
    }

    public function ApplyDiscount(Request $r)
    {
        Log::info("Order Id - ");

        $order = OrderDetail::find($r->orderId);
        $order->dis_per = $r->discountPerc;
        $order->save();

         switch ($order->rate_type) {
             case '0':
                $rate_type = "Dealer Price";
                $price = $order->item_rate;
                $price = $price - $order->dis_per/100 * $price;
                $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                $finalPrice = number_format((float)($price * $order->qty), 2, '.', '');
                break;
             case '1':
                $rate_type = "MRP";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             default:
                break;
         }
         
         $totalPrice = 0;
         $order_detail = OrderDetail::where('branch_tracking_no',$order->branch_tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                 $rate_type = "Dealer Price";
                 $price = $o->item_rate;
                 $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                 $finalRate = number_format((float)($rate * $o->qty), 2, '.', '');
                 break;
             case '1':
                 $rate_type = "MRP";
                 $price = $o->item_rate;
                 $dis_rate = $o->item_rate;
                 $finalRate = $o->item_rate * $o->qty;
                 break;
             case '2':
                 $rate_type = "Subsidy";
                 $price = $order->item_rate;
                 $dis_rate = $order->item_rate;
                 $finalRate = $order->item_rate * $order->qty;
                 break;
             default:
                 break;
             }
             $totalPrice += $finalRate; 
         }
           $T_Amount = 0;
         $order_detail = OrderDetail::where('tracking_no',$order->tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                $rate_type = "Dealer Price";
                $price = $o->item_rate;
                $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                $amount = number_format((float)($rate * $o->qty), 2, '.', '');
                break;
             case '1':
                $rate_type = "MRP";
                $price = $o->item_rate;
                $dis_rate = $o->item_rate;
                $amount = $o->item_rate * $o->qty;
                break;
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $amount = $order->item_rate * $order->qty;
                break;
             default:
                break;
             }
            $T_Amount += $amount; 
        }
         Log::info("Total Amount - ".$T_Amount);
         $T_Freight = 0;
         $freight_detail = OrderDetail::where('tracking_no',$order->tracking_no)->groupBy('branch_tracking_no')->get();
         foreach ($freight_detail as $o) {
                 $freight = $o->freight;
                 $T_Freight += $freight; 
         }
         $tax = 0;
        $taxableAmt = 0;
        $orders_table_id = $order_detail[0]->order_id;
        $branch = $order->branch;
            
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branch' Group by branch");
            $branchTotal = collect($branchTotal);
            foreach ($branchTotal as $value) 
            {
                $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branch and order_id = $orders_table_id) total_rate) a) a";
                $data=DB::select($query);
                foreach ($data as $item) 
                {
                    $tax+=$item->totalTax;
                    $taxableAmt += $item->taxable;
                }
            }
         $purchaseList = OrderDetail::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->pluck('id');
        
        $finalAmount = 0;
        $branches = OrderDetail::where('order_id',$orders_table_id)->distinct('branch')->get(['branch']);
        foreach ($branches as $branche) 
        {
            $tax_total = 0;
            $taxable_total = 0;
             $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch");
            $branchTotal = collect($branchTotal);
            $freight = 0;
            foreach ($branchTotal as $value) {
                    $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branche->branch and order_id = $orders_table_id) total_rate) a) a";
                    $data=DB::select($query);
                    foreach ($data as $item) {
                        $tax_total+=$item->totalTax;
                        $taxable_total += $item->taxable;
                    }
                    $freight = $value->freight;
            }
            $finalAmount += $tax_total + $taxable_total + $freight;
        }
        $item = array(
                'orderId'   => $order->id,
                'price'     => $price,
                'finalPrice'=> $finalPrice,
                'purchaseList' => $purchaseList,
                'branch'    => $order->branch,
                'totalPrice'=> $taxableAmt+$order->freight+$tax,
                'tfinal'    => $finalAmount
                );

        $ordr=Order::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->first();
        Order::where('tracking_no',$ordr->tracking_no)->update(['is_dirty'=>$ordr->is_dirty+1]);
        
        return $item;
    }

    public function ApplyDiscountByExecutive(Request $r)
    {
        $order = OrderDetail::find($r->orderId);
        $order->dis_per = $r->discountPerc;
        $order->save();

         switch ($order->rate_type) {
             
             case '0':
                $rate_type = "Dealer Price";
                $price = $order->item_rate;
                $price = $price - $order->dis_per/100 * $price;
                $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                $finalPrice = number_format((float)($price * $order->qty), 2, '.', '');
                break;
             
             case '1':
                $rate_type = "MRP";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $finalPrice = $order->item_rate * $order->qty;
                break;
             
             default:
                break;
        }
         
         $totalPrice = 0;
         $order_detail = OrderDetail::where('branch_tracking_no',$order->branch_tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                 $rate_type = "Dealer Price";
                 $price = $o->item_rate;
                 $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                 $finalRate = number_format((float)($rate * $o->qty), 2, '.', '');
                 break;
             case '1':
                 $rate_type = "MRP";
                 $price = $o->item_rate;
                 $dis_rate = $o->item_rate;
                 $finalRate = $o->item_rate * $o->qty;
                 break;
             case '2':
                 $rate_type = "Subsidy";
                 $price = $order->item_rate;
                 $dis_rate = $order->item_rate;
                 $finalRate = $order->item_rate * $order->qty;
                 break;
             default:
                 break;
             }
             $totalPrice += $finalRate; 
         }
           $T_Amount = 0;
         $order_detail = OrderDetail::where('tracking_no',$order->tracking_no)->get();
         foreach ($order_detail as $o) {
             switch ($o->rate_type) {
             case '0':
                $rate_type = "Dealer Price";
                $price = $o->item_rate;
                $rate = number_format((float)$price - $o->dis_per/100 * $price, 2, '.', '');
                $amount = number_format((float)($rate * $o->qty), 2, '.', '');
                break;
             case '1':
                $rate_type = "MRP";
                $price = $o->item_rate;
                $dis_rate = $o->item_rate;
                $amount = $o->item_rate * $o->qty;
                break;
             case '2':
                $rate_type = "Subsidy";
                $price = $order->item_rate;
                $dis_rate = $order->item_rate;
                $amount = $order->item_rate * $order->qty;
                break;
             default:
                break;
             }
            $T_Amount += $amount; 
        }
         $T_Freight = 0;
         $freight_detail = OrderDetail::where('tracking_no',$order->tracking_no)->groupBy('branch_tracking_no')->get();
         foreach ($freight_detail as $o) {
                 $freight = $o->freight;
                 $T_Freight += $freight; 
         }
         $tax = 0;
        $taxableAmt = 0;
        $orders_table_id = $order_detail[0]->order_id;
        $branch = $order->branch;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$' Group by branch");
            $branchTotal = collect($branchTotal);
            foreach ($branchTotal as $value) {
                $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM(((($value->freight/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branch and order_id = $orders_table_id) total_rate) a) a";
                $data=DB::select($query);
                foreach ($data as $item) {
                    $tax+=$item->totalTax;
                    $taxableAmt += $item->taxable;
                }
            }
        $purchaseList = OrderDetail::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->pluck('id');
        
        $finalAmount = 0;
        $branches = OrderDetail::where('order_id',$orders_table_id)->distinct('branch')->get(['branch']);

        foreach ($branches as $branche) {
            $tax_total = 0;
            $taxable_total = 0;
            $branchTotal = DB::select("SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,IFNULL(freight,0) as freight FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch");
            
            $query = "SELECT branch,SUM(branchTotal) as branchTotal,freight from (SELECT branch,(CASE WHEN rate_type=0 THEN qty*item_rate-((qty*item_rate)/100)*IFNULL(dis_per, 0) ELSE (item_rate*100/(gst+100))*qty END) as branchTotal,IFNULL(freight,0) FROM `order_details` where order_id='$orders_table_id') amount where branch='$branche->branch' Group by branch";

            $branchTotal = collect($branchTotal);
            $freight = 0;
            foreach ($branchTotal as $value) {
                    $query="SELECT *,totalTax-((taxable/100)*gst) as taxOnFreight FROM (SELECT SUM((((IFNULL($value->freight,0)/$value->branchTotal)*taxable+taxable)/100)*gst) as totalTax,SUM(taxable) as taxable,gst FROM (select item_rate * qty as taxable,gst from (select (CASE WHEN rate_type = 0 then item_rate-(item_rate/100)*IFNULL(dis_per,0) ELSE item_rate*100/(100+gst) END) item_rate, qty,gst from order_details where branch = $branche->branch and order_id = $orders_table_id) total_rate) a) a";
                    Log::info("Query - ".json_encode($value));
                    $data=DB::select($query);
                    foreach ($data as $item) {
                        $tax_total+=$item->totalTax;
                        $taxable_total += $item->taxable;
                    }
                    $freight = $value->freight;
            }
            $finalAmount += $tax_total + $taxable_total + $freight;
        }
        
        Log::info("FINAL AMT - ".$finalAmount);

        $item = array(
                'orderId'   => $order->id,
                'price'     => $price,
                'finalPrice'=> $finalPrice,
                'purchaseList' => $purchaseList,
                'branch'    => $order->branch,
                'totalPrice'=> $taxableAmt+$order->freight+$tax,
                'tfinal'    => $finalAmount
                );
        return $item;
    }

    public function GenerateTrackingNo()
    {
        $now = Carbon::now();
        $tracking_no = $now->format('Y.m.d');
        $date=Carbon::now()->format('Y-m-d');
        $order_detail = Order::where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)->orderBy('id', 'desc')->first();
        if($order_detail != null)
        {
            $order_flag = explode(".", $order_detail->tracking_no);
            $current_tracking_no = $order_flag[3]+1;
            $current_tracking_no = str_pad( $current_tracking_no, 5, "0", STR_PAD_LEFT );
            $tracking_no = "".$tracking_no.".".($current_tracking_no);
        }
        else
        {
            $tracking_no = "".$tracking_no.".00001";
        }
        return $tracking_no;
    }

    public function ViewOrderPdf(Request $r)
    {
        $tracking_no = decrypt($r->tracking_no);
        $AOrder = OrderDetail::where('branch_tracking_no',$tracking_no)->get();
        OrderDetail::where('branch_tracking_no',$tracking_no)->update(['readStatus'=>2,'updated_at'=>$AOrder[0]->updated_at]);
        $emp = $AOrder[0]->order->employee->name;
        $dealer = Dealer::where('id','=',$AOrder[0]->order->dealer_id)->get();
        $dealerName = $AOrder[0]->order->dealer->name;
        $placedOrders = OrderDetail::where('branch_tracking_no',$tracking_no)->get();
        return view('order.pdf.placedOrder')->with(compact('placedOrders'));
    }

    public function ViewOrderDetails(Request $request)
    {
        $view = Self::GetOrderDetails('details_exec', $request);
        return $view;
    }

    public function ViewExecOrderDetails(Request $request)
    {
        $view = Self::GetOrderDetails('payment.details_approve', $request);
        return $view;
    }
     public function ViewOrderDetailsPlacedByDealer(Request $request)
    {
        $view = Self::GetOrderDetails('dealer.details_dealer_placed_order', $request);
        return $view;
    }
     public function ViewPayApprovedOrders(Request $request)
    {
        $view = Self::GetOrderDetails('mgmt.details_mgmt', $request);
        return $view;
    }

    public function ViewPartsOrder(Request $request)
    {
        OrderDetail::where('tracking_no',decrypt($request->tracking_no))->update(['readStatus'=>1]);
        $view = Self::GetOrderDetails('parts.details_part_split', $request);
        return $view;
    }    

     public function ViewDealerOrderDetails(Request $request)
    {
        $view = Self::GetOrderDetails('executive.details_dealer', $request);
        return $view;
    }
     public function ViewMgmtApprOrderDetail(Request $request)
    {
        $placedOrders = OrderDetail::where('branch_tracking_no',decrypt($request->tracking_no))->get();
        $branches = Branch::all(['id','code','rate']);
        return view('order.coordinator.order_detail')->with(compact('placedOrders','branches'));
    }
     public function GetOrderDetails($role, $r)
    {
        $placedOrders = OrderDetail::where('tracking_no',decrypt($r->tracking_no))->orderBy('gst')->get();
        if($placedOrders->count() == 0)
             return redirect()->route('home');
        $branches = Branch::all(['id','code','rate']);
        $dispatchBranches = OrderDetail::where('tracking_no',decrypt($r->tracking_no))->groupBy('branch')->pluck('branch');
        return view('order.'.$role)->with(compact('placedOrders','branches','dispatchBranches'));
    }

    public function FetchPriceRate(Request $r)
    {
        // $priceRate = Branch::where('id',$r->branchId)->value('rate');
        // $selDispatchBranch = OrderDetail::where('id',$r->orderId)->value('branch');
        // $purchaseRequests = OrderDetail::where('tracking_no',OrderDetail::where('id',$r->orderId)->value('tracking_no'))->pluck('dispatch_purchNo');
        // $branchCodes = array();
        // $tracking_no = 0;
        // foreach ($purchaseRequests as $purchase) {
        //            $data = explode('-', $purchase);
        //            $tracking_no = $data[0];
        //            if(!in_array($data[1], $branchCodes))
        //             $branchCodes[$selDispatchBranch] = $data[1];
        //        }
        // if (array_key_exists($r->branchId,$branchCodes))
        // {
        //      OrderDetail::where('id',$r->orderId)->update(['priceRate'=>$priceRate,'branch'=>$r->branchId,'dispatch_purchNo'=>OrderDetail::where('tracking_no',$tracking_no)->where('branch',$r->branchId)->value('dispatch_purchNo')]);
        // }
        // else
        // {
        //     end($branchCodes);
        //     $key = key($branchCodes);
        //     $newBranchCode = ++$branchCodes[$key];
        //     OrderDetail::where('id',$r->orderId)->update(['priceRate'=>$priceRate,'branch'=>$r->branchId,'dispatch_purchNo'=>$tracking_no.'-'.$newBranchCode]);
        // }
         Log::info("Order ID ".$r->orderId);
         $alphabet = range('A', 'Z');
        $trackingNo = OrderDetail::where('id',$r->orderId)->value('tracking_no');
        $itemRate = OrderDetail::where('id',$r->orderId)->value('item_rate');
        $branchRate = Branch::where('id',OrderDetail::where('id',$r->orderId)->value('branch'))->value('rate');
        $newBranchRate = Branch::where('id',$r->branchId)->value('rate');
        
        Log::info("New Branch Rate - ".$newBranchRate);
        Log::info("Old Branch Rate - ".$branchRate);
        Log::info("Item Rate - ".$itemRate);
         if($r->branchId != 1)
            OrderDetail::where('id',$r->orderId)->update(['branch'=>$r->branchId,'branch_tracking_no'=>$trackingNo."-".$alphabet[$r->branchId],'item_rate'=>$itemRate*$newBranchRate/$branchRate]);
        else
            OrderDetail::where('id',$r->orderId)->update(['branch'=>$r->branchId,'branch_tracking_no'=>$trackingNo."-A",'item_rate'=>$itemRate*$newBranchRate/$branchRate]);
    }
     public function SplitOrder(Request $request)
    {
        $order_details = OrderDetail::where('id',$request->order_pin)->get();
        DB::table('order_details')->insertGetId(
             [
               'order_id'           => $order_details[0]->order_id,
               'order_date'         => $order_details[0]->order_date,
               'tracking_no'        => $order_details[0]->tracking_no,
               'branch_tracking_no' => $order_details[0]->branch_tracking_no,
               'classification'     => $order_details[0]->classification,
               'grp'                => $order_details[0]->grp,
               'subgrp'             => $order_details[0]->subgrp,
               'hsn'                => $order_details[0]->hsn,
               'sku'                => $order_details[0]->sku,
               'qty'                => $order_details[0]->qty,
               'branch'             => $order_details[0]->branch,
               'rate_type'          => $order_details[0]->rate_type,
               'subsidy_dept_id'    => $order_details[0]->subsidy_dept_id,
               'item_rate'          => $order_details[0]->item_rate,
               'dis_per'            => $order_details[0]->dis_per,
               'gst'                => $order_details[0]->gst,
               'order_status'       => 1,
             ]);
        $trackingNo = encrypt($order_details[0]->tracking_no);
        return redirect()->route('order.dealerPlaced',array('tracking_no'=>$trackingNo));
    }
     
    

     /**
      * Display the specified resource.
      *
      * @param  \App\Models\Order  $order
      * @return \Illuminate\Http\Response
      */
     public function show(Order $order)
     {
         //
     }
       /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Models\Order  $order
      * @return \Illuminate\Http\Response
      */
     public function edit(Order $order)
     {
         //
     }
       /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\Models\Order  $order
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, Order $order)
     {
         //
     }
       /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Models\Order  $order
      * @return \Illuminate\Http\Response
      */
     public function destroy(Order $order)
     {
         //
     }
       public function FetchMchGrpList()
     {
         $machineGroups = MachineGroup::all(['name','id']);
         return $machineGroups;
     }
       public function FetchGroup(Request $request)
     {
         $category = $request->category;
         $type = $request->type;
         switch ($category) {
             case '1':
                 $mchGrpNames = MachineSku::distinct('grp')->pluck('grp');
                 return $mchGrpNames;
                 break;
             case '2':
                 $accGrpNames = AccessoriesSku::distinct('grp')->pluck('grp');
                 return $accGrpNames;
                 break;
             case '3':
                 if($type==0)
                    return MachineSku::distinct('grp')->pluck('grp');
                 else if($type==1)
                    return AccessoriesSku::distinct('grp')->pluck('grp');
                 return "null";
                 break;
         }
     }
       public function FetchSubgrpByGrp(Request $request)
     {
         $category = $request->category;
         $grp = $request->grp;
         $type = $request->type;
           switch ($category) {
             case '1':
                 $mchSubgrpNames = MachineSku::where('grp','=',$grp)->distinct('subgrp')->pluck('subgrp');
                 return $mchSubgrpNames;
                 break;
             case '2':
                 $accSubgrpNames = AccessoriesSku::where('grp','=',$grp)->distinct('subgrp')->pluck('subgrp');
                 return $accSubgrpNames;
                 break;
             case '3':
                 if($type==0)
                    return MachineSku::where('grp','=',$grp)->distinct('subgrp')->pluck('subgrp');
                 else if($type==1)
                    return AccessoriesSku::where('grp','=',$grp)->distinct('subgrp')->pluck('subgrp');
                 return "null";
                 break;
         }
     }
       public function FetchSkuBySubgrp(Request $request)
     {
         $category = $request->category;
         $subgrp = $request->subgrp;
         $type = $request->type;
        
        $grp = $request->grp;
         switch ($category) {
             case '1':
                 $mchSkuNames = MachineSku::where('subgrp',$subgrp)->where('grp',$grp)->pluck('sku');
                 return $mchSkuNames;
                 break;
             case '2':
                 $accSkuNames = AccessoriesSku::where('subgrp',$subgrp)->where('grp',$grp)->pluck('sku');
                 return $accSkuNames;
                 break;
             case '3':
                 if($type==0)
                    return MachineSku::where('subgrp',$subgrp)->where('grp',$grp)->pluck('sku');
                 else if($type==1)
                    return AccessoriesSku::where('subgrp',$subgrp)->where('grp',$grp)->pluck('sku');
                 return "null";
                 break;
         }
     }

     public function FetchPartBySubgrp(Request $request)
     {
         $sku = $request->sku;
         $type = $request->type;
         switch ($type) {
             case '0':
                 $id=MachineSku::where('sku',$sku)->value('id');
                 return Part::whereRaw("FIND_IN_SET('$id',mch_ids)")->where('status',1)->get(['id','sku']);
                 break;
             case '1':
                 $id=AccessoriesSku::where('sku',$sku)->value('id');
                 return Part::whereRaw("FIND_IN_SET('$id',mch_ids)")->where('status',1)->get(['id','sku']);
                 break;
         }
     }


       public function FetchPriceBySku(Request $request)
    {
        $order_dealer = 0;
        $order_dealer = $request->dealer;
         $state_id =  Dealer::where('id',$order_dealer)->pluck('state_id');
        $mch_id = $request->mch_id;
        $acc_id = $request->acc_id;
         $category = $request->category;
        $sku = $request->sku;
         switch ($category) {
             case '1':
                $itemPrice = MachineSubsidy::where('mch_id',MachineSku::where('sku',$sku)->value('id'))->where('state_id',$state_id)->where('status',1)->value('subsidy_price');
                $mchPriceInfo = MachineSku::where('sku',$sku)->get();
                if($itemPrice > 0)
                    return array('itemInfo'=>$mchPriceInfo,'branchRatePer'=>Session::get('branchRatePer'),'subsidy_applicable'=>1);
                else
                    return array('itemInfo'=>$mchPriceInfo,'branchRatePer'=>Session::get('branchRatePer'),'subsidy_applicable'=>0);
                break;
             case '2':
                $itemPrice = AccSubsidy::where('acc_id',AccessoriesSku::where('sku',$sku)->value('id'))->where('state_id',$state_id)->where('status',1)->value('subsidy_price');
                $accPriceInfo = AccessoriesSku::where('sku',$sku)->get();
                if($itemPrice > 0)
                    return array('itemInfo'=>$accPriceInfo,'branchRatePer'=>Session::get('branchRatePer'),'subsidy_applicable'=>1);
                else
                    return array('itemInfo'=>$accPriceInfo,'branchRatePer'=>Session::get('branchRatePer'),'subsidy_applicable'=>0);
                break;
             case '3':
                $partInfo = Part::where('id',$sku)->get();
                return array('itemInfo'=>$partInfo,'branchRatePer'=>1,'subsidy_applicable'=>0);
                    
                break;
        }
    }
     public function FetchSubsidyRateByDealer(Request $request)
    {
        $order_dealer = 0;
         $order_dealer = $request->dealer;
        $category = $request->category;
        $mch_id = $request->mch_id;
        $acc_id = $request->acc_id;
         $state_id =  Dealer::where('id',$order_dealer)->pluck('state_id');
         // $branch = $request->branch;
         switch ($category)
        {
            case '1':
                $itemPrice = MachineSubsidy::where('mch_id',$mch_id)->where('state_id',$state_id)->value('subsidy_price');
                $itemPrice = $itemPrice - $itemPrice * 5/100;
                $subsidy_dept = SubsidyDept::where('id',MachineSubsidy::where('mch_id',$mch_id)->where('state_id',$state_id)->value('subsidy_dept_id'))->value('code');
                break;
             case '2':
                $itemPrice = AccSubsidy::where('acc_id',$acc_id)->where('state_id',$state_id)->value('subsidy_price');
                $itemPrice = $itemPrice - $itemPrice * 5/100;
                $subsidy_dept = SubsidyDept::where('id',AccSubsidy::where('acc_id',$acc_id)->where('state_id',$state_id)->value('subsidy_dept_id'))->value('code');
                break;
             case '3':
                break;
        }
         if($itemPrice == null)
            $itemPrice=-1;
         $itemData = SubsidyDeptState::leftJoin('subsidy_dept', function($join) {
                      $join->on('subsidy_dept.id', '=', 'subsidy_dept_state.subsidy_dept_id');
                    })
                    ->where('state_id',$state_id)
                    ->pluck('subsidy_dept.code')
                    ->toArray();
        $data = [
            'itemData'    => $itemData,
            'itemPrice'   => $itemPrice,
            'subsidy_dept'=> $subsidy_dept   
        ];
        return $data;
    }
     public function FetchSubsidyRateByDept(Request $request)
    {
        $category = $request->category;
         $state_id =  Dealer::where('id',$request->dealer)->pluck('state_id');
         switch ($category)
        {
            case '1':
                $itemPrice = MachineSubsidy::where('mch_id',MachineSku::where('sku',$request->sku)->value('id'))->where('state_id',$state_id)->where('subsidy_dept_id',SubsidyDept::where('code',$request->subsidy_dept)->value('id'))->value('subsidy_price');
                $itemPrice = $itemPrice - $itemPrice * 5/100;
                break;
             case '2':
                $itemPrice = AccSubsidy::where('mch_id',AccessoriesSku::where('sku',$request->sku)->value('id'))->where('state_id',$state_id)->where('subsidy_dept_id',SubsidyDept::where('code',$request->subsidy_dept)->value('id'))->value('subsidy_price');
                $itemPrice = $itemPrice - $itemPrice * 5/100;
                break;
             case '3':
                break;
        }
         if($itemPrice == null)
            $itemPrice=-1;
         return $itemPrice;
    }
             function notifyUser($firebaseIds,$data){
                 
                 Log::info("Firebase ID's - ".json_encode($firebaseIds));

                 $url = 'https://fcm.googleapis.com/fcm/send';
                 $fields = array (
                         'registration_ids' => $firebaseIds,
                         'data'=>$data
                 );
                 $fields = json_encode ( $fields );
                 $headers = array (
                         'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                         'Content-Type: application/json'
                 );
                 $ch = curl_init ();
                 curl_setopt ( $ch, CURLOPT_URL, $url );
                 curl_setopt ( $ch, CURLOPT_POST, true );
                 curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                 curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                 curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                 $result = curl_exec ( $ch );
                 curl_close ( $ch );
            }

          function notifyManagement($data){
            $url = 'https://fcm.googleapis.com/fcm/send';
                 $fields = array (
                         'to' => '/topics/Management',
                         'data' => $data
                 );
                 $fields = json_encode ( $fields );
                 $headers = array (
                         'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                         'Content-Type: application/json'
                 );
                 $ch = curl_init ();
                 curl_setopt ( $ch, CURLOPT_URL, $url );
                 curl_setopt ( $ch, CURLOPT_POST, true );
                 curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                 curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                 curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                 $result = curl_exec ( $ch );
                 curl_close ( $ch );
     }
      public function Executive__Placed_orders()
    {
         $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('e_mo_fromDate'))
            $fromDate = Session::get('e_mo_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('e_mo_toDate'))
            $toDate = Session::get('e_mo_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
         //State
        if(Session::has('e_mo_state')){
            $state=Session::get('e_mo_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
        {
            $state="%";
        }
         //District
        if(Session::has('e_mo_district')){
            $districts = Session::get('e_mo_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('e_mo_dealer')){
            $dealer = Session::get('e_mo_dealer');
        }
        else
            $dealer = "%";
          //Order Status
        if(Session::has('e_mo_orderStatus'))
            $status=Session::get('e_mo_orderStatus');
        else
            $status="%";
         //Tracking No
        if(Session::has('e_mo_tracking'))
            $tracking_no=Session::get('e_mo_tracking');
        else
            $tracking_no="%";
         $user=Auth::user()->employee;
        $emp_id=$user->id;
        if($user->role_id==4 || $user->role_id==14 || $user->role_id==19)
            $emp_id="%";
        if($user->role_id==19){
            $allIndiaOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status','LIKE',$status)->where('classification','Parts');
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('emp_id','LIKE',$emp_id)
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
        }
        else{
           $allIndiaOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status','LIKE',$status);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('emp_id','LIKE',$emp_id)
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
            }
                                     
         //Log::info($dealer);
         return view('order.mgmt.all_over_orders')->with(compact('allIndiaOrders','states','districtList','dealerList'));
    }
      public function Executive__rejected_orders()
    {
         $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('e_mo_fromDate'))
            $fromDate = Session::get('e_mo_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('e_mo_toDate'))
            $toDate = Session::get('e_mo_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //State
        if(Session::has('e_mo_state')){
            $state=Session::get('e_mo_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";
         //District
        if(Session::has('e_mo_district')){
            $districts = Session::get('e_mo_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('e_mo_dealer')){
            $dealer = Session::get('e_mo_dealer');
        }
        else
            $dealer = "%";
          //Order Status
        if(Session::has('e_mo_orderStatus'))
            $status=Session::get('e_mo_orderStatus');
        else
            $status="%";
         //Tracking No
        if(Session::has('e_mo_tracking'))
            $tracking_no=Session::get('e_mo_tracking');
        else
            $tracking_no="%";
         $user=Auth::user()->employee;
        $emp_id=$user->id;
        if($user->role_id==4 || $user->role_id==14)
            $emp_id="%";
         Log::info($districtList);
          $allIndiaOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status',7);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('emp_id','LIKE',$emp_id)
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
         return view('order.mgmt.all_over_orders')->with(compact('allIndiaOrders','states','districtList','dealerList','order_rejected_flag'));
     }
      public function Executive__Myregion_orders()
    {
         //From Date
        if(Session::has('region_fromDate'))
            $fromDate = Session::get('region_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('region_toDate'))
            $toDate = Session::get('region_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //Dealer Name
        if(Session::has('region_dealer')){
            $dealer = Session::get('region_dealer');
        }
        else
            $dealer = "%";
          //Order Status
        if(Session::has('region_orderStatus'))
            $status=Session::get('region_orderStatus');
        else
            $status="%";
         
        $districts=EmpDistrict::where('emp_id',Auth::user()->employee->id)->pluck('district_id');
        $dealers = Dealer::where('status',1)->whereIn('district_id',$districts)->orderBy('name', 'asc')->get(['id','name']);
        
           $regionOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status','LIKE',$status);
                                     })
                                    ->whereHas('dealer',function($query) use($districts) {
                                        $query->whereIn('district_id',$districts);
                                    }) 
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
        return view('order.executive.my_region_orders')->with(compact('regionOrders','dealers'));
     }
      
      public function PayApprovedOrders()
    {
         $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('payapproved_fromDate'))
            $fromDate = Session::get('payapproved_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('payapproved_toDate'))
            $toDate = Session::get('payapproved_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //State
        if(Session::has('payapproved_state')){
            $state=Session::get('payapproved_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";
         //District
        if(Session::has('payapproved_district')){
            $districts = Session::get('payapproved_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('payapproved_dealer')){
            $dealer = Session::get('payapproved_dealer');
        }
        else
            $dealer = "%";
          //Order Status
        if(Session::has('payapproved_orderStatus'))
            $status=Session::get('payapproved_orderStatus');
        else
            $status="%";
         //Tracking No
        if(Session::has('payapproved_tracking'))
            $tracking_no=Session::get('payapproved_tracking');
        else
            $tracking_no="%";
           $PayApprovedOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status',3);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
         return view('order.mgmt.pay_approved')->with(compact('PayApprovedOrders','states','districtList','dealerList'));
     }


    public function PartsPendingOrders()
    {
         $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('pending_parts_fromDate'))
            $fromDate = Session::get('pending_parts_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('pending_parts_toDate'))
            $toDate = Session::get('pending_parts_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //State
        if(Session::has('pending_parts_state')){
            $state=Session::get('pending_parts_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";
         //District
        if(Session::has('pending_parts_district')){
            $districts = Session::get('pending_parts_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('pending_parts_dealer')){
            $dealer = Session::get('pending_parts_dealer');
        }
        else
            $dealer = "%";
          //Order Status
        if(Session::has('pending_parts_orderStatus'))
            $status=Session::get('pending_parts_orderStatus');
        else
            $status="%";
         //Tracking No
        if(Session::has('pending_parts_tracking'))
            $tracking_no=Session::get('pending_parts_tracking');
        else
            $tracking_no="%";
           $PendingPartsOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate,$status) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status',9);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);

         return view('order.parts.parts_pending')->with(compact('PendingPartsOrders','states','districtList','dealerList'));
     }



      public function rejectedOrders()
    {
        $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('rejected_fromDate'))
            $fromDate = Session::get('rejected_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('rejected_toDate'))
            $toDate = Session::get('rejected_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //State
        if(Session::has('rejected_state')){
            $state=Session::get('rejected_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";
         //District
        if(Session::has('rejected_district')){
            $districts = Session::get('rejected_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('rejected_dealer')){
            $dealer = Session::get('rejected_dealer');
        }
        else
            $dealer = "%";
           //Tracking No
        if(Session::has('rejected_tracking'))
            $tracking_no=Session::get('rejected_tracking');
        else
            $tracking_no="%";
            $RejectedOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status',7);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->where('status','LIKE',1)
                                    ->orderBy('updated_at', 'desc')
                                    ->paginate(10);
         return view('order.general.rejected_list')->with(compact('RejectedOrders','states','districtList','dealerList'));
    }
      public function ExecApprovedOrders()
    {
        $states = State::all();
        $districtList=array();
        $dealerList=array();
         //From Date
        if(Session::has('exe_placed_fromDate'))
            $fromDate = Session::get('exe_placed_fromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
         //To Date
        if(Session::has('exe_placed_toDate'))
            $toDate = Session::get('exe_placed_toDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
           //State
        if(Session::has('exe_placed_state')){
            $state=Session::get('exe_placed_state');
            $districtList=District::where('state_id',$state)->get();
        }
        else
            $state="%";
         //District
        if(Session::has('exe_placed_district')){
            $districts = Session::get('exe_placed_district');
            $dealerList=Dealer::where('district_id',$districts)->get();
        }
        else
            $districts="%";
          //Dealer Name
        if(Session::has('exe_placed_dealer')){
            $dealer = Session::get('exe_placed_dealer');
        }
        else
            $dealer = "%";
           //Tracking No
        if(Session::has('exe_placed_tracking'))
            $tracking_no=Session::get('exe_placed_tracking');
        else
            $tracking_no="%";
            $execApprovedOrders = Order::whereHas('OrderDetail', function ($query)  use($fromDate,$toDate) {
                                         $query->where('order_date','>=', $fromDate)->where('order_date','<=',$toDate)->where('order_status',2);
                                     })
                                    ->whereHas('dealer',function($query) use($districts,$state) {
                                        $query->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                    })    
                                    ->where('dealer_id','LIKE',$dealer)
                                    ->where('status','LIKE',1)
                                    ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                                    ->orderBy('id', 'desc')
                                    ->paginate(10);
         return view('order.payment.exec_placed')->with(compact('execApprovedOrders','states','districtList','dealerList'));
    }
       public function MgmtApprovedOrders()
       {
            $states = State::all();
            $districtList=array();
            $dealerList=array();
             //From Date
            if(Session::has('mgmt_appoved_fromDate'))
                $fromDate = Session::get('mgmt_appoved_fromDate');            
            else
                $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
             //To Date
            if(Session::has('mgmt_appoved_toDate'))
                $toDate = Session::get('mgmt_appoved_toDate');            
            else
                $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');
               //State
            if(Session::has('mgmt_appoved_state')){
                $state=Session::get('mgmt_appoved_state');
                $districtList=District::where('state_id',$state)->get();
            }
            else
                $state="%";
             //District
            if(Session::has('mgmt_appoved_district'))
            {
                $districts = Session::get('mgmt_appoved_district');
                $dealerList=Dealer::where('district_id',$districts)->get();
            }
            else
                $districts="%";
              //Dealer Name
            if(Session::has('mgmt_appoved_dealer')){
                $dealer = Session::get('mgmt_appoved_dealer');
            }
            else
                $dealer = "%";
               //Tracking No
            if(Session::has('mgmt_appoved_tracking'))
                $tracking_no=Session::get('mgmt_appoved_tracking');
            else
                $tracking_no="%";
			
			$role=Auth::user()->employee->role_id;
			if($role==20){
               $mgmtApprOrders=OrderDetail::whereHas('Order',function($query) use($dealer,$districts,$state){
                                $query->where('dealer_id','LIKE',$dealer)->where('status',1)
                                ->whereHas('dealer',function($query2) use($districts,$state){
                                    $query2->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                });
                            })
							->where('classification','Parts')
                            ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                            ->where('order_date','>=', $fromDate)
                            ->where('order_date','<=',$toDate)
                            ->where('order_status',4)
                            ->groupby('branch_tracking_no')
                            ->orderBy('updated_at', 'desc')
                            ->paginate(10);
                            return view('order.coordinator.mgmt_approved')->with(compact('mgmtApprOrders','states','districtList','dealerList'));
			}
			else{
				$mgmtApprOrders=OrderDetail::whereHas('Order',function($query) use($dealer,$districts,$state){
                                $query->where('dealer_id','LIKE',$dealer)->where('status',1)
                                ->whereHas('dealer',function($query2) use($districts,$state){
                                    $query2->where('district_id','LIKE',$districts)->where('state_id','LIKE',$state);
                                });
                            })
                            ->where('classification','<>','Parts')
                            ->where('tracking_no','LIKE','%'.$tracking_no.'%')
                            ->where('order_date','>=', $fromDate)
                            ->where('order_date','<=',$toDate)
                            ->where('order_status',4)
                            ->groupby('branch_tracking_no')
                            ->orderBy('updated_at', 'desc')
                            ->paginate(10);
                            return view('order.coordinator.mgmt_approved')->with(compact('mgmtApprOrders','states','districtList','dealerList'));
			}
       }
    }
