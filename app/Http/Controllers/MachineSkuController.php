<?php



namespace App\Http\Controllers;



use App\Models\MachineSku;
use App\Models\MachineSubsidy;
use App\Models\Version;
use App\Models\State;
use App\Models\SubsidyDept;
use Illuminate\Http\Request;
use DB;
use Log;
use Redirect;
use Rap2hpoutre\FastExcel\FastExcel;

class MachineSkuController extends Controller
{

    public function importMachinerySku(Request $request)
    {

        //Update mrp wherever 0
        // (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {
        //     MachineSku::where('sku',$line['sku'])->update(['mrp'=>$line['mrp']]);
        // });

        /* Import Machinery Sku */
        // if($request->file('mchExcel') == null)
        // {
        //     return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        // }
        // $users = (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {
        //     $mch = new MachineSku();
        //     $mch->item = $line['items'];
        //     $mch->grp = $line['grp'];
        //     $mch->subgrp = $line['subgrp'];
        //     $mch->sku = $line['sku'];
        //     // $mch->hsn = $line['hsn'];
        //     $mch->gst = $line['gst'];
        //     $mch->dp = $line['dp'];
        //     $mch->mrp = $line['mrp'];
        //     if($line['subsidy'] > 0)
        //         $mch->subsidy = 1;
        //     $mch->save();
        // });
        // Version::where('tbl_name','mch_sku')->update(['version'=> DB::raw('version+1')]);

        // /* Import Machine Subsidy */
        // if($request->file('mchExcel') == null)
        // {
        //     return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        // }
        // $users = (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {
        //     // if($line['subsidy'] == 0)
        //     //     $temp = 0;
        //     // else if($line['subsidy'] > 0)
        //     // {
        //         $mchId = MachineSku::where('sku',$line['sku'])->value('id');
        //             $mchSubsidy = new MachineSubsidy();
        //             $mchSubsidy->mch_id = $mchId;
        //             $mchSubsidy->state_id = State::where('name',$line['state'])->value('id');
        //             $mchSubsidy->subsidy_dept_id = SubsidyDept::where('name',$line['subsidy_dept'])->value('id');
        //             $mchSubsidy->subsidy_price = $line['rate'];
        //             $mchSubsidy->save();
        //     // }
        // });

        /* Update Machinery Hsn */
        // if($request->file('mchExcel') == null)
        // {
        //     return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        // }
        // $users = (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {
        //     MachineSku::where('sku',$line['sku'])->update(['hsn'=>$line['hsn']]);
        // });

        /* Update Machinery Subsidy */
        if($request->file('mchExcel') == null)
        {
            return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        }
        $users = (new FastExcel)->import($request->file('mchExcel')->getRealPath(), function ($line) {
            MachineSku::where('id',$line['mch_id'])->update(['subsidy'=>1]);
        });
        return back();
    }



     /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $items = $request->items ?? 5;

        $mchSkus = MachineSku::where('status','=',1)->paginate($items);

        return view('machineSku.index')->with(compact('mchSkus','items'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $mchGroups = MachineSku::distinct('grp')->pluck('grp');

        $mchSubgroups = MachineSku::distinct('subgrp')->pluck('subgrp');

        return view('machineSku.create')->with(compact('mchGroups','mchSubgroups'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        try{

            $mchSku = new MachineSku();

            $mchSku->grp = $request->mchGroup;

            $mchSku->subgrp = $request->mchSubgroup;

            $mchSku->sku = $request->sku;

            $mchSku->hsn = $request->hsn;

            $mchSku->gst = $request->gst;

            $mchSku->dp = $request->dp;

            $mchSku->mrp = $request->mrp;

            $mchSku->subsidy = $request->subsidy;

            $mchSku->bulk_qty = $request->bulkQty;

            if($mchSku->save())

                Version::where('tbl_name','mch_sku')->update(['version'=> DB::raw('version+1')]);

            return redirect()->route("machineSku.index")->with(['result'=>'New Machine Sku added','color'=>'bg-green']);

        }

        catch(\Exception $e){

            return redirect()->route("mchSku.index")->with(['result'=>'Error adding Machine Group','color'=>'alert-warning']);

        }

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Models\MachineSku  $mchSku

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

            $mchSku = MachineSku::find($id);

            $mchSku->grp = $request->mchGroup;

            $mchSku->subgrp = $request->mchSubgroup;

            $mchSku->sku = $request->sku;

            $mchSku->hsn = $request->hsn;

            $mchSku->gst = $request->gst;

            $mchSku->dp = $request->dp;

            $mchSku->mrp = $request->mrp;

            $mchSku->subsidy = $request->subsidy;

            $mchSku->bulk_qty = $request->bulkQty;

            if($mchSku->save())

                    Version::where('tbl_name','mch_sku')->update(['version'=> DB::raw('version+1')]);

        return Redirect::route('machineSku.index',array('items' => 5));

    }



    /**

     * Display the specified resource.

     *

     * @param  \App\Models\MachineSku  $mchSku

     * @return \Illuminate\Http\Response

     */

    public function show(MachineSku $mchSku)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Models\MachineSku  $mchSku

     * @return \Illuminate\Http\Response

     */

    public function edit(MachineSku $machineSku)

    {   

        $mchGroups = MachineSku::distinct('grp')->pluck('grp');

        $mchSubgroups = MachineSku::distinct('grp')->pluck('subgrp');

        $mchSku = MachineSku::find($machineSku->id);

        return view('machineSku.edit')->with(compact('mchSku','mchGroups','mchSubgroups'));

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Models\MachineSku  $mchSku

     * @return \Illuminate\Http\Response

     */

    public function destroy(MachineSku $machineSku)
    {
        MachineSku::where('id',$machineSku->id)->update(['status'=>0]);
        Version::where('tbl_name','mch_sku')->update(['version'=> DB::raw('version+1')]);
        return Redirect::route('machineSku.index',array('items' => 5));
    }

    public function FetchSkuByGroup(Request $r)        
    {
        $mchSku = MachineSku::where('mg_id',$r->groupId)->get(['name','id']);
        return $mchSku;
    }

}

