<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Transfer; 
use App\TransferDetails; 
use App\Godown; 
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Validator;
use Log;
use DB;
use App\Models\Branch;
use App\Models\Shipping;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\NewStockNotification;
use Illuminate\Support\Facades\Mail;

class TransferController extends Controller
{
	 public function create(Request $request){
		$validator = Validator::make($request->all(), [
		'req_branch' => 'required',
		'source_branch' => 'required',
		]);
		if ($validator->fails()) {  
		     
		}

		//Log::info($request);

		$employee=Auth::user()->employee;
		$details=json_decode(base64_decode($request->transfer_details),TRUE);

		$transfer_id=$request->transfer_id;
		if($transfer_id!=-1){
			//Order Need to update
			$transfer=Transfer::where('id',$transfer_id)->first();
			if($request->isEdited=='false'){
				if($transfer->status==1)
					$transfer->status=2;
				else if($transfer->status==2)
					$transfer->status=3;
				//$transfer->status=$transfer->status+1;
				if($employee->role_id==4 || $employee->role_id==14){
					$transfer->management_id=$employee->id;
					$transfer->ip=$request->ip();
					$transfer->transport=$request->transport;
					$transfer->transport_type=$request->transport_type;
					$transfer->vehicle=$request->vehicle;
					DB::select("UPDATE transfer_details SET management_quantity=quantity WHERE transfer_id='$transfer_id'");
				}
				else{
					DB::select("UPDATE transfer_details SET final_quantity=management_quantity WHERE transfer_id='$transfer_id'");
					$transfer->source_emp_id=$employee->id;
					$transfer->transport=$request->transport;
					$transfer->transport_type=$request->transport_type;
					$transfer->vehicle=$request->vehicle;
				}
				$transfer->save();
				if($transfer->status==2){
					$data=array('transfer_id'=>$transfer_id);
            				Mail::to([Branch::where('id',$transfer->source_branch)->value('email')])->send(new NewStockNotification($data));
				}
				else if($transfer->status==3){
					$data=array('transfer_id'=>$transfer_id);
            				Mail::to(['billing@kisankraft.com','admin.sales2@kisankraft.com','admin.sales1@kisankraft.com'])->send(new NewStockNotification($data));
				}
				
				return response()->json(['status'=>true,'color'=>'#19c3aa','head'=>'Sucess']);
			}
			else{
				if($employee->role_id==4 || $employee->role_id==14){
					$transfer->req_branch = $request->req_branch;
					$transfer->req_godown = $request->req_godown;
					$transfer->source_branch = $request->source_branch;
					$transfer->source_godown = $request->source_godown;
					$transfer->management_id=$employee->id;
					$transfer->transport=$request->transport;
					$transfer->transport_type=$request->transport_type;
					$transfer->ip=$request->ip();
					$transfer->vehicle=$request->vehicle;
				}
				else{
					$transfer->source_emp_id=$employee->id;
					$transfer->transport=$request->transport;
					$transfer->transport_type=$request->transport_type;
					$transfer->vehicle=$request->vehicle;
					$transfer->source_godown = $request->source_godown;
				}

				if($transfer->status==1)
					$transfer->status=2;
				else if($transfer->status==2)
					$transfer->status=3;

				//$transfer->status=$transfer->status+1;
				if($transfer->status>3)
					$transfer->status=3;

				$transfer->save();
				$nav_ids=array();
				foreach ($details as $td) {
					$nav_ids[]=$td['nav_no'];
					$detail=TransferDetails::where('transfer_id',$transfer_id)->where('nav_no',$td['nav_no'])->first();
					if($detail!=null && ($employee->role_id==4 || $employee->role_id==14)){
						$detail->management_quantity=$td['quantity'];
						$detail->save();
					}
					else if($detail!=null){
						$detail->final_quantity=$td['quantity'];
						$detail->save();
					}
					else{
						$data= new TransferDetails();  
						$data->transfer_id = $transfer_id;
						$data->nav_no = $td['nav_no'];
						$data->item = $td['item'];
						$data->priority = $td['priority'];
						$data->quantity = 0;
						if($employee->role_id==14 || $employee->role_id==4)
							$data->management_quantity=$td['quantity'];
						else
							$data->final_quantity=$td['quantity'];
						$data->save();
					}
				}
				if($employee->role_id==14 || $employee->role_id==4)
					TransferDetails::where('transfer_id',$transfer_id)->whereNotIn('nav_no',$nav_ids)->update(['management_quantity'=>0]);
				else
					TransferDetails::where('transfer_id',$transfer_id)->whereNotIn('nav_no',$nav_ids)->update(['final_quantity'=>0]);

				if($transfer->status==2){
					$data=array('transfer_id'=>$transfer_id);
            				Mail::to([Branch::where('id',$transfer->source_branch)->value('email')])->send(new NewStockNotification($data));
				}
				else if($transfer->status==3){
					$data=array('transfer_id'=>$transfer_id);
            				Mail::to(['billing@kisankraft.com','admin.sales2@kisankraft.com','admin.sales1@kisankraft.com'])->send(new NewStockNotification($data));
				}
				return response()->json(['status'=>true,'color'=>'#19c3aa','msg'=>'Transfer Request successfully Added.']);
			}
		}


		try {
		$input= new Transfer();  
		$input->emp_id = Auth::user()->employee->id;
		$input->req_branch = $request->req_branch;
		$input->req_godown = $request->req_godown;
		$input->source_branch = $request->source_branch;
		$input->source_godown = $request->source_godown;
		$input->location_mode = "";
		$input->transport=$request->transport;
		$input->transport_type=$request->transport_type;
		$input->tracking_no=$this->GenerateTrackingNo();
		$input->save();
		$id = $input->id;
		} catch(Exception $e){

		}

		foreach ($details as $td) {
		$data= new TransferDetails();  
		$data->transfer_id = $id;
		$data->nav_no = $td['nav_no'];
		$data->item = $td['item'];
		$data->quantity = $td['quantity'];
		$data->priority = $td['priority'];
		$data->save();
		}

		$this->notifyManagement("HELLO");
		return response()->json(['status'=>true,'color'=>'#19c3aa','head'=>'Sucess']);
	}

	public function requestPage(Request $request){
        $branches=Branch::where('status',1)->get();
        $data=array();
        $transfer=array();
        $reqGodown=Godown::where('branch_id',Auth::user()->employee->branch_id)->get(['id','code']);
        $sourceGodown=Godown::where('branch_id',1)->get(['id','code']);
        $shipping=Shipping::where('status',1)->where('branch_id',1)->where('name','Transport')->orderBy('agency')->get();
        return view('transfer.create')->with(compact(['branches','data','transfer','sourceGodown','reqGodown','shipping']));
	}

	public function printTransferReq(Request $request){
		$transfer=Transfer::where('id',$request->transfer_id)->first();
		$details=TransferDetails::where('transfer_id',$request->transfer_id)->get();
		return view('transfer.print')->with(compact(['transfer','details']));
	}

	public function printTransferBill(Request $request){
		$transfer=Transfer::where('id',$request->transfer_id)->first();
		$details=TransferDetails::where('transfer_id',$request->transfer_id)->where('final_quantity','>',0)->get();
		return view('transfer.print_bill')->with(compact(['transfer','details']));
	}

	public function rejectTransfer(Request $req){
		Transfer::where('id',$req->transfer_id)->update(['status'=>4]);
		return response()->json(['status'=>true,'msg'=>'Transfer rejected successfully']);
	}


	public function getTransferDetails(Request $request){
		$branches=Branch::where('status',1)->get();
		$transfer=Transfer::where('id',$request->transfer_id)->first();
		$data = TransferDetails::where('transfer_id','=',$request->transfer_id)->get();
		$sourceGodown=Godown::where('branch_id',$transfer->source_branch)->get(['id','code']);
		$reqGodown=Godown::where('branch_id',$transfer->req_branch)->get(['id','code']);
		$shipping=Shipping::where('status',1)->where('branch_id',$transfer->source_branch)->where('name',$transfer->transport_type)->orderBy('agency')->get();
		return view('transfer.create')->with(compact('branches','data','transfer','sourceGodown','reqGodown','shipping'));
	}

	public function listPage(Request $request){
        $branches=Branch::where('status',1)->get();
        return view('transfer.create')->with(compact(['branches']));
	}

	public function viewPage(Request $request){
        $branches=Branch::where('status',1)->get();
        return view('transfer.create')->with(compact(['branches']));
	}


	public function getSearchedProducts(Request $req){
        //Log::info($req);
        $part=$req->part;
        $category=$req->type;
        $partArray=explode(' ', $part);
        $condition="";
        for ($i=0; $i < COUNT($partArray); $i++) { 
            $condition=$condition."sku LIKE '%$partArray[$i]%' AND ";
        }
        $condition=substr_replace($condition, "", -4);
        $query="";
        $table="machine_skus";
        if($category==1)
        	$table="accessories_skus";
        else if($category==2)
        	$table="parts";
        if(COUNT($partArray)>1)
            $query="SELECT nav_no as value,sku as label FROM (SELECT id,sku,nav_no FROM $table WHERE ($condition) AND status=1) item LIMIT 50";
        else
            $query="SELECT  nav_no as value,sku as label FROM (SELECT id,sku,nav_no FROM $table WHERE (sku LIKE '%$part%' OR nav_no LIKE '%$part%' ) AND status=1) item LIMIT 50";
        //Log::info($query);
        return DB::select($query);

    }

    public function getGodownList(Request $request){
    	$godowns=Godown::where('branch_id',$request->branch_id)->get(['id','code']);
    	return response()->json(['data'=>$godowns]);
    }

     public function getTransfers(Request $request) {
		$emp=Auth::user()->employee;
		$status=1;
		if($emp->role_id==4 || $emp->role_id==14){
			$status=1;
			$data = Transfer::where('status',$status)->orderBy('id','DESC')->paginate(10);
		}
		else if($emp->role_id==7 || $emp->role_id==19){
			$status=2;
			$data = Transfer::where('status',$status)->where('source_branch',$emp->branch_id)->orderBy('id','DESC')->paginate(10);
		}
		else{
			$status=3;
			$data = Transfer::where('status',$status)->orderBy('id','DESC')->paginate(10);

		}

		$isNonEditable=false;
		
		return view('transfer.transfer_list')->with(compact('data','isNonEditable'));
	 }


	 public function viewTransferHistory(Request $request){
	 	$emp=Auth::user()->employee;
		$status=1;
		if($emp->role_id==4 || $emp->role_id==14){
			$status=1;
			$data = Transfer::orderBy('id','DESC')->paginate(10);
		}
		else if($emp->role_id==7  || $emp->role_id==19){
			$status=2;
			$data = Transfer::where('source_branch',$emp->branch_id)->orWhere('req_branch',$emp->branch_id)->orderBy('id','DESC')->paginate(10);
		}
		else{
			$status=3;
			if($emp->role_id==6){
				$data=Transfer::leftJoin('transfer_details','transfers.id','=','transfer_details.transfer_id')->where('transfers.status','=','3')->where('transfer_details.item','NOT LIKE','P:%')->select(DB::raw("transfers.*"))->groupBy('transfers.id')->orderBy('transfers.updated_at','DESC')->paginate(10);
			}
			else if($emp->role_id==20){
				$data=Transfer::leftJoin('transfer_details','transfers.id','=','transfer_details.transfer_id')->where('transfers.status',$status)->where('transfer_details.item','LIKE','P:%')->select(DB::raw("transfers.*"))->groupBy('transfers.id')->orderBy('transfers.updated_at','DESC')->paginate(10);
			}
			//$data = Transfer::where('status',$status)->orderBy('id','DESC')->paginate(10);
			/*$data=Transfer::leftJoin('transfer_details','transfers.id','=','transfer_details.transfer_id')->where('transfers.status',$status)->orderBy('transfers.id','DESC')->select(DB::raw("transfers.*"))->paginate(10);*/
		}

		$isNonEditable=true;
		return view('transfer.transfer_list')->with(compact('data','isNonEditable'));
	 }


	

	public function update(Request $request){
		if($request->isEdited == true){
			if($request->isFrom == 'Management'){
				try {
				$input = Transfer::find($request->id);
				$input->source_branch = $request->source_branch;
				$input->status = 3;
				$input->save();
				} catch(Exception $e){

				}
				foreach ($request->transfer_details as $td) {
					$item = DB::select("select nav_no from transfer_details where nav_no=$td->nav_no");

					if($item){
						$data = TransferDetails::find($td->id);
						$data->management_quantity = $td->quantity;
					}else{
						$data= new TransferDetails();  
						$data->transfer_id = $request->id;
						$data->nav_no = $td->nav_no;
						$data->item = $td->item;
						$data->quantity = $td->quantity;
						$data->type = $td->type;
						$data->save();
					}
				}

				}else {
				foreach ($request->transfer_details as $td) {
				$data = TransferDetails::find($td->id);
				$data->final_quantity = $td->quantity;
				}
			}
		}
		else{
			if($request->isFrom == "Management") {
				$input = Transfer::find($request->id);
				$input->status = 2;
				$input->save();
			}
		}
	}


	function notifyManagement($data){
            $url = 'https://fcm.googleapis.com/fcm/send';
                 $fields = array (
                         'to' => '/topics/Transfer',
                         'data' => $data
                 );
                 $fields = json_encode ( $fields );
                 $headers = array (
                         'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
                         'Content-Type: application/json'
                 );
                 $ch = curl_init ();
                 curl_setopt ( $ch, CURLOPT_URL, $url );
                 curl_setopt ( $ch, CURLOPT_POST, true );
                 curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
                 curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
                 curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
                 $result = curl_exec ( $ch );
                 curl_close ( $ch );
     }


    public function GenerateTrackingNo()
    {
        $now = Carbon::now();
        $tracking_no = $now->format('Y.m.d');
        $date=Carbon::now()->format('Y-m-d');
        $transfer = Transfer::where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)->orderBy('id', 'desc')->first();
        if($transfer != null && $transfer->tracking_no!=null)
        {
            $order_flag = explode(".", $transfer->tracking_no);
            $current_tracking_no = $order_flag[3]+1;
            $current_tracking_no = str_pad( $current_tracking_no, 5, "0", STR_PAD_LEFT );
            $tracking_no = "".$tracking_no.".".($current_tracking_no);
        }
        else
        {
            $tracking_no = "".$tracking_no.".00001";
        }
        return $tracking_no;
    }
}
