<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\IssuedInTheFuture;
use App\Models\User;
use App\Models\Email;
use App\Models\Employee;
use App\Models\Notification;
use App\Models\Dealer;
use Illuminate\Support\Facades\Auth;
use Log;
use Hash;
use Illuminate\Support\Facades\Input;
use ApiConsumer;
use Lcobucci\JWT\Parser;
use Kreait\Firebase;
use DB;


class TestController extends Controller{

	public function getCurrentUserEmail(Request $request)

	{

		if(Auth::user())

		{

			return Auth::user()->email;

		}

	}



	public function sendTokenToServer(Request $request)
	{

		//Log::info("receving token ".$request);

		if (Email::where('email', $request->email)->where('status',1)->count() != 0) {

		  	Email::where('email', $request->email)->where('status',1)->update(['fcm_token'=>$request->token]);

		  	$role = Employee::where('email_id', Email::where('email',$request->email)->value('id'))->value('role_id');
		  	
			$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

			$firebase = (new Factory)
		    ->withServiceAccount($serviceAccount)
		    ->create();

		  	switch($role)
		  	{
		  		case '1' :
		  		break;

		  		case '2' :
		  		break;

		  		case '3' :
		  		break;

		  		case '4' :
		  			$topic = 'Management';
					$registrationTokens = [ 
						$request->token
					];
					$firebase
					    ->getMessaging()
					    ->subscribeToTopic($topic, $registrationTokens);
					Log::info("Management Subscription Done");
		  		break;

		  		case '5' :
		  			$topic = 'Payment_Approver';
					$registrationTokens = [ 
						$request->token
					];
					$firebase
					    ->getMessaging()
					    ->subscribeToTopic($topic, $registrationTokens);
					Log::info("Payment Approver Subscription Done");
		  		break;

		  		case '6' :
		  		break;

		  		case '7' :
		  		break;
		  	}


		  	return "success";

		}

	}



	public function CheckUser(Request $request)	

	{

		//Verfiy if user email exists in the database

		//Log::info($request->user_email);

		if (Email::where('email', $request->user_email)->where('status',1)->count() == 0) {

		  	return "failure";

		}

		//Verify user token with firebase server (if email exists)

		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

		$firebase = (new Factory)

		    ->withServiceAccount($serviceAccount)

		    ->create();

		$idTokenString = $request->token;

		try {

		    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString,false);

			Log::info("Token Verified");

		} catch (IssuedInTheFuture $e) {

			Log::info("Token Verified due to IssuedInTheFuture Exception");

		    $verifiedIdToken = (new Parser())->parse($idTokenString);

		}



		$uid = $verifiedIdToken->getClaim('sub');

		$user = $firebase->getAuth()->getUser($uid);

		Log::info($uid);



		//Check if user password is created

		if(Email::where('email',$user->email)->value('isDealer') == 1)

		{

			$reg_user = Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->get();

		}

		else

		{

			$reg_user = Employee::where('email_id', Email::where('email',$user->email)->value('id'))->get();
			User::where('email',$user->email)->update(['password'=>(Hash::make($uid))]);
		}



		if (User::where('email',$user->email)->count() == 0){

			try{

		          $new_user = User::create([

		          			'name' => $user->displayName,

		                    'email' => $user->email,

		                    'password' => Hash::make($uid),

		                    ]);

		          Log::info($new_user);

		        }

		        catch(\Exception $e)

		        {

		            return $e->getMessage();

		        }



		        if(Email::where('email',$user->email)->value('isDealer') == 1)

				{

					Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$new_user->id]);

				}

				else

				{

					Employee::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$new_user->id]);
					
				}

		}

		Auth::attempt(['email' => $user->email, 'password' => $uid]);

	}	



	public function verifyEmail(Request $request)

	{

		$userEmail = Input::all();

		Log::info($userEmail);

		if (Email::where('email', $userEmail[0])->count() > 0) {

		  	return [

            	'status' => true

        	];

		}

		else

			return [

	            'status' => false

	        ];

	}



	public function getAccessToken(Request $request)

	{

		$data = Input::all();
		//Log::info($data);


		//Verify user token with firebase server (if email exists)

		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/google-service-account.json');

		$firebase = (new Factory)

		    ->withServiceAccount($serviceAccount)

		    ->create();

		$idTokenString = $data[0];

		try {

		    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString,false);

			Log::info("Token Verified");

		} catch (IssuedInTheFuture $e) {

			Log::info("Token Verified due to IssuedInTheFuture Exception");

		    $verifiedIdToken = (new Parser())->parse($idTokenString);

		}

		// Get User Firebase Id (if user token verified with server)

		$uid = $verifiedIdToken->getClaim('sub');

		$user = $firebase->getAuth()->getUser($uid);



		//Check if user password is created

		Log::info($user->email);

		if(Email::where('email',$user->email)->value('isDealer') == 1)

		{

			$reg_user = Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->get();

		}

		else

		{

			$reg_user = Employee::where('email_id', Email::where('email',$user->email)->value('id'))->get();
			User::where('email',$user->email)->update(['password'=>(Hash::make($uid))]);

		}

		

		if (User::where('email',$user->email)->count() == 0){

			try{

		          $user_info = User::create([

		          			'name' => $user->displayName,

		                    'email' => $user->email,

		                    'password' => Hash::make($uid),

		                    ]);

		        }

		        catch(\Exception $e)

		        {

		            Log::info($e->getMessage());
		            return $e->getMessage();

		        }

		        if(Email::where('email',$user->email)->value('isDealer') == 1)

				{

					Dealer::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$user_info->id]);

				}

				else

				{

					Employee::where('email_id', Email::where('email',$user->email)->value('id'))->update(['login_id'=>$user_info->id]);

				}

		}

		//Log::info($user->email." Access Token Generating");



		$request->request->add([

              'client_id' => '2',

              'client_secret' => 'yfMBM7dNV1AduHNRaC8soGN9R5lwhXU3CUp7NESA',

              'grant_type' => 'password',

              'username' => $user->email,

              'password' => $uid

            ]);

            $tokenRequest = $request->create('/oauth/token', 'POST', $request->all());
            

            $token =  \Route::dispatch($tokenRequest);

            $data = $token->getContent();

            $access_token = json_decode($data, true);

            //Log::info($user->email." Access Token Generated");
            

        	return [

	            'access_token' => $access_token['access_token'],

	            'reg_userId' => $reg_user[0]->id,

	            'role_id' => $reg_user[0]->role_id

	        ];

	}


}











