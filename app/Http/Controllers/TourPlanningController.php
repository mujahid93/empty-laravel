<?php

namespace App\Http\Controllers;

use App\Models\Dispatch;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Order;
use App\Models\Dealer;
use App\Models\District;
use App\Models\DealerVisit;
use App\Models\Employee;
use App\Models\Tour;
use App\Models\DemoPlan;
use App\Models\TourItem;
use App\Models\Expense;
use App\Models\ExpenseItem;
use App\Exports\DealerVisitExport;
use Illuminate\Support\Facades\Auth;  
use Session;
use Log;
use Redirect;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;

class TourPlanningController extends Controller
{
    //
    public function saveTourPlan(){

        $data=Input::all();
        //Log::info($data);
        if(COUNT($data)==0){
            //No Data Found
            return array('status'=>false);  
        }
        $emp_id=0;
        $token="";
        DB::beginTransaction();
         try{
            $usrData=$data['tourPlanningData'][0];
            $transaction=$this->generateTransactionNo($usrData['emp_id'],"TP");
            //Adding Tour Plan
            foreach ($data['tourPlanningData'] as $key => $value) {
                $tour=new Tour();
                $tour->date=$value['date'];
                $tour->emp_id=$value['emp_id'];
                $tour->state_id=$value['stateId'];
                if($value['districtId']==-1)
                    $tour->district_id=null;
                else
                    $tour->district_id=$value['districtId'];
                $tour->district_name=$value['districtName'];
                $tour->city=$value['cityName'];
                $tour->localExps=$value['localExps'];
                $tour->lodgingExps=$value['lodgingExps'];
                $tour->mealExps=$value['mealExps'];
                $tour->miscExps=$value['miscExps'];
                $tour->outstationExps=$value['outstationExps'];
                $tour->token=$value['token'];
                $tour->transaction_code=$transaction;
                $tour->save();
                foreach ($value['dealerData'] as $key => $item) {
                    $tourItem=new TourItem();
                    $tourItem->tour_id=$tour->id;
                    if($item['id']==-1)
                        $tourItem->dealer_id=null;
                    else
                        $tourItem->dealer_id=$item['id'];
                    $tourItem->dealer_name=$item['name'];
                    $tourItem->save();
                }
                $emp_id=$tour->emp_id;
                $token=$tour->token;
            }
         }
         catch(\Exeption $e){
            DB::rollback();
            return array('status'=>false);
         }
         app('App\Http\Controllers\NotifController')->sendTourNotification($emp_id,$token);
         DB::commit();
        return array('status'=>true);
    }


    public function saveUpdatedTourPlan(){
        $data=Input::all();
        if(COUNT($data)==0){
            //No Data Found
            return array('status'=>false);  
        }
        $emp_id=0;
        $token="";
        DB::beginTransaction();
         try{

            //Deleting previous entries
            $usrData=$data['tourPlanningData'][0];
            //Log::info($usrData);
            $tour_id=Tour::where('emp_id',$usrData['emp_id'])->where('token',$usrData['token'])->pluck('id');
            TourItem::whereIn('tour_id',$tour_id)->delete();
            Tour::whereIn('id',$tour_id)->delete();
            $transaction=$this->generateTransactionNo($usrData['emp_id'],"TP");

            //Adding Tour Plan
            foreach ($data['tourPlanningData'] as $key => $value) {
                $tour=new Tour();
                $tour->date=$value['date'];
                $tour->emp_id=$value['emp_id'];
                $tour->state_id=$value['stateId'];
                if($value['districtId']==-1)
                    $tour->district_id=null;
                else
                    $tour->district_id=$value['districtId'];
                $tour->district_name=$value['districtName'];
                $tour->city=$value['cityName'];
                $tour->localExps=$value['localExps'];
                $tour->lodgingExps=$value['lodgingExps'];
                $tour->mealExps=$value['mealExps'];
                $tour->miscExps=$value['miscExps'];
                $tour->outstationExps=$value['outstationExps'];
                $tour->token=$value['token'];
                $tour->status=4;
                $tour->manager_id=Auth::user()->employee->id;
                $tour->save();
                foreach ($value['dealerData'] as $key => $item) {
                    $tourItem=new TourItem();
                    $tourItem->tour_id=$tour->id;
                    if($item['id']==-1)
                        $tourItem->dealer_id=null;
                    else
                        $tourItem->dealer_id=$item['id'];
                    $tourItem->dealer_name=$item['name'];
                    $tourItem->save();
                }
                $emp_id=$tour->emp_id;
                $token=$tour->token;
            }
         }
         catch(\Exeption $e){
            DB::rollback();
            return array('status'=>false);
         }
         DB::commit();
         app('App\Http\Controllers\NotifController')->sendTourNotification($emp_id,$token);
        return array('status'=>true);
    }

    public function getTourPlanList(){
         $emp_id=Auth::user()->employee->id;
        $data=DB::select("SELECT employees.id as emp_id,employees.name,MIN(plan.date) as start_date,MAX(plan.date) as end_date,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,token FROM tour_plan plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE plan.status=1 AND plan.emp_id IN (SELECT id FROM employees WHERE manager_id='$emp_id' AND role_id NOT IN (17,18)) GROUP BY plan.emp_id,plan.token");
        return $data;
    }

    public function getTourSummary(Request $req,$emp_id,$token){
        $data=DB::select("SELECT employees.id AS emp_id, employees.name, MIN(plan.date) AS start_date, MAX(plan.date) AS end_date, token, GROUP_CONCAT(DISTINCT (states.name) SEPARATOR ',') AS state, GROUP_CONCAT(DISTINCT (plan.district_name) SEPARATOR ',') AS district, GROUP_CONCAT(DISTINCT (plan.city) SEPARATOR ',') AS city, SUM(localExps) AS localExps, SUM(lodgingExps) AS lodgingExps, SUM(mealExps) AS mealExps, SUM(outstationExps) AS outstationExps, SUM(miscExps) AS miscExps, SUM(localExps + lodgingExps + mealExps + miscExps + outstationExps) AS cost, (SELECT GROUP_CONCAT(dealer_name SEPARATOR ',') AS dealers FROM tour_plan_items LEFT JOIN tour_plan plan ON plan.id=tour_plan_items.tour_id WHERE plan.token='$token' AND emp_id=$emp_id ) as dealers FROM tour_plan plan LEFT JOIN employees ON employees.id = plan.emp_id LEFT JOIN states ON states.id = plan.state_id WHERE employees.id = $emp_id AND plan.token = '$token' GROUP BY plan.emp_id , plan.token");
        if(COUNT($data)>0){
            $data= json_encode($data[0]);
            return $data;
        }
        return $data;
    }

    public function setTourAction(Request $req,$emp_id,$token,$action){
        //Log::info($action);
        if($action==2){
             Tour::where('emp_id',$emp_id)->where('token',$token)->update(['status'=>2,'manager_id'=>Auth::user()->employee->id]);
             app('App\Http\Controllers\NotifController')->sendTourNotification($emp_id,$token);
        }
        else if($action==3){
             Tour::where('emp_id',$emp_id)->where('token',$token)->update(['status'=>3,'manager_id'=>Auth::user()->employee->id]);
             app('App\Http\Controllers\NotifController')->sendTourNotification($emp_id,$token);
            }
        return array('status'=>true);
    }

    public function index(){

    }

    public function create(){

    }

    public function testNotif(){
        app('App\Http\Controllers\NotifController')->sendTourNotification(1007,'1561022391449');
        return "TEST";
    }

    public function pendingTour(){
        $items = $request->items ?? 10;
        $user=Auth::user()->employee;
        if($user->role_id==4 || $user->role_id==14){
            $tour=Tour::where('status',1)->whereIn('emp_id',Employee::where('manager_id',null)->whereNotIn('role_id',[17,18])->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        else if($user->role_id==18){
            $tour=Tour::where('status',1)->whereIn('emp_id',Employee::whereIn('role_id',[17,18])->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        else{
            $tour=Tour::where('status',1)->whereIn('emp_id',Employee::where('manager_id',$user->id)->pluck('id'))
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        return view('tour.tourlist')->with(compact('tour'));
    }

    public function pendingAllTour(){
        $items = $request->items ?? 10;
        $tour=Tour::where('status',1)
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        
        return view('tour.tourlist')->with(compact('tour'));
    }


    public function getPendingTourApi(Request $req,$emp_id,$token){
        $tour=DB::select("SELECT id as tour_id,token,date,district_name FROM `tour_plan` where emp_id=$emp_id AND token='$token' AND status=1");
        return $tour;
    }

    public function getPendingTourDetailsApi(Request $req,$emp_id,$token){
        $detail=DB::select("SELECT plan.id as tourId,transaction_code,date,state_id,states.name as stateName,COALESCE(plan.district_id,-1) district_id,plan.district_name,city as cityName,localExps,lodgingExps,mealExps,miscExps,outstationExps,token,emp_id,plan.status as status FROM tour_plan plan LEFT JOIN states ON states.id=plan.state_id WHERE plan.emp_id=$emp_id AND plan.token='$token'");

        $detail=json_encode($detail);
        $detail=json_decode($detail,true);

        $response=array();

        for($i=0; $i<COUNT($detail); $i++){

            $tour_id=$detail[$i]['tourId'];

            $dealers=DB::select("SELECT COALESCE(item.dealer_id,-1) as id,item.dealer_name as name FROM tour_plan plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id WHERE plan.id=$tour_id");

            $data=array('date'=>$detail[$i]['date'],
                        'transaction_code'=>$detail[$i]['transaction_code'],
                        'stateId'=>$detail[$i]['state_id'],
                        'stateName'=>$detail[$i]['stateName'],
                        'districtId'=>$detail[$i]['district_id'],
                        'districtName'=>$detail[$i]['district_name'],
                        'cityName'=>$detail[$i]['cityName'],
                        'localExps'=>$detail[$i]['localExps'],
                        'lodgingExps'=>$detail[$i]['lodgingExps'],
                        'mealExps'=>$detail[$i]['mealExps'],
                        'miscExps'=>$detail[$i]['miscExps'],
                        'outstationExps'=>$detail[$i]['outstationExps'],
                        'token'=>$detail[$i]['token'],
                        'emp_id'=>$detail[$i]['emp_id'],
                        'tourId'=>$detail[$i]['tourId'],
                        'status'=>$detail[$i]['status'],
                        'dealerData'=>$dealers);
            array_push($response, $data);
        }

        return $response;
    }

    public function tourDetails(Request $req, $emp_id, $token){
        $items = $request->items ?? 10;
        $data=DB::select("SELECT employees.id as emp_id,employees.name,MAX(plan.date) as start_date,MIN(plan.date) as end_date,token,GROUP_CONCAT(DISTINCT(states.name) SEPARATOR ',') as state,GROUP_CONCAT(DISTINCT(plan.district_name) SEPARATOR ',') as district,GROUP_CONCAT(DISTINCT(plan.city) SEPARATOR ',') as city,COUNT(item.dealer_name) as dealersQty,GROUP_CONCAT(item.dealer_name SEPARATOR ',') as dealers,SUM(localExps) as localExps,SUM(lodgingExps) as lodgingExps,SUM(mealExps) as mealExps,SUM(outstationExps) as outstationExps,SUM(miscExps) as miscExps,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost FROM tour_plan plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id LEFT JOIN employees ON employees.id=plan.emp_id LEFT JOIN states ON states.id=plan.state_id WHERE employees.id=$emp_id AND plan.token='$token' GROUP BY plan.emp_id,plan.token");

        $tour=DB::select("SELECT plan.id as tour_id,plan.date,plan.token,states.name as state,plan.district_name,plan.city,(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expenses  FROM tour_plan plan LEFT JOIN states ON states.id=plan.state_id WHERE emp_id=$emp_id AND token='$token'");
        return view('tour.details')->with(compact('tour','data'));
    }


    public function tourApprove(Request $req){
        Tour::where('emp_id',$req->emp_id)->where('token',$req->token)->update(["status"=>2,'manager_id'=>Auth::user()->employee->id]);
        app('App\Http\Controllers\NotifController')->sendTourNotification($req->emp_id,$req->token);
        return redirect()->route('pendingTour');
    }

    public function tourReject(Request $req){
        Tour::where('emp_id',$req->emp_id)->where('token',$req->token)->update(["status"=>3,'manager_id'=>Auth::user()->employee->id]);
        app('App\Http\Controllers\NotifController')->sendTourNotification($req->emp_id,$req->token);
        return redirect()->route('pendingTour');
    }

    public function tourUpdate(Request $req){
        $tour=Tour::where('id',$req->tour_id)->first();
        $tour->outstationExps=$req->outstationExps;
        $tour->localExps=$req->localExps;
        $tour->lodgingExps=$req->lodgingExps;
        $tour->mealExps=$req->mealExps;
        $tour->miscExps=$req->miscExps;
        $tour->save();
        $data=DB::select("SELECT employees.id as emp_id,employees.name,MAX(plan.date) as start_date,MIN(plan.date) as end_date,token,GROUP_CONCAT(DISTINCT(states.name) SEPARATOR ',') as state,GROUP_CONCAT(DISTINCT(plan.district_name) SEPARATOR ',') as district,GROUP_CONCAT(DISTINCT(plan.city) SEPARATOR ',') as city,COUNT(item.dealer_name) as dealersQty,GROUP_CONCAT(item.dealer_name SEPARATOR ',') as dealers,SUM(localExps) as localExps,SUM(lodgingExps) as lodgingExps,SUM(mealExps) as mealExps,SUM(outstationExps) as outstationExps,SUM(miscExps) as miscExps,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost FROM tour_plan plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id LEFT JOIN employees ON employees.id=plan.emp_id LEFT JOIN states ON states.id=plan.state_id WHERE employees.id=$req->emp_id AND plan.token='$req->token' GROUP BY plan.emp_id,plan.token");

        $tour=DB::select("SELECT plan.id as tour_id,plan.date,plan.token,states.name as state,plan.district_name,plan.city,(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expenses  FROM tour_plan plan LEFT JOIN states ON states.id=plan.state_id WHERE emp_id=$req->emp_id AND token='$req->token'");
        return view('tour.details')->with(compact('tour','data'));
    }

    public function tourEdit(Request $req,  $tour_id){
        $tour=DB::select("SELECT plan.emp_id,plan.token,employees.name as empName,plan.date,plan.id as tour_id,plan.date,plan.token,states.name as state,plan.district_name,plan.city,(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expenses,localExps,lodgingExps,mealExps,miscExps,outstationExps  FROM tour_plan plan LEFT JOIN states ON states.id=plan.state_id LEFT JOIN employees ON employees.id=plan.emp_id WHERE plan.id=$tour_id");

        $dealers=TourItem::where('tour_id',$tour_id)->get();
        return view('tour.edit')->with(compact('tour','dealers'));
    }

    public function getTourStatus(Request $req, $emp_id){
        $date=Carbon::now()->format('Y-m-d');
        $data=DB::select("SELECT plan.token,plan.status FROM `tour_plan` plan LEFT JOIN tour_plan_items item ON plan.id=item.tour_id WHERE plan.date>='$date' AND plan.emp_id='$emp_id' GROUP BY plan.token");
        //Log::info($data);
        return $data;
    }

    public function approvedTour(){
        $items = $request->items ?? 10;
        $emp=Auth::user()->employee;
        if($emp->role_id==4 || $emp->role_id==14){
            $tour=Tour::whereIn('status',[2,4])
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token,is_deleted"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        else{
            $tour=Tour::whereIn('status',[2,4])
            ->select(DB::raw("CONCAT(DATE_FORMAT(MIN(date),'%d-%M-%Y'),'---',DATE_FORMAT(MAX(date),'%d-%M-%Y')) as date,emp_id,GROUP_CONCAT(DISTINCT(district_name)) as district_name,token,is_deleted"))
            ->groupBY('token')->orderBy('id','DESC')->paginate($items);
        }
        return view('tour.approved')->with(compact('tour'));
    }

    public function tourPrint(Request $req, $emp_id, $token){
        $tour=Tour::whereIn('status',[2,4])->where('emp_id',$emp_id)->where('token',$token)->get();
        return view('tour.tourPrint')->with(compact('tour'));
    }

    public function tourDelete(Request $req, $emp_id, $token,$action){
        Tour::whereIn('status',[2,4])->where('emp_id',$emp_id)->where('token',$token)->update(['is_deleted'=>$action]);
        return redirect()->route('approvedTour');
    }


    public function getTourTrackList(Request $req, $date,$page){
        $emp=Auth::user()->employee;
        $offset=$page*15;
        if($emp->role_id==4 || $emp->role_id==14){
            $dealerVisit=DB::select("SELECT employees.id as emp_id, employees.name,DATE(COALESCE(check_in,dealer_visit.created_at)) as start_date FROM `dealer_visit` LEFT JOIN employees ON employees.id=dealer_visit.emp_id WHERE DATE(COALESCE(check_in,dealer_visit.created_at))='$date' GROUP BY DATE(COALESCE(check_in,dealer_visit.created_at)) LIMIT 15  OFFSET $offset");
            return $dealerVisit;
        }
        else{
            $query="SELECT employees.id as emp_id, employees.name,DATE(COALESCE(check_in,dealer_visit.created_at)) as start_date FROM `dealer_visit` LEFT JOIN employees ON employees.id=dealer_visit.emp_id WHERE DATE(COALESCE(check_in,dealer_visit.created_at))='$date' AND emp_id IN (SELECT id FROM employees WHERE manager_id='$emp->id') GROUP BY DATE(COALESCE(check_in,dealer_visit.created_at)) LIMIT 15 OFFSET $offset";
            //Log::info($query);
            $dealerVisit=DB::select($query);
            return $dealerVisit;
        }
    }

    public function getTourTrackDetails(Request $req, $date,$emp_id){
        $tourDistrict=DB::select("SELECT GROUP_CONCAT(DISTINCT(district_name)) as district FROM tour_plan WHERE emp_id=$emp_id AND date='$date'");
        $visitDistrict=DB::select("SELECT GROUP_CONCAT(DISTINCT(district_name)) as district FROM dealer_visit WHERE emp_id=$emp_id AND DATE(COALESCE(check_in,dealer_visit.created_at))='$date'");
        $tour=DB::select("SELECT item.dealer_name FROM `tour_plan` plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id WHERE emp_id=$emp_id AND date='$date' AND plan.status IN (2,4)");
        $dealerVisit=DB::select("SELECT dealer_name FROM `dealer_visit` WHERE emp_id=$emp_id AND DATE(COALESCE(check_in,dealer_visit.created_at))='$date'");
        $response = array('date' => $date,
                           'scheduleDistrict'=>$tourDistrict[0]->district,
                           'actualDistrict'=>$visitDistrict[0]->district,
                           'tour' => $tour,
                          'visit' => $dealerVisit );
        return $response;
    }


    public function generateTransactionNo($emp_id,$type){
        $tracking=$type.$emp_id.Carbon::now()->month."0";
        $tour=Tour::orderBy('id','DESC')->first();
        if($tour==null)
            $tracking=$tracking."1";
        else
            $tracking=$tracking.($tour->id+1);
        return $tracking;
    }

    public function generateExpenseTransactionNo($emp_id,$type){
        $tracking=$type.$emp_id.Carbon::now()->month."0";
        $exp=Expense::orderBy('id','DESC')->first();
        if($exp==null)
            $tracking=$tracking."1";
        else
            $tracking=$tracking.($exp->id+1);
        return $tracking;
    }

    public function getRecentTours(Request $req,$emp_id,$page){
        $offset=$page*15;
        $data=DB::select("SELECT * FROM (SELECT transaction_code,MIN(date) as startDate,MAX(date) as endDate,token,'0' as type FROM `tour_plan` WHERE emp_id=$emp_id AND transaction_code is NOT NULL AND completed=0 AND status IN (2,4) GROUP BY token UNION ALL SELECT transaction_code,MIN(date) as startDate,MAX(date) as endDate,token,'1' as type FROM demo_plans WHERE emp_id=$emp_id AND transaction_code is NOT NULL AND completed=0 AND status IN (2,4) GROUP BY token) tour LIMIT 15 OFFSET $offset");
        return $data;
    }



    public function saveTourExpense(){

        $data=Input::all();
        //Log::info($data);
        if($data==null || COUNT($data['tourPlanningData'])==0){
            //No Data Found
            return array('status'=>false);  
        }
        $emp_id=0;
        $token="";
        $tour_code="";
        DB::beginTransaction();
         try{
            $usrData=$data['tourPlanningData'][0];
            $transaction=$this->generateExpenseTransactionNo($usrData['emp_id'],"EXP");
            //Adding Tour Plan
            foreach ($data['tourPlanningData'] as $key => $value) {
                $tour=new Expense();
                $tour->date=$value['date'];
                $tour->emp_id=$value['emp_id'];
                if($value['stateId']==-1)
                    $tour->state_id=null;
                else
                    $tour->state_id=$value['stateId'];
                if($value['districtId']==-1)
                    $tour->district_id=null;
                else
                    $tour->district_id=$value['districtId'];
                $tour->district_name=$value['districtName'];
                $tour->city=$value['cityName'];
                $tour->startTime=$value['startTime'];
                $tour->endTime=$value['endTime'];
                $tour->localExps=$value['localExps'];
                $tour->mealExps=$value['mealExps'];
                $tour->lodgingExps=$value['lodgingExps'];
                $tour->miscExps=$value['miscExps'];
                $tour->outstationExps=$value['outstationExps'];
                if(array_key_exists('localExpComment', $value)){
                    $tour->localExpCmt=$value['localExpComment'];
                    $tour->lodgExpCmt=$value['lodgingExpComment'];
                    $tour->mealExpCmt=$value['mealExpComment'];
                    $tour->miscExpCmt=$value['miscExpComment'];
                    $tour->outExpCmt=$value['outstationExpComment'];
                }
                $tour->token=$value['token'];
                $tour->type=$value['type'];
                $tour->transaction_code=$transaction;
                $tour->tour_code=$value['transaction_code'];
				if(array_key_exists("empData",$value)){
					$tour->other_emp=implode(',', $value['empData']);
				}
                $tour->status=1;
                $tour_code=$value['transaction_code'];
                $tour->save();
                foreach ($value['dealerData'] as $key => $item) {
                    $tourItem=new ExpenseItem();
                    $tourItem->tour_id=$tour->id;
                    if($value['type']==1){
                        $item['id']==-1;
                    }
                    if($item['id']==-1)
                        $tourItem->dealer_id=null;
                    else
                        $tourItem->dealer_id=$item['id'];
                    $tourItem->dealer_name=$item['name'];
                    $tourItem->save();
                }
                $emp_id=$tour->emp_id;
                $token=$tour->token;
            }
            Tour::where('transaction_code',$tour_code)->update(["completed"=>1]);
            DemoPlan::where('transaction_code',$tour_code)->update(["completed"=>1]);
         }
         catch(\Exeption $e){
            DB::rollback();
            Log::info("Exeption is expense");
            return array('status'=>false);
         }
         DB::commit();
        return array('status'=>true);
    }



    public function getTourDetailsApi(Request $req,$emp_id,$token,$type){
        if($type==0){
            $detail=DB::select("SELECT plan.id as tourId,transaction_code,date,state_id,states.name as stateName,COALESCE(plan.district_id,-1) district_id,plan.district_name,city as cityName,localExps,lodgingExps,mealExps,miscExps,outstationExps,localExpCmt,lodgExpCmt,mealExpCmt,miscExpCmt,outExpCmt,token,emp_id,plan.status as status FROM tour_plan plan LEFT JOIN states ON states.id=plan.state_id WHERE plan.emp_id=$emp_id AND plan.token='$token'");
        }
        else{
            $detail=DB::select("SELECT plan.id as tourId,transaction_code,'-1' as state_id,'-1' as district_id,date,state as stateName,plan.district as district_name,taluk as cityName,localExps,lodgingExps,mealExps,miscExps,outstationExps,localExpCmt,lodgExpCmt,mealExpCmt,miscExpCmt,outExpCmt,token,emp_id,plan.status as status,subcat_id FROM demo_plans plan WHERE plan.emp_id=$emp_id AND plan.token='$token'");
        }

        $detail=json_encode($detail);
        $detail=json_decode($detail,true);

        $response=array();

        for($i=0; $i<COUNT($detail); $i++){

            $tour_id=$detail[$i]['tourId'];
            if($type==0){
                $dealers=DB::select("SELECT COALESCE(item.dealer_id,-1) as id,item.dealer_name as name FROM tour_plan plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id WHERE plan.id=$tour_id");
            }
            else{
                $subcat_id=$detail[$i]['subcat_id'];
                $dealers=DB::select("SELECT id,subCatName as name FROM `demo_categories` WHERE id IN ($subcat_id)");
            }

            $data=array('date'=>$detail[$i]['date'],
                        'transaction_code'=>$detail[$i]['transaction_code'],
                        'stateId'=>$detail[$i]['state_id'],
                        'stateName'=>$detail[$i]['stateName'],
                        'districtId'=>$detail[$i]['district_id'],
                        'districtName'=>$detail[$i]['district_name'],
                        'cityName'=>$detail[$i]['cityName'],
                        'localExps'=>$detail[$i]['localExps'],
                        'localExpComment'=>$detail[$i]['localExpCmt'],
                        'lodgingExps'=>$detail[$i]['lodgingExps'],
                        'lodgingExpComment'=>$detail[$i]['lodgExpCmt'],
                        'mealExps'=>$detail[$i]['mealExps'],
                        'mealExpComment'=>$detail[$i]['mealExpCmt'],
                        'miscExps'=>$detail[$i]['miscExps'],
                        'miscExpComment'=>$detail[$i]['miscExpCmt'],
                        'outstationExps'=>$detail[$i]['outstationExps'],
                        'outstationExpComment'=>$detail[$i]['outExpCmt'],
                        'token'=>$detail[$i]['token'],
                        'emp_id'=>$detail[$i]['emp_id'],
                        'tourId'=>$detail[$i]['tourId'],
                        'status'=>$detail[$i]['status'],
                        'type'=>$type,
                        'dealerData'=>$dealers);
            array_push($response, $data);
        }

        return $response;
    }

    public function getExpenseList(Request $req){

        //From Date
        if(Session::has('expenseFromDate'))
            $fromDate = Session::get('expenseFromDate');            
        else
            $fromDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');

        //To Date
        if(Session::has('expenseToDate'))
            $toDate = Session::get('expenseToDate');            
        else
            $toDate = \Carbon\Carbon::now()->endOfWeek()->format('Y-m-d');

        //Employee Data
        if(Session::has('expenseEmp')){
            $emp_id = Session::get('expenseEmp');      
        }
        else
            $emp_id =-1;

        $items = $req->items ?? 10;
        if($emp_id==-1)
            $expenses=Expense::whereIn('status',[2,4])->groupBy('token')->orderBy('date','DESC')->select(DB::raw("MIN(date) as startDate,MAX(date) as endDate,emp_id,transaction_code,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expense, token,is_deleted"))
                ->where('date','>=',$fromDate)
                ->where('date','<=',$toDate)
                ->paginate($items);
        else
            $expenses=Expense::whereIn('status',[2,4])->groupBy('token')->orderBy('date','DESC')->select(DB::raw("MIN(date) as startDate,MAX(date) as endDate,emp_id,transaction_code,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expense, token,is_deleted"))
                ->where('date','>=',$fromDate)
                ->where('date','<=',$toDate)
                ->where('emp_id',$emp_id)
                ->paginate($items);
        $employees=Employee::where('status',1)->get();
        //Log::info($expenses);
        return view('expense.expenseList')->with(compact('expenses','employees'));
    }


    public function getPendingExpenseWeb(Request $req){
        $emp=Auth::user()->employee;
        $expense=null;
        $items = $req->items ?? 10;
        if($emp->role_id==4 || $emp->role_id==14){
            $expenses=Expense::where('status',1)->whereIn('emp_id',Employee::where('manager_id',null)->whereNotIn('role_id',[17,18])->pluck('id'))->groupBy('token')->orderBy('date','DESC')->select(DB::raw("MIN(date) as startDate,MAX(date) as endDate,emp_id,transaction_code,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expense, token"))
                ->paginate($items);
        }
        elseif($emp->role_id==18){
            $expenses=Expense::where('status',1)->whereIn('emp_id',Employee::whereIn('role_id',[17,18])->pluck('id'))->groupBy('token')->orderBy('date','DESC')->select(DB::raw("MIN(date) as startDate,MAX(date) as endDate,emp_id,transaction_code,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as expense, token"))
                ->paginate($items);
        }
        return view('expense.pendingExpenseList')->with(compact('expenses'));
    }

    public function getExpenseDetailsWeb(Request $req,$transaction_code){

       /* $data=DB::select("SELECT employees.id as emp_id,employees.name,MAX(plan.date) as start_date,MIN(plan.date) as end_date,token,GROUP_CONCAT(DISTINCT(states.name) SEPARATOR ',') as state,GROUP_CONCAT(DISTINCT(plan.district_name) SEPARATOR ',') as district,GROUP_CONCAT(DISTINCT(plan.city) SEPARATOR ',') as city,COUNT(item.dealer_name) as dealersQty,GROUP_CONCAT(item.dealer_name SEPARATOR ',') as dealers,SUM(localExps) as localExps,SUM(lodgingExps) as lodgingExps,SUM(mealExps) as mealExps,SUM(outstationExps) as outstationExps,SUM(miscExps) as miscExps,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost FROM tour_plan plan LEFT JOIN tour_plan_items item ON item.tour_id=plan.id LEFT JOIN employees ON employees.id=plan.emp_id LEFT JOIN states ON states.id=plan.state_id WHERE employees.id=$emp_id AND plan.token='$token' GROUP BY plan.emp_id,plan.token");*/

        $expense=Expense::where('transaction_code',$transaction_code)->get();
        return view('expense.expenseDetails')->with(compact('expense','data'));
    }

    public function expenseApproveWeb(Request $req){
        $transaction_code=$req->transaction_code;
        $manager_id=Auth::user()->employee->id;
        $expense=Expense::where('transaction_code',$transaction_code)->get();
        //return $req;
        foreach ($expense as $key => $value) {
            Expense::where('id',$value['id'])
                    ->update(["outstationExps"=>$req[$value['id']."-outstationExps"],
                              "localExps"=>$req[$value['id']."-localExps"],
                              "lodgingExps"=>$req[$value['id']."-lodgingExps"],
                              "mealExps"=>$req[$value['id']."-mealExps"],
                              "miscExps"=>$req[$value['id']."-miscExps"],
                              "status"=>4,
                              "manager_id"=>$manager_id]);

        }
        return redirect()->route('getPendingExpense')->with('message','Operation Successful !');
    }

    public function expenseRejectWeb(Request $req){
        $transaction_code=$req->transaction_code;
        $manager_id=Auth::user()->employee->id;
        Expense::where('transaction_code',$transaction_code)->update(["status"=>3,"manager_id"=>$manager_id]);
        return redirect()->route('getPendingExpense')->with('message','Operation Successful !');
    }

    public function printExpense(Request $req, $emp_id,$token){
        $expense=Expense::where('emp_id',$emp_id)->where('token',$token)->get();
        return view('expense.expensePrint')->with(compact('expense'));

    }

    public function blockExpense(Request $req, $emp_id,$token){
        Expense::where('emp_id',$emp_id)->where('token',$token)->update(['is_deleted'=>1]);
        return Redirect::back()->with('message','Operation Successful !');
    }

    public function unblockExpense(Request $req, $emp_id,$token){
        Expense::where('emp_id',$emp_id)->where('token',$token)->update(['is_deleted'=>0]);
        return Redirect::back()->with('message','Operation Successful !');
    }


    public function getPendingExpenseListApi(Request $req,$page){
        $emp_id=Auth::user()->employee->id;
        $offset=$page*15;
        $data= DB::select("SELECT plan.transaction_code,employees.id as emp_id,employees.name,MIN(plan.date) as start_date,MAX(plan.date) as end_date,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,token FROM tour_expense plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE plan.status=1 AND plan.emp_id IN (SELECT id FROM employees WHERE manager_id='$emp_id' AND role_id NOT IN (17,18)) AND plan.status=1 GROUP BY plan.token,plan.emp_id LIMIT 15 OFFSET $offset");
        return $data;
    }

    public function getExpenseDetailsApi(Request $req,$transaction_code){
        $detail=DB::select("SELECT plan.id as tourId,transaction_code,date,state_id,states.name as stateName,COALESCE(plan.district_id,-1) district_id,plan.district_name,city as cityName,localExps,lodgingExps,mealExps,miscExps,outstationExps,localExpCmt,lodgExpCmt,mealExpCmt,miscExpCmt,outExpCmt,token,emp_id,plan.status as status,startTime,endTime,type FROM tour_expense plan LEFT JOIN states ON states.id=plan.state_id WHERE plan.transaction_code='$transaction_code'");

        $detail=json_encode($detail);
        $detail=json_decode($detail,true);

        $response=array();

        for($i=0; $i<COUNT($detail); $i++){

            $tour_id=$detail[$i]['tourId'];

            $dealers=DB::select("SELECT COALESCE(item.dealer_id,-1) as id,item.dealer_name as name FROM tour_expense plan LEFT JOIN tour_expense_items item ON item.tour_id=plan.id WHERE plan.id=$tour_id");

            $data=array('date'=>$detail[$i]['date'],
                        'transaction_code'=>$detail[$i]['transaction_code'],
                        'startTime'=>$detail[$i]['startTime'],
                        'endTime'=>$detail[$i]['endTime'],
                        'stateId'=>$detail[$i]['state_id'],
                        'stateName'=>$detail[$i]['stateName'],
                        'districtId'=>$detail[$i]['district_id'],
                        'districtName'=>$detail[$i]['district_name'],
                        'cityName'=>$detail[$i]['cityName'],
                        'localExps'=>$detail[$i]['localExps'],
                        'localExpComment'=>$detail[$i]['localExpCmt'],
                        'lodgingExps'=>$detail[$i]['lodgingExps'],
                        'lodgingExpComment'=>$detail[$i]['lodgExpCmt'],
                        'mealExps'=>$detail[$i]['mealExps'],
                        'mealExpComment'=>$detail[$i]['mealExpCmt'],
                        'miscExps'=>$detail[$i]['miscExps'],
                        'miscExpComment'=>$detail[$i]['miscExpCmt'],
                        'outstationExps'=>$detail[$i]['outstationExps'],
                        'outstationExpComment'=>$detail[$i]['outExpCmt'],
                        'token'=>$detail[$i]['token'],
                        'emp_id'=>$detail[$i]['emp_id'],
                        'tourId'=>$detail[$i]['tourId'],
                        'status'=>$detail[$i]['status'],
                        'type'=>$detail[$i]['type'],
                        'dealerData'=>$dealers);
            array_push($response, $data);
        }

        return $response;
    }

    public function rejectExpense(Request $req,$transaction_code){
             Expense::where('transaction_code',$transaction_code)->update(['status'=>3,'manager_id'=>Auth::user()->employee->id]);
             app('App\Http\Controllers\NotifController')->sendExpenseNotification($transaction_code);
        return array('status'=>true);
    }


    public function expenseUpdateAndApproveApi(Request $req){
        $data=Input::all();
        //Log::info($data);
        if($data==null || COUNT($data['tourPlanningData'])==0){
            //No Data Found
            return array('status'=>false);  
        }
        DB::beginTransaction();
         try{
            $manager_id=Auth::user()->employee->id;
            $transaction_code="";
            foreach ($data['tourPlanningData'] as $key => $value) {

                Expense::where('transaction_code',$value['transaction_code'])->where('id',$value['tourId'])
                        ->update([ "localExps"=>$value['localExps'],
                                   "lodgingExps"=>$value['lodgingExps'],
                                   "mealExps"=>$value['mealExps'],
                                   "miscExps"=>$value['miscExps'],
                                   "outstationExps"=>$value['outstationExps'],
                                   "status"=>4,
                                   "manager_id"=>$manager_id]);
                $transaction_code=$value['transaction_code'];

            }

         }
         catch(\Exeption $e){
            DB::rollback();
            return array('status'=>false);
         }
         DB::commit();
         app('App\Http\Controllers\NotifController')->sendExpenseNotification($transaction_code);
        return array('status'=>true);
    }


     public function getExpenseListHistoryApi(Request $req,$page){
        $emp_id=Auth::user()->employee->id;
        $offset=$page*6;
        $query="SELECT plan.transaction_code,employees.id as emp_id,employees.name,MIN(plan.date) as start_date,MAX(plan.date) as end_date,SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,token,plan.status FROM tour_expense plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE plan.emp_id IN (SELECT id FROM employees WHERE plan.emp_id='$emp_id') GROUP BY plan.token,plan.emp_id ORDER BY plan.id DESC LIMIT 6 OFFSET $offset";
        //Log::info($query);
        $data= DB::select($query);
        return $data;
    }





}
