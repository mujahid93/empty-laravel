<?php

namespace App\Http\Controllers;

use App\Models\Dept;
use Illuminate\Http\Request;

class DeptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = $request->items ?? 5;
        $depts = Dept::where('status','=',1)->paginate($items);
        return view('dept.index')->with(compact('depts','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('dept.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $dept = new Dept();
            $dept->name = $request->name;
            $dept->code = $request->code;
            $dept->save();
            return redirect()->route("dept.index")->with(['result'=>'New dept added','color'=>'bg-green']);
        }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("dept.index")->with(['result'=>'Error adding dept','color'=>'alert-warning']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dept  $dept
     * @return \Illuminate\Http\Response
     */
    public function show(Dept $dept)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dept  $dept
     * @return \Illuminate\Http\Response
     */
    public function edit(Dept $dept)
    {
          $dept = Dept::find($dept->id);
          $id = $dept->id;
          return view('dept.edit')->with(compact('dept','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dept  $dept
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dept $dept)
    {
        try{
         $dept = Dept::find($dept->id);
         $dept->name = $request->name;
         $dept->code = $request->code;
         $dept->save();
         return redirect()->route('dept.index')->with(['result'=>'dept updated successfully','color'=>'bg-green']);
         }
        catch(\Exception $e){
           // do task when error
            return redirect()->route("dept.index")->with(['result'=>'Error updating dept','color'=>'alert-warning']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dept  $dept
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dept $dept)
    {
        $depts = Dept::find($dept->id);
        $depts->status = 0;
        $depts->save();
        return redirect()->route('dept.index')->with(['result'=>'dept deleted successfully','color'=>'bg-green']);
    }

    public function FilterRows(Request $request)
    {
        $depts = Dept::where('name', 'LIKE', '%' . $request->queryFilter . '%')->where('status','=',1)->paginate(5);
        return $depts;
    }
}
