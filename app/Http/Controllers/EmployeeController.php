<?php



namespace App\Http\Controllers;

use App\Models\Employee;

use App\Models\EmpDistrict;

use App\Models\EmployeeState;

use App\Models\Branch;

use App\Models\Role;

use App\Models\Dept;

use App\Models\State;

use App\Models\District;

use App\Models\User;

use App\Models\Email;

use Illuminate\Http\Request;

use Hash;

use Log;

use DB;

use Rap2hpoutre\FastExcel\FastExcel;



class EmployeeController extends Controller

{



    public function importEmployees(Request $request)

    {

        if($request->file('empExcel') == null)

        {

            return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);

        }



        /* Import Employees */

        // $employees = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {

            

        //     if(Email::where('email',$line['email'])->value('id'))

        //     {

        //         $emp = new Employee();

        //         $emp->id = $line['id'];

        //         $emp->name = $line['name'];

        //         $emp->branch_id = Branch::where('name',$line['branch'])->value('id');

        //         $emp->dept_id = Dept::where('name',$line['dept'])->value('id');

        //         $emp->date_of_join = $line['date_of_join'];

        //         $emp->email_id = Email::where('email',$line['email'])->value('id');

        //         $emp->role_id = Role::where('name',$line['role'])->value('id');

        //         $emp->mobile = $line['mobile'];

        //         $emp->save();

        //     }

        //     else

        //     {

        //         $email_model = new Email();

        //         $email_model->email = $line['email'];

        //         if($email_model->save())

        //         {

        //             $emp = new Employee();

        //             $emp->id = $line['id'];

        //             $emp->name = $line['name'];

        //             $emp->branch_id = Branch::where('name',$line['branch'])->value('id');

        //             $emp->dept_id = Dept::where('name',$line['dept'])->value('id');

        //             $emp->date_of_join = $line['date_of_join'];

        //             $emp->email_id = $email_model->id;

        //             $emp->role_id = Role::where('name',$line['role'])->value('id');

        //             $emp->mobile = $line['mobile'];

        //             $emp->save();

        //         }

        //     }

            

        // });

        /* Import Employee District from executivemst table(mayur) */
        // $emp_district_excel = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {
        //         if($line['emp_id4'] != 0)
        //         {
        //             $emp_dist = new EmpDistrict();
        //             $emp_dist->emp_id = $line['emp_id4'];
        //             $emp_dist->district_id = District::where('name',$line['district'])->value('id');
        //             $emp_dist->save();
        //         }
        // });

        /* Import Employee State from executivemst table(mayur) */
        // $emp_state = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {
        //         if($line['emp_id2'] != 0)
        //         {
        //             $emp_state = new EmployeeState();
        //             $emp_state->emp_id = $line['emp_id2'];
        //             $emp_state->state_id = District::where('name',$line['district'])->value('state_id');
        //             $emp_state->save();
        //         }
        // });
        // $emp_state = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {
        //         if($line['emp_id3'] != 0)
        //         {
        //             $emp_state = new EmployeeState();
        //             $emp_state->emp_id = $line['emp_id3'];
        //             $emp_state->state_id = District::where('name',$line['district'])->value('state_id');
        //             $emp_state->save();
        //         }
        // });
        // $emp_state = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {
        //         if($line['emp_id4'] != 0)
        //         {
        //             $emp_state = new EmployeeState();
        //             $emp_state->emp_id = $line['emp_id4'];
        //             $emp_state->state_id = District::where('name',$line['district'])->value('state_id');
        //             $emp_state->save();
        //         }
        // });
         
        /* Import Employee Email from emp_email */
        // $emp_email = (new FastExcel)->import($request->file('empExcel')->getRealPath(), function ($line) {
        //             if($line['email'] != 'null')
        //             {
        //                 $email_id = Employee::where('id',$line['emp_id'])->value('email_id');
        //                 $em = Email::find($email_id);
        //                 $em->email = $line['email'];
        //                 $em->save();
        //             }
        // });

        return back();
    }



    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()
    {
        $items = $request->items ?? 5;
        $employees = Employee::with(['user','branch','dept','state','supervisor'])->where('status','=',1)->paginate($items);
        return view('employee.index')->with(compact('employees','items'));
    }


    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {
        $empList = User::where('status','=',1)->get(['id', 'name']);
        $branches = Branch::where('status','=',1)->get(['id', 'name']);
        $roles = Role::where('status','=',1)->get(['id', 'name']);
        $depts = Dept::where('status','=',1)->get(['id', 'name']);
        $states = State::where('status','=',1)->get(['id', 'name']);
        $districts = District::where('status','=',1)->where('state_id','=',2)->get(['id', 'name']);
        return view('employee.create')->with(compact('empList','branches','roles','depts','states','districts'));
    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        try{

            $user = new Email();

            $user->email = $request->email;

            if($user->save())

                $emailId = $user->id;

        }

        catch(\Exception $e){

            return $e->getMessage();

        }



        try{

            $employee = new Employee();

            if($request->supervisor)

                $employee->supervisor_id = $request->supervisor;

            $employee->email_id = $emailId;

            $employee->name = $request->name;

            $employee->role_id = $request->role;

            $employee->mobile = $request->mobile;

            $employee->address = $request->address;

            $employee->gender = $request->gender;

            $employee->branch_id = $request->branch;

            $employee->dept_id = $request->dept;

            $employee->date_of_join = $request->date_of_join;

            if($employee->save())

                $empId = $employee->id;

        }

        catch(\Exception $e){

           // do task when error

            return $e->getMessage();

        }



        $sqlData = array();

        foreach ($request->districts as $key => $value) {

            $sqlData[] = array(

                'emp_id'         => $empId,

                'district_id'    => $value,

            );

        }

        DB::table('employee_district')->insert($sqlData);



        $sqlData1 = array();

        foreach ($request->state as $key => $value) {

            $sqlData1[] = array(

                'emp_id'         => $empId,

                'state_id'    => $value,

            );

        }

        DB::table('employee_state')->insert($sqlData1);



        return redirect()->route("employee.index")->with(['result'=>'New employee added','color'=>'bg-green']);

    } 





    /**

     * Display the specified resource.

     *

     * @param  \App\Models\Employee  $employee

     * @return \Illuminate\Http\Response

     */

    public function show(Employee $employee)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Models\Employee  $employee

     * @return \Illuminate\Http\Response

     */

    public function edit(Employee $employee)

    {

            $employee = Employee::find($employee->id);

            $id = $employee->id;

            $empList = User::where('status','=',1)->get(['id', 'name']);

            $branches = Branch::where('status','=',1)->get(['id', 'name']);

            $roles = Role::where('status','=',1)->get(['id', 'name']);

            $depts = Dept::where('status','=',1)->get(['id', 'name']);

            $states = State::where('status','=',1)->get(['id', 'name']);

            $districts = District::where('status','=',1)->where('state_id','=',$employee->state_id)->get(['id','name']);

            $alldistricts = District::where('status','=',1)->get();

            $sel_state_ids = DB::table('employee_state')->where('emp_id','=',$employee->id)->get();

            $sel_states = [];

            foreach($sel_state_ids as $category){

                array_push($sel_states, $category->state_id);

            }

            $sel_dist_ids = DB::table('employee_district')->where('emp_id','=',$employee->id)->get();

            $sel_districts = [];

            foreach($sel_dist_ids as $category){



                array_push($sel_districts, $category->district_id);

            }

            // Log::info($sel_districts);

            return view('employee.edit')->with(compact('employee','empList','branches','roles','depts','states','districts','sel_districts','id','sel_states','alldistricts'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Models\Employee  $employee

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Employee $employee)
    {
        $user = User::find($employee->login_id);
        $user->name = $request->name;
        $user->role_id = $request->role;
        $user->save();

        try{
            $employee = Employee::find($employee->id);
            $employee->supervisor_id = $request->supervisor;
            $employee->mobile = $request->mobile;
            $employee->address = $request->address;
            $employee->gender = $request->gender;
            $employee->branch_id = $request->branch;
            $employee->dept_id = $request->dept;
            $employee->date_of_join = $request->date_of_join;
            if($employee->save())
                $empId = $employee->id;
        }
        catch(\Exception $e){
           // do task when error
            return $e->getMessage();
        }
        $sqlData = array();
        foreach ($request->districts as $key => $value) {
            $sqlData[] = array(
                'emp_id'         => $empId,
                'district_id'    => $value,
            );
        }
        DB::table('employee_district')->where('emp_id','=',$employee->id)->delete();
        DB::table('employee_district')->insert($sqlData);
        $sqlData = array();
        foreach ($request->state as $key => $value) {
            $sqlData[] = array(
                'emp_id'      => $empId,
                'state_id'    => $value,
            );
        }
        DB::table('employee_state')->where('emp_id','=',$employee->id)->delete();
        DB::table('employee_state')->insert($sqlData);
        return redirect()->route("employee.index")->with(['result'=>'Employee successfully update','color'=>'bg-green']);
    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Models\Employee  $employee

     * @return \Illuminate\Http\Response

     */
    public function destroy(Employee $employee)
    {
        $employee = Employee::find($_POST["empId"]);
        $employee->status = 0;
        $employee->login_id = NULL;
        $employee->save();
        Email::where('email',$employee->email->email)->update(['status'=>0]);
        User::where('email',$employee->email->email)->delete();
        return redirect()->route('employee.index')->with(['result'=>'Employee deleted successfully','color'=>'bg-green']);
    }

    public static function FetchDistByState(Request $r)
    {   
        $districts = array();
        foreach ($r->states as $stateId) {
            $districts[] = District::where('state_id','=',$stateId)->get(['id','name']); 
        }
        return $districts;   
    }

    public function FilterEmployee()
    {
        $search_query = $_GET['queryFilter'];
        $employees = Employee::with(['user','branch','dept','state','supervisor','role'])->whereHas('user', function ($query)  use($search_query) {
                    $query->where('name', 'LIKE', '%' . $search_query . '%');
                })->where('status','=',1)->paginate(5);
        return $employees;
    }

    public function FilterByFromAndToDate()
    {
        $fromDate = $_GET['fromDate'];
        $toDate = $_GET['toDate'];
        $employees = Employee::with(['user','branch','dept','state','supervisor','role'])->whereHas('user', function ($query)  use($search_query) {
                    $query->where('name', 'LIKE', '%' . $search_query . '%');
                })->where('status','=',1)->paginate(5);
        return $employees;
    }



}

