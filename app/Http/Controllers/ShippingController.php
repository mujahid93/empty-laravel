<?php

namespace App\Http\Controllers;

use App\Models\Shipping;
use App\Models\Dealer;
use Illuminate\Http\Request;
use Session;
use App\Models\MachineSku;
use App\Models\AccessoriesSku;
use App\Models\Branch;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use Log;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $shipping = new Shipping();
            $shipping->branch_id = $request->branchId;
            $shipping->name = $request->shippingMode;
            $shipping->agency = $request->shippingCompany;
            $shipping->save();
            $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                        $query->select('name');
                    })->get();
                    $branches = Cart::distinct()->pluck('branch');
                    $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
                    return view('order.executive.cart')->with(compact('cart','branches','destination'));
        }
        catch(\Exception $e)
        {
                    $cart = Cart::where('emp_id',Auth::user()->employee->id)->where('dealer_id',Session::get('current_dealer'))->whereHas('dealer', function ($query) {
                        $query->select('name');
                    })->get();
                    $branches = Cart::distinct()->pluck('branch');
                    $destination = Dealer::where('id',Session::get('current_dealer'))->get(['address1','address2','address3']);
                    return view('order.executive.cart')->with(compact('cart','branches','destination'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function show(Shipping $shipping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipping $shipping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shipping $shipping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipping $shipping)
    {
        //
    }
}
