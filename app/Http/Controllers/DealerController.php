<?php

namespace App\Http\Controllers;

use App\Models\Dealer;
use App\Models\DealerState;
use App\Models\State;
use App\Models\District;
use App\Models\User;
use App\Models\Role;
use App\Models\Email;
use App\Models\Version;
use Illuminate\Http\Request;
use Hash;
use DB;
use Log;
use Session;
use Rap2hpoutre\FastExcel\FastExcel;

class DealerController extends Controller
{


    public function importDealers(Request $request)
    {
        if($request->file('dealerExcel') == null)
        {
            return back()->with(['result'=>'Please chose a file','color'=>'bg-red']);
        }

        /* Import dealerloyees */
        $dealers = (new FastExcel)->import($request->file('dealerExcel')->getRealPath(), function ($line) {
                    if($line['state'] != 'null' and $line['district'] != 'null')
                    {
                        $dealer = new Dealer();
                        $dealer->role_id = 2;
                        $dealer->state_id = State::where('name',$line['state'])->value('id');
                        $dealer->district_id = District::where('name',$line['district'])->value('id');
                        $dealer->name = $line['name'];
                        $dealer->contact_person = $line['person'];
                        $dealer->address = $line['address'];
                        $dealer->address1 = $line['Address1'];
                        $dealer->address2 = $line['Address2'];
                        $dealer->address3 = $line['Address3'];
                        $dealer->city = $line['city'];
                        $dealer->taluk = $line['taluk'];
                        $dealer->pincode = $line['pin'];
                        $dealer->gstn = $line['gstn'];
                        $dealer->vat = $line['vat'];
                        $dealer->cst = $line['cst'];
                        $dealer->mobile1 = $line['m1'];
                        $dealer->mobile2 = $line['m2'];
                        $dealer->save();
                    }
        });
        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = $request->items ?? 5;
        $dealers = Dealer::with(['user'])->where('status','=',1)->paginate($items);
        Log::info($dealers);
        return view('dealer.index')->with(compact('dealers','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::where('status','=',1)->get(['id', 'name']);
        $districts = District::where('status','=',1)->where('state_id','=',2)->get(['id', 'name']);
        return view('dealer.create')->with(compact('states','districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try{
        //     $user = new Email();
        //     $user->email = $request->email;
        //     $user->isDealer = 1;
        //     if($user->save())
        //         $emailId = $user->id;
        // }
        // catch(\Exception $e){
        //     return $e->getMessage();
        // }

        try{
            $dealer = new Dealer();
            $dealer->role_id = 2;
            $dealer->state_id = $request->state;
            $dealer->district_id = $request->district;
            $dealer->contact_person = $request->cPerson;
            $dealer->name = $request->name;
            $dealer->code = $request->code;
            $dealer->address1 = $request->address1;
            $dealer->address2 = $request->address2;
            $dealer->dealer_since = $request->dealer_since;
            $dealer->gstn = $request->gstn;
            $dealer->landline = $request->landline;
            $dealer->mobile = $request->mobile;
            $dealer->website = $request->website;

            if($dealer->save()){
                $dealerId = $dealer->id;
                Version::where('tbl_name','dealer')->update(['version'=> DB::raw('version+1')]);
            }

        }
        catch(\Exception $e){
            return $e->getMessage();
        }
        return redirect()->route("dealer.index")->with(['result'=>'New dealer added','color'=>'bg-green']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Dealer $dealer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Dealer $dealer)
    {
            $dealer = Dealer::find($dealer->id);
            $id = $dealer->id;
            $states = State::where('status','=',1)->get(['id', 'name']);
            $districts = District::where('status','=',1)->where('state_id','=',$dealer->state_id)->get(['id','name']);
            $alldistricts = District::where('status','=',1)->get();
            return view('dealer.edit')->with(compact('dealer','states','districts','id','alldistricts'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dealer $dealer)
    {

            Log::info($request->address1);
            
            if($dealer->login_id != null)
            {
                $user = User::find($dealer->login_id);
                $user->name = $request->name;
                $user->save();
            }

            try{
                $dealer = Dealer::find($dealer->id);
                $dealer->state_id = $request->state;
                $dealer->district_id = $request->district;
                $dealer->contact_person = $request->cPerson;
                $dealer->name = $request->name;
                $dealer->code = $request->code;
                $dealer->address1 = $request->address1;
                $dealer->address2 = $request->address2;
                $dealer->dealer_since = $request->dealer_since;
                $dealer->gstn = $request->gstn;
                $dealer->landline = $request->landline;
                $dealer->mobile = $request->mobile;
                $dealer->website = $request->website;
                if($dealer->save()){
                    $dealerId = $dealer->id;
                    Version::where('tbl_name','dealer')->update(['version'=> DB::raw('version+1')]);
                }
            }

            catch(\Exception $e){
               // do task when error
                return $e->getMessage();
            }
            return redirect()->route("dealer.index")->with(['result'=>'Dealer successfully updated','color'=>'bg-green']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dealer $dealer)
    {
        $dealers = Dealer::find($dealer->id);
        $dealers->status = 0;
        if($dealer->save()){
            Version::where('tbl_name','dealer')->update(['version'=> DB::raw('version+1')]);
        }
        return redirect()->route('dealer.index')->with(['result'=>'dealer deleted successfully','color'=>'bg-green']);
    }
    
    public static function FetchDistByOneState()
    {
         $stateId = $_GET['stateId'];
         $districts = District::where('state_id','=',$stateId)->get(['id','name']); 
         return $districts;   
    }

    public function FetchDealersByDistricts(Request $request)
    {   
        $dealers = Dealer::where('district_id',$request->districtId)->orderBy('name')->get();
        return $dealers;
    }

    public function FilterDealer()
    {
        $query = $_GET['queryFilter'];
        $dealers = Dealer::with(['state'])->where('name', 'LIKE', '%' . $query . '%')->where('status','=',1)->paginate(5);
        return $dealers;
    }

}
