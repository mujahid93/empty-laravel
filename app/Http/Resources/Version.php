<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Version extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $updated_at = $this->updated_at;
        if($updated_at != null)
        {
            $updated_at = $this->updated_at->format('y-M-d H:m:s');
        }
        return [
            'id' => $this->id,
            'tbl_name'=> $this->tbl_name,
            'version'=> $this->version,
            'updated_at'=> $updated_at
        ];
    }
}
