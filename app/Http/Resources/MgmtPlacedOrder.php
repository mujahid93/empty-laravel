<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;

class MgmtPlacedOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $totalPrice = 0;
        foreach ($this->orderDetail as $key => $value) {
            $unitPrice = $value->branch_rate;
            $disPer = $value->dis_per;
            $qty = $value->qty;
            $gst = $value->gst;
            $freight=$value->freight;
            $discount=(($qty*$unitPrice)/100)*$disPer;
            $baseTotal=($qty*$unitPrice)-$discount;
            $taxAmt=($baseTotal/100)*$gst;
            $finalPrice = $baseTotal+$taxAmt;
            $totalPrice = $totalPrice + $finalPrice;
        }
        
        return [
            'id' => $this->id,
            'orderDate' => $this->created_at->format('d-M-y'),
            'dealer' => $this->dealer->name,
            'totalPrice' => number_format((float)$totalPrice, 2, '.', ''),
            'purchaseNo' => $this->purchase_no,
        ];
    }
}
