<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;


class MachineSku extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'grp' => $this->grp,
            'subgrp' => $this->subgrp,
            'nav_no' => $this->nav_no,
            'gst' => $this->gst,
            'name' => $this->sku,
            'hsn' => $this->hsn,
            'dp' => $this->dp,
            'bulk_qty' => $this->bulk_qty,
            'mrp' => $this->mrp,
            'subsidy' => $this->subsidy,
            'status' => $this->status,
            'updated_at'=> (string)$this->updated_at,
        ];
    }
}
