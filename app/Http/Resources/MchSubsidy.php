<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MchSubsidy extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id' => $this->id,
            'mch_id' => $this->mch_id,
            'state_id' => $this->state_id,
            'dept_id'=>$this->subsidy_dept_id,
            'subsidy_price' => $this->subsidy_price,
            'updated_at'=> (string)$this->updated_at,
            'status'=>$this->status,
        ];
    }
}
