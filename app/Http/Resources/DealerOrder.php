<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;

class DealerOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'tracking_no' => $this->tracking_no,
            'dealerName'=>$this->dealerName,
            'orderDate' => $this->orderDate,
            'amount' => $this->amount,
        ];
    }
}
