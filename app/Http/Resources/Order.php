<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;
use App\Models\OrderDetail;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      $name="NA";
      $status="";
      $Order=OrderDetail::where('order_id',$this->id)->first();
      switch ($Order->order_status) {
        case 2:
          $status="Executive Placed Order";
          break;

        case 1:
            $status="Dealer Placed Order";
            break;

        case 3:
            $status="Payment Approved";
            break;

        case 4:
            $status="Management Approved";
            break;

        case 5:
            $status="Invoice Generated";
            break;

                case 6:
                    $status="Order Dispatched";
                    break;

                    case 7:
                        $status="Order Rejected";
                        break;

                            case 8:
                        $status="Sent for dealer confirmation";
                        break;

        default:
            $status='';
          break;
      }
      if($this->employee!=null)
        $name=$this->employee->name;
        return [
            'id' => $this->id,
            'dealer' => $this->dealer->name,
            'orderDate' => $this->created_at->format('d-M-y'),
            'employee' => $name,
            'tracking_no'=>$this->tracking_no,
            'status'=>$status,
            'status_code'=>$Order->order_status,
        ];
    }
}
