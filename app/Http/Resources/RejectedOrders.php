<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RejectedOrders extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'dealer' => $this->dealer->name,
            'orderDate' => $this->created_at->format('d-M-y'),
            'employee' => $this->employee->user->name,
        ];
    }
}
