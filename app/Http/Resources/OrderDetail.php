<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;
use App\Models\Branch;
use App\Models\Order;
use App\Models\Dealer;

class OrderDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $Branch=Branch::where('id',$this->branch)->first();
        $Order=Order::where('id',$this->order_id)->first();
        $Dealer=Dealer::where('id',$Order->dealer_id)->first();
        $orignalAddress=$Dealer->address1;
	if($Dealer->address1==$this->destination || $Dealer->address2==$this->destination || $Dealer->address3==$this->destination)
		$orignalAddress=$this->destination;

        $dest="";
        if($this->destination!=null)
            $dest=$this->destination;

        if($orignalAddress==null)
            $orignalAddress="";


         return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'purchase_no' => $this->purchase_no,
            'sku' => $this->sku,
            'qty' => $this->qty,
            'item_rate' => $this->item_rate,
            'rate_type' => $this->rate_type,
            'branch' => $Branch->code,
            'dis_per' => $this->dis_per,
            'gst' => $this->gst,
            'freight'=>$this->freight,
            'spl_disc'=>$this->spl_disc,
            'other_ship'=>$this->other_ship,
            'destination'=>$dest,
            'OrignalDest'=>$orignalAddress,
            'BalInAcc'=>$Order->p_balance,
        ];
    }
}
