<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Log;

class District extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'district' => $this->name,
            'state_id'=>$this->state_id,
            'zone' => $this->zone,
            'status' => $this->status,
            'updated_at'=> (string)$this->updated_at,
        ];
    }
}
