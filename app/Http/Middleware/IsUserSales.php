<?php

namespace App\Http\Middleware;

use Closure;

class IsUserSales
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->employee->role_id == 3 || \Auth::user()->employee->role_id == 4 || \Auth::user()->employee->role_id == 8 || \Auth::user()->employee->role_id == 9 || \Auth::user()->employee->role_id == 10 || \Auth::user()->employee->role_id == 11)
        {
            return $next($request);
        }
        return redirect('home');
    }
}
