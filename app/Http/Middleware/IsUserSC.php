<?php

namespace App\Http\Middleware;

use Closure;

class IsUserSC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->employee->role_id == 6)
        {
            return $next($request);
        }
        return redirect('home');
    }
}
