<?php

namespace App\Http\Middleware;

use Closure;

class IsUserSD
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->employee->role_id == 15)
        {
            return $next($request);
        }
        return redirect('home');
    }
}
