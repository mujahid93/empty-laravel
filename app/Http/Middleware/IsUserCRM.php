<?php

namespace App\Http\Middleware;

use Closure;

class IsUserCRM
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->employee->role_id == 16)
        {
            return $next($request);
        }
        return redirect('home');
    }
}
