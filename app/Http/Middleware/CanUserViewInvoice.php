<?php

namespace App\Http\Middleware;

use Closure;

class CanUserViewInvoice
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->employee->role_id == 6 || \Auth::user()->employee->role_id == 4  || \Auth::user()->employee->role_id == 13)
        {
            return $next($request);
        }
        return redirect('home');
    }
}
