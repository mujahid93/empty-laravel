<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    //

    function reqBranch()
    {
    	return $this->hasOne('App\Models\Branch', 'id', 'req_branch');
    }

    function reqGodown()
    {
        return $this->hasOne('App\Godown', 'id', 'req_godown');
    }

    function sourceBranch()
    {
    	return $this->hasOne('App\Models\Branch', 'id', 'source_branch');
    } 

    function sourceGodown()
    {
        return $this->hasOne('App\Godown', 'id', 'source_godown');
    }

    public function employee()
	{
		return $this->hasOne('App\Models\Employee','id','emp_id');
	}

	public function management()
	{
		return $this->hasOne('App\Models\Employee','id','management_id');
	}

    public function dispatchBy()
    {
        return $this->hasOne('App\Models\Employee','id','source_emp_id');
    }
}
