<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerVisit extends Model
{
    protected $table = 'dealer_visit';

    function employee()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'emp_id');
    }
}
