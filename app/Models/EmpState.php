<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpState extends Model
{
    protected $table = 'employee_state';
}
