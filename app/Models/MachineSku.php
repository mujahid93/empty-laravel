<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MachineSku extends Model
{
    public function gst()
    {
    	return $this->hasOne('App\Models\Tax','id','tax_id');
    }

    public function subsidy_rate()
    {
    	return $this->hasOne('App\Models\MachineSubsidy','mch_id','id');
    }

}
