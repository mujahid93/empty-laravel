<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = 'tour_expense';

    function employee()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'emp_id');
    }

    function manager()
    {
    	return $this->hasOne('App\Models\Employee', 'id', 'manager_id');
    }

    function state()
    {
    	return $this->hasOne('App\Models\State', 'id', 'state_id');
    }

    function otherEmp($emp_id)
    {
    	$emp_ids=explode(',', $emp_id);
    	return Employee::whereIn('id',$emp_ids)->pluck('name');
    }

    function otherEmpObj($emp_id)
    {
    	$emp_ids=explode(',', $emp_id);
    	return Employee::whereIn('id',$emp_ids)->get();
    }

}
