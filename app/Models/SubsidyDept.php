<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubsidyDept extends Model
{
    protected $table = 'subsidy_dept';
}
