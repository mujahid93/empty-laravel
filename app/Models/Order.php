<?php



namespace App\Models;



use Illuminate\Database\Eloquent\Model;



class Order extends Model

{



	public $timestamps = true;

	

	public function dealer()

	{

		return $this->hasOne('App\Models\Dealer','id','dealer_id');

	}



	public function employee()

	{
		return $this->hasOne('App\Models\Employee','id','emp_id');
	}

	public function placedBy()

	{
		return $this->hasOne(Employee::class, 'id','emp_id');
	}

	public function OrderDetail()

	{
		return $this->hasMany('App\Models\OrderDetail','order_id','id');
	}

}

