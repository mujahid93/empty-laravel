<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function OrderDetail()
	{
		return $this->hasOne(OrderDetail::class, 'id', 'split_id');
	}

	public function Order()
	{
		return $this->hasOne(Order::class,'id','order_id');
	}
}
