<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{

    function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'login_id_app');
    }

    function state()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }

    function district()
    {
    	return $this->hasOne('App\Models\District','id','district_id');
    }

    function branch()
    {
        return $this->hasOne('App\Models\StateBranch','state_id','state_id');
    }
}
