<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

	protected $hidden = [
        'login_id',
    ];
    
    function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'login_id');
    }

    function branch()
    {
    	return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    } 

    function dept()
    {
    	return $this->hasOne('App\Models\Dept', 'id', 'dept_id');
    }

    function state()
    {
    	return $this->hasMany('App\Models\EmpState', 'emp_id', 'id');
    } 

    function districts()
    {
        return $this->hasMany(EmpDistrict::class, 'emp_id','id');
    }

    function manager(){
        return $this->hasOne('App\Models\Employee', 'id', 'manager_id');
    }

    function supervisor()
    {
    	return $this->hasOne('App\Models\User', 'id', 'supervisor_id');
    } 

    function email()
    {
        return $this->hasOne('App\Models\Email','id', 'email_id');
    }

    function role()
    {
        return $this->hasOne('App\Models\Role','id','role_id');
    }
}
