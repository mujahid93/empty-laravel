<?php

namespace App\Models;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
     function state() {
        return $this->hasOne('App\Models\State', 'id', 'state_id');
    }

    public function employees()
    {
        return $this->belongsToMany('App\Models\Employee');
    }
}
