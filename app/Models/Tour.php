<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table = 'tour_plan';

    function employee()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'emp_id');
    }

    function manager()
    {
    	return $this->hasOne('App\Models\Employee', 'id', 'manager_id');
    }
}
