<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function dealer()
	{
		return $this->hasOne('App\Models\Dealer','id','dealer_id');
	}

	public function data()
	{
		switch ($this->classification) {
			case '1':
				return $this->hasOne('App\Models\MachineSku','id','sku');
				break;
			case '2':
				return $this->hasOne('App\Models\AccessoriesSku','id','sku');
				break;
			case '3':
				return $this->hasOne('App\Models\Part','id','sku');
				break;
			default:
				break;
		}
	}
}
