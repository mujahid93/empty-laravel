<?php

namespace App\Models;
use App\Models\DemoCategory;

use Illuminate\Database\Eloquent\Model;

class DemoPlan extends Model
{
    protected $table = 'demo_plans';

    function employee()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'emp_id');
    }

    function manager()
    {
    	return $this->hasOne('App\Models\Employee', 'id', 'manager_id');
    }

    function demo($subcat_id)
    {
    	$cat=explode(',', $subcat_id);
    	return DemoCategory::whereIn('id',$cat)->pluck('subCatName');
    }
}
