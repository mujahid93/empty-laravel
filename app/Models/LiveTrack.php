<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveTrack extends Model
{
    protected $table = 'live_track';
}
