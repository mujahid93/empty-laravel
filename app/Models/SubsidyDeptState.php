<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubsidyDeptState extends Model
{
    protected $table = 'subsidy_dept_state';
    
    public function subsidy_dept()
    {
    	return $this->hasOne(SubsidyDept::class,'id','subsidy_dept_id');
    }

}
