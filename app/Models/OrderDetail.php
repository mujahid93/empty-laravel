<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	public $timestamps = true;

    public function branch_name()
	{
		return $this->hasOne('App\Models\Branch','id','branch');
	}

	public function order()
	{
		return $this->hasOne('App\Models\Order','id','order_id');
	}

	public function shipping()
	{
		return $this->hasOne('App\Models\Shipping','id','shipping_id');
	}

	public function mgmt_approver()
	{
		return $this->hasOne('App\Models\Employee','id','mgmt_id');
	}

	public function subsidy_dept()
	{
		return $this->hasOne('App\Models\SubsidyDept','id','subsidy_dept_id');
	}
}
