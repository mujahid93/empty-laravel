<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'demo_feedback';

    function employee()
    {
        return $this->hasOne('App\Models\Employee', 'id', 'emp_id');
    }


    function demo()
    {
    	return $this->hasOne('App\Models\DemoCategory', 'id', 'subcat_id');
    }

}
