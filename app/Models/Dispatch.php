<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dispatch extends Model
{
    function OrderDetail(){
		return $this->hasOne('App\Models\OrderDetail','id','order_detail_id');
    }
}
