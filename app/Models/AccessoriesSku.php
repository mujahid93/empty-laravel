<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessoriesSku extends Model
{
     public function gst()
    {
    	return $this->hasOne('App\Models\Tax','id','tax_id');
    }

    public function subsidy_rate()
    {
    	return $this->hasOne('App\Models\AccSubsidy','acc_id','id');
    }
}
