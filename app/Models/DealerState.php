<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DealerState extends Model
{
    function state()
    {
    	return $this->hasOne('App\Models\State','id','state_id');
    }
}
