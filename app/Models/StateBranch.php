<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateBranch extends Model
{
    protected $table = 'state_branches';

    function branch_name()
    {
        return $this->hasOne('App\Models\Branch', 'id', 'branch_id');
    }
}
