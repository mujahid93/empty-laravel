<?php



namespace App\Models;



use Laravel\Passport\HasApiTokens;

use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable

{

    use HasApiTokens, Notifiable;

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email', 'password', 'role_id',

    ];



    /**s

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        'password', 'remember_token',

    ];



    public function role()

    {

        return $this->belongsTo(Role::class, 'role_id');

    }



    public function employee()
    {

        return $this->hasOne('App\Models\Employee','login_id','id');

    }

    public function dealer()
    {

        return $this->hasOne(Dealer::class, 'login_id');

    }

}

