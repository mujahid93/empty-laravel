<?php



namespace App\Providers;



use Laravel\Passport\Passport;

use App\Models\Role;

use Illuminate\Support\Facades\Gate;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;



class AuthServiceProvider extends ServiceProvider

{

    /**

     * The policy mappings for the application.

     *

     * @var array

     */

    protected $policies = [

        'App\Model' => 'App\Policies\ModelPolicy',

    ];



    /**

     * Register any authentication / authorization services.

     *

     * @return void

     */

    public function boot()

    {

        $this->registerPolicies();

        Passport::routes();

        $user = \Auth::user();



        Gate::define('user_management_access', function ($user) {

            return in_array(\Auth::user()->employee->role_id, Role::where('name','Developer')->value('id'));

        });



        Gate::define('board',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,14]);

        });



        Gate::define('tour_gate', function($user){

            return in_array(\Auth::user()->employee->role_id,[15,17,18,19,20,21]);

        });



        Gate::define('create_order_gate', function($user){

            return in_array(\Auth::user()->employee->role_id,[15,2,3]);

        });



        // Gate::define('order_info_gate', function($user){

        //     return in_array($user->role_id,[15,17,19,22]);

        // });



        Gate::define('full_access', function($user){

            return in_array(\Auth::user()->employee->role_id,[15,14]);

        });

        

        Gate::define('isCoordinator',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,6]);

        });



        Gate::define('isDispatcher',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,7,12,13]);

        });



        Gate::define('isExecutive',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,3,8,9,10,11]);

        });



        Gate::define('isDealer',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,2]);

        });



        Gate::define('isManagement',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,4,14]);

        });


        Gate::define('isCRM',function($user){

            return in_array(\Auth::user()->employee->role_id,[16]);

        });

        Gate::define('isUserAgroManager',function($user){

            return in_array(\Auth::user()->employee->role_id,[18]);

        });



        Gate::define('isPaymentApprover',function($user){

            return in_array(\Auth::user()->employee->role_id,[15,5]);

        });


        Gate::define('isParts',function($user){

            return in_array(\Auth::user()->employee->role_id,[19]);

        });


        Gate::define('isPartsSC',function($user){

            return in_array(\Auth::user()->employee->role_id,[20]);

        });

    }



}



