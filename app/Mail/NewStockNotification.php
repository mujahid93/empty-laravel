<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Transfer; 
use App\TransferDetails; 
use Illuminate\Support\Facades\Log;

class NewStockNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $request=$this->mailData;

        $transfer=Transfer::where('id',$request['transfer_id'])->first();
        $details=TransferDetails::where('transfer_id',$request['transfer_id'])->get();
        $subject="";
        if($transfer->status==2)
            $subject="Stock Transfer Approved ".$transfer->tracking_no;
        else
            $subject="Stock Transfer Dispatch ".$transfer->tracking_no;
        return $this->subject($subject)->view('transfer.print')->with(compact(['transfer','details']));
    }
}
