<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateInvoicesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('invoices', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('order_detail_id')->unsigned()->nullable();

            $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('set null');

            $table->integer('iNumber')->default(0);

            $table->double('iAmt')->default(0);

            $table->string('iPdf',350)->default("null");

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('invoices');

    }

}

