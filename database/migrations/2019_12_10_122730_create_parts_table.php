<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nav_no')->nullable();
            $table->string('name1')->nullable();
            $table->string('name2')->nullable();
            $table->string('oldName')->nullable();
            $table->string('sku')->nullable();
            $table->string('hsn')->nullable();
            $table->decimal('gst',5,2)->nullable();
            $table->double('dp', 8, 2)->nullable();
            $table->double('mrp', 8, 2)->nullable();
            $table->smallInteger('subsidy')->default(0)->nullable();
            $table->string('mch_ids')->nullable();
            $table->string('acc_ids')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parts');
    }
}
