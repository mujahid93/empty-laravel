<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckTimeAndTaluk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_visit', function (Blueprint $table) {
            $table->dateTime('check_in')->after('id')->default('00-00-0000 00:00:00')->nullable();
            $table->dateTime('check_out')->after('check_in')->default('00-00-0000 00:00:00')->nullable();
            $table->string('taluk')->after('district_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_visit', function (Blueprint $table) {
            $table->dropColumn('check_in');
            $table->dropColumn('check_out');
            $table->dropColumn('taluk');
        });
    }
}
