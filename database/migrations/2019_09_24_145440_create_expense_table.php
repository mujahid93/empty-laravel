<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_code')->nullable();
            $table->string('tour_code')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->date('date')->nullable();
            $table->string('startTime')->nullable();
            $table->string('endTime')->nullable();
            $table->bigInteger('emp_id')->unsigned()->nullable();
            $table->string('other_emp')->nullable();
            $table->bigInteger('state_id')->unsigned()->nullable();
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');
            $table->string('district_name')->nullable();
            $table->string('city')->nullable();
            $table->decimal('localExps',15,2)->nullable();
            $table->decimal('lodgingExps',15,2)->nullable();
            $table->decimal('mealExps',15,2)->nullable();
            $table->decimal('miscExps',15,2)->nullable();
            $table->decimal('outstationExps',15,2)->nullable();
            $table->string('token')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->foreign('manager_id')->references('id')->on('employees')->onDelete('set null');
            $table->tinyInteger('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_expense');
    }
}
