<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateCartsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('carts', function (Blueprint $table) {

            $table->bigInteger('id',true,true);

            $table->date('order_date');

            $table->bigInteger('emp_id')->unsigned()->nullable();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');

            $table->bigInteger('dealer_id')->unsigned()->nullable();

            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('set null');

            $table->integer('branch')->default(0);

            $table->string('classification')->default("null");

            $table->string('sku')->default("null");

            $table->integer('qty')->default(1);

            $table->decimal('dis_per',5,2)->default(0);

            $table->tinyInteger('rate_type')->default(0);

            $table->integer('subsidy_dept_id')->unsigned()->nullable();
            $table->foreign('subsidy_dept_id')->references('id')->on('subsidy_dept')->onDelete('set null');

            $table->double('item_rate',15,2)->nullable();

            $table->timestamps();

        });

    }


    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('cart');

    }

}

