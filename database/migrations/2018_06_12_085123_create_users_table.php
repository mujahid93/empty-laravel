<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateUsersTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('users', function (Blueprint $table) {

            $table->bigInteger('id',true,true);

            $table->string('name')->nullable();

            $table->string('mobile')->nullable();

            $table->string('email')->nullable();

            $table->string('password')->nullable();

            $table->integer('status')->default(1);

            $table->rememberToken();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('users');

    }

}

