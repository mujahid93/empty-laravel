<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateEmployeesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('employees', function (Blueprint $table) {

            $table->bigInteger('id',false,true);

            $table->string('name')->nullable();

            $table->integer('branch_id')->unsigned()->nullable();

            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('set null');

            $table->integer('dept_id')->unsigned()->nullable();

            $table->foreign('dept_id')->references('id')->on('depts')->onDelete('set null');

            $table->bigInteger('login_id')->unsigned()->nullable();

            $table->foreign('login_id')->references('id')->on('users')->onDelete('set null');

            $table->date('date_of_join')->default(date('Y-m-d H:i:s', strtotime('0000-00-00 00-00-00')));

            $table->bigInteger('email_id')->unsigned()->nullable();

            $table->foreign('email_id')->references('id')->on('emails')->onDelete('set null');

            $table->integer('role_id')->unsigned()->nullable();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');

            $table->string('mobile')->nullable();

            $table->integer('status')->default(1);

            $table->timestamps();

            $table->primary('id');

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('employees');

    }

}

