<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemoFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demo_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('emp_id')->unsigned()->nullable();
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');
            $table->datetime('check_in')->nullable();
            $table->datetime('check_out')->nullable();
            $table->string('check_in_img')->nullable();
            $table->string('check_out_img')->nullable();
            $table->bigInteger('cat_id')->nullable();
            $table->bigInteger('subcat_id')->unsigned()->nullable();
            $table->foreign('cat_id')->references('cat_id')->on('demo_categories')->onDelete('set null');
            $table->foreign('subcat_id')->references('id')->on('demo_categories')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demo_feedback');
    }
}
