<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateEmployeeDistrictTable extends Migration
{
    
    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()
    {
        Schema::create('employee_district', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('emp_id')->unsigned()->nullable();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');

            $table->bigInteger('district_id')->unsigned()->nullable();

            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');

            $table->timestamps();

        });
    }


    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('employee_district');

    }

}

