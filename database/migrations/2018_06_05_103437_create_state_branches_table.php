<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateStateBranchesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('state_branches', function (Blueprint $table) {

           $table->bigIncrements('id');

            $table->bigInteger('state_id')->unsigned()->nullable();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');

            $table->integer('branch_id')->unsigned()->nullable();

            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('set null');

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('state_branches');

    }

}

