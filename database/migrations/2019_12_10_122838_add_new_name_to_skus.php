<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewNameToSkus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('machine_skus', function (Blueprint $table) {
            $table->string('nav_no')->after('id')->nullable();
            $table->string('name1')->after('item')->nullable();
            $table->string('name2')->after('name1')->nullable();
            $table->string('image_url')->after('subsidy')->nullable();
        });

        Schema::table('accessories_skus', function (Blueprint $table) {
            $table->string('nav_no')->after('id')->nullable();
            $table->string('name1')->after('item')->nullable();
            $table->string('name2')->after('name1')->nullable();
            $table->string('image_url')->after('subsidy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('machine_skus', function (Blueprint $table) {
            $table->dropColumn('nav_no');
            $table->dropColumn('name1');
            $table->dropColumn('name2');
            $table->dropColumn('image_url');
        });

        Schema::table('accessories_skus', function (Blueprint $table) {
            $table->dropColumn('nav_no');
            $table->dropColumn('name1');
            $table->dropColumn('name2');
            $table->dropColumn('image_url');
        });
    }
}
