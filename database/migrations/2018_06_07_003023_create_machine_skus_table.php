<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineSkusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_skus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item')->nullable();
            $table->string('grp')->nullable();
            $table->string('subgrp')->nullable();
            $table->string('sku')->nullable();
            $table->string('hsn')->nullable();
            $table->decimal('gst',5,2)->nullable();
            $table->double('dp', 8, 2)->nullable();
            $table->double('mrp', 8, 2)->nullable();
            $table->smallInteger('subsidy')->default(0)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_skus');
    }
}
