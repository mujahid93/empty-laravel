<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealerVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealer_visit', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('is_new')->unsigned()->nullable();
            $table->bigInteger('dealer_id')->unsigned()->nullable();
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->bigInteger('emp_id')->unsigned()->nullable();
            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('set null');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');
            $table->string('dealer_name')->nullable();
            $table->string('district_name')->nullable();
            $table->mediumText('segments')->nullable();
            $table->mediumText('competition_products')->nullable();
            $table->mediumText('feedback')->nullable();
            $table->mediumText('address')->nullable();
            $table->decimal('lat',10,7)->nullable();
            $table->decimal('lng',10,7)->nullable();
            $table->string('image_url1')->nullable();
            $table->string('image_url2')->nullable();
            $table->tinyInteger('is_deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dealer_visit');
    }
}
