<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemoPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demo_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date')->nullable();
            $table->integer('days')->unsigned()->nullable();
            $table->bigInteger('emp_id')->unsigned()->nullable();
            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');
            $table->string('cat_id')->nullable();
            $table->string('subcat_id')->nullable();
            $table->string('purpose')->nullable();
            $table->string('state')->nullable();
            $table->string('district')->nullable();
            $table->string('taluk')->nullable();
            $table->string('village')->nullable();
            $table->string('dealerName')->nullable();
            $table->string('model')->nullable();
            $table->string('cropName')->nullable();
            $table->decimal('cropRowGap')->nullable();
            $table->decimal('cropHeight')->nullable();
            $table->string('salesPersonName')->nullable();
            $table->string('technicianName')->nullable();
            $table->string('vehicleType')->nullable();
            $table->decimal('localExps',15,2)->nullable();
            $table->decimal('lodgingExps',15,2)->nullable();
            $table->decimal('mealExps',15,2)->nullable();
            $table->decimal('miscExps',15,2)->nullable();
            $table->decimal('outstationExps',15,2)->nullable();
            $table->string('token')->nullable();
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->foreign('manager_id')->references('id')->on('employees')->onDelete('set null');
            $table->tinyInteger('status')->default(1)->nullable();
            $table->tinyInteger('is_deleted')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demo_plans');
    }
}
