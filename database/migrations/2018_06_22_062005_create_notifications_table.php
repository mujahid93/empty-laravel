<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateNotificationsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('notifications', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('emp_id')->unsigned()->nullable();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');

            $table->bigInteger('dealer_id')->unsigned()->nullable();

            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('set null');

            $table->bigInteger('order_id')->unsigned()->nullable();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');

            $table->bigInteger('split_id')->unsigned()->nullable();

            $table->foreign('split_id')->references('id')->on('order_details')->onDelete('set null');

            $table->tinyInteger('type')->unsigned()->nullable();

            $table->string('file_url')->nullable();

            $table->tinyInteger('priority')->nullable();

            $table->mediumText('message')->nullable();

            $table->tinyInteger('seen')->default(1)->nullable();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('notifications');

    }

}

