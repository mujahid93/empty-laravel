<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateOrderDetailsTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('order_details', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('order_id')->unsigned()->nullable();

            $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');

            $table->date('order_date');

            $table->string('tracking_no')->nullable();

            $table->string('branch_tracking_no')->nullable();

            $table->string('classification')->nullable();

            $table->string('grp')->nullable();

            $table->string('subgrp')->nullable();

            $table->string('hsn')->nullable();

            $table->string('sku')->nullable();

            $table->integer('qty')->default(1);

            $table->integer('branch')->nullable();

            $table->tinyInteger('rate_type')->nullable()->comment("Dealer Price - 0 | MRP Price - 1 | Subsidy - 2");

            $table->integer('subsidy_dept_id')->unsigned()->nullable();
            $table->foreign('subsidy_dept_id')->references('id')->on('subsidy_dept')->onDelete('set null');

            $table->double('item_rate',15,2)->nullable();

            $table->integer('bulk_qty')->nullable();

            $table->decimal('dis_per',5,2)->nullable();

            $table->decimal('gst',5,2)->nullable();

            $table->integer('shipping_id')->unsigned()->nullable();

            $table->foreign('shipping_id')->references('id')->on('shippings')->onDelete('set null');

            $table->string('destination',300)->nullable()->default("null");

            $table->double('freight', 15, 8)->nullable();

            $table->integer('order_status')->default(1)->comment('Pending/DealerPlaced - 1 ; Executive Placed - 2 ; Payment Approved - 3 ; Management Approved - 4; Invoice Raised - 5, Dispatched - 6, Rejected - 7');

            $table->bigInteger('mgmt_id')->unsigned()->nullable();
            $table->foreign('mgmt_id')->references('id')->on('employees')->onDelete('set null');

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('order_details');

    }

}

