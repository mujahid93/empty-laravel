<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateEmployeeStateTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('employee_state', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('emp_id')->unsigned()->nullable();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');

            $table->bigInteger('state_id')->unsigned()->nullable();

            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('employee_state');

    }

}

