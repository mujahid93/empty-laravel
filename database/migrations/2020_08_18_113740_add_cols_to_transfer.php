<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->string('tracking_no')->unique()->after('id')->nullable();
            $table->string('transport_type')->after('ip')->nullable();
            $table->string('vehicle')->after('transport')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transfers', function (Blueprint $table) {
            $table->dropColumn('tracking_no');
            $table->dropColumn('transport_type');
            $table->dropColumn('vehicle');
        });
    }
}
