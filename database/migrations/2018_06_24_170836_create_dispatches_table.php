<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateDispatchesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('dispatches', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('emp_id')->unsigned()->nullable();

            $table->foreign('emp_id')->references('id')->on('employees')->onDelete('set null');

            $table->bigInteger('order_detail_id')->unsigned()->nullable();

            $table->foreign('order_detail_id')->references('id')->on('order_details')->onDelete('set null');

            $table->date('date');

            $table->string('lr_number')->nullable();

            $table->string('transport')->nullable();

            $table->string('noc')->nullable();

            $table->string('freight')->nullable();

            $table->string('destination')->nullable();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('dispatches');

    }

}

