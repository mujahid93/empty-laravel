<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherBusinessToDealerVisit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dealer_visit', function (Blueprint $table) {
            $table->tinyInteger('none')->after('segments')->default(0);
            $table->tinyInteger('pesticide')->after('segments')->default(0);
            $table->tinyInteger('fertlizer')->after('segments')->default(0);
            $table->tinyInteger('seed')->after('segments')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dealer_visit', function (Blueprint $table) {
            $table->dropColumn('none');
            $table->dropColumn('pesticide');
            $table->dropColumn('fertlizer');
            $table->dropColumn('seed');
        });
    }
}
