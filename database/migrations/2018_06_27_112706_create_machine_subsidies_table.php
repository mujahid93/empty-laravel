<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateMachineSubsidiesTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {

        Schema::create('machine_subsidies', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('mch_id')->unsigned()->nullable();
            $table->foreign('mch_id')->references('id')->on('machine_skus')->onDelete('set null');

            $table->bigInteger('state_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');

            $table->integer('subsidy_dept_id')->unsigned()->nullable();
            $table->foreign('subsidy_dept_id')->references('id')->on('subsidy_dept')->onDelete('set null');

            $table->double('subsidy_price', 8, 2)->nullable();

            $table->timestamps();

        });

    }



    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('machine_subsidies');

    }

}

