<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_details', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('transfer_id')->unsigned()->nullable();
			$table->foreign('transfer_id')->references('id')->on('transfers')->onDelete('set null');
			$table->string('nav_no')->nullable();
			$table->string('item')->nullable();
			$table->bigInteger('quantity')->unsigned()->nullable();
			$table->bigInteger('management_quantity')->unsigned()->nullable();
			$table->bigInteger('final_quantity')->unsigned()->nullable();
			$table->bigInteger('type')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_details');
    }
}
