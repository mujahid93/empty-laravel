<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompletedToTourPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_plan', function (Blueprint $table) {
            $table->tinyInteger('completed')->after('is_deleted')->default(0);
        });

        Schema::table('demo_plans', function (Blueprint $table) {
            $table->string('transaction_code')->after('id')->nullable();
            $table->tinyInteger('completed')->after('is_deleted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_plan', function (Blueprint $table) {
            $table->dropColumn('completed');
        });

        Schema::table('demo_plans', function (Blueprint $table) {
            $table->dropColumn('transaction_code');
            $table->dropColumn('completed');
        });
    }
}
