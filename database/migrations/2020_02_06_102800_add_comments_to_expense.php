<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentsToExpense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_expense', function (Blueprint $table) {
            $table->string('localExpCmt')->after('localExps')->nullable();
            $table->string('lodgExpCmt')->after('lodgingExps')->nullable();
            $table->string('mealExpCmt')->after('mealExps')->nullable();
            $table->string('miscExpCmt')->after('miscExps')->nullable();
            $table->string('outExpCmt')->after('outStationExps')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_expense', function (Blueprint $table) {
            $table->dropColumn('localExpCmt');
            $table->dropColumn('lodgExpCmt');
            $table->dropColumn('mealExpCmt');
            $table->dropColumn('miscExpCmt');
            $table->dropColumn('outExpCmt');
        });
    }
}
