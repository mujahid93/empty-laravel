<?php



use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;



class CreateDealersTable extends Migration

{

    /**

     * Run the migrations.

     *

     * @return void

     */

    public function up()

    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('login_id')->unsigned()->nullable();
            $table->foreign('login_id')->references('id')->on('users')->onDelete('set null');

            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
            $table->bigInteger('state_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('set null');
            $table->bigInteger('district_id')->unsigned()->nullable();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('set null');
            
            $table->string('name')->nullable();
            $table->string('contact_person')->nullable();

            $table->string('address')->nullable();

            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('city')->nullable();
            $table->string('taluk')->nullable();
            $table->integer('pincode')->nullable();

            $table->string('gstn')->nullable();
            $table->string('vat')->nullable();
            $table->string('cst')->nullable();

            $table->string('mobile1')->nullable();
            $table->string('mobile2')->nullable();

            $table->integer('status')->default(1);

            $table->timestamps();
        });
    }

    

    /**

     * Reverse the migrations.

     *

     * @return void

     */

    public function down()

    {

        Schema::dropIfExists('dealers');

    }

    

}

