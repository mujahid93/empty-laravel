<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->bigInteger('emp_id')->unsigned()->nullable();
            $table->integer('req_branch')->nullable();
			$table->integer('req_godown')->nullable();
            $table->integer('source_branch')->nullable();
			$table->integer('source_godown')->nullable();
			$table->Integer('status')->default(1);
            $table->bigInteger('management_id')->unsigned()->nullable();
			$table->bigInteger('source_emp_id')->unsigned()->nullable();
            $table->string('ip')->nullable();
			$table->string('transport')->nullable();
			$table->string('location_mode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
