<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->bigInteger('id',true,true);
            $table->string('email')->unique();
            $table->string('imei')->unique()->nullable();
            $table->string('fcm_mobile',200)->nullable();
            $table->string('web_token',200)->nullable();
            $table->string('fcm_token')->nullable()->default(null);
            $table->integer('isDealer')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
