-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2018 at 03:10 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kisankraft`
--

-- --------------------------------------------------------


--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(15, 'akshay', 'dev2.it@kisankraft.co', '$2y$10$/ck0HMt/mmGP1.wTF/pcv.gv2vpJwviQTl6booqjqyVZ0tvAk6wmy', 1, 1, 'vlTABtIRzh1QNqtbRB0hmfLKG2hJ3KNs0uNFU4tcKZI0PX2eJBM5IQ5Kljbw', '2018-07-06 02:47:03', '2018-07-06 02:47:03'),
(16, 'Bhopal Dealer', 'bhopalDealer@gmail.com', '$2y$10$WK6baEaEMz8f1eLwy9KJz.CBNLqCaq41AIwSTI3J0.7iazxZIB4YK', 2, 1, 'wvB3lm2ktsalXZSu6hzEOHrXyegmM5yzhYo2TKTaRBGr0Qht4aBgCXaDhh48', '2018-07-06 03:02:58', '2018-07-06 03:02:58'),
(17, 'sales', 'sales@kisankraft.com', '$2y$10$.oF9ARPVvlkJWyILnjSTqOGfefqgrDcqB4uqMv2NCEzyegstlXMEW', 3, 1, 'ya3dFmaGM1Af3FSPdVn6RCIaNPkfvAPr0Mfr5rV1gJ8SxuLcsHGcedpntaGs', '2018-07-06 03:28:33', '2018-07-11 06:40:23'),
(18, 'mgmt', 'mgmt@kisankraft.com', '$2y$10$GUSYdTn5RHVPQSzWy9AHsOOhN8ZDXNaXzGStZMLL4lpufisgHFaW6', 4, 1, 'DSTNrsei5iCS2AdAK3xXH0KXmPIunqncxSBFbHdqdkWVg1iRKw05XJ9fETJK', '2018-07-06 03:31:46', '2018-07-06 03:31:46'),
(19, 'suma', 'suma@kisankraft.com', '$2y$10$iOFWgN/n7rGO0VDjNjqGtubpkpEbYiEZdPEER4r04X2/VXcekzcS6', 5, 1, 'xLayx7iVTWdLXODRtkn1M1yLUDH7tOwyc8aEkLTnZ0awXCP0tasB2K4cTAv7', '2018-07-11 06:40:06', '2018-07-11 06:40:06'),
(22, 'dealer1', 'dealer1@gmail.com', '$2y$10$JOlTHTakoVoAYZS8GTiEW.8L/0g0VVdkizLlhtui8pxWDv5yzPgDO', 2, 1, NULL, '2018-07-17 08:17:50', '2018-07-17 08:17:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
