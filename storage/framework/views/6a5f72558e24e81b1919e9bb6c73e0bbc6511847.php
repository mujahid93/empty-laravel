<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">



<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">

            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                        <div class="header">

                            <h2>

                                Executive Placed Orders

                            </h2>

                        </div>

                        <div class="header">
                                    <?php if(session()->has('exe_placed_fromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('exe_placed_fromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('exe_placed_toDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('exe_placed_toDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>
                        </div>

                         <div class="body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('exe_placed_state') && session()->get('exe_placed_state')==$state->id): ?>
                                                        <option value="<?php echo e($state->id); ?>" selected><?php echo e($state->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($state->id); ?>"><?php echo e($state->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <input type="hidden" id="selectedDist" value="<?php echo e(Session::get('e_mo_district')); ?>"  />
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $districtList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('exe_placed_district') && session()->get('exe_placed_district')==$district->id): ?>
                                                        <option value="<?php echo e($district->id); ?>" selected><?php echo e($district->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($district->id); ?>"><?php echo e($district->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $dealerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dealer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('exe_placed_dealer') && session()->get('exe_placed_dealer')==$dealer->id): ?>
                                                        <option value="<?php echo e($dealer->id); ?>" selected><?php echo e($dealer->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($dealer->id); ?>"><?php echo e($dealer->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_tracking_no_filter" class="form-control" value="<?php echo e(session()->get('exe_placed_tracking','')); ?>">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>


                         <?php if(session()->has('result')): ?>

                         <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                <?php echo e(Session::get('result')); ?>


                         </div>

                         <?php endif; ?>

                        <div id="order_list" class="body">

                            <div class="table-responsive">

                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>

                                        <tr>

                                            <th>Tracking No</th>

                                            <th>Dealer Name</th>

                                            <th>Sales Exec</th>

                                            <th>Order Date</th>

                                            <th width="40">View/Edit</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        <?php $__currentLoopData = $execApprovedOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td width="50">
                                                        <?php echo e($order->tracking_no); ?>

                                                        <?php if($order->OrderDetail[0]->classification=='Parts'): ?>
                                                            (<?php echo e($order->OrderDetail[0]->classification); ?>)
                                                        <?php endif; ?>
                                                    </td>
                                                    <td width="700"><?php echo e($order->dealer->name); ?></td>
                                                    <td width="700"><?php echo e($order->employee['name']); ?></td>
                                                    <td width="50"><?php echo e(date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))); ?></td>
                                                    <td>
                                                        <form action="/ViewExecOrderDetails" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="tracking_no" type="hidden" value="<?php echo e(encrypt($order->tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>

                                </table>

                                <?php echo e($execApprovedOrders->links()); ?>


                            </div>

                        </div>

                    </div>

                </div>

            </div>

<?php $__env->stopSection(); ?>

 <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-select/js/bootstrap-select.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/js/admin.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/js/demo.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js")); ?>"></script>

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>

    <script type="text/javascript">

        // Retrieve an instance of Firebase Messaging so that it can handle background

        // messages.

    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterPayPendingOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
               // window.location = window.location.pathname;
               //alert("reload called");
               if(jsonData["status"]=="OK")
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }


        var config = {

            apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",

            authDomain: "krishi-6d123.firebaseapp.com",

            databaseURL: "https://krishi-6d123.firebaseio.com",

            projectId: "krishi-6d123",

            storageBucket: "krishi-6d123.appspot.com",

            messagingSenderId: "988261907749"

          };

          firebase.initializeApp(config);

         const messaging = firebase.messaging();

         messaging.usePublicVapidKey('BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY');

       messaging.requestPermission().then(function() {

                          console.log('Notification permission granted.');

                          var user_email = "null";    

                              //Get current user email id

                              $.ajax({

                                    headers: {

                                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                                      },

                                    url: "/getCurrentUserEmail",

                                    type: 'POST',

                                    data: {'data': "null"},

                                    success: function(jsonData)

                                    {   

                                       messaging.getToken().then(function(currentToken) {

                                            if (currentToken) {

                                                sendTokenToServer(jsonData,currentToken);

                                                // updateUIForPushEnabled(currentToken);

                                            } else {

                                                // Show permission request.

                                                console.log('No Instance ID token available. Request permission to generate one.');

                                                // Show permission UI.

                                                updateUIForPushPermissionRequired();

                                                setTokenSentToServer(false);

                                            }

                                        }).catch(function(err) {

                                            console.log('An error occurred while retrieving token. ', err);

                                            showToken('Error retrieving Instance ID token. ', err);

                                            setTokenSentToServer(false);

                                        });

                                    },

                                    error: function (xhr, ajaxOptions, thrownError) 

                                    {

                                    }

                              });

                          // TODO(developer): Retrieve an Instance ID token for use with FCM.

                        }).catch(function(err) {

                          console.log('Unable to get permission to notify.', err);

                        });



          messaging.onMessage(function(payload) {

            console.log('Message received in home. ', payload['data']);

            var colorName = 'bg-black';

            var text = payload['data']['message'];

            var animateEnter = "animated fadeInDown";

            var animateExit = "animated fadeOutUp";

            var placementFrom = "top";

            var placementAlign = "center";



            if (colorName === null || colorName === '') { colorName = 'bg-black'; }

              if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }

              if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }

              if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }

              var allowDismiss = true;



              $.notify(

                  {

                      message: text

                  },

                  {

                      type: colorName,

                      allow_dismiss: allowDismiss,

                      newest_on_top: true,

                      timer: 1000,

                      placement: {

                          from: placementFrom,

                          align: placementAlign

                      },

                      animate: {

                          enter: animateEnter,

                          exit: animateExit

                      },

                      template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +

                      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +

                      '<span data-notify="icon"></span> ' +

                      '<span data-notify="title">{1}</span> ' +

                      '<span data-notify="message">{2}</span>' +

                      '<div class="progress" data-notify="progressbar">' +

                      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +

                      '</div>' +

                      '<a href="{3}" target="{4}" data-notify="url"></a>' +

                      '</div>'

                  }

              );

              $("#order_list").load(location.href + " #order_list");

          });



          $(document).ready(function(){



            $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/fetchNotifications",

                    type: 'POST',

                    data: {'data': "null"},

                    success: function(jsonData)

                    {   

                        $('#notify_count').text(jsonData.length);

                        for(var i=0; i<jsonData.length; i++)

                        {

                          $("#notify_header").prepend('<li> <a href="javascript:void(0);"><div class="menu-info"><h4>'+jsonData[i]['message']+'</h4><p><i class="material-icons">priority_high</i>order status</p></div></a></li>');

                        }

                    },

                    error: function (xhr, ajaxOptions, thrownError) 

                    {

                              

                    }

              });


          });



         function sendTokenToServer(email, currentToken)

         {

              $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/sendTokenToServer",

                    type: 'POST',

                    data: {'email': email, 'token': currentToken},

                    success: function(jsonData)

                    {   

                        // alert(jsonData);


                    },

                    error: function (xhr, ajaxOptions, thrownError) 

                    {

                              

                    }

              });

         }   





    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }



    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }



    </script>

    <?php echo $__env->yieldContent('scripts'); ?>


<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>