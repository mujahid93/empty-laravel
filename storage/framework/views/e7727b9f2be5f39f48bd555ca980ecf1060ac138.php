<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<div class="container">
		<div class="row">
			<!-- <div class="text-xs-center">
				<i class="fa fa-search-plus float-xs-left icon"></i>
				<h2>Estimate Travel Voucher</h2>
			</div>
			<hr> -->
			<div class="row">
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4">
					<h1 style="font-family: 'Monotype Corsiva';"><i>KisanKraft<sup class="font-20"> &reg;</sup></i></h1>
					<h5>ISO 9001:2015 Certified</h5>
					<h6 style="font-family:comic sans MS">Krushaka Mantram - Krushi Yantram</h6>
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
					<h2>Transfer Request</h2>
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
					<h5> Tracking No: <?php echo e($transfer->tracking_no); ?></h5>
					<h5> Transfer Req By: <?php echo e($transfer->reqBranch->name); ?></h5>
					<h5> Godown: <?php echo e($transfer->reqGodown->code); ?></h5>
					<br/>
					<h5> Transfer From: <?php echo e($transfer->sourceBranch->name); ?></h5>
					<h5> Godown: <?php echo e($transfer->sourceGodown->code); ?></h5>
					<h5>DATE : <?php echo e(date('d-m-Y', strtotime($transfer->created_at))); ?></h5>
					<br/>
				</div>
			</div>
			<div class="row" style="margin-top:-30px;">
				<div class="col-xs-5 col-md-5 col-lg-5 col-sm-5">
					<div class="card">
						<div class="card-header"><strong>Employee Name</strong></div>
							<div class="card-block">
								<?php echo e($transfer->employee->name); ?>- <?php echo e($transfer->employee->id); ?><br>
							</div>
						</br>
					</div>
				</div>
				<div class="col-xs-5 col-md-5 col-lg-5 col-sm-5">
					<div class="card">
						<div class="card-header"><strong>Shipping</strong></div>
							<div class="card-block">
								<?php echo e($transfer->transport); ?><br>
								Vehicle: <?php echo e($transfer->vehicle); ?>

							</div>
						</br>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card ">
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-sm">
								<?php $count=1; $total=0; ?>
								<thead>
									<tr>
										<td><strong>NAV No</strong></td>
										<td><strong>Item</strong></td>
										<td><strong>Orignal Qty</strong></td>
										<td><strong>Mgmt Approved Qty</strong></td>
										<td><strong>Dispatched Qty</strong></td>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td><?php echo e($item->nav_no); ?></td>
										<td><?php echo e($item->item); ?></td>
										<td><?php echo e($item->quantity); ?></td>
										<td><?php echo e($item->management_quantity); ?></td>
										<td><?php echo e($item->final_quantity); ?></td>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td colspan="5">
											<?php if($transfer->management!=null): ?>
												Management Approved By: <?php echo e($transfer->management->name); ?>

											<?php endif; ?>
										</td>
									</tr>
									<tr>
										<td  colspan="5">
											<?php if($transfer->dispatchBy!=null): ?>
												Dispatch Approved By: <?php echo e($transfer->dispatchBy->name); ?>

											<?php endif; ?>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
	.height {
	min-height: 200px;
	}
	.icon {
	font-size: 47px;
	color: #5CB85C;
	}
	.iconbig {
	font-size: 77px;
	color: #5CB85C;
	}
	.table > tbody > tr > .emptyrow {
	border-top: none;
	}
	.table > thead > tr > .emptyrow {
	border-bottom: none;
	}
	.table > tbody > tr > .emptyrow {
	border-top: 1px solid;
	border-bottom: 1px solid;
	}
	.table > tbody > tr > .highrow {
	border-top: none;
	border-bottom: none;
	}
	</style>
</body>
</html>