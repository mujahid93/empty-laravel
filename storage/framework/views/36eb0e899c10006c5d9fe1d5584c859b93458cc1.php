    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js")); ?>"></script>
    
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-select/js/bootstrap-select.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/js/admin.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/js/demo.js")); ?>"></script>

    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js")); ?>"></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase-app.js"></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase-messaging.js"></script>

    <script>

        // Retrieve an instance of Firebase Messaging so that it can handle background messages.

        var config = {

            apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",

            authDomain: "krishi-6d123.firebaseapp.com",

            databaseURL: "https://krishi-6d123.firebaseio.com",

            projectId: "krishi-6d123",

            storageBucket: "krishi-6d123.appspot.com",

            messagingSenderId: "988261907749"

          };

          firebase.initializeApp(config);

         const messaging = firebase.messaging();

         messaging.usePublicVapidKey('BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY');

       messaging.requestPermission().then(function() {

                          console.log('Notification permission granted.');

                          var user_email = "null";    

                              //Get current user email id

                              $.ajax({

                                    headers: {

                                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                                      },

                                    url: "/getCurrentUserEmail",

                                    type: 'POST',

                                    data: {'data': "null"},

                                    success: function(jsonData)

                                    {   

                                       messaging.getToken().then(function(currentToken) {

                                            if (currentToken) {

                                                sendTokenToServer(jsonData,currentToken);

                                                // updateUIForPushEnabled(currentToken);

                                            } else {

                                                // Show permission request.

                                                console.log('No Instance ID token available. Request permission to generate one.');

                                                // Show permission UI.

                                                updateUIForPushPermissionRequired();

                                                setTokenSentToServer(false);

                                            }

                                        }).catch(function(err) {

                                            console.log('An error occurred while retrieving token. ', err);

                                            showToken('Error retrieving Instance ID token. ', err);

                                            setTokenSentToServer(false);

                                        });

                                    },

                                    error: function (xhr, ajaxOptions, thrownError) 

                                    {

                                              

                                    }

                              });



                          // TODO(developer): Retrieve an Instance ID token for use with FCM.

                          // alert("permission granted");





                        }).catch(function(err) {

                          console.log('Unable to get permission to notify.', err);

                        });



          messaging.onMessage(function(payload) {

            console.log('Message received in home. ', payload['data']);

            var colorName = 'bg-black';

            var text = payload['data']['message'];

            var animateEnter = "animated fadeInDown";

            var animateExit = "animated fadeOutUp";

            var placementFrom = "top";

            var placementAlign = "center";

            if (colorName === null || colorName === '') { colorName = 'bg-black'; }

              if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }

              if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }

              if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }

              var allowDismiss = true;

              $.notify(

                  {

                      message: text

                  },

                  {

                      type: colorName,

                      allow_dismiss: allowDismiss,

                      newest_on_top: true,

                      timer: 1000,

                      placement: {

                          from: placementFrom,

                          align: placementAlign

                      },

                      animate: {

                          enter: animateEnter,

                          exit: animateExit

                      },

                      template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +

                      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +

                      '<span data-notify="icon"></span> ' +

                      '<span data-notify="title">{1}</span> ' +

                      '<span data-notify="message">{2}</span>' +

                      '<div class="progress" data-notify="progressbar">' +

                      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +

                      '</div>' +

                      '<a href="{3}" target="{4}" data-notify="url"></a>' +

                      '</div>'

                  }

              );



          });



          $(document).ready(function(){

            $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/GetNotificationCount",

                    type: 'POST',

                    data: {'data': "null"},

                    success: function(jsonData)
                    {   
                        $('#notify_count').text(jsonData);
                    },
                    error: function (xhr, ajaxOptions, thrownError) 
                    {
                        
                    }

              });

          });



         function sendTokenToServer(email, currentToken)
         {
            
              $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/sendTokenToServer",

                    type: 'POST',

                    data: {'email': email, 'token': currentToken},

                    success: function(jsonData)

                    {   

                        // alert(jsonData);

                    },

                    error: function (xhr, ajaxOptions, thrownError) 

                    {


                    }

              });

         }   

    </script>

    <?php echo $__env->yieldContent('scripts'); ?>