<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Incomplete Orders 
                            </h2>
                        </div>
                         <?php if(session()->has('result')): ?>
                         <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo e(Session::get('result')); ?>

                         </div>
                         <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Dealer Name</th>
                                            <th>Order Date</th>
                                            <th>Total Items</th>
                                            <th width="40">View</th>
                                        </tr>
                                    </thead>
                                  	<tbody>
                                  		<?php $__currentLoopData = $cartItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  		<tr>
                                  			<td><?php echo e($order->dealer->name); ?></td>
                                  			<td><?php echo e($order->order_date); ?></td>
                                  			<td><?php echo e($order->items); ?></td>
                                  			<td width="300">
                                              <form action="/ViewCheckoutPage" method="post">
                                                  <?php echo csrf_field(); ?>
                                                  <input name="dealer_id" type="hidden" value="<?php echo e($order->dealer_id); ?>">
                                                  <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                              </form>
                                        </td>
                                  		</tr>
                                  		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  	</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>