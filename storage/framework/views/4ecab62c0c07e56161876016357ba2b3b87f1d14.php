<?php $__env->startSection('content'); ?>
<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Final Expenses
                            </h2>
                        </div>

                        <div class="header">

                            <input type="button" id="btnget" value="Search"  onclick="FilterAllOrders();" />

                                    <?php if(session()->has('expenseFromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('expenseFromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('expenseToDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('expenseToDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>

                                    <div class="col-sm-3">
                                    <div class="form-group ">
                                            <select id="emp_filter"  class="form-control" onchange="FilterAllOrders()">
                                                    <?php if(session()->has('expenseEmp')): ?>
                                                      <option value="0">All</option>
                                                    <?php else: ?>
                                                      <option value="0" selected>All</option>
                                                    <?php endif; ?>
                                                    <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      <?php if(session()->has('expenseEmp') && $employee->id==session()->get('expenseEmp')): ?>
                                                          <option value="<?php echo e($employee->id); ?>" selected><?php echo e($employee->name); ?></option>                                     
                                                      <?php else: ?>
                                                          <option value="<?php echo e($employee->id); ?>"><?php echo e($employee->name); ?></option>
                                                      <?php endif; ?>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                            </select>
                                    </div>
                                </div>

                                  

                                    
                        </div>

                        

                    


                        <?php if(session()->has('result')): ?>
                             <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <?php echo e(Session::get('result')); ?>

                             </div>
                        <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Tracking_no</th>
                                            <th>Employee</th>
                                            <th>Expense</th>
                                            <th>Print</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $expenses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $exp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e(date('d-m-Y',strtotime($exp->startDate)).'--'.date('d-m-Y',strtotime($exp->endDate))); ?></td>
                                                <td><?php echo e($exp->transaction_code); ?></td>
                                                <td><?php echo e($exp->employee->name); ?></td>
                                                <td><?php echo e($exp->expense); ?></td>
                                                <?php if($exp->is_deleted==0): ?>
                                                <td><a href="<?php echo e('/printExpense/'.$exp->emp_id.'/'.$exp->token); ?>"> Print </a></td>
                                                <?php else: ?>
                                                  <td><a class="btn disabled"> Print </a></td>
                                                <?php endif; ?>
                                                <?php if($exp->is_deleted==0): ?>
                                                  <td><a href="<?php echo e('/blockExpense/'.$exp->emp_id.'/'.$exp->token); ?>"> Block </a></td>
                                                <?php else: ?>
                                                  <td><a href="<?php echo e('/unblockExpense/'.$exp->emp_id.'/'.$exp->token); ?>"> Unblock </a></td>
                                                <?php endif; ?>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($expenses->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>            
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    
    


   function  FilterAllOrders()
    {
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterExpenses",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'emp_id': $('#emp_filter').val()},
            success: function(jsonData)
            {
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

  function blockRow(rowId)
  {
    /*$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/RemoveImageFromFolder",
          type: 'POST',
          data: {
              'id': rowId
          },
          success: function(jsonData) {
              window.location.reload();
          }
      });*/
  }
  
    
</script>   
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>