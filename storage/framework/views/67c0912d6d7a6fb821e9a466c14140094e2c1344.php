<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css")); ?>" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/style.css")); ?>" href="css/style.css" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/themes/all-themes.css")); ?>" rel="stylesheet" />
</head>


<?php $__env->startSection('content'); ?>
            <div class="row clearfix">
                <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">

                    <div class="card">
                        <div class="header">
                            <h3>
                                Order Details
                            </h3>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Tracking Number</p>
                                        <h5 id="tracking_no"><?php echo e($placedOrders[0]->order->tracking_no); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Dealer Name</p>
                                        <h5><?php echo e($placedOrders[0]->order->dealer->name); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Date</p>
                                        <h5><?php echo e(date('d-m-Y', strtotime($placedOrders[0]->order_date))); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Status</p>
                                        <h4><span id="orderStatus" class="label bg-green">
                                            Management Approved
                                            </span></h4>
                                    </blockquote>
                                </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <?php $finalAmt = 0; ?>
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Sku</th>
                                            <th>Qty</th>
                                            <th>Branch</th>
                                            <th>Rate Type</th>
                                            <th>Rate</th>
                                            <th>GST (%)</th>
                                            <th style="background: #DBEEBC">Disc %</th>
                                            <th style="background: #DBEEBC" width="30">Disc Rate</th>
                                            <th>Final Amt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php unset($items); ?>
                                        <?php ($items = []); ?>
                                        <?php $totalPrice = 0; $freight = 0; $dis_rate = 0; $finalRate = 0;?>
                                        <?php $__currentLoopData = $placedOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $freight = $order->freight; $destination = $order->destination; $newDestFlag = 0;
                                                    if(!($order->order->dealer->address1 == $order->destination || $order->order->dealer->address2 == $order->destination || $order->order->dealer->address3 == $order->destination))
                                                        $newDestFlag = 1;
                                                    else
                                                        $newDestFlag = 0;
                                                 ?>
                                            <tr>
                                                <td><?php echo e($order->sku); ?></td>
                                                <td align="right">
                                                    <?php echo e($order->qty); ?>

                                                </td>
                                                <td>
                                                    <span class="custom-dropdown big">
                                                        <style type="text/css">
                                                            .custom-dropdown select {
                                                              background-color: #fff;
                                                              color: #000;
                                                              font-size: inherit;
                                                              padding: .5em;
                                                              padding-right: 3.5em; 
                                                              border: 0;
                                                              margin: 0;
                                                              border-radius: 3px;
                                                              text-indent: 0.01px;
                                                              text-overflow: '';
                                                              /*Hiding the select arrow for firefox*/
                                                              -moz-appearance: none;
                                                              /*Hiding the select arrow for chrome*/
                                                              -webkit-appearance:none;
                                                              /*Hiding the select arrow default implementation*/
                                                              appearance: none;
                                                            }
                                                            /*Hiding the select arrow for IE10*/
                                                        </style>
                                                        <select id="<?php echo e($order->id); ?>dispatchbranch" disabled="true">
                                                            <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                <?php if($order->branch == $branch->id): ?>
                                                                    <option value="<?php echo e($branch->id.':'.$order->id); ?>" selected><?php echo e($branch->code); ?></option>
                                                                <?php else: ?>
                                                                    <option value="<?php echo e($branch->id.':'.$order->id); ?>"><?php echo e($branch->code); ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                                                        </select>
                                                    </span>
                                                </td>
                                                <?php $price = 0;  $rate_without_tax = 0; ?>
                                                <?php 
                                                $rate_type = "";
                                                switch ($order->rate_type) {
                                                    case '0':
                                                        $rate_type = "Dealer Price";
                                                        $price = $order->item_rate;
                                                        $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                                        $finalRate = number_format((float)($price - $order->dis_per/100 * $price) * $order->qty, 2, '.', '');
                                                        $finalRate_NC = ($finalRate*$order->gst/100) + $finalRate ;
                                                        break;
                                                    
                                                    case '1':
                                                        $rate_type = "MRP";
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    case '2':
                                                        $rate_type = "Subsidy";
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    default:
                                                        break;
                                                }
                                                ?>
                                                <td>
                                                  <?php echo e($rate_type); ?>

                                                </td>
                                                <td align="right">
                                                    <input id="up-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($price)); ?>" readonly>
                                                </td>
                                                <td align="right"><?php echo e($order->gst); ?></td>
                                                <td align="right">
                                                   <input id="disperc-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->dis_per); ?>" readonly>
                                                </td>
                                                <td align="right">
                                                    <input id="disamt-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($dis_rate)); ?>" readonly>
                                                </td>
                                                <td align="right">
                                                     <?php
                                                            switch ($order->rate_type) {
                                                            case '0':
                                                                    $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                                                                    $totalPrice += $finalRate;
                                                                break;
                                                            
                                                            case '1':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            case '2':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            default:
                                                                break;
                                                        }
                                                        ?>
                                                        <?php echo e(IND_money_format($finalRate_NC)); ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td colspan="7"></td>
                                                    <td colspan="1">Freight</td>
                                                    <td align="right" colspan="1"><?php echo e(IND_money_format($freight)); ?></td>
                                                </tr>
                                                    <?php $finalTotal_With_Freight = 0; ?>
                                                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php 
                                                            $finalTotal_With_Freight = $finalTotal_With_Freight + (($freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
                                                        ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                    <?php $finalAmt += $freight + $totalPrice + $finalTotal_With_Freight; ?>

                                                <tr>
                                                   <td colspan="7"></td>
                                                        <td align="right"> TCS: </td>
                                                        <td align="right">
                                                            <?php echo e(tcsCalculation($placedOrders[0]->order->current_sales,$finalAmt)); ?>

                                                        </td>
                                                </tr>   

                                                <?php
                                                        $finalAmt=$finalAmt+tcsCalculation($placedOrders[0]->order->current_sales,$finalAmt);
                                                    ?> 
                                                <tr>
                                                    <td colspan="1"><b>Destination</b></td>
                                                    <td colspan="1">
                                                        <?php if($newDestFlag): ?>
                                                            <?php echo e($destination); ?><sup style="top: 0px;left: 10px"><span class="badge bg-green">Consignee address changed</span></sup>
                                                        <?php else: ?>
                                                            <?php echo e($destination); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td colspan="5"></td>
                                                    <td colspan="1">Total</td> 
                                                    
                                                    <td align="right" colspan="1"><input id="totalPrice" style="width: 100px" type="text" value="<?php echo e(IND_money_format($finalAmt)); ?>" readonly></td>
                                                </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                                <div class="row">
                                    <div class="m-l-40 pull-left">
                                            <form action="<?php echo e(route('order_pdf')); ?>" method="post">
                                                <?php echo csrf_field(); ?>
                                                <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->branch_tracking_no)); ?>">
                                                <button type="submit" class="btn btn-danger"><i class="col-white material-icons">picture_as_pdf</i> Print</button>
                                            </form>
                                    </div>

                                    <div class="m-l-40 pull-right">
                                            <form action="<?php echo e(route('order.exec_approve')); ?>" method="post">
							                    <?php echo csrf_field(); ?>
							                    <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->tracking_no)); ?>">
							                    <input type="hidden" name="orderStatus" value="7">
							                    <button type="submit" class="btn btn-link waves-effect">Cancel Order</button>
							                </form>
                                    </div>

                                </div>
                    </div>
                </div>
            </div>
<?php $__env->stopSection(); ?>
    
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/admin.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/demo.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/pages/ui/notifications.js")); ?>"></script>
  
<script type="text/javascript">

    var selectedBranch = 0;

    function FetchPriceRate(rowData)
    {
        var rowData = rowData.split(':');
        branchId = rowData[0];
        orderId = rowData[1];

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/FetchPriceRate",
            type: 'POST',
            data: {
                'branchId': branchId,
                'orderId': orderId,
            },
            success: function(jsonData) {
                // alert(JSON.stringify(jsonData));
                window.location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    
$(function () {
    $('.js-modal-buttons .btn').on('click', function () {
        var orderStatus = $('#orderStatus').text();
        $('#selOrderStatus').val(orderStatus);
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-');
        $('#mdModal').modal('show');
    });
});

</script>

<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>