<?php $__env->startSection('content'); ?>
<script type="text/javascript">
function back_block() {
    history.pushState(null, null, document.title);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.title);
    });
}
</script>

<body onload="javascript:back_block()"  class="four-zero-four"">

	<center>
	    <div class="four-zero-four-container m-t-100">
	        <div class="error-code"><h2>Order Status</h2></div>
	        <div class="error-message"><h5>
	        	<?php if(session()->has('result')): ?>
	        		<?php echo e(session()->get('result')); ?>

	        	<?php endif; ?>
	        </h5></div>
	        <div class="button-place m-t-70">
	            <a href="<?php echo e(route('home')); ?>" class="btn btn-default btn-lg waves-effect">Go to home</a>
	        </div>
	    </div>
	</center>
	
</body>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>