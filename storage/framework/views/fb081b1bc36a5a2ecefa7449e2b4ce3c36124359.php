<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    <?php if(session()->has('pending_parts_fromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('pending_parts_fromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('pending_parts_toDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('pending_parts_toDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>
                        </div>

                      

                        

                        
                        <div class="body">
                            <div class="table-responsive" id="tbl_aio">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <?php if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20): ?>
                                                <th>Dispatch Date</th>
                                            <?php else: ?>
                                                <th>Req Date</th>
                                            <?php endif; ?>
                                            <th>Tracking No</th>
                                            <th>Employee Name</th>
                                            <th>Req Branch</th>
                                            <th>Source Branch</th>
                                            <th>Status</th>
                                            <?php if(!$isNonEditable && Auth::user()->employee->role_id!=6): ?>
                                            <th width="40">View/Edit</th>
                                            <?php endif; ?>
                                            <th width="40">Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transfer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <?php if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20): ?>
                                                        <td><?php echo e(date('d-m-Y h:i A', strtotime($transfer->updated_at))); ?>

                                                        </td>
                                                    <?php else: ?>
                                                        <td><?php echo e(date('d-m-Y h:i A', strtotime($transfer->created_at))); ?>

                                                        </td>
                                                    <?php endif; ?>
                                                    <td><?php echo e($transfer->tracking_no); ?></td>
                                                    <td><?php echo e($transfer->employee->name); ?></td>
                                                    <td>
                                                       <?php echo e($transfer->reqBranch->name); ?>

                                                    </td>

                                                    <td>
                                                       <?php echo e($transfer->sourceBranch->name); ?>

                                                    </td>

                                                    <td>
                                                        <?php if($transfer->status==1): ?>
                                                            <span>Request Submitted</span>
                                                        <?php elseif($transfer->status==2): ?>
                                                            <span>Management Approved</span>

                                                        <?php elseif($transfer->status==3): ?>
                                                            <span>Request Dispatched</span>
                                                        <?php else: ?>
                                                            <span>Rejected</span>
                                                        <?php endif; ?>

                                                    </td>
                                                    <?php if(!$isNonEditable && Auth::user()->employee->role_id!=6): ?>
                                                    <td>
                                                        <form action="/getTransferDetails" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="transfer_id" type="hidden" value="<?php echo e($transfer->id); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                    <?php endif; ?>

                                                    <?php if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20): ?>
                                                    <td>
                                                        <form action="/printTransferBill" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="transfer_id" type="hidden" value="<?php echo e($transfer->id); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">print</i></button>
                                                        </form>
                                                    </td>
                                                    <?php else: ?>
                                                    <td>
                                                        <form action="/printTransferReq" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="transfer_id" type="hidden" value="<?php echo e($transfer->id); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">print</i></button>
                                                        </form>
                                                    </td>
                                                    <?php endif; ?>
                                                </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($data->links()); ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    
    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterPendingPartsOrder",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>