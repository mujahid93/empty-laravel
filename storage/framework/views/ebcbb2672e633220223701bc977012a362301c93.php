<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<div class="container">
		<div class="row">
			<!-- <div class="text-xs-center">
				<i class="fa fa-search-plus float-xs-left icon"></i>
				<h2>Estimate Travel Voucher</h2>
			</div>
			<hr> -->
			<div class="row">
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4">
					<h1 style="font-family: 'Monotype Corsiva';"><i>KisanKraft<sup class="font-20"> &reg;</sup></i></h1>
					<h5>ISO 9001:2015 Certified</h5>
					<h6 style="font-family:comic sans MS">Krushaka Mantram - Krushi Yantram</h6>
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
					<h2>Expense Voucher</h2>
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
					<h5> Final Transaction Code: <?php echo e($expense[0]->transaction_code); ?></h5>
					<h5> Tour/Demo Transaction Code: <?php echo e($expense[0]->tour_code); ?></h5>
					<h5>DATE : <?php echo e(date('d-M-Y h:i:s a')); ?></h5>
					<br/>
				</div>
			</div>
			<div class="row" style="margin-top:30px;">
				<div class="col-xs-5 col-md-5 col-lg-5 col-sm-5">
					<div class="card">
						<div class="card-header"><strong>Employee Name</strong></div>
							<div class="card-block">
								<?php echo e($expense[0]->employee->name); ?>- <?php echo e($expense[0]->employee->role->name); ?><br>
								<?php $__currentLoopData = $expense[0]->otherEmpObj($expense[0]->other_emp); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($emp->name); ?>-<?php echo e($emp->role->name); ?> <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                BANK: <?php echo e($expense[0]->employee->bank); ?><br>
								ACCOUNT.: <?php echo e($expense[0]->employee->account); ?><br>
								IFSC: <?php echo e($expense[0]->employee->ifsc); ?><br>
							</div>
						</br>
					</div>
				</div>
				<div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
					<div class="card">
						<div class="card-header"><strong>Employee Id</strong></div>
						<div class="card-block">
							<?php echo e($expense[0]->employee->id); ?>,<?php echo e($expense[0]->other_emp); ?>

						</div>		
					</div>
				</div>
				<div class="col-xs-3 col-md-3 col-lg-3 col-sm-3">
					<div class="card">
						<div class="card-header"><strong>Branch</strong> </div>
						<div class="card-block">
							<?php echo e($expense[0]->employee->branch->name); ?><br>
							<?php $__currentLoopData = $expense[0]->otherEmpObj($expense[0]->other_emp); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php echo e($emp->branch->name); ?> <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card ">
					<div class="card-header">
						<h3 class="text-xs-center">Travel summary</h3>
					</div>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-sm">
								<?php $count=1; $total=0; ?>
								<thead>
									<tr>
										<td><strong>Date</strong></td>
										<td><strong>Location</strong></td>
										<td><strong>Start Time</strong></td>
										<td><strong>End Time</strong></td>
										<td align="right" class="text-xs-right"><strong>Outstation Travel</strong></td>
										<td align="right" class="text-xs-right"><strong>Local Travel</strong></td>
										<td align="right" class="text-xs-center"><strong>Meals</strong></td>
										<td align="right" class="text-xs-center"><strong>lodging</strong></td>
										<td align="right" class="text-xs-center"><strong>Misc</strong></td>
										<td align="right" class="text-xs-right"><strong>Total</strong></td>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $expense; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td><?php echo e(date('d-m-Y', strtotime($item->date))); ?></td>
										<td><?php echo e($item->district_name); ?>-><?php echo e($item->city); ?></td>
										<td><?php echo e($item->startTime); ?></td>
										<td><?php echo e($item->endTime); ?></td>
                                        <td align="right" class="text-xs-right"><?php echo e($item->outstationExps); ?></td>        
										<td align="right" class="text-xs-right"><?php echo e($item->localExps); ?></td>
                                        <td align="right" class="text-xs-center"><?php echo e($item->mealExps); ?></td>
										<td align="right" class="text-xs-center">
                                                        <?php echo e($item->lodgingExps); ?>

                                        </td>
										<td align="right" class="text-xs-center"><?php echo e($item->miscExps); ?></td>
										<td align="right" class="text-xs-right">Rs <?php echo e($item->outstationExps+$item->localExps+$item->mealExps+$item->lodgingExps+$item->miscExps); ?></td>
										<?php $total+=$item->outstationExps+$item->localExps+$item->mealExps+$item->lodgingExps+$item->miscExps; ?>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td align="right" class="emptyrow text-xs-center" colspan="2"><strong>Total Exp.</strong></td>
										<td align="right" class="emptyrow text-xs-right">Rs <?php echo e($total); ?></td>
									</tr>
									<tr>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td align="right" class="highrow text-xs-center"  colspan="2"><strong>Less Adv.</strong></td>
										<td align="right" class="highrow text-xs-right"></td>
									</tr>
									<tr>
										<td class="emptyrow" colspan="7">
											<?php if($expense[0]->manager!=null): ?>
												Approved By: <?php echo e($expense[0]->manager->name); ?>

											<?php endif; ?>
										</td>
										<td align="right" class="emptyrow text-xs-center"  colspan="2"><strong>Net Due</strong></td>
										<td align="right" class="emptyrow text-xs-right"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
	.height {
	min-height: 200px;
	}
	.icon {
	font-size: 47px;
	color: #5CB85C;
	}
	.iconbig {
	font-size: 77px;
	color: #5CB85C;
	}
	.table > tbody > tr > .emptyrow {
	border-top: none;
	}
	.table > thead > tr > .emptyrow {
	border-bottom: none;
	}
	.table > tbody > tr > .emptyrow {
	border-top: 1px solid;
	border-bottom: 1px solid;
	}
	.table > tbody > tr > .highrow {
	border-top: none;
	border-bottom: none;
	}
	</style>
</body>
</html>