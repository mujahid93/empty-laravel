<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content'); ?>
<?php $user = Auth::user(); ?>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">playlist_add_check</i>
                        </div>
                        <div class="content">
                          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isManagement')): ?>
                            <div class="text">Payment Approved Orders</div>
                          <?php endif; ?>
                          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('isExecutive')): ?>
                            <div class="text">Placed Orders</div>
                          <?php endif; ?>
                            <div id="ordersCount" class="number count-to" data-from="0" data-to="125" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Employees</div>
                            <div id="empCount" class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Dealers</div>
                            <div id="dealerCount" class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">Monthly Sales</div>
                            <div id="monthlySales" class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>
<?php $__env->stopSection(); ?>

<script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>
<script>
	$(document).ready(function(){
		$.ajax({
			headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "/FetchDashboardSummary",
              type: 'POST',
              data: {'data' : 'null'},
              success: function(jsonData)
              {
                  $('#ordersCount').html(jsonData['ordersCount']);
                  $('#empCount').html(jsonData['empCount']);
                  $('#dealerCount').html(jsonData['dealerCount']);
                  $('#monthlySales').html(jsonData['monthlySales']);
              },
              error: function (xhr, ajaxOptions, thrownError) 
              {
                 alert(xhr.status);
                 alert(thrownError);
              }
		});
	})
</script>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>