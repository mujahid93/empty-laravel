<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    <?php if(session()->has('e_mo_fromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('e_mo_fromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('e_mo_toDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('e_mo_toDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>
                        </div>

                         <div class="body">
                            <div class="row">
                                <?php if(!isset($order_rejected_flag)): ?>
                                    <div class="col-sm-1">
                                        <div class="form-group ">
                                                <select id="_order_status_filter" class="form-control" onchange="FilterAllOrders()">
                                                    <?php if(session()->get('e_mo_orderStatus','-1')==0): ?>
                                                        <option value="0" selected="selected">All</option>
                                                    <?php else: ?>
                                                        <option value="0" >All</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==1): ?>
                                                        <option value="1" selected="selected">Pending</option>
                                                    <?php else: ?>
                                                        <option value="1" >Pending</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==2): ?>
                                                        <option value="2" selected="selected">Executive Placed</option>
                                                    <?php else: ?>
                                                        <option value="2" >Executive Placed</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==3): ?>
                                                        <option value="3" selected="selected">Mgmt Approved</option>
                                                    <?php else: ?>
                                                        <option value="3" >Mgmt Approved</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==4): ?>
                                                        <option value="4" selected="selected">Payment Verified</option>
                                                    <?php else: ?>
                                                        <option value="4" >Payment Verified</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==5): ?>
                                                        <option value="5" selected="selected">Invoice Raised</option>
                                                    <?php else: ?>
                                                        <option value="5" >Invoice Raised</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==6): ?>
                                                        <option value="6" selected="selected">Dispatched</option>
                                                    <?php else: ?>
                                                        <option value="6" >Dispatched</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==7): ?>
                                                        <option value="7" selected="selected">Rejected</option>
                                                    <?php else: ?>
                                                        <option value="7" >Rejected</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('e_mo_orderStatus','-1')==9): ?>
                                                        <option value="9" selected="selected">Parts Approval Pending</option>
                                                    <?php else: ?>
                                                        <option value="9" >Parts Approval Pending</option>
                                                    <?php endif; ?>

                                                </select>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('e_mo_state') && session()->get('e_mo_state')==$state->id): ?>
                                                        <option value="<?php echo e($state->id); ?>" selected><?php echo e($state->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($state->id); ?>"><?php echo e($state->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $districtList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('e_mo_district') && session()->get('e_mo_district')==$district->id): ?>
                                                        <option value="<?php echo e($district->id); ?>" selected><?php echo e($district->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($district->id); ?>"><?php echo e($district->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $dealerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dealer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('e_mo_dealer') && session()->get('e_mo_dealer')==$dealer->id): ?>
                                                        <option value="<?php echo e($dealer->id); ?>" selected><?php echo e($dealer->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($dealer->id); ?>"><?php echo e($dealer->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">

                                            <input type="text" id="_tracking_no_filter" class="form-control" value="<?php echo e(session()->get('e_mo_tracking','')); ?>">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="body">
                            <div class="table-responsive" id="tbl_aio">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Sales Exec</th>
                                            <th>Order Date</th>
                                            <?php if(!isset($order_rejected_flag)): ?>
                                                <th>Order Status</th>
                                            <?php endif; ?>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $allIndiaOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td width="50"><?php echo e($order->tracking_no); ?></td>
                                                    <td width="700"><?php echo e($order->dealer->name); ?></td>
                                                    <td width="400">
                                                            <?php echo e($order->placedBy->name); ?>

                                                    </td>
                                                    <td width="50"><?php echo e(date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))); ?></td>
                                                    <?php if(!isset($order_rejected_flag)): ?>
                                                        <td>
                                                            <?php switch($order->OrderDetail[0]->order_status):
                                                                case (1): ?>
                                                                    <span> Pending</span>
                                                                    <?php break; ?>

                                                                <?php case (2): ?>
                                                                    <span>Executive Placed</span>
                                                                    <?php break; ?>

                                                                <?php case (3): ?>
                                                                    <span>Payment Verified</span>
                                                                    <?php break; ?>    

                                                                <?php case (4): ?>
                                                                    <span>Management Approved by</span>
                                                                    <?php echo e($order->OrderDetail[0]->mgmt_approver->name); ?>

                                                                    <?php break; ?> 

                                                                <?php case (5): ?>
                                                                    <span>Invoice Raised</span>
                                                                    <?php break; ?> 

                                                                <?php case (6): ?>
                                                                    <span>Order Dispatched</span>
                                                                    <?php break; ?>

                                                                <?php case (9): ?>
                                                                    <span>Parts Order Placed</span>
                                                                    <?php break; ?>
                                                            <?php endswitch; ?>
                                                        </td>
                                                    <?php endif; ?>
                                                    <td width="50">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="tracking_no" type="hidden" value="<?php echo e(encrypt($order->tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($allIndiaOrders->links()); ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    
    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterExePlacedOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'order_status': $('#_order_status_filter').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>