<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css")); ?>" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/style.css")); ?>" href="css/style.css" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/themes/all-themes.css")); ?>" rel="stylesheet" />
    <style>

      #loaderBody{
        display: none;
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background-color: #000;
        z-index: 777;
        opacity: 0.2;
      }

      #loader {
        display: block;
        border: 5px solid #f3f3f3;
        border-radius: 50%;
        border-top: 5px solid #000;
        width: 200px;
        height: 200px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
        position: absolute;
        top:0;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 999;
        margin: auto;  
      }

      @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes  spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
      }
    </style>
</head>
<div id="loaderBody"><div id="loader"></div></div>

<?php $__env->startSection('content'); ?>
<div class="row clearfix">
    <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">

        <div class="card">
            <div class="header">
                <h3>
                    Order Details
                </h3>
            </div>

            <div style="display: none;">
                <input type="text" name="" id="orignalDealer_id" value="<?php echo e($placedOrders[0]->order->dealer->id); ?>">
                <input type="text" name="" id="orignalEmp_id" value="<?php echo e($placedOrders[0]->order->emp_id); ?>">
            </div>
            
            <div class="body">


                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                        <blockquote>
                            <p>Tracking Number</p>
                            <h5 id="tracking_no"><?php echo e($placedOrders[0]->tracking_no); ?></h5>
                        </blockquote>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                        <blockquote>
                            <p>Dealer Name</p>
                            <h5><?php echo e($placedOrders[0]->order->dealer->name); ?></h5>
                        </blockquote>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                        <blockquote>
                            <p>Order Date</p>
                            <h5><?php echo e(date('d-m-Y h:i A', strtotime($placedOrders[0]->order->created_at))); ?></h5>
                        </blockquote>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                        <blockquote>
                            <p>Order Status</p>
                            <h4><span id="orderStatus" class="label bg-pink">
                                <?php switch($placedOrders[0]->order_status):
                                case (1): ?>
                                <span>Dealer Placed</span>
                                <?php break; ?>

                                <?php case (2): ?>
                                <span>Executive Placed</span>
                                <?php break; ?>

                                <?php case (3): ?>
                                <span>Payment Verified</span>
                                <?php break; ?>    

                                <?php case (4): ?>
                                <span>Management Approved</span>
                                <?php break; ?> 

                                <?php case (5): ?>
                                <span>Invoice Uploaded</span>
                                <?php break; ?> 

                                <?php case (6): ?>
                                <span>Dispatched</span>
                                <?php break; ?>

                                <?php case (7): ?>
                                <span>Rejected</span>
                                <?php break; ?>

                                <?php case (9): ?>
                                <span>Parts Order Placed</span>
                                <?php break; ?>                 
                                <?php endswitch; ?>
                            </span></h4>
                        </blockquote>
                    </div>
                </div>

                <div class="body">
                    <div class="table-responsive" id="tableContainer">
                        <?php $finalAmt = 0; $count = 0; $chkCount=0; ?>
                        <?php $__currentLoopData = $dispatchBranches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dispBranch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <table id="parts_table-<?php echo e($dispBranch); ?>" class="table table-bordered table-striped table-hover js-basic-example">
                            <thead>
                                <tr>
                                    <th>NavNo</th>
                                    <th>Sku</th>
                                    <th>Qty</th>
                                    <th>Org. Qty</th>
                                    <th>Branch</th>
                                    <th>Rate Type</th>
                                    <th>Rate</th>
                                    <th>GST (%)</th>
                                    <th style="background: #DBEEBC">Disc % Req</th>
                                    <th style="background: #DBEEBC" width="30">Disc Rate Req</th>
                                    <th>Final Amt</th>
                                    <th>Split</th>
                                    <th>Remove</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="parts_body" class="partBody">
                                <?php unset($items); ?>
                                <?php ($items = []); ?>
                                <?php  $totalPrice = 0; $freight = 0; $freight_gst = 0;?>
                                <?php $__currentLoopData = $placedOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($dispBranch != $order->branch): ?>
                                <?php continue; ?>
                                <?php endif; ?>
                                <?php $freight = $order->freight; $destination = $order->destination; $newDestFlag = 0;
                                if(!($order->order->dealer->address1 == $order->destination || $order->order->dealer->address2 == $order->destination || $order->order->dealer->address3 == $order->destination))
                                    $newDestFlag = 1;
                                else
                                    $newDestFlag = 0;
                                ?>
                                <tr>
                                    <td><?php echo e($order->nav_no); ?></td>
                                    <td style="display: inline-table; width: 100px;" ondblclick="showPartDialog(this);"><?php echo e($order->sku); ?></td>
                                    <td align="right">
                                     <input id="qty-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->qty); ?>" oninput="ChangeQty(this.id)">
                                    </td>
                                    <td align="right">
                                     <input id="orignalqty-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->orignal_qty); ?>" oninput="ChangeQty(this.id)">
                                    </td>
                                 <td>
                                    <span class="custom-dropdown big">
                                        <style type="text/css">
                                            .custom-dropdown select {
                                              background-color: #1abc9c;
                                              color: #fff;
                                              font-size: inherit;
                                              padding: .5em;
                                              padding-right: 3.5em; 
                                              border: 0;
                                              margin: 0;
                                              border-radius: 3px;
                                              text-indent: 0.01px;
                                              text-overflow: '';
                                              /*Hiding the select arrow for firefox*/
                                              -moz-appearance: none;
                                              /*Hiding the select arrow for chrome*/
                                              -webkit-appearance:none;
                                              /*Hiding the select arrow default implementation*/
                                              appearance: none;
                                          }
                                          /*Hiding the select arrow for IE10*/
                                      </style>

                                      <select id="<?php echo e($order->id); ?>dispatchbranch" class="branchSelector" onchange="ChangeBranch(this);">
                                        <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($order->branch == $branch->id): ?>
                                        <option value="<?php echo e($branch->id); ?>" selected><?php echo e($branch->code); ?></option>
                                        <?php else: ?>
                                        <option value="<?php echo e($branch->id); ?>"><?php echo e($branch->code); ?></option>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                                    </select>
                                </span>
                            </td>
                            <?php $price = 0;  $rate_without_tax = 0; ?>
                            <?php 
                            $rate_type = "";
                            switch ($order->rate_type) {

                                case '0':
                                $rate_type = "Dealer Price";
                                $price = $order->item_rate;
                                $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                $finalRate = number_format((float)($price - $order->dis_per/100 * $price) * $order->qty, 2, '.', '');
                                $finalRate_NC = ($finalRate*$order->gst/100) + $finalRate ;
                                break;

                                case '1':
                                $rate_type = "MRP";
                                $price = $order->item_rate;
                                $dis_rate = $order->item_rate;
                                $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                $finalRate = $order->item_rate * $order->qty;
                                $finalRate_NC = $finalRate;
                                break;

                                case '2':
                                $rate_type = "Subsidy";
                                $price = $order->item_rate;
                                $dis_rate = $order->item_rate;
                                $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                $finalRate = $order->item_rate * $order->qty;
                                $finalRate_NC = $finalRate;
                                break;

                                default:
                                break;
                            }
                            ?>
                            <td>
                              <?php echo e($rate_type); ?>

                          </td>
                          <td align="right">
                            <input id="up-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($price)); ?>" readonly>
                        </td>
                        <td align="right"><?php echo e($order->gst); ?></td>
                        <?php if($rate_type == "MRP" || $rate_type == "Subsidy"): ?>
                        <td align="right">
                         <input id="disperc-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->dis_per); ?>" readonly>
                     </td>
                     <td align="right">
                        <input id="disamt-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($dis_rate)); ?>"  readonly>
                    </td>
                    <?php else: ?>
                    <td align="right">
                     <input id="disperc-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->dis_per); ?>"  readonly>
                 </td>
                 <td align="right">
                    <input id="disamt-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($dis_rate)); ?>" readonly>
                </td>
                <?php endif; ?>
                <td align="right">
                    <?php
                    switch ($order->rate_type) {
                        case '0':
                        $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                        $totalPrice += $finalRate;
                        break;

                        case '1':
                        $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                        $totalPrice += $rate_without_tax;
                        break;

                        case '2':
                        $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                        $totalPrice += $rate_without_tax;
                        break;

                        default:
                        break;
                    }
                    ?>
                    <input id="finalAmt-<?php echo e($order->id); ?>"  style="width: 100px" value="<?php echo e(IND_money_format($finalRate_NC)); ?>" readonly/>
                </td>

                <td>
                    <BUTTON class="btn btn-primary right" onclick="splitOrder(this);">Split</BUTTON>
                </td>
                <td>
                    <button type="button" class="close" aria-label="Close" onclick="removeItem(this);">
                      <span aria-hidden="true">&times;</span>
                    </button>  
                </td>
                
                <?php $chkCount++; ?>
            </tr>

            <?php $ship_id=$order->shipping_id; ?>


            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php    
            $ship=DB::table('shippings')->where('id',$ship_id)->first();
            $otherShip=$order->other_ship;
            if($ship!=null)
                $shipList=DB::table('shippings')->where('branch_id',$ship->branch_id)->get();
            else
                $shipList=null;

            ?>


            <tr>
                <td colspan="11"></td>
                <td colspan="1">Freight</td>
                <td align="right" colspan="1"><?php echo e(IND_money_format($freight)); ?></td>
            </tr>
            <?php $finalTotal_With_Freight = 0; ?>
            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php 
            if($totalPrice>0){
                $finalTotal_With_Freight = $finalTotal_With_Freight + (($freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
            }
            ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


            <tr>
                <td colspan="1">Shipping Mode</td>
                <td colspan="2">
                    <select name="shippingMode" id="shippingMode" class="form-control" required onchange="ChangeMode(this);">
                        <?php if($ship!=null && $ship->name=="Transport"): ?>
                        <option value="Transport" selected>Transport</option>
                        <?php else: ?>
                        <option value="Transport">Transport</option>
                        <?php endif; ?>
                        <?php if($ship!=null && $ship->name=="Courier"): ?>
                        <option value="Courier" selected>Courier</option>
                        <?php else: ?>
                        <option value="Courier">Courier</option>
                        <?php endif; ?>
                        <?php if($ship!=null && $ship->name=="By Hand"): ?>
                        <option value="By Hand" selected>By Hand</option>
                        <?php else: ?>
                        <option value="By Hand">By Hand</option>
                        <?php endif; ?>
                        <?php if($ship!=null && $ship->name=="By KKTT"): ?>
                        <option value="By KKTT" selected>By KKTT</option>
                        <?php else: ?>
                        <option value="By KKTT">By KKTT</option>
                        <?php endif; ?>
                        <?php if($ship!=null && $ship->name=="By Omni"): ?>
                        <option value="By Omni" selected>By Omni</option>
                        <?php else: ?>
                        <option value="By Omni">By Omni</option>
                        <?php endif; ?>
                        <?php if($ship==null): ?>
                        <option value="Other" selected>Other</option>
                        <?php else: ?>
                        <option value="Other">Other</option>
                        <?php endif; ?>
                    </select>
                </td>
                <td colspan="1">Shipping</td>
                <td colspan="6">
                    <?php if($ship!=null): ?>
                    <select name="shippingCompany" id="shippingCompany" class="form-control" required>
                        <?php $__currentLoopData = $shipList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shipping): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($shipping->id==$ship_id): ?>
                        <option value='<?php echo e($shipping->id); ?>' selected><?php echo e($shipping->agency); ?></option>
                        <?php else: ?>
                        <option value='<?php echo e($shipping->id); ?>'><?php echo e($shipping->agency); ?></option>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <?php else: ?>
                        <input type="text" name="otherShipping" value="<?php echo e($otherShip); ?>" />
                    <?php endif; ?>
                </td>
                <?php $finalAmt += $freight + $totalPrice + $finalTotal_With_Freight; ?>
                <td align="right" colspan="2">Total</td>
                <td align="right" colspan="1"><input id="totalPrice-<?php echo e($dispBranch); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($freight + $totalPrice + $finalTotal_With_Freight)); ?>" readonly></td>
            </tr>
        </tbody>
    </table>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
</div>
<?php if($placedOrders[0]->order_status == 9): ?>
<div class="row">
    <div class="pull-right button-demo btn-reject-order">
      <button type="button" data-color="red" name="" class="btn bg-purple waves-effect">Reject Order</button>
  </div>
  <div class="pull-right button-demo js-modal-buttons">
    <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect">Approve</button>
</div>
</div>
<?php endif; ?>
</div>
</div>
</div>
<div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="defaultModalLabel">Order Status</h3>
            </div>
            <div class="modal-body">
                <p>Submit order ?</p>
            </div>
            <div class="modal-footer">
                <form action="<?php echo e(route('order.exec_approve')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->tracking_no)); ?>">
                    <input type="hidden" name="orderStatus" value="2">
                    <input type="hidden" name="final_amt" value="<?php echo e(encrypt($finalAmt)); ?>">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>  
<div class="modal fade" id="rejectOrderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="defaultModalLabel">Reject Order</h3>
            </div>
            <div class="modal-body">
                <p>Do you want to reject the placed order ?</p>
            </div>
            <div class="modal-footer">
                <form action="<?php echo e(route('order.exec_approve')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->tracking_no)); ?>">
                    <input type="hidden" name="orderStatus" value="7">
                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mdModalUpdateOrder" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="defaultModalLabel">Order Status Edited</h3>
            </div>
            <div class="modal-body">
                <p>Submit order ?</p>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" onclick="updateAndApprove();">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  

<div class="modal fade" id="mdModalUpdateItem" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="defaultModalLabel">Update Parts Item</h3>
            </div>
            <div class="modal-body">
                <p>Select Part to Add.</p>
                <div>
                    <input list="partList" name="part" id="part" type="text">
                    <datalist id="partList">
                    </datalist>
                    <div class="loaderBody"> <div class="loader"></div></div>
                    <button onclick="searchPart();">Search</button>
                </div>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-link waves-effect" onclick="updateItem();">Yes</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<?php $__env->stopSection(); ?>

<script language="javascript">/*document.onmousedown=disableclick;status="Right Click Disabled";function disableclick(event){  if(event.button==2)   {     alert(status);     return false;       }}*/
</script>

<script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/js/admin.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/js/demo.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js")); ?>"></script>
<script src="<?php echo e(asset("/MaterialTheme/js/pages/ui/notifications.js")); ?>"></script>

<script type="text/javascript">

    var orignalData=<?php echo json_encode($placedOrders->toArray(), JSON_HEX_TAG); ?>;
    var isOrderEdited=false;
    var currentObj=null;
    var latestJsonData=null;
    var chkCount=<?php echo $chkCount; ?>;

    function showPartDialog(obj){
        var color = $(this).data('color');
        currentObj=obj;
        $('#mdModalUpdateItem .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#mdModalUpdateItem').modal('show');
    }   

    function searchPart(){
        var part=$('#part').val();
        if(part.length==0){
            alert("Search Box is empty");
            return;
        }
        $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/getSearchedParts",
                  type: 'POST',
                  data: {
                      'part': part
                  },
                  success: function(jsonData) {
                    latestJsonData=jsonData;
                    document.getElementById('partList').innerHTML = '';
                      $.each(jsonData, function(i, item) {
                          $('#partList').append($('<option>', {
                              value: item.nav_no,
                              text: item.sku
                          }));
                      });
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert(xhr.status);
                      alert(thrownError);
                  }
              });

    }


    function updateItem(){
        isOrderEdited=true;
        var selectedInv=$('#part').val();
        $.each(latestJsonData, function(i, item) {
            if(item.nav_no==selectedInv){
                //update data in row
                var currentNode=currentObj.parentElement.closest('tr');
                var tableBody=currentNode.parentElement;
                var table=tableBody.parentElement;
                var row=currentNode.rowIndex;
                table.rows[row].cells[0].innerHTML=selectedInv;
                table.rows[row].cells[1].innerHTML=item.sku;
                table.rows[row].cells[6].children[0].value=item.dp;
                table.rows[row].cells[7].innerHTML=item.gst;
                var disPer=table.rows[row].cells[8].children[0].value;
                table.rows[row].cells[9].children[0].value=item.dp-((item.dp/100)*disPer);
                calculateTotal();
                $('#mdModalUpdateItem').modal('hide');
                return;
            }                  
        });
    }

    function removeItem(obj){
        isOrderEdited=true;
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;
        table.deleteRow(currentNode.rowIndex);
        if(table.rows.length<=3){
            table.parentElement.removeChild(table);
        }
    }

    function addCheck(obj){
        $(obj).prop('checked', true);
    }


    function splitOrder(obj){
        //Before splitting validation is needed 
        var newNode=obj.parentElement.closest('tr').cloneNode(true);
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;

        //Reducing Qty into half for each branch
        var orignalQty=table.rows[currentNode.rowIndex].cells[2].children[0].value;

        if(orignalQty<=1){
            alert("Single value cannot be splitted");
            return false;
        }
        var qty=Math.ceil(orignalQty/2);
        table.rows[currentNode.rowIndex].cells[2].children[0].value=qty;
        chkCount++;

        //$(newNode.cells[13].children[0]).prop("id", "md_checkbox"+chkCount);
        //$(newNode.cells[13].children[1]).prop("for", "md_checkbox"+chkCount);
        newNode.cells[2].children[0].value=orignalQty-qty;
        (newNode.cells[4].children[0]).querySelectorAll(".branchSelector")[0].value=(currentNode.cells[4].children[0]).querySelectorAll(".branchSelector")[0].value;
        tableBody.insertBefore(newNode, tableBody.children[currentNode.rowIndex]);
        calculateTotal();
    }

    function calculateTotal(){
        var branchCount=$('#tableContainer').children().size();
        for (var i =0; i < branchCount; i++) {
            var table=$('#tableContainer').children().get(i);
            var rows=table.rows.length;
            var branchTotal=0;
            for (var row =1; row< rows-2; row++) {
                var qty=table.rows[row].cells[2].children[0].value;
                var rate=table.rows[row].cells[6].children[0].value;
                var gst=table.rows[row].cells[7].innerHTML;
                var discPer=table.rows[row].cells[8].children[0].value;
                var rateReq=table.rows[row].cells[9].children[0].value;
                var finalAmount=table.rows[row].cells[10].children[0];
                var taxable=(qty*rate)-(((qty*rate)/100)*discPer);
                var gstValue=(taxable/100)*gst;
                finalAmount.value=(taxable+gstValue).toFixed(2);
                branchTotal+=taxable+gstValue;
            }
            table.rows[rows-1].cells[5].children[0].value=branchTotal.toFixed(2);
        }
    }

    function ChangeBranch(obj){
        isOrderEdited=true;
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;
        if(obj.value==1 || obj.value==2){
            currentNode.cells[8].children[0].value="35";
            currentNode.cells[8].children[0].defaultValue="35";
        }
        else{
            currentNode.cells[8].children[0].value="30";
            currentNode.cells[8].children[0].defaultValue="30";
        }

        //First Checking is branch available?
        var branchCount=$('#tableContainer').children().size();
        var branchIndex=-1;
        for (var i =0; i < branchCount; i++) {
            var id=$('#tableContainer').children().get(i).id;
            var branch_id=(id.split('-'))[1];
            if(branch_id==obj.value)
                branchIndex=i;
        }
        //If branchIndex is not -1 then the brach table exist
        if(branchIndex!=-1){
            //remove node from current table and add to new table
            var newCurrentRow=currentNode.cloneNode(true);
            var newTable=$('#tableContainer').children().get(branchIndex);
            table.deleteRow(currentNode.rowIndex);
            if(table.rows.length<=3){
                table.parentElement.removeChild(table);
            }
            newTable.children[0].append(newCurrentRow);
            (newCurrentRow.cells[4].children[0]).querySelectorAll(".branchSelector")[0].value=obj.value;
        }
        else if(branchCount==1 && table.rows.length-2==1){
            //No Need to do anything
            table.id="parts_table-"+obj.value;
            setNewBranchShipping(obj.value,table);
        }
        else{
            var newCurrentRow=currentNode.cloneNode(true);
            var newTable=table.cloneNode(true);
            table.deleteRow(currentNode.rowIndex);
            if(table.rows.length==3)
                table.parentElement.removeChild(table);
            var rows=newTable.rows.length;
            for (var row = 1; row < rows-2; row++) {
                newTable.deleteRow(1);
            }
            newTable.id="parts_table-"+obj.value;
            newTable.children[0].append(newCurrentRow);
            $('#tableContainer').append(newTable);
            (newCurrentRow.cells[4].children[0]).querySelectorAll(".branchSelector")[0].value=obj.value;
            setNewBranchShipping(obj.value,newTable);
       }
       calculateTotal();
   }

   function setNewBranchShipping(new_branch_id,table){
        //Now we have table object we can get shipping mode and access the data from database
        var rows=table.rows.length;
        var mode=table.rows[rows-1].cells[1].children[0].value;

        

        if(mode!="Other"){
            $("#loaderBody").css("display","block");
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/GetShippingCompanies",
                  type: 'POST',
                  data: {
                      'shippingMode': mode,
                      'dispatchBranch': new_branch_id
                  },
                  success: function(jsonData) {
                      $(table.rows[rows-1].cells[3].children[0]).empty();
                      $.each(jsonData, function(i, item) {
                          $(table.rows[rows-1].cells[3].children[0]).append($('<option>', {
                              value: item.id,
                              text: item.agency
                          }));
                      });
                      $("#loaderBody").css("display","none");
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert(xhr.status);
                      alert(thrownError);
                  }
              });
        }
   }

   function ChangeMode(obj){
        isOrderEdited=true;
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;
        var branch_id=(table.id.split('-'))[1];
        var mode=obj.value;
        var rows=table.rows.length;
        if(mode!="Other"){
            $("#loaderBody").css("display","block");
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/GetShippingCompanies",
                  type: 'POST',
                  data: {
                      'shippingMode': mode,
                      'dispatchBranch': branch_id
                  },
                  success: function(jsonData) {
                      $(table.rows[rows-1].cells[3].children[0]).remove();
                         table.rows[rows-1].cells[3].innerHTML="<select name='shippingCompany' id='shippingCompany' class='form-control' required>";
                      $(table.rows[rows-1].cells[3].children[0]).empty();
                      $.each(jsonData, function(i, item) {
                          $(table.rows[rows-1].cells[3].children[0]).append($('<option>', {
                              value: item.id,
                              text: item.agency
                          }));
                      });
                      $("#loaderBody").css("display","none");
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert(xhr.status);
                      alert(thrownError);
                  }
              });
        }
        else{
            $(table.rows[rows-1].cells[3].children[0]).remove();
            table.rows[rows-1].cells[3].innerHTML="<input type='text' name='otherShipping'>";
        }

   }


    function ChangeQty(orderId) {
            isOrderEdited=true;
            calculateTotal();
    }

    function EnableApprove()
    {
        if($('#cbShortage').prop('checked')==true)
            $('#btnApprove').prop('disabled',false);
        else
            $('#btnApprove').prop('disabled',true);
    }

    
    $(function () {
        $('.js-modal-buttons .btn').on('click', function () {
        if(!isOrderEdited){
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
        }
        else{
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModalUpdateOrder .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModalUpdateOrder').modal('show');
        }
    });

    $('.btn-reject-order .btn').on('click', function () {
        var orderStatus = $('#orderStatus').text();
        $('#selOrderStatus').val(orderStatus);
        var color = $(this).data('color');
        $('#rejectOrderModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#rejectOrderModal').modal('show');
        });
    });

    function updateAndApprove(){
        $('#mdModalUpdateOrder').modal('hide');
        var branchCount=$('#tableContainer').children().size();
        var branchIndex=-1;
        var jsonRequest=[];
        $("#loaderBody").css("display","block");
        for (var i =0; i < branchCount; i++) {
            var table=$('#tableContainer').children().get(i);
            var branch_id=(table.id.split('-'))[1];
            var skuMap={};
            for (var j = 1; j < table.rows.length-2; j++) {
                var rowData={};
                if(table.rows[j].cells[1].innerHTML in skuMap){
                    var index=skuMap[table.rows[j].cells[1].innerHTML];
                    jsonRequest[index]["qty"]=parseInt(jsonRequest[index]["qty"])+ parseInt(table.rows[j].cells[2].children[0].value);
                }
                else{
                    rowData["nav_no"]=table.rows[j].cells[0].innerHTML;
                    rowData["sku"]=table.rows[j].cells[1].innerHTML;
                    rowData["qty"]=table.rows[j].cells[2].children[0].value;
                    rowData["original_qty"]=table.rows[j].cells[3].children[0].value;
                    rowData["branch"]=(table.rows[j].cells[4].children[0]).querySelectorAll(".branchSelector")[0].value;
                    rowData["tracking_no"]=orignalData[0]["tracking_no"];
                    if((table.rows[j].cells[5].innerHTML).replace(/\s/g, '')=="DealerPrice")
                        rowData["rate_type"]=0;
                    else if((table.rows[j].cells[5].innerHTML).replace(/\s/g, '')=="MRP")
                        rowData["rate_type"]=1;
                    else
                        rowData["rate_type"]=2;
                    rowData["item_rate"]=table.rows[j].cells[6].children[0].value;
                    rowData["gst"]=table.rows[j].cells[7].innerHTML;
                    rowData["discPer"]=table.rows[j].cells[8].children[0].value;
                    rowData["destination"]=orignalData[0]["destination"];
                    var mode=table.rows[table.rows.length-1].cells[1].children[0].value;
                    if(mode=="Other"){
                        rowData["shipping_id"]=null;
                        rowData["other_ship"]=table.rows[table.rows.length-1].cells[3].children[0].value;
                    }
                    else{
                        rowData["shipping_id"]=table.rows[table.rows.length-1].cells[3].children[0].value;
                        rowData["other_ship"]=null;
                    }
                    rowData["freight"]=table.rows[table.rows.length-2].cells[2].innerHTML;
                    if(rowData["qty"]>0){
                        jsonRequest.push(rowData); 
                        skuMap[rowData["sku"]]=Object.keys(skuMap).length; 
                    }
                }          
            }
        }
        if(jsonRequest.length==0){
            $("#loaderBody").css("display","none");
            alert("No Item qty available to proceed");
            return;
        }
        let objJsonStr = JSON.stringify(jsonRequest);
        let objJsonB64 = window.btoa(objJsonStr);
        console.log(objJsonB64);
        $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/updatePartsOrder",
                  type: 'POST',
                  data: {
                      'order_data': objJsonB64,
                      'isOrderEdited':isOrderEdited
                  },
                  success: function(jsonData) {
                    $("#loaderBody").css("display","none");
                    if(jsonData['status']==true)
                        document.location.replace("/PendingPartsOrders")
                    else
                        alert("Server error!!! Please try again.");
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      $("#loaderBody").css("display","none");
                      alert("Server not responding, Please check your internet connection.");
                      //alert(xhr.status);
                      //alert(thrownError);
                  }
              });

    }

</script>

<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>