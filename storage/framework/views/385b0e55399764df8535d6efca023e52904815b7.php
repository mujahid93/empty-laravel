<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Dealer Placed Orders 
                            </h2>
                        </div>
                         <?php if(session()->has('result')): ?>
                         <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo e(Session::get('result')); ?>

                         </div>
                         <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>
                                        <tr>
                                            <th>Purchase No</th>
                                            <th>Dealer Name</th>
                                            <th>Placed By</th>
                                            <th>Order Date</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $dealerPlacedOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td width="300"><?php echo e($order->tracking_no); ?></td>
                                                    <td width="300"><?php echo e($order->dealer->name); ?></td>
                                                    <td width="300">
                                                        <?php if($order->placedBy): ?>
                                                            <?php echo e($order->placedBy->user->name); ?>

                                                        <?php else: ?>
                                                            <?php echo e($order->dealer->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td width="300"><?php echo e(date('d-m-Y', strtotime($order->created_at))); ?></td>
                                                    <td width="300">
                                                        <form action="/ViewOrderDetailsPlacedByDealer" method="get">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="tracking_no" type="hidden" value="<?php echo e(encrypt($order->tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($dealerPlacedOrders->links()); ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
</script>   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>