<?php $__env->startSection('content'); ?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Management Approved Orders 
                            </h2>
                        </div>

                        <div class="header">
                                    <?php if(session()->has('mgmt_appoved_fromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('mgmt_appoved_fromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('mgmt_appoved_toDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('mgmt_appoved_toDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>
                        </div>

                         <div class="body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                <?php $__currentLoopData = $states; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $state): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('mgmt_appoved_state') && session()->get('mgmt_appoved_state')==$state->id): ?>
                                                        <option value="<?php echo e($state->id); ?>" selected><?php echo e($state->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($state->id); ?>"><?php echo e($state->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <input type="hidden" id="selectedDist" value="<?php echo e(Session::get('e_mo_district')); ?>"  />
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $districtList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $district): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('mgmt_appoved_district') && session()->get('mgmt_appoved_district')==$district->id): ?>
                                                        <option value="<?php echo e($district->id); ?>" selected><?php echo e($district->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($district->id); ?>"><?php echo e($district->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                <?php $__currentLoopData = $dealerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dealer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(session()->has('mgmt_appoved_dealer') && session()->get('mgmt_appoved_dealer')==$dealer->id): ?>
                                                        <option value="<?php echo e($dealer->id); ?>" selected><?php echo e($dealer->name); ?></option>                                     
                                                    <?php else: ?>
                                                        <option value="<?php echo e($dealer->id); ?>"><?php echo e($dealer->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_tracking_no_filter" class="form-control" value="<?php echo e(session()->get('mgmt_appoved_tracking','')); ?>">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>


                         <?php if(session()->has('result')): ?>
                         <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo e(Session::get('result')); ?>

                         </div>
                         <?php endif; ?>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr class="header">
                                            <th>Tracking No</th>
                                            <th>Branch Tracking No</th>
                                            <th>Branch</th>
                                            <th>Dealer Name</th>
                                            <th>Sales Exec</th>
                                            <th width='100'>Order Date</th>
                                            <th width="80">View</th>
                                            <th>Raise Invoice</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $mgmtApprOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $aorder): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($aorder->tracking_no); ?>

                                                        <?php if($aorder->readStatus==1): ?>
                                                            <span style="background: green; color:white;font-size: 11px;">New</span>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td><?php echo e($aorder->branch_tracking_no); ?></td>
                                                    <td><?php echo e($aorder->branch_name['name']); ?></td>
                                                    <td width="300"><?php echo e($aorder->order->dealer->name); ?></td>
                                                    <td width="150"><?php echo e($aorder->order->placedBy->name); ?></td>
                                                    <td width="180"><?php echo e(date('d-m-Y h:i A', strtotime($aorder->order->created_at))); ?></td>
                                                    <td>
                                                        <form action="/ViewMgmtApprOrderDetail" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($aorder->branch_tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-orange material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                    <td>
                                                        <form action="<?php echo e(route('invoice.new_invoice')); ?>" method="POST">
                                                            <?php echo csrf_field(); ?>
                                                            <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($aorder->branch_tracking_no)); ?>">
                                                            <button type="submit" class="btn bg-purple"><i class="col-black material-icons">fingerprint</i> Raise Invoice</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($mgmtApprOrders->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>      
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterMgmtApprovedOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }


    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
</script>   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>