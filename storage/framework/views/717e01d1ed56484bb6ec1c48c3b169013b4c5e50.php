<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Welcome To Kisankraft Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css")); ?>" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css")); ?>" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/style.css")); ?>" href="css/style.css" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.css")); ?>" rel="stylesheet" />
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/animate-css/animate.css")); ?>" rel="stylesheet" />
    <link  href="<?php echo e(asset("/MaterialTheme/css/themes/all-themes.css")); ?>" rel="stylesheet" />
    <?php echo $__env->yieldContent('css_links'); ?>
</head>
