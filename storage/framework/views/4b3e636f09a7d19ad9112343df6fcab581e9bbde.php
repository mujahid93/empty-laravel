<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css")); ?>" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/style.css")); ?>" href="css/style.css" rel="stylesheet">
    <link  href="<?php echo e(asset("/MaterialTheme/css/themes/all-themes.css")); ?>" rel="stylesheet" />
</head>

<?php $__env->startSection('content'); ?>
            <div class="row clearfix">
                <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">

                    <div class="card">
                        <div class="header">
                            <h3>
                                Order Details
                            </h3>
                        </div>
                        <div class="body">
                           

                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Tracking Number</p>
                                        <h5 id="tracking_no"><?php echo e($placedOrders[0]->tracking_no); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Dealer Name</p>
                                        <h5><?php echo e($placedOrders[0]->order->dealer->name); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Date</p>
                                        <h5><?php echo e(date('d-m-Y', strtotime($placedOrders[0]->order_date))); ?></h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Status</p>
                                        <h4><span id="orderStatus" class="label bg-pink">
                                            <?php switch($placedOrders[0]->order_status):
                                                 case (1): ?>
                                                                <span>Dealer Placed</span>
                                                                <?php break; ?>

                                                            <?php case (2): ?>
                                                                <span>Executive Placed</span>
                                                                <?php break; ?>

                                                            <?php case (3): ?>
                                                                <span>Payment Verified</span>
                                                                <?php break; ?>    

                                                            <?php case (4): ?>
                                                                <span>Management Approved</span>
                                                                <?php break; ?> 

                                                            <?php case (5): ?>
                                                                <span>Invoice Uploaded</span>
                                                                <?php break; ?> 

                                                            <?php case (6): ?>
                                                                <span>Dispatched</span>
                                                                <?php break; ?>

                                                            <?php case (7): ?>
                                                                <span>Rejected</span>
                                                                <?php break; ?>               
                                            <?php endswitch; ?>
                                            </span></h4>
                                    </blockquote>
                                </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <?php $finalAmt = 0; $count = 0  ?>
                                <?php $__currentLoopData = $dispatchBranches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dispBranch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                        <thead>
                                            <tr>
                                                <th>Sku</th>
                                                <th>Qty</th>
                                                <th>Branch</th>
                                                <th>Rate Type</th>
                                                <th>Rate</th>
                                                <th>GST (%)</th>
                                                <th>Spl. Disc %</th>
                                                <th style="background:#DBEEBC" >Disc % Req</th>
                                                <th style="background:#DBEEBC" width="30">Disc Rate Req</th>
                                                <th>Final Amt</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php unset($items); ?>
                                            <?php ($items = []); ?>
                                            <?php  $totalPrice = 0; $freight = 0; $freight_gst = 0;?>
                                            <?php $__currentLoopData = $placedOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($dispBranch != $order->branch): ?>
                                                    <?php continue; ?>
                                                <?php endif; ?>
                                                 <?php $freight = $order->freight; $destination = $order->destination; $newDestFlag = 0;
                                                    if(!($order->order->dealer->address1 == $order->destination || $order->order->dealer->address2 == $order->destination || $order->order->dealer->address3 == $order->destination))
                                                        $newDestFlag = 1;
                                                    else
                                                        $newDestFlag = 0;
                                                 ?>
                                                <tr>
                                                    <td style="display: inline-table; width: 100px;"><?php echo e($order->sku); ?></td>
                                                    <td align="right">
                                                       <input id="qty-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->qty); ?>" oninput="ChangeQty(this.id)">
                                                    </td>
                                                    <td>
                                                        <span class="custom-dropdown big">
                                                            <style type="text/css">
                                                                .custom-dropdown select {
                                                                  background-color: #1abc9c;
                                                                  color: #fff;
                                                                  font-size: inherit;
                                                                  padding: .5em;
                                                                  padding-right: 3.5em; 
                                                                  border: 0;
                                                                  margin: 0;
                                                                  border-radius: 3px;
                                                                  text-indent: 0.01px;
                                                                  text-overflow: '';
                                                                  /*Hiding the select arrow for firefox*/
                                                                  -moz-appearance: none;
                                                                  /*Hiding the select arrow for chrome*/
                                                                  -webkit-appearance:none;
                                                                  /*Hiding the select arrow default implementation*/
                                                                  appearance: none;
                                                                }
                                                                /*Hiding the select arrow for IE10*/
                                                            </style>

                                                            <select id="<?php echo e($order->id); ?>dispatchbranch" disabled="true">
                                                                <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if($order->branch == $branch->id): ?>
                                                                        <option value="<?php echo e($branch->id.':'.$order->id); ?>" selected><?php echo e($branch->code); ?></option>
                                                                    <?php else: ?>
                                                                        <option value="<?php echo e($branch->id.':'.$order->id); ?>"><?php echo e($branch->code); ?></option>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>        
                                                            </select>
                                                        </span>
                                                    </td>
                                                    <?php $price = 0;  $rate_without_tax = 0; ?>
                                                    <?php 
                                                    $rate_type = "";
                                                    switch ($order->rate_type) {
                                                        
                                                        case '0':
                                                            $rate_type = "Dealer Price";
                                                            $price = $order->item_rate;
                                                            $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                                            $finalRate = number_format((float)($price - $order->dis_per/100 * $price) * $order->qty, 2, '.', '');
                                                            $finalRate_NC = ($finalRate*$order->gst/100) + $finalRate ;
                                                            break;
                                                        
                                                        case '1':
                                                            $rate_type = "MRP";
                                                            $price = $order->item_rate;
                                                            $dis_rate = $order->item_rate;
                                                            $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                            $finalRate = $order->item_rate * $order->qty;
                                                            $finalRate_NC = $finalRate;
                                                            break;

                                                        case '2':
                                                            $rate_type = "Subsidy";
                                                            $price = $order->item_rate;
                                                            $dis_rate = $order->item_rate;
                                                            $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                            $finalRate = $order->item_rate * $order->qty;
                                                            $finalRate_NC = $finalRate;
                                                            break;

                                                        default:
                                                            break;
                                                    }
                                                    ?>
                                                    <td>
                                                      <?php echo e($rate_type); ?>

                                                    </td>
                                                    <td align="right">
                                                        <input id="up-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($price)); ?>" readonly>
                                                    </td>
                                                    <td align="right"><?php echo e($order->gst); ?></td>
                                                    <td align="right"><?php echo e($order->spl_disc); ?></td>
                                                    <?php if($rate_type == "MRP" || $rate_type == "Subsidy"): ?>
                                                        <td align="right">
                                                           <input id="disperc-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->dis_per); ?>" oninput="CalDiscount(this.id)" readonly>
                                                        </td>
                                                        <td align="right">
                                                            <input id="disamt-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($dis_rate)); ?>" oninput="CalDiscount(this.id)" readonly>
                                                        </td>
                                                    <?php else: ?>
                                                        <td align="right">
                                                           <input id="disperc-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e($order->dis_per); ?>" oninput="CalDiscount(this.id)">
                                                        </td>
                                                        <td align="right">
                                                            <input id="disamt-<?php echo e($order->id); ?>" style="width: 70px" type="text" value="<?php echo e(IND_money_format($dis_rate)); ?>" oninput="CalDiscount(this.id)">
                                                        </td>
                                                    <?php endif; ?>
                                                    <td align="right">
                                                        <?php
                                                            switch ($order->rate_type) {
                                                            case '0':
                                                                    $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                                                                    $totalPrice += $finalRate;
                                                                break;
                                                            
                                                            case '1':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            case '2':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            default:
                                                                break;
                                                        }
                                                        ?>
                                                        <input id="finalAmt-<?php echo e($order->id); ?>"  style="width: 100px" value="<?php echo e(IND_money_format($finalRate_NC)); ?>" readonly/>
                                                    </td>
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td colspan="8"></td>
                                                    <td colspan="1">Freight</td>
                                                    <td align="right" colspan="1"><?php echo e(IND_money_format($freight)); ?></td>
                                                </tr>
                                                    <?php $finalTotal_With_Freight = 0; ?>
                                                    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php 
                                                            if($totalPrice>0){
                                                            $finalTotal_With_Freight = $finalTotal_With_Freight + (($freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
                                                            }
                                                        ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                <?php $finalAmt += $freight + $totalPrice + $finalTotal_With_Freight; ?>

                                                <tr>
                                                    <td colspan="8"></td>
                                                        <td align="right"> TCS: </td>
                                                        <td align="right">
                                                            <?php echo e(tcsCalculation($placedOrders[0]->order->current_sales,$finalAmt)); ?>

                                                        </td>
                                                </tr>
                                                <?php
                                                        $finalAmt=$finalAmt+tcsCalculation($placedOrders[0]->order->current_sales,$finalAmt);
                                                    ?>
                                                <tr>
                                                    <td colspan="7"></td>
                                                    <td align="right" colspan="2">Total</td>
                                                    <td align="right" colspan="1"><input id="totalPrice-<?php echo e($dispBranch); ?>" style="width: 100px" type="text" value="<?php echo e(IND_money_format($finalAmt)); ?>" readonly></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1"><b>Destination</b></td>
                                                    <td colspan="1">
                                                        <?php if($newDestFlag): ?>
                                                            <?php echo e($destination); ?><sup style="top: 0px;left: 10px"><span class="badge bg-pink">Consignee address changed</span></sup>
                                                        <?php else: ?>
                                                            <?php echo e($destination); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                     <?php if($loop->last): ?>
                                                        <td colspan="3"></td>
                                                        <td colspan="1"><b>Account Balance</b></td>
                                                        <td colspan="1">
                                                            Rs <?php echo e($placedOrders[0]->order->p_balance); ?>

                                                        </td>
                                                        <td align="right" colspan="2"><b>Difference Amount</b></td>
                                                        <td align="right" colspan="1">
                                                            <?php if($placedOrders[0]->order->p_balance-$finalAmt < 0): ?>
                                                                <h2 class="card-inside-title col-red" id="fundCard"><input id="shortageFund" style="width: 100px" type="text" value="<?php echo e($placedOrders[0]->order->p_balance-$finalAmt); ?>" readonly></h2>
                                                                <div class="demo-checkbox" id="chkShortage">
                                                                    <input type="checkbox" id="cbShortage" onchange="EnableApprove()" class="filled-in chk-col-light-green" />
                                                                    <label for="cbShortage">Insufficient Funds</label>
                                                                </div>
                                                            <?php else: ?>
                                                                <h2 class="card-inside-title col-green" id="fundCard"><input id="sufficientFund" style="width: 70px" type="text" value="<?php echo e($placedOrders[0]->order->p_balance-$finalAmt); ?>" readonly></h2>
                                                                <div class="demo-checkbox" id="chkShortage" style="visibility: hidden;">
                                                                    <input type="checkbox" id="cbShortage" onchange="EnableApprove()" class="filled-in chk-col-light-green" />
                                                                    <label for="cbShortage">Insufficient Funds</label>
                                                                </div>
                                                            <?php endif; ?>
                                                        </td>
                                                    <?php endif; ?>
                                                </tr>
                                        </tbody>
                                    </table>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                            <?php if($placedOrders[0]->order_status == 3): ?>
                            <div class="row">
                                    <div class="pull-right button-demo btn-reject-order">
                                          <button type="button" data-color="red" name="" class="btn bg-purple waves-effect">Reject Order</button>
                                    </div>
                                    <div class="pull-right button-demo js-modal-buttons">
                                        <?php if($placedOrders[0]->order->p_balance-$finalAmt < 0): ?>
                                          <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect" disabled="true">Approve</button>
                                        <?php else: ?>
                                          <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect">Approve</button>
                                        <?php endif; ?>
                                    </div>
                            </div>
                            <?php endif; ?>
                    </div>
                </div>
            </div>
              <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Order Status</h3>
                        </div>
                        <div class="modal-body">
                            <p>Submit order ?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="<?php echo e(route('order.exec_approve')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->tracking_no)); ?>">
                                <input type="hidden" name="orderStatus" value="4">
                                <input type="hidden" name="final_amt" value="<?php echo e(encrypt($finalAmt)); ?>">
                                <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>  
              <div class="modal fade" id="rejectOrderModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Reject Order</h3>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to reject the placed order ?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="<?php echo e(route('order.exec_approve')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                <input type="hidden" name="tracking_no" value="<?php echo e(encrypt($placedOrders[0]->tracking_no)); ?>">
                                <input type="hidden" name="orderStatus" value="7">
                                <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>
<?php $__env->stopSection(); ?>

<script language="javascript">document.onmousedown=disableclick;status="Right Click Disabled";function disableclick(event){  if(event.button==2)   {     alert(status);     return false;       }}
    </script>

<script src="<?php echo e(asset("/MaterialTheme/plugins/jquery/jquery.min.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/admin.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/node-waves/waves.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/demo.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js")); ?>"></script>
    <script src="<?php echo e(asset("/MaterialTheme/js/pages/ui/notifications.js")); ?>"></script>
  
<script type="text/javascript">

    $(document).keydown(function (event) {
            if (event.keyCode == 123) { // Prevent F12
                return false;
            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
                return false;
            }
        });

    function ChangeQty(orderId) {
        var newQty = 1;
        var orderId = orderId.split('-');
        if($('#qty-'+orderId[1]).val() > 0)
            {
                newQty = $('#qty-'+orderId[1]).val();
            }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ChangeItemQtyByExec",
            type: 'POST',
            data: {
                'orderId': orderId[1],
                'qty': newQty,
            },
            success: function(jsonData) {
                $('#finalAmt-'+jsonData['orderId']).val(jsonData['finalPrice']);
                $('#totalPrice-'+jsonData['branch']).val(jsonData['totalPrice']);
                if((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal'])<0)
                {
                    $('#fundCard').removeClass('col-green');
                    $('#fundCard').addClass('col-red');
                    $('#shortageFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#sufficientFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#chkShortage').css('visibility','visible');
                    $("#btnApprove").prop('disabled', true);
                }
                else
                {
                    $('#fundCard').removeClass('col-red');
                    $('#fundCard').addClass('col-green');
                    $('#shortageFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#sufficientFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#chkShortage').css('visibility','hidden');
                    $("#btnApprove").prop('disabled', false);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function EnableApprove()
    {
        if($('#cbShortage').prop('checked')==true)
            $('#btnApprove').prop('disabled',false);
        else
            $('#btnApprove').prop('disabled',true);
    }

    function CalDiscount(orderId)
    {
        var orderId = orderId.split('-');

        if($('#disperc-'+orderId[1]).is(":focus"))
        {
            if($('#disperc-'+orderId[1]).val() <= 0)
            {
                $('#disamt-'+orderId[1]).val(0);
                $('#disperc-'+orderId[1]).val(0);
            }
            var amt=$('#up-'+orderId[1]).val();
            var up= amt.replace(/,/g, "");
            var disAmt = up - $('#disperc-'+orderId[1]).val()/100 * up ;
            $('#disamt-'+orderId[1]).val(disAmt.toFixed(2));
        }
        else
        if($('#disamt-'+orderId[1]).is(":focus"))
        {
            if($('#disamt-'+orderId[1]).val() <= 0 )
            {
                 $('#disamt-'+orderId[1]).val(0);
                 $('#disperc-'+orderId[1]).val(0);
            }
            var amt=$('#up-'+orderId[1]).val();
            var up= amt.replace(/,/g, "");
            var disPerc = (1 - $('#disamt-'+orderId[1]).val()/up)*100;
            $('#disperc-'+orderId[1]).val(disPerc.toFixed(2));
        }
        var discountPerc = $('#disperc-'+orderId[1]).val();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ApplyDiscount",
            type: 'POST',
            data: {
                'orderId': orderId[1],
                'discountPerc': discountPerc,
            },
            success: function(jsonData) {
                $('#finalAmt-'+jsonData['orderId']).val(jsonData['finalPrice']);
                $('#totalPrice-'+jsonData['branch']).val(jsonData['totalPrice']);
                if((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal'])<0)
                {
                    $('#fundCard').removeClass('col-green');
                    $('#fundCard').addClass('col-red');
                    $('#shortageFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#sufficientFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#chkShortage').css('visibility','visible');
                    $('#chkShortage').prop('checked',false);
                    $("#btnApprove").prop('disabled', true);
                }
                else
                {
                    $('#fundCard').removeClass('col-red');
                    $('#fundCard').addClass('col-green');
                    $('#shortageFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#sufficientFund').val((<?php echo e($placedOrders[0]->order->p_balance); ?>-jsonData['tfinal']).toFixed(2));
                    $('#chkShortage').css('visibility','hidden');
                    $('#chkShortage').prop('checked',true);
                    $("#btnApprove").prop('disabled', false);
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    $(function () {
        $('.js-modal-buttons .btn').on('click', function () {
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
        });

        $('.btn-reject-order .btn').on('click', function () {
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#rejectOrderModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#rejectOrderModal').modal('show');
        });
    });

</script>

<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>