<?php $__env->startSection('content'); ?>

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Approved Tour Information 
                            </h2>
                        </div>
                         <?php if(session()->has('result')): ?>
                         <div class="alert <?php echo e(Session::get('color')); ?> alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo e(Session::get('result')); ?>

                         </div>
                         <?php endif; ?>
                        <div class="body">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Employee</th>
                                            <th>District</th>
                                            <th>Print</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $tour; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($item->date); ?></td>
                                                <td><?php echo e($item->employee->name); ?></td>
                                                <td><?php echo e($item->district_name); ?></td>
												<?php if($item->is_deleted==0): ?>
                                                <td><a href="/tourPrint/<?php echo e($item->emp_id); ?>/<?php echo e($item->token); ?>">Print</a></td>
												<?php else: ?>
													<td><a >Print</a></td>
												<?php endif; ?>
													
												<?php if($item->is_deleted==0): ?>
                                                <td><a href="/tourDelete/<?php echo e($item->emp_id); ?>/<?php echo e($item->token); ?>/1">Block</a></td>
												<?php else: ?>
													<td><a href="/tourDelete/<?php echo e($item->emp_id); ?>/<?php echo e($item->token); ?>/0">Unblock</a></td>
												<?php endif; ?>
													
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                                <?php echo e($tour->links()); ?>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script type="text/javascript">
    
    
</script>   
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>