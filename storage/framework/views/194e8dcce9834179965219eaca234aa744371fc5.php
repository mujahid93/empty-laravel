<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

<?php $__env->startSection('content'); ?>

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    <?php if(session()->has('region_fromDate')): ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('region_fromDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>


                                    <?php if(session()->has('region_toDate')): ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::parse(session()->get('region_toDate'))->format('Y-m-d')); ?>">
                                    <?php else: ?>
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="<?php echo e(Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')); ?>">
                                    <?php endif; ?>
                        </div>
                         <div class="body">
                            <div class="row">
                                    <div class="col-sm-1">
                                        <div class="form-group ">
                                                <select id="_order_status_filter" class="form-control" onchange="FilterAllOrders()">
                                                    <?php if(session()->get('region_orderStatus','-1')==0): ?>
                                                        <option value="0" selected="selected">All</option>
                                                    <?php else: ?>
                                                        <option value="0" >All</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==1): ?>
                                                        <option value="1" selected="selected">Pending</option>
                                                    <?php else: ?>
                                                        <option value="1" >Pending</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==2): ?>
                                                        <option value="2" selected="selected">Executive Placed</option>
                                                    <?php else: ?>
                                                        <option value="2" >Executive Placed</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==3): ?>
                                                        <option value="3" selected="selected">Mgmt Approved</option>
                                                    <?php else: ?>
                                                        <option value="3" >Mgmt Approved</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==4): ?>
                                                        <option value="4" selected="selected">Payment Verified</option>
                                                    <?php else: ?>
                                                        <option value="4" >Payment Verified</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==5): ?>
                                                        <option value="5" selected="selected">Invoice Raised</option>
                                                    <?php else: ?>
                                                        <option value="5" >Invoice Raised</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==6): ?>
                                                        <option value="6" selected="selected">Dispatched</option>
                                                    <?php else: ?>
                                                        <option value="6" >Dispatched</option>
                                                    <?php endif; ?>


                                                    <?php if(session()->get('region_orderStatus','-1')==7): ?>
                                                        <option value="7" selected="selected">Rejected</option>
                                                    <?php else: ?>
                                                        <option value="7" >Rejected</option>
                                                    <?php endif; ?>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label id="lblDealer"></label>
                                        <div class="form-group" id="dealerGroup">
                                                <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                    <option value="0" >All</option>
                                                    <?php $__currentLoopData = $dealers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dealer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                        <?php if(session()->has('region_dealer') && session()->get('region_dealer')==$dealer->id): ?>
                                                            <option value="<?php echo e($dealer->id); ?>" selected><?php echo e($dealer->name); ?></option>                                     
                                                        <?php else: ?>
                                                            <option value="<?php echo e($dealer->id); ?>"><?php echo e($dealer->name); ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tbl_aio" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Placed By</th>
                                            <th>Order Date</th>
                                            <th>Order Status</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $regionOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($order == $regionOrders->first()): ?>    
                                                <tr style="background: #efe">
                                                    <td style="position: relative;" width="300"><?php echo e($order->tracking_no); ?> &nbsp<span class="badge bg-green" style="color: white;padding:5px;position: absolute; top: 10px;">new</span></td>
                                                    <td width="300"><?php echo e($order->dealer->name); ?></td>
                                                    <td width="300">
                                                        <?php if($order->placedBy!=null): ?>
                                                            <?php echo e($order->placedBy->name); ?>

                                                        <?php else: ?>
                                                            <?php echo e($order->dealer->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td width="300"><?php echo e(date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))); ?></td>
                                                    <td>
                                                        <?php switch($order->OrderDetail[0]->order_status):
                                                            case (1): ?>
                                                                <span>Dealer Placed</span>
                                                                <?php break; ?>

                                                            <?php case (2): ?>
                                                                <span>Executive Placed</span>
                                                                <?php break; ?>

                                                            <?php case (3): ?>
                                                                <span>Payment Verified</span>
                                                                <?php break; ?>    

                                                            <?php case (4): ?>
                                                                <span>Management Approved</span>
                                                                <?php break; ?> 

                                                            <?php case (5): ?>
                                                                <span>Invoice Uploaded</span>
                                                                <?php break; ?> 

                                                            <?php case (6): ?>
                                                                <span>Dispatched</span>
                                                                <?php break; ?>

                                                            <?php case (7): ?>
                                                                <span>Rejected</span>
                                                                <?php break; ?>
                                                        <?php endswitch; ?>
                                                    </td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="tracking_no" type="hidden" value="<?php echo e(encrypt($order->tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php else: ?>
                                                <tr>
                                                    <td width="300"><?php echo e($order->tracking_no); ?></td>
                                                    <td width="300"><?php echo e($order->dealer->name); ?></td>
                                                    <td width="300">
                                                        <?php if($order->placedBy): ?>
                                                            <?php echo e($order->placedBy->name); ?>

                                                        <?php else: ?>
                                                            <?php echo e($order->dealer->name); ?>

                                                        <?php endif; ?>
                                                    </td>
                                                    <td width="300"><?php echo e(date('d-m-Y', strtotime($order->created_at))); ?></td>
                                                    <td>
                                                         <?php switch($order->OrderDetail[0]->order_status):
                                                            case (1): ?>
                                                                <span>Dealer Placed</span>
                                                                <?php break; ?>

                                                            <?php case (2): ?>
                                                                <span>Executive Placed</span>
                                                                <?php break; ?>

                                                            <?php case (3): ?>
                                                                <span>Payment Verified</span>
                                                                <?php break; ?>    

                                                            <?php case (4): ?>
                                                                <span>Management Approved</span>
                                                                <?php break; ?> 

                                                            <?php case (5): ?>
                                                                <span>Invoice Uploaded</span>
                                                                <?php break; ?> 

                                                            <?php case (6): ?>
                                                                <span>Dispatched</span>
                                                                <?php break; ?>

                                                            <?php case (7): ?>
                                                                <span>Rejected</span>
                                                                <?php break; ?>
                                                        <?php endswitch; ?>
                                                    </td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            <?php echo csrf_field(); ?>
                                                            <input name="tracking_no" type="hidden" value="<?php echo e(encrypt($order->tracking_no)); ?>">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>

                                <?php echo e($regionOrders->links()); ?>   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

    function  FilterAllOrders()
    {
        // alert($('#_order_status_filter').val());
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterMyRegionOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'order_status': $('#_order_status_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                // window.location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.content', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>