@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }

    #map {
        height: 60%;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Employee Tracking
                            </h2>
                        </div>

                        <div class="header">

                                    @if(session()->has('tracking_date'))
                                        <input type="date" id="date" name="date" onchange="setFilter()" value="{{Carbon\Carbon::parse(session()->get('tracking_date'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="date" name="date" onchange="setFilter()" value="{{Carbon\Carbon::now()->format('Y-m-d')}}">
                                    @endif

                                    <div class="col-sm-3">

        

                                    <div class="form-group ">
                                            <select id="emp_filter"  class="form-control" onchange="setFilter()">
                                                    @foreach($empList as $employee)
                                                      @if(session()->has('tracking_emp') && session()->get('tracking_emp')==$employee->id)
                                                          <option value="{{$employee->id}}" selected>{{$employee->name}}</option>                                     
                                                      @else
                                                          <option value="{{$employee->id}}">{{$employee->name}}</option>
                                                      @endif
                                                  @endforeach
                                                   

                                            </select>
                                    </div>

                                </div>

                              

                                    
                        </div>

                        

                    


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">

  window.onload=function(){
    initMap();
  }

// Initialize and add the map
function initMap() {
  var locations = <?php print_r(json_encode($locations)) ?>;
  // The location of Uluru


  if(locations.length==0)
    return;

  var uluru = {lat: parseFloat(locations[0]['lat']), lng: parseFloat(locations[0]['lng'])};
  // The map, centered at Uluru

  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 13, center: uluru});
  // The marker, positioned at Uluru

   /*$.each( locations, function( index, value ){
      var uluru = {lat: parseFloat(locations[0]['lat']), lng: parseFloat(locations[0]['lng'])};
       new google.maps.Marker({position: uluru, map: map});
   });*/


   for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lng']),
        map: map,
        title: locations[i]['time'],
        label: (i+1).toString()
      });
    }
}

  function setFilter(){

    //Filter
    $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterEmployeeTracking",
            type: 'GET',
            data: {'date':$('#date').val(), 'emp_id': $('#emp_filter').val()},
            success: function(jsonData)
            {
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });  
  }
  
    
</script>   
 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxwIsTyBiYKmWp5arJlOHXo-o_uuCwFuo&callback=initMap">
</script>
  
@endSection