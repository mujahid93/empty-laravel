<!DOCTYPE html>
<html>
  <head>
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <!--The div element for the map -->
    <div id="map"></div>
    <script>
// Initialize and add the map
function initMap() {
  var locations = <?php print_r(json_encode($locations)) ?>;
  // The location of Uluru
  var uluru = {lat: parseFloat(locations[0]['lat']), lng: parseFloat(locations[0]['lng'])};
  // The map, centered at Uluru

  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 13, center: uluru});
  // The marker, positioned at Uluru

   /*$.each( locations, function( index, value ){
      var uluru = {lat: parseFloat(locations[0]['lat']), lng: parseFloat(locations[0]['lng'])};
       new google.maps.Marker({position: uluru, map: map});
   });*/


   for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i]['lat'], locations[i]['lng']),
        map: map,
        title: locations[i]['time'],
        label: (i+1).toString()
      });
    }
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxwIsTyBiYKmWp5arJlOHXo-o_uuCwFuo&callback=initMap">
    </script>
  </body>
</html>