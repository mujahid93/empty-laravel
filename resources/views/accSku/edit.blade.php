@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
  <h2 id="temp">Accessories Sku </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Accessories Sku Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('accSku.update',$accSku->id) }}" method="post">
                                @csrf
                                @method('PUT')
                                <label for="accGroup">Accessories Group</label>
                                <div class="form-group">
                                  <select  name="accGroup" id="accGroup" class="form-control" onchange="FetchSubgroupByGroup(this.value)">
                                    @foreach($accGroups as $item)
                                      @if($item == $accSku->grp)
                                        <option value='{{$item}}' selected>{{$item}}</option>
                                      @else
                                        <option value='{{$item}}'>{{$item}}</option>
                                      @endif
                                    @endforeach
                                  </select>
                                </div>
                                
                                <label for="accSubgroup" id="lblsubgrp">Accessories Subgroup</label>
                                <div class="form-group" id="divSubgrp">
                                  <select  name="accSubgroup" id="accSubgroup" class="form-control">
                                    @foreach($accSubgroups as $item)
                                      @if($item == $accSku->subgrp)
                                        <option value='{{$item}}' selected>{{$item}}</option>
                                      @else
                                        <option value='{{$item}}'>{{$item}}</option>
                                      @endif
                                    @endforeach
                                  </select>
                                </div>

                                <label for="sku">Sku</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="sku" value="{{$accSku->sku}}" id="" class="form-control" placeholder="Enter SKU Name">
                                    </div>
                                </div>  
                                <label for="taxCode" id="lbltaxCode">HSN Code</label>
                                  <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="hsn" id="" value="{{$accSku->hsn}}" class="form-control" placeholder="Enter HSN Code">
                                    </div>
                                </div>

                                <label for="gst" id="gst">GST Rate</label>
                                <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="gst" id="" value="{{$accSku->gst}}" class="form-control" placeholder="Enter GST Rate">
                                  </div>
                                </div>
                                <label for="dp">DP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="dp" id="dp" value="{{$accSku->dp}}" class="form-control" placeholder="Enter Dealer Price">
                                    </div>
                                </div>
                                <label for="mrp">MRP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="mrp" id="mrp" value="{{$accSku->mrp}}" class="form-control" placeholder="Enter MRP">
                                    </div>
                                </div>
                                <label for="subsidy">Subsidy</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select  name="subsidy" id="subsidy" class="form-control">
                                          @if($accSku->subsidy == 0)
                                            <option value='0' selected>No</option>
                                            <option value='1'>Yes</option>
                                          @else
                                            <option value='0'>No</option>
                                            <option value='1' selected>Yes</option>
                                          @endif
                                        </select>
                                    </div>
                                </div>
                                <label for="bulkQty">Bulk Qty</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="bulkQty" id="bulkQty" value="{{$accSku->bulk_qty}}" class="form-control" placeholder="Enter Bulk Qty">
                                    </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
@section('scripts')
<script type="text/javascript">
    function FetchSubgroupByGroup(grp)
    {
        $.ajax({
          headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },   
          url: "/FetchSubgroupByGroup",
          type: 'POST',
          data: {'grp': grp},
          success: function(jsonData)
          {
             $('#divSubgrp').remove();
             $('#lblsubgrp').after('<div class="form-group" id="divSubgrp"><div class="form-line"><select name="accSubgroup" class="form-control" id="accSubgroup"></select></div></div>');
             $.each(jsonData, function (i, item) {
                $('#accSubgroup').append($('<option>', { 
                    value: item,
                    text : item 
                }));
            });
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
</script>
@endsection


