@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
    <h2 id="temp">New Accessories Sku </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Accessories Sku Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('accSku.store') }}" method="post">
                                @csrf
                                <label for="accGroup">Accessories Group</label>
                                <div class="form-group">
                                  <select  name="accGroup" class="form-control"  onchange="FetchSubgroupByGroup(this.value)">
                                    @foreach($accGroups as $item)
                                      <option value='{{$item}}'>{{$item}}</option>
                                    @endforeach
                                  </select>
                                </div>

                                <label for="accSubgroup" id="lblsubgrp">Accessories Subgroup</label>
                                <div class="form-group" id="divSubgrp">
                                  <select  name="accSubgroup" id="accSubgroup" class="form-control">
                                    @foreach($accSubgroups as $item)
                                      <option value='{{$item}}'>{{$item}}</option>
                                    @endforeach
                                  </select>
                                </div>
                               <label for="sku">Sku</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="sku" id="" class="form-control" placeholder="Enter SKU Name">
                                    </div>
                                </div>
                                <label for="hsn" id="hsn">HSN Code</label>
                                <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="hsn" id="" class="form-control" placeholder="Enter HSN Code">
                                    </div>
                                </div>

                                <label for="gst" id="gst">GST Rate</label>
                                <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="gst" id="" class="form-control" placeholder="Enter GST Rate">
                                  </div>
                                </div>

                               
                                <label for="dp">DP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="dp" id="dp"  class="form-control" placeholder="Enter Dealer Price">
                                    </div>
                                </div>
                                <label for="mrp">MRP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="mrp" id="mrp"  class="form-control" placeholder="Enter MRP">
                                    </div>
                                </div>
                                <label for="subsidy">Subsidy</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select  name="subsidy" id="subsidy" class="form-control">
                                          <option value='0'>No</option>
                                          <option value='1'>Yes</option>
                                        </select>
                                    </div>
                                </div>
                                <label for="bulkQty">Bulk Qty</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="bulkQty" id="bulkQty"  class="form-control" placeholder="Enter Bulk Qty">
                                    </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#accGroup').trigger('change');
    });
    function FetchSubgroupByGroup(grp)
    {
        $.ajax({
          headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },   
          url: "/FetchSubgroupByGroup",
          type: 'POST',
          data: {'grp': grp},
          success: function(jsonData)
          {
             $('#divSubgrp').remove();
             $('#lblsubgrp').after('<div class="form-group" id="divSubgrp"><div class="form-line"><select name="accSubgroup" class="form-control" id="accSubgroup"></select></div></div>');
             $.each(jsonData, function (i, item) {
                $('#accSubgroup').append($('<option>', { 
                    value: item,
                    text : item 
                }));
            });
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
</script>
@endsection
