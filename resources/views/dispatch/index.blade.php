@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Dispatched Orders 
                            </h2>
                        </div>

                        <div class="header">
                                    @if(session()->has('dispatch_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('dispatch_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('dispatch_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('dispatch_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif
                        </div>

                         <div class="body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                @foreach($states as $state)
                                                    @if(session()->has('dispatch_state') && session()->get('dispatch_state')==$state->id)
                                                        <option value="{{$state->id}}" selected>{{$state->name}}</option>                                     
                                                    @else
                                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <input type="hidden" id="selectedDist" value="{{Session::get('e_mo_district')}}"  />
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                @foreach($districtList as $district)
                                                    @if(session()->has('dispatch_district') && session()->get('dispatch_district')==$district->id)
                                                        <option value="{{$district->id}}" selected>{{$district->name}}</option>                                     
                                                    @else
                                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                @foreach($dealerList as $dealer)
                                                    @if(session()->has('dispatch_dealer') && session()->get('dispatch_dealer')==$dealer->id)
                                                        <option value="{{$dealer->id}}" selected>{{$dealer->name}}</option>                                     
                                                    @else
                                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_tracking_no_filter" class="form-control" value="{{session()->get('dispatch_tracking','')}}">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer</th>
                                            <th>Sales Exec</th>
                                            <th>Date</th>
                                            <th>LR Number</th>
                                            <th>Transport</th>
                                            <th>No of cartons</th>
                                            <th>Freight</th>
                                            <th>Destination</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dispatches as $dispatch)
                                            <tr>
                                                <td>{{$dispatch->OrderDetail->branch_tracking_no}}</td>
                                                <td>{{$dispatch->OrderDetail->order->dealer->name}}</td>
                                                <td>{{$dispatch->OrderDetail->order->employee->name}}</td>
                                                <td>{{date('d-m-Y',strtotime($dispatch->date))}}</td>
                                                <td>{{$dispatch->lr_number}}</td>
                                                <td>{{$dispatch->transport}}</td>
                                                <td>{{$dispatch->noc}}</td>
                                                <td>{{$dispatch->freight}}</td>
                                                <td>{{$dispatch->destination}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $dispatches->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
/*    $(document).ready(function(){

        $('#pagination').change(function(){
            window.location = "{{ $dispatches->url(1) }}&items=" + this.value;
        });
        
        $('#_search').keyup(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterRows",
            type: 'POST',
            data: {'queryFilter':this.value},
            success: function(jsonData)
            {
                // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var deptId = jsonData.data[i].id;

                    var eBtn = '<form action="{{ route('dispatch.edit','+deptId+') }}" method="get"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">edit</i></button></form>';

                    var dBtn = '<form action="{{ route('dispatch.destroy','+deptId+') }}" method="post"> @csrf @method('PUT') <button type="submit" class="btn btn-default"><i class="col-red material-icons">delete</i></button></form>'

                    var row = $("<tr><td>"+jsonData.data[i].name+"</td><td>"+jsonData.data[i].code+"</td><td>"+eBtn+"</td><td>"+dBtn+"</td><tr>");
                    $("#_table").append(row);
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        })
    })*/


    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterDispatchOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'order_status': $('#_order_status_filter').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
    
</script>   
  
@endSection