@extends('layouts.header')
@extends('layouts.content')
@section('content')
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Dispatch Entry
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('dispatch.store') }}" method="post" enctype="multipart/form-data">
                            	@csrf
                                <label for="date">Date</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="date" name="date" id="date" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control" >
                                    </div>
                                </div>
                                <label for="lr_number">LR Number</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="lr_number" id="lr_number" class="form-control" placeholder="Enter LR number">
                                    </div>
                                </div>
                                <label for="transport">Transport</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="transport" id="transport" class="form-control" placeholder="Enter transport name">
                                    </div>
                                </div>
                                <label for="noc">No of cartons</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="noc" id="noc" class="form-control" placeholder="Enter no of cartons">
                                    </div>
                                </div>
                                <label for="freight">Freight</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="freight" id="freight" class="form-control" placeholder="Enter freight details">
                                    </div>
                                </div>
                                <label for="destination">Destination</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="destination" id="destination" class="form-control" placeholder="Enter destination">
                                    </div>
                                </div>
                                <input type="hidden" name="tracking_no" value="{{$order->tracking_no }}">
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
