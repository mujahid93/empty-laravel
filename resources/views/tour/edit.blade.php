@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Tour Information
                            </h2>
                            <br/>
                            <table>
                            <tr><td>Employee Name:</td><td><span> {{$tour[0]->empName}} </span></td></tr>
                            <tr><td>Date:</td><td><span> {{$tour[0]->date}}   </span></td></tr>
                            <tr><td>District Name:</td><td><span> {{$tour[0]->district_name}} </span></td></tr>
                            <tr><td>City Name:</td><td><span> {{$tour[0]->city}} </span></td></tr>
                            </table>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <form action="{{ route('tour.update') }}" method="post">
                            @csrf
                            <input type="hidden" name="emp_id" value="{{$tour[0]->emp_id}}">
                            <input type="hidden" name="token" value="{{$tour[0]->token}}">
                            <input type="hidden" name="tour_id" value="{{$tour[0]->tour_id}}">
                            <div role="tabpanel" class="tab-pane fade in active" id="Machinery">
                                <div class="card p-l-20 p-t-20" style="background: #fff">
                                    <div class="row clearfix">

                                        <div class="col-sm-8">
                                            <div class="col-sm-4">
                                                <label for="disPerc">State</label>
                                                <input type="text" name="state" class="allownumericwithdecimal form-control" value="{{$tour[0]->state}}"/>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="disPerc">District</label>
                                                <input type="text" name="district" class="allownumericwithdecimal form-control" value="{{$tour[0]->district_name}}"/>
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="disPerc">City</label>
                                                <input type="text" name="city" class="allownumericwithdecimal form-control" value="{{$tour[0]->city}}"/>
                                            </div>
                                            <div class="col-sm-12">
                                                @foreach($dealers as $item)
                                                    <span class="card" style="padding: 5px;">{{$item->dealer_name}}</span>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-sm-4 clearfix">
                                            <div class="col-sm-6">
                                                <label for="disPerc">Outstation Expenses</label>
                                                <input type="number" name="outstationExps" id="outstationExps" oninput="calTotal()" class="allownumericwithdecimal form-control" value="{{$tour[0]->outstationExps}}" />
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="disPerc">Local Expenses</label>
                                                <input type="number" name="localExps" id="localExps" oninput="calTotal()" class="allownumericwithdecimal form-control" value="{{$tour[0]->localExps}}"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="disPerc">Meal Expenses</label>
                                                <input type="number" name="mealExps" id="mealExps" oninput="calTotal()" class="allownumericwithdecimal form-control" value="{{$tour[0]->mealExps}}"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="disPerc">Lodging Expenses</label>
                                                <input type="number" name="lodgingExps" id="lodgingExps" oninput="calTotal()" class="allownumericwithdecimal form-control" value="{{$tour[0]->lodgingExps}}"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="disPerc">Misc Expenses</label>
                                                <input type="number" name="miscExps" id="miscExps" oninput="calTotal()" class="allownumericwithdecimal form-control" value="{{$tour[0]->miscExps}}"/>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="disPerc">Total Expenses</label>
                                                <input type="text" name="total" id="total" class="allownumericwithdecimal form-control" readonly value="{{$tour[0]->expenses}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-t-10 m-b-20">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <button type="submit" style="border-radius: 20px" name="orderSubmit" value="addToCart" class="btn bg-blue btn-block pull-right">
                                                    Update
                                                </button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     
      
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    

        function showConfirmDialog(){
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
        }

        function showRejectDialog(){
                var orderStatus = $('#orderStatus').text();
                $('#selOrderStatus').val(orderStatus);
                var color = $(this).data('color');
                $('#rejectTourModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
                $('#rejectTourModal').modal('show');
        }

        function calTotal(){
            var total=parseFloat($('#outstationExps').val())+parseFloat($('#localExps').val())+parseFloat($('#lodgingExps').val())+parseFloat($('#mealExps').val())+parseFloat($('#miscExps').val());
            $('#total').val(Math.round(total));
        }
   
    
</script>   
  
@endSection