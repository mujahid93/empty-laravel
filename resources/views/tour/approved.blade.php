@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Approved Tour Information 
                            </h2>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Employee</th>
                                            <th>District</th>
                                            <th>Print</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tour as $item)
                                            <tr>
                                                <td>{{$item->date}}</td>
                                                <td>{{$item->employee->name}}</td>
                                                <td>{{$item->district_name}}</td>
												@if($item->is_deleted==0)
                                                <td><a href="/tourPrint/{{$item->emp_id}}/{{$item->token}}">Print</a></td>
												@else
													<td><a >Print</a></td>
												@endif
													
												@if($item->is_deleted==0)
                                                <td><a href="/tourDelete/{{$item->emp_id}}/{{$item->token}}/1">Block</a></td>
												@else
													<td><a href="/tourDelete/{{$item->emp_id}}/{{$item->token}}/0">Unblock</a></td>
												@endif
													
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $tour->links() }}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    
</script>   
  
@endSection