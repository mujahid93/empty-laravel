@extends('layouts.header')
@extends('layouts.content')

@section('content')
<div class="block-header">
	<h2 id="temp">New Tour </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Enter Tour Information
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('tour.store') }}" method="post">
                            	@csrf
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="_name" class="form-control" placeholder="Enter tour name">
                                    </div>
                                </div>
                                <label for="date_of_join">Start Date</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <input type="date" name="startDate" id="startDate" class="datepicker form-control">
                                        </div>
                                </div>
                                <label for="date_of_join">End Date</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <input type="date" name="endDate" id="endDate" class="datepicker form-control">
                                        </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">Next</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
	
@endSection