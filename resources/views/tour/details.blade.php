@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Tour Information
                            </h2>
                            <br/>
                            <table>
                            <tr><td>Employee Name:</td><td><span> {{$data[0]->name}} </span></td></tr>
                            <tr><td>Date:</td><td><span> {{$data[0]->start_date}} to {{$data[0]->end_date}}  </span></td></tr>
                            <tr><td>District Name:</td><td><span> {{$data[0]->district}} </span></td></tr>
                            <tr><td>City Name:</td><td><span> {{$data[0]->city}} </span></td></tr>
                            <tr><td>Dealers Name:</td><td><span> {{ str_limit($data[0]->dealers, $limit = 200, $end = '...') }} </span></td></tr>
                            </table>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>State</th>
                                            <th>District</th>
                                            <th>City</th>
                                            <th>Expenses</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total=0; ?>
                                        @foreach($tour as $item)
                                            <?php $total+=$item->expenses; ?>
                                            <tr>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ $item->state }}</td>
                                                <td>{{ $item->district_name }}</td>
                                                <td>{{ $item->city }}</td>
                                                <td>{{ $item->expenses }}</td>
                                                <td><a href="/tourEdit/{{$item->tour_id}}">Edit</a></td>
                                            </tr> 
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                             <div class="row">
                                 <div class="pull-right button-demo btn-reject-order">
                                          <button type="button" class="btn btn-default">Expenses Total Rs {{IND_money_format($total)}}</button>
                                    </div>
                             </div>
                              <div class="row">
                                    <div class="pull-right button-demo btn-reject-order" onclick="showRejectDialog();">
                                          <button type="button" data-color="red" name="" class="btn bg-purple waves-effect">Reject Tour</button>
                                    </div>
                                    <div class="pull-right button-demo js-modal-buttons" onclick="javaScript:showConfirmDialog();">
                                          <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect">Approve</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     

             <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Approve Tour</h3>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to approve tour ?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="{{ route('tour.approve') }}" method="post">
                                @csrf
                                <input type="hidden" name="emp_id" value="{{$data[0]->emp_id}}">
                                <input type="hidden" name="token" value="{{$data[0]->token}}">
                                <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>  
              <div class="modal fade" id="rejectTourModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  bg-red">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Reject Tour</h3>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to reject this tour?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="{{ route('tour.reject') }}" method="post">
                                @csrf
                                <input type="hidden" name="emp_id" value="{{$data[0]->emp_id}}">
                                <input type="hidden" name="token" value="{{$data[0]->token}}">
                                <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>       
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    

        function showConfirmDialog(){
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
        }

        function showRejectDialog(){
                var orderStatus = $('#orderStatus').text();
                $('#selOrderStatus').val(orderStatus);
                var color = $(this).data('color');
                $('#rejectTourModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
                $('#rejectTourModal').modal('show');
        }
   
    
</script>   
  
@endSection