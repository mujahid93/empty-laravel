@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Machine Sku Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('machineSku.update',$mchSku->id) }}" method="post">
                            	  @csrf
                                @method('PUT')
                                <label for="mchGroup">Machine Group</label>
                                <div class="form-group">
                                  <select  name="mchGroup" id="mchGroup" class="form-control" onchange="FetchSubgroupByGroup(this.value)">
                                    @foreach($mchGroups as $item)
                                      @if($item == $mchSku->grp)
                                        <option value='{{$item}}' selected>{{$item}}</option>
                                      @else
                                        <option value='{{$item}}'>{{$item}}</option>
                                      @endif
                                    @endforeach
                                  </select>
                                </div>
                                
                                <label for="mchSubgroup" id="lblsubgrp">Machine Subgroup</label>
                                <div class="form-group" id="divSubgrp">
                                  <select  name="mchSubgroup" id="mchSubgroup" class="form-control">
                                    @foreach($mchSubgroups as $item)
                                      @if($item == $mchSku->subgrp)
                                        <option value='{{$item}}' selected>{{$item}}</option>
                                      @else
                                        <option value='{{$item}}'>{{$item}}</option>
                                      @endif
                                    @endforeach
                                  </select>
                                </div>
                                <label for="sku">Sku</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="sku" value="{{$mchSku->sku}}" id="" class="form-control" placeholder="Enter SKU Name">
                                    </div>
                                </div>  
                                <label for="taxCode" id="lbltaxCode">HSN Code</label>
                                  <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="hsn" id="" value="{{$mchSku->hsn}}" class="form-control" placeholder="Enter HSN Code">
                                    </div>
                                </div>

                                <label for="gst" id="gst">GST Rate</label>
                                <div class="form-group">
                                  <div class="form-line">
                                        <input type="text" name="gst" id="" value="{{$mchSku->gst}}" class="form-control" placeholder="Enter GST Rate">
                                  </div>
                                </div>
                                <label for="dp">DP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="dp" id="dp" value="{{$mchSku->dp}}" class="form-control" placeholder="Enter Dealer Price">
                                    </div>
                                </div>
                                <label for="mrp">MRP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="mrp" id="mrp" value="{{$mchSku->mrp}}" class="form-control" placeholder="Enter MRP">
                                    </div>
                                </div>
                                <label for="subsidy">Subsidy</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <select  name="subsidy" id="subsidy" class="form-control">
                                          @if($mchSku->subsidy == 0)
                                            <option value='0' selected>No</option>
                                            <option value='1'>Yes</option>
                                          @else
                                            <option value='0'>No</option>
                                            <option value='1' selected>Yes</option>
                                          @endif
                                        </select>
                                    </div>
                                </div>
                                <label for="bulkQty">Bulk Qty</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="bulkQty" id="bulkQty" value="{{$mchSku->bulk_qty}}" class="form-control" placeholder="Enter Bulk Qty">
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
@section('scripts')
<script type="text/javascript">
    function FetchSubgroupByGroup(groupId)
    {
        $.ajax({
          headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },   
          url: "/FetchSubgroupByGroup",
          type: 'POST',
          data: {'grp': grp},
          success: function(jsonData)
          {
            // alert(JSON.stringify(jsonData));
             $('#divSubgrp').remove();
             $('#lblsubgrp').after('<div class="form-group" id="divSubgrp"><div class="form-line"><select name="mchSubgroup" class="form-control" id="mchSubgroup"></select></div></div>');
             $.each(jsonData, function (i, item) {
                $('#mchSubgroup').append($('<option>', { 
                    value: item,
                    text : item 
                }));
            });
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
</script>
@endsection


