

<body class="theme-blue" style="background: #fff">
   <section>
        <style>
            .navbar-brand{
                 font-family: Monotype Corsiva;
                 font-size: 25px;
                 margin: 0px;
            }

            @media only screen and (max-width: 600px) {
                
            }
        </style>
        <nav class="navbar" style="height:20px;padding-top: -5px;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="index.html">KisanKraft <sup style="font-size: 15px;">&reg;</sup></a> 
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Tasks -->
                        <li class="dropdown">
                            <li><a href="{{ url('/logout') }}">Sign Out</a></li>
                        </li>
                        <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                        
                       <li class="dropdown">
                            <a href="/GetAllNotifications">
                                <i class="material-icons">notifications</i>
                                <span id="notify_count" class="label-count">new</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <style type="text/css">
            #leftsidebar{
            -webkit-transition: width 0.5s, height 4s; /* For Safari 3.1 to 6.0 */
            transition: width 0.5s, height 4s;
            }
            
             .content: {
            -webkit-transition: margin-left 0.5s, height 4s; /* For Safari 3.1 to 6.0 */
            transition: margin-left 0.5s, height 4s;
            }
        </style>
        <aside id="leftsidebar" class="sidebar" style="width: 55px;">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <!-- <img src="images/user.png" width="48" height="48" alt="User" /> -->
                </div>
                <div class="info-container">
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    </div>
                </div>
            </div>
             <div class="menu">
                <ul class="list">
                    <li class="active">
                        <a href='{{ route('home') }}'>
                            <span>Home</span>
                        </a>
                    </li>
                    
                    <ul class="ml-menu">            
                        @can('board')
                        <li>
                            <a href='{{ route('dealer.index') }}'>
                                <span>Dealers</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('employee.index') }}'>
                                <span>Employees</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('role.index') }}'>
                                <i class="material-icons">verified_user</i>
                                <span>Roles</span>
                            </a>
                        </li>
                        @endcan
                    </ul>

                    
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">shopping_cart</i>
                            <span>Orders</span>
                        </a>
                         <ul class="ml-menu">
                                @can('isExecutive')
                                <li>
                                    <a href="{{ route('order.create') }}">Create New Order</a>
                                </li>
                                @endcan
                                @can('isCoordinator')
                                    <li>
                                        <a href="/MgmtApprovedOrders">Mgmt Approved Orders</a>
                                    </li>
                                @endcan
                                @can('isDispatcher')
                                    <li>
                                        <a href="{{ route('invoice.index') }}">Invoice Raised</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('dispatch.index') }}">Dispatched</a>
                                    </li>
                                @endcan
                                @can('isManagement')
                                    <li>
                                        <a href="{{ route('order.create') }}">Create New Order</a>
                                    </li>

                                    <li>
                                        <a href="/PayApprovedOrders">Payment Approved Orders</a>
                                    </li>

                                    <li>
                                        <a href="/DealerPlacedOrders">Dealer Orders</a>
                                    </li>

                                    <li>
                                        <a href="/rejectedOrders">Rejected Orders</a>
                                    </li>
                                    <li>
                                        <a href="/AllIndiaOrders">All India Orders</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('invoice.index') }}">Invoice Raised</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('dispatch.index') }}">Dispatched</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('dealerVisit.index') }}">Dealer Visits</a>
                                    </li>
                                @endcan
                                @can('isDealer')
                                    <li>
                                        <a href="{{ route('order.dealer_orders') }}">My Orders</a>
                                    </li>
                                @endcan
                                @can('isCRM')
                                    <li>
                                        <a href="{{ route('dealerVisit.index') }}">Dealer Visits</a>
                                    </li>
                                @endcan
                                @can('isPaymentApprover')
                                <li>
                                        <a href="/ExecApprovedOrders">Executive Placed Orders</a>
                                </li>
                                @endcan
                                @can('isExecutive')
                                <li>
                                    <a href="/Executive__Placed_orders">My Orders</a>
                                </li>
                                <li>
                                    <a href="/PendingOrders">Pending Orders</a>
                                </li>
                                <li>
                                    <a href="/Executive__rejected_orders">Rejected Orders</a>
                                </li>
                                <li>
                                    <a href="{{ route('order.incomplete_orders') }}">Incomplete Orders</a>
                                </li>
                                <li>
                                    <a href="/Executive__Myregion_orders">All Orders</a>
                                </li>
                                <li>
                                    <a href="/DealerPlacedOrders">Dealer Orders</a>
                                </li>
                                @endcan
                                    <li>
                                        <a href="{{route('order.mch_price_list')}}">Machinery Price List</a>
                                    </li>
                                     <li>
                                        <a href="{{route('order.acc_price_list')}}">Accessories Price List</a>
                                    </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Tours</span>
                        </a>
                         <ul class="ml-menu">
                            @can('isExecutive')
                            <li>
                                <a href="/pendingTour">Pending</a>
                            </li>
                            <li>
                                <a href="/approvedTour">Approved</a>
                            </li>
                            <li>
                                <a href="#">Rejected</a>
                            </li>
                            @endcan
                            @can('isManagement')
                            <li>
                                <a href="/pendingTour">Pending</a>
                            </li>
                            <li>
                                <a href="/pendingAllTour">All Tours Pending</a>
                            </li>
                            <li>
                                <a href="/approvedTour">Approved</a>
                            </li>
                            <li>
                                <a href="#">Rejected</a>
                            </li>
                            @endcan
                            @can('isCRM')
                            <li>
                                <a href="/approvedTour">Approved</a>
                            </li>
                            @endcan
                            
                        </ul>
                    </li>

                    @can('board')
                    <li>
                        <a href="/NewShippingRequest">
                            <i class="material-icons">all_inclusive</i>
                            <span>New Shipping Request</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('machineSku.index') }}">
                            <i class="material-icons">all_inclusive</i>
                            <span>Machinery</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('accSku.index') }}">
                            <i class="material-icons">linear_scale</i>
                            <span>Accessories</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">watch</i>
                            <span>Parts</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href=''>Parts Group</a>
                            </li>
                            <li>
                                <a href=''>Parts SKU</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <li>
                            <a href='{{ route('dept.index') }}'>
                                <i class="material-icons">text_fields</i>
                                <span>Departments</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('state.index') }}'>
                                <i class="material-icons">text_fields</i>
                                <span>States</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('district.index') }}'>
                                <i class="material-icons">face</i>
                                <span>Districts</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('branch.index') }}'>
                                <i class="material-icons">verified_user</i>
                                <span>Branches</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('tax.index') }}'>
                                <i class="material-icons">verified_user</i>
                                <span>Taxes</span>
                            </a>
                        </li>
                        @endcan
                        <li class="dropdown">
                            <li><a href="{{ url('/logout') }}"><i class="material-icons">exit_to_app</i><span>Sign Out</span></a></li>
                        </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="version">
                    <a href="https://www.kisankraft.com">Kisankraft Ltd </a>&copy; 2018 - 2019
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

        <section class="content" style="margin-left: 65px;">
        <div class="container-fluid">
            
            @yield('content')
            
        </div>
        </section>
    </div>

    <script type="text/javascript">
        $("#leftsidebar").hover(function(){
        $(this).css("width", "240px");
        $(".content").css("margin-left", "250px");
       // alert("dasdsds");
        }, function(){
        $(this).css("width", "55px");
        $(".content").css("margin-left", "65px");
    });
    </script>
</body>
