<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=9">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Welcome To Kisankraft Limited</title>

    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Google Fonts -->

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

    <!-- Bootstrap Core Css -->

    <link  href="MaterialTheme/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->

    <link  href="MaterialTheme/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->

    <link  href="MaterialTheme/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->

    <link  href="MaterialTheme/plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->

    <link  href="MaterialTheme/css/style.css" href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->

    <link  href="MaterialTheme/css/themes/all-themes.css" rel="stylesheet" />

    <link rel="manifest" href="firebase/manifest.json">

    <script src="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.js"></script>

    <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.1.1/firebaseui.css" />

    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase.js"></script>

    <script>

      // Initialize Firebase

      var config = {

        apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",

        authDomain: "krishi-6d123.firebaseapp.com",

        databaseURL: "https://krishi-6d123.firebaseio.com",

        projectId: "krishi-6d123",

        storageBucket: "krishi-6d123.appspot.com",

        messagingSenderId: "988261907749"

      };
      firebase.initializeApp(config);
    </script>

</head>



<style>

  #loader {

    display: none;

    border: 5px solid #f3f3f3;

    border-radius: 50%;

    border-top: 5px solid #000;

    width: 50px;

    height: 50px;

    -webkit-animation: spin 2s linear infinite; /* Safari */

    animation: spin 2s linear infinite;  

  }



  @-webkit-keyframes spin {

    0% { -webkit-transform: rotate(0deg); }

    100% { -webkit-transform: rotate(360deg); }

  }



  @keyframes spin {

    0% { transform: rotate(0deg); }

    100% { transform: rotate(360deg); }

  }

</style>



<body class="login-page"  style="position: relative;height:500px;background-size:100% 150%; background-repeat:no-repeat; background-image: url('{{ asset("/images/stones.jpg") }}');">

    <div class="login-box">

            <center>

                <div class="col-lg-3">

                </div>

                <div class="col-lg-6">

                    <div class="body">

                        <div id="firebaseui-auth-container"></div>

                    </div>

                </div>

                <div id="loader"></div>

            </center>

        </div>

    </div>

    <script src="firebase/config.js"></script>

    <script src="MaterialTheme/plugins/jquery/jquery.min.js"></script>

    <script src="MaterialTheme/plugins/bootstrap/js/bootstrap.js"></script>

    <script src="MaterialTheme/plugins/node-waves/waves.js"></script>

    <script src="MaterialTheme/js/pages/examples/sign-in.js"></script>

    <script src="MaterialTheme/js/admin.js"></script>

    <script src="MaterialTheme/plugins/jquery-validation/jquery.validate.js"></script>

    

</body>



</html>