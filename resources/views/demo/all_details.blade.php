@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Demo Information
                            </h2>
                            <br/>
                            <a href="{{ '/demoPlanExcel/'.$emp_id.'/'.$token }}" class="btn btn-info"> Export to Excel </a>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Purpose</th>
                                            <th>State</th>
                                            <th>District</th>
                                            <th>Taluk</th>
                                            <th>Village</th>
                                            <th>Dealer</th>
                                            <th>Model</th>
                                            <th>Crop Name</th>
                                            <th>Crop Row Gap</th>
                                            <th>Crop Height</th>
                                            <th>Sales Person</th>
                                            <th>Technician</th>
                                            <th>Vehicle Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total=0; ?>
                                        @foreach($demo as $item)
                                            <?php $total+=$item->expenses; ?>
                                            <tr>
                                                <td>{{ $item->date }}</td>
                                                <td>{{ $item->purpose }}</td>
                                                <td>{{ $item->state }}</td>
                                                <td>{{ $item->district }}</td>
                                                <td>{{ $item->taluk }}</td>
                                                <td>{{ $item->village }}</td>
                                                <td>{{ $item->dealerName }}</td>
                                                <td>{{ $item->model }}</td>
                                                <td>{{ $item->cropName }}</td>
                                                <td>{{ $item->cropRowGap }}</td>
                                                <td>{{ $item->cropHeight }}</td>
                                                <td>{{ $item->salesPersonName }}</td>
                                                <td>{{ $item->technicianName }}</td>
                                                <td>{{ $item->vehicleType }}</td>
                                            </tr> 
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>     

                   
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
   
    
</script>   
  
@endSection