@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>

<div class="block-header row  clearfix">
                <h2 class="col-lg-10 col-md-10" >Branch </h2>
                <a href='{{ route('branch.create') }}'>
                    <button type="button" class="col-lg-1 col-md-1 btn btn-default waves-effect">
                        <i class="material-icons">add</i>
                        <span>New</span>
                    </button>
                </a>
                <button type="button" class="btn bg-teal waves-effect">
                    <i class="material-icons">publish</i>
                </button>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Branch Information 
                            </h2>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <span>Show &nbsp</span>
                                    <select id="pagination" class="">
                                        <option value="">-- Please select --</option>
                                        <option value="5" @if($items == 5) selected @endif >5</option>
                                        <option value="10" @if($items == 10) selected @endif >10</option>
                                        <option value="25" @if($items == 25) selected @endif >25</option>
                                    </select>
                                    <span>&nbsp  rows</span>
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-3">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_search" class="form-control">
                                            <label class="form-label">Search Branch</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Code</th>
                                            <th width="40">Edit</th>
                                            <th width="40">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($branches as $branch)
                                            <tr>
                                                <td>{{$branch->name}}</td>
                                                <td>{{$branch->code}}</td>
                                                <td>
                                                    <form action="{{ route('branch.edit',$branch->id) }}" method="get">
                                                        <button type="submit" class="btn btn-default"><i class="col-grey material-icons">edit</i></button>
                                                    </form>
                                                </td>
                                                <td>
                                                    <form action="{{ route('branch.destroy',$branch->id) }}" method="post">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-default"><i class="col-red material-icons">delete</i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $branches->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')

@section('scripts')

<script type="text/javascript">
    
    $(document).ready(function(){

        $('#pagination').change(function(){
            window.location = "{{ $branches->url(1) }}&items=" + this.value;
        })
        
        $('#_search').keyup(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterRows",
            type: 'POST',
            data: {'queryFilter':this.value},
            success: function(jsonData)
            {
                // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var branchId = jsonData.data[i].id;

                    var eBtn = '<form action="{{ route('branch.edit','+branchId+') }}" method="get"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">edit</i></button></form>';

                    var dBtn = '<form action="{{ route('branch.destroy','+branchId+') }}" method="post"> @csrf @method('PUT') <button type="submit" class="btn btn-default"><i class="col-red material-icons">delete</i></button></form>'

                    var row = $("<tr><td>"+jsonData.data[i].name+"</td><td>"+jsonData.data[i].code+"</td><td>"+eBtn+"</td><td>"+dBtn+"</td><tr>");
                    $("#_table").append(row);
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        })
    })
    
</script>   
  
@endSection