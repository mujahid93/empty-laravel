@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
	<h2 id="temp">New Role </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Role Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('role.store') }}" method="post">
                            	@csrf
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="_name" class="form-control" placeholder="Enter role name">
                                    </div>
                                </div>
                                <label for="code">Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="code" id="_code" class="form-control" placeholder="Enter role code">
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
	
@endSection