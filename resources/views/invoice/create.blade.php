@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
	<h2 id="temp">New Invoice </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Invoice Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('invoice.store') }}" method="post" enctype="multipart/form-data">
                            	@csrf
                                <label for="number">Invoice No</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="number" id="number" class="form-control" placeholder="Enter invoice number">
                                    </div>
                                </div>
                                <label for="amt">Amount</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="amt" id="amt" class="form-control" placeholder="Enter invoice amount">
                                    </div>
                                </div>
                                <label for="pdf">Invoice File</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" name="pdf" id="pdf"  class="form-control">
                                    </div>
                                </div>
                                <input type="hidden" name="tracking_no" value="{{$order->tracking_no }}">
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
