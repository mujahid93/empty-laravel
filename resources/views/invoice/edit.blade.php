@extends('layouts.app')

@section('css_links')

@endsection

@section('content')
<div class="block-header">
	<h2 id="temp">Invoice </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Invoice Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('invoice.update',$invoice->id) }}" method="post">
                            	@csrf
                                @method('PUT')
                                <label for="number">Invoice Number</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="number" id="number" value="{{ $invoice->iNumber }}" class="form-control" placeholder="Enter invoice number">
                                    </div>
                                </div>
                                <label for="amt">Amount</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="amt" id="amt"  value="{{ $invoice->iAmt }}" class="form-control" placeholder="Enter invoice amount">
                                    </div>
                                </div>
                                <label for="pdf">Invoice File</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="hidden" name="oldPdf" value="{{asset('$invoice->iPdf')}}">
                                        <input type="file" name="pdf" id="pdf" class="form-control">
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection



