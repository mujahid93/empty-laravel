@extends('layouts.header')
@extends('layouts.content')

@section('css_links')

@endsection

@section('content')
<div class="block-header">
	<h2 id="temp">New District </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill District Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('district.store') }}" method="post">
                            	@csrf
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="_name" class="form-control" placeholder="Enter district name">
                                    </div>
                                </div>
                                <label for="code">Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="code" id="_code" class="form-control" placeholder="Enter district code">
                                    </div>
                                </div>

                                <label for="state">State</label>
                                <div class="form-group">
                                  <select  name="state" class="form-control">
                                    @foreach($items as $item)
                                      <option value='{{$item->id}}'>{{$item->name}}</option>
                                    @endforeach
                                  </select>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
	
@endSection