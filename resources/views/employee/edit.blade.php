@extends('layouts.header')
@extends('layouts.content')
@section('content')

<div class="block-header">
	<h2 id="temp">Employee </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Employee Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('employee.update',$employee->id) }}" method="post">
                                @csrf
                                @method('PUT')
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" value="{{ $employee->user['name']}}" class="form-control" name="name" placeholder="Enter your name">
                                    </div>
                                </div>
                                <label for="supervisor">Supervisor</label>
                                <div class="form-group">
                                  <select  name="supervisor" class="form-control">
                                     @foreach($empList as $emp)
                                      @if($emp->id == $employee->supervisor_id)
                                      {
                                        <option value='{{$emp->id}}' selected>{{$emp->name}}</option>
                                      }
                                      @else
                                      {
                                        <option value='{{$emp->id}}'>{{$emp->name}}</option>
                                      }
                                      @endif
                                    @endforeach
                                  </select>
                                </div>
                                <label for="mobile">Mobile</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="mobile" value="{{ $employee->mobile}}" name="mobile" class="form-control" placeholder="Enter your mobile number">
                                    </div>
                                </div>
                                <label for="address">Address</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address" rows="4" class="form-control no-resize" placeholder="Type your Address..">{{ $employee->address }}</textarea>
                                        </div>
                                </div>

                                  <div class="form-group">
                                    <input type="radio" name="gender" id="male" value="male" class="with-gap" {{ ($employee->gender=="male")? "checked" : "" }}>
                                    <label for="male">Male</label>
                                    <input type="radio" name="gender" id="female" value="female" class="with-gap" {{ ($employee->gender=="female")? "checked" : "" }}>
                                    <label for="female" class="m-l-20">Female</label>
                                  </div>
                                
                                <label for="roles">Roles</label>
                                <div class="form-group">
                                    <select id="roles" name="role" class="form-control show-tick">
                                        @foreach($roles as $role){
                                          @if($role->id == $employee->role_id)
                                          {
                                            <option value='{{$role->id}}' selected>{{$role->name}}</option>
                                          }
                                          @else
                                          {
                                            <option value='{{$role->id}}'>{{$role->name}}</option>
                                          }
                                          @endif
                                        }
                                        @endforeach
                                    </select>
                                </div>
                                <label for="employee">Branch</label>
                                <div class="form-group">
                                  <select  name="branch" class="form-control">
                                        @foreach($branches as $branch)
                                          @if($branch->id == $employee->branch_id)
                                          {
                                            <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                          }
                                          @else
                                          {
                                            <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                          }
                                          @endif
                                        @endforeach
                                  </select>
                                </div>
                                <label for="dept">Dept</label>
                                 <div class="form-group">
                                  <select  name="dept" class="form-control">
                                        @foreach($depts as $dept)
                                          @if($dept->id == $employee->dept_id)
                                          {
                                            <option value='{{$dept->id}}' selected>{{$dept->name}}</option>
                                          }
                                          @else
                                          {
                                            <option value='{{$dept->id}}'>{{$dept->name}}</option>
                                          }
                                          @endif
                                        @endforeach
                                  </select>
                                </div>
                                <label for="date_of_join">Date of joining</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <input type="date" name="date_of_join" id="date_of_join" value="{{ $employee->date_of_join }}" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                </div>

                                 


                                <label for="state">State</label>
                               
                                 <div class="form-group">
                                  <select  name="state[]" id="state" class="form-control" onchange="FetchDistByState(this.value);" multiple>
                                           @foreach($states as $state)
                                                <option value="{{ $state->id }}" {{ in_array($state->id, $sel_states ) == 1 ? 'selected' : '' }}>{{ $state->name }}</option>
                                           @endforeach
                                  </select>
                                </div>
                                 <label for="_districtList" id="lblDistrict">Districts</label>
                                <div class="form-group" id="districtGroup">

                                       <select name="districts[]" class="form-control" id = "districts" multiple>
                                             @foreach($alldistricts as $district)
                                             @if(in_array($district->id, $sel_districts)){ 
                                              <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                            }
                                            @endif
                                             @endforeach
                                       </select>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
<script type="text/javascript">
    function FetchDistByState(stateId)
    {
        var states = $('#state').val();

        $.ajax({
          url: "/FetchDistByState",
          type: 'GET',
          data: {'states': states},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#districtGroup').remove();
             $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select name="districts[]" class="form-control" id = "districts" multiple></select></div>');
             for(var i=0;i<jsonData.length;i++){
                $.each(jsonData[i], function (i, item) {
                $('#districts').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             }
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }
</script>
@endSection

