@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
    <h2 id="temp">New Employee </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Employee Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('employee.store') }}" method="post">
                                @csrf
                                <label for="email_address">Email Address</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="email_address" name="email" class="form-control" placeholder="Enter your email address" required>
                                    </div>
                                </div>
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" class="form-control" name="name" placeholder="Enter your name">
                                    </div>
                                </div>
                               <label for="supervisor">Supervisor</label>
                                <div class="form-group">
                                  <select  name="supervisor" class="form-control">
                                    @foreach($empList as $emp)
                                      <option value='{{$emp->id}}'>{{$emp->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <label for="mobile">Mobile</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter your mobile number">
                                    </div>
                                </div>
                                <label for="address">Address</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address" rows="4" class="form-control no-resize" placeholder="Type your Address.."></textarea>
                                        </div>
                                </div>

                                  <div class="form-group">
                                    <input type="radio" name="gender" id="male" value="male" class="with-gap">
                                    <label for="male">Male</label>
                                    <input type="radio" name="gender" id="female" value="female" class="with-gap">
                                    <label for="female" class="m-l-20">Female</label>
                                  </div>

                                <label for="roles">Roles</label>
                                <div class="form-group">
                                    <select id="roles" name="role" class="form-control show-tick">
                                         @foreach($roles as $role)
                                          <option value='{{$role->id}}'>{{$role->name}}</option>
                                         @endforeach
                                    </select>
                                </div>
                                <label for="employee">Branch</label>
                                <div class="form-group">
                                  <select  name="branch" class="form-control">
                                    @foreach($branches as $branch)
                                      <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <label for="dept">Dept</label>
                                 <div class="form-group">
                                  <select  name="dept" class="form-control">
                                    @foreach($depts as $dept)
                                      <option value='{{$dept->id}}'>{{$dept->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <label for="date_of_join">Date of joining</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <input type="date" name="date_of_join" id="date_of_join" class="datepicker form-control" placeholder="Please choose a date...">
                                        </div>
                                </div>
                                <label for="state">State</label>
                                 <div class="form-group">
                                  <select  name="state[]" id="state" class="form-control" onchange="FetchDistByState(this.value);" multiple>
                                    @foreach($states as $state)
                                      <option value='{{$state->id}}'>{{$state->name}}</option>
                                    @endforeach
                                  </select>
                                </div>

                                <label for="_districtList" id="lblDistrict">Districts</label>
                                <div class="form-group" id="districtGroup">
                                       <select name="district[]" class="form-control" id = "districts" multiple>
                                             @foreach($districts as $district)
                                              <option value='{{$district->id}}'>{{$district->name}}</option>
                                             @endforeach
                                       </select>
                                </div>

                                <button class="btn btn-blue waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
@section('scripts')
<script type="text/javascript">
    function FetchDistByState(stateId)
    {
        var states = $('#state').val();

        $.ajax({
          url: "/FetchDistByState",
          type: 'GET',
          data: {'states': states},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#districtGroup').remove();
             $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select name="districts[]" class="form-control" id = "districts" multiple></select></div>');
             for(var i=0;i<jsonData.length;i++){
                $.each(jsonData[i], function (i, item) {
                $('#districts').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             }
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script>
  
@endSection