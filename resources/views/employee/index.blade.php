@extends('layouts.header')

@extends('layouts.content')

@section('content')



<style type="text/css">

    .btn

    {

        margin-right: 10px;

    }

</style>

    

<div class="block-header row  clearfix">

                <h2 class="col-lg-10 col-md-10" >Employee </h2>

                <a href='{{ route('employee.create') }}'>

                    <button type="button" class="col-lg-1 col-md-1 btn btn-default waves-effect">

                        <i class="material-icons">add</i>

                        <span>New</span>

                    </button>

                </a>

                <button type="button" id="btnImport" class="btn bg-teal waves-effect">

                    <i class="material-icons">publish</i>

                </button>

            </div>

            

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                    <div class="card">

                        <div class="header">

                            <h2>

                                Employee Information 

                            </h2>

                        </div>

                         @if(session()->has('result'))

                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                {{ Session::get('result') }}

                         </div>

                         @endif

                        <div class="body">

                            <div class="row">

                                <div class="col-sm-6">

                                    <span>Show &nbsp</span>

                                    <select id="pagination" class="">

                                        <option value="">-- Please select --</option>

                                        <option value="5" @if($items == 5) selected @endif >5</option>

                                        <option value="10" @if($items == 10) selected @endif >10</option>

                                        <option value="25" @if($items == 25) selected @endif >25</option>

                                    </select>

                                    <span>&nbsp  rows</span>

                                </div>

                                <div class="col-sm-3">

                                </div>

                                <div class="col-sm-3">

                                     <div class="form-group form-float">

                                        <div class="form-line">

                                            <input type="text" id="_search" class="form-control">

                                            <label class="form-label">Search Employee</label>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>

                                        <tr>

                                            <th>Name</th>

                                            <th>Dept</th>

                                            <th>Branch</th>

                                            <th>Role</th>

                                            <th>Joined Date</th>

                                            <th width="40">Edit</th>

                                            <th width="40">Delete</th>

                                        </tr>

                                    </thead>

                                    <tbody>

                                        @foreach($employees as $employee)

                                            <tr>

                                                <td>

                                                    {{$employee->name}}

                                                </td>

                                                <td>{{$employee->dept['name']}}</td>

                                                <td>{{$employee->branch->name}}</td>

                                                <td>

                                                    {{$employee->role->name}}

                                                </td>

                                                <td>{{date('d-m-Y', strtotime($employee->date_of_join))}}</td>

                                                <td>

                                                    <form action="{{ route('employee.edit',$employee->id) }}" method="get">

                                                        <button type="submit" class="btn btn-default"><i class="col-grey material-icons">edit</i></button>

                                                    </form>

                                                </td>

                                                <td>

                                                    <form action="{{ route('employee.destroy',$employee->id) }}" method="post">

                                                        @csrf

                                                        @method('DELETE')
                                                        <input type="hidden" name="empId" value="{{$employee->id}}">

                                                        <button type="submit" class="btn btn-default"><i class="col-red material-icons">delete</i></button>

                                                    </form>

                                                </td>

                                            </tr>

                                        @endforeach

                                    </tbody>

                                </table>



                                {{ $employees->links() }}

                            </div>

                        </div>

                    </div>

                </div>

            </div>  

            <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">

                <div class="modal-dialog" role="document">

                    <div class="modal-content">

                        <div class="modal-header">

                            <h3 class="modal-title" id="defaultModalLabel">Upload Employees</h3>

                        </div>

                        <div class="modal-body">

                            <p>Import Excel Data ?</p>

                        </div>

                        <div class="modal-footer">

                            <form action="{{ route('employee.import') }}" method="post" enctype="multipart/form-data">

                                @csrf

                                <input type="file" name="empExcel">

                                <button type="submit" class="btn btn-link waves-effect">Yes</button>

                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>

                            </form>

                        </div>

                    </div>

                </div>

            </div>   



@endsection

@extends('layouts.footer')



@section('scripts')



<script type="text/javascript">

    

    $(document).ready(function(){



        $('#pagination').change(function(){

            window.location = "{{ $employees->url(1) }}&items=" + this.value;

        })

        

        $('#_search').keyup(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterEmployee",

            type: 'GET',

            data: {'queryFilter':this.value},

            success: function(jsonData)

            {

                // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var employeeId = jsonData.data[i].id;



                    var eBtn = '<form action="{{ route('employee.edit','+employeeId+') }}" method="get"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">edit</i></button></form>';



                    var dBtn = '<form action="{{ route('employee.destroy','+employeeId+') }}" method="post"> @csrf @method('PUT') <button type="submit" class="btn btn-default"><i class="col-red material-icons">delete</i></button></form>'



                    var date = new Date(jsonData.data[i].date_of_join);

                    var supervisor = null;

                    if(jsonData.data[i]['supervisor'] != null)

                        supervisor = jsonData.data[i]['supervisor']['name'];

                    var row = $("<tr><td>"+jsonData.data[i]['user']['name']+"</td><td>"+supervisor+"</td><td>"+jsonData.data[i]['dept']['name']+"</td><td>"+jsonData.data[i]['branch']['name']+"</td><td>"+jsonData.data[i]['role']['name']+"</td><td>"+(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear()+"</td><td>"+eBtn+"</td><td>"+dBtn+"</td><tr>");

                    $("#_table").append(row);

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 

            {

               alert(xhr.status);

               alert(thrownError);

            }

            });      

        })

    });



    $(function () {

        $('#btnImport').on('click', function () {

            var color = $(this).data('color');

            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-teal');

            $('#mdModal').modal('show');

        });

    });

     

</script>   

  

@endSection