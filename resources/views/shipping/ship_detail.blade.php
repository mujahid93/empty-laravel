<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

</head>

@extends('layouts.content')
@section('content')
            <div class="row clearfix">
                <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">
                    <div class="card">
                        <div class="header">
                            <h3>
                               New Shipping Details
                            </h3>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Branch Name</p>
                                        <h5 id="">{{$shipping->branch->name}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Category</p>
                                        <h5 id="">{{$shipping->name}}</h5>
                                    </blockquote>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Agency Name</p>
                                        <h5 id="">{{$shipping->agency}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Search Agency</p>
                                        <h5 id="">
		                                       <select class="selectpicker" name="shippingCompany" id="shippingCompany" data-size="8" data-live-search="true" data-dropup-auto="false"  class="form-control" >
			                                        @foreach($ShippingDetails as $ship)
			                                          <option value='{{$ship->id}}'>{{$ship->agency}}</option>
			                                        @endforeach
		                                    	</select>
	                                	</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Update All Orders</p>
                                        <h5 id="">
			                                <button  onclick="UpdateOrderShipping()" type="button" data-color="red" name="" class="btn bg-green waves-effect">Update</button>
	                                	</h5>
                                    </blockquote>
                                </div>
                            </div>
                        
                            <div class="row">
                            	<div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
	                            	<blockquote>
	                                        <p>Keep New Shipping</p>
			                                <form action="/KeepNewShipping" method="post">
			                                    @csrf
			                                          <input type="hidden" name="shipping" id="shipping" value="{{encrypt($shipping->id)}}">
			                                          <button type="submit" data-color="red" name="" class="btn bg-purple waves-effect">New Shipping OK</button>
			                                </form>
			                        </blockquote>
		                    	</div>
                            </div>

                    </div>
                </div>
            </div>
@endsection
    <script language="javascript">document.onmousedown=disableclick;status="Right Click Disabled";function disableclick(event){  if(event.button==2)   {     alert(status); return false;       }}
    </script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>

    
<script type="text/javascript">

	function UpdateOrderShipping()
	{
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/UpdateOrderShipping",
            type: 'POST',
            data: {
                'old_shipping': $('#shipping').val(),
                'new_shipping': $('#shippingCompany').val()
            },
            success: function(jsonData) {
            	window.location.href = '/NewShippingRequest';
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
	}

    var selectedBranch = 0;

    $(document).keydown(function (event) {
            if (event.keyCode == 123) { // Prevent F12
                return false;
            } else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
                return false;
            }
    });

</script>
