<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">
</head>

<meta name="csrf-token" content="{{ csrf_token() }}">

@extends('layouts.content')

@section('content')



<div class="block-header row  clearfix">

            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                    <div class="card">

                        <div class="header">
                            <h2>
                                New Shipping Requests
                            </h2>
                        </div>

                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif

                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>
                                        <tr>

                                            <th>Branch</th>

                                            <th>Name</th>

                                            <th>Agency</th>
                                            <th>View</th>
                                        </tr>

                                    </thead>

                                    <tbody>
                                        @foreach($shippings as $s)
                                            <tr>
                                                <td>{{$s->branch->name}}</td>
                                                <td>{{$s->name}}</td>
                                                <td>{{$s->agency}}</td>
                                                <td>
                                                    <form action="/NewShippingDetail" method="get">
                                                        @csrf
                                                        <input name="shipping" type="hidden" value="{{encrypt($s->id)}}">
                                                        <button type="submit" class="btn btn-success">View</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>

                                </table>

                                {{ $shippings->links() }}   

                            </div>

                        </div>

                    </div>

                </div>

            </div>            

@endsection

    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase-messaging.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>

    <script>

        function GetShipSuggestions()
        {
            alert("hi ");
        }


        // Retrieve an instance of Firebase Messaging so that it can handle background messages.
        var config = {

            apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",

            authDomain: "krishi-6d123.firebaseapp.com",

            databaseURL: "https://krishi-6d123.firebaseio.com",

            projectId: "krishi-6d123",

            storageBucket: "krishi-6d123.appspot.com",

            messagingSenderId: "988261907749"

          };

          firebase.initializeApp(config);

         const messaging = firebase.messaging();

         messaging.usePublicVapidKey('BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY');

       messaging.requestPermission().then(function() {

                          console.log('Notification permission granted.');

                          var user_email = "null";    

                              //Get current user email id

                              $.ajax({

                                    headers: {

                                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                                      },

                                    url: "/getCurrentUserEmail",

                                    type: 'POST',

                                    data: {'data': "null"},

                                    success: function(jsonData)

                                    {   

                                       messaging.getToken().then(function(currentToken) {

                                            if (currentToken) {

                                                sendTokenToServer(jsonData,currentToken);

                                                // updateUIForPushEnabled(currentToken);

                                            } else {

                                                // Show permission request.

                                                console.log('No Instance ID token available. Request permission to generate one.');

                                                // Show permission UI.

                                                updateUIForPushPermissionRequired();

                                                setTokenSentToServer(false);

                                            }

                                        }).catch(function(err) {

                                            console.log('An error occurred while retrieving token. ', err);

                                            showToken('Error retrieving Instance ID token. ', err);

                                            setTokenSentToServer(false);

                                        });

                                    },

                                    error: function (xhr, ajaxOptions, thrownError) 

                                    {

                                              

                                    }

                              });



                          // TODO(developer): Retrieve an Instance ID token for use with FCM.

                          // alert("permission granted");





                        }).catch(function(err) {

                          console.log('Unable to get permission to notify.', err);

                        });



          messaging.onMessage(function(payload) {

            console.log('Message received in home. ', payload['data']);

            var colorName = 'bg-black';

            var text = payload['data']['message'];

            var animateEnter = "animated fadeInDown";

            var animateExit = "animated fadeOutUp";

            var placementFrom = "top";

            var placementAlign = "center";

            if (colorName === null || colorName === '') { colorName = 'bg-black'; }

              if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }

              if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }

              if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }

              var allowDismiss = true;

              $.notify(

                  {

                      message: text

                  },

                  {

                      type: colorName,

                      allow_dismiss: allowDismiss,

                      newest_on_top: true,

                      timer: 1000,

                      placement: {

                          from: placementFrom,

                          align: placementAlign

                      },

                      animate: {

                          enter: animateEnter,

                          exit: animateExit

                      },

                      template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +

                      '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +

                      '<span data-notify="icon"></span> ' +

                      '<span data-notify="title">{1}</span> ' +

                      '<span data-notify="message">{2}</span>' +

                      '<div class="progress" data-notify="progressbar">' +

                      '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +

                      '</div>' +

                      '<a href="{3}" target="{4}" data-notify="url"></a>' +

                      '</div>'

                  }

              );



          });



          $(document).ready(function(){

            $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/GetNotificationCount",

                    type: 'POST',

                    data: {'data': "null"},

                    success: function(jsonData)
                    {   
                        $('#notify_count').text(jsonData);
                    },
                    error: function (xhr, ajaxOptions, thrownError) 
                    {
                        
                    }

              });

          });



         function sendTokenToServer(email, currentToken)
         {
            
              $.ajax({

                    headers: {

                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                      },

                    url: "/sendTokenToServer",

                    type: 'POST',

                    data: {'email': email, 'token': currentToken},

                    success: function(jsonData)

                    {   

                        // alert(jsonData);

                    },

                    error: function (xhr, ajaxOptions, thrownError) 

                    {


                    }

              });

         }   

    </script>

    @yield('scripts')

