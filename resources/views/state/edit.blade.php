@extends('layouts.app')

@section('css_links')

@endsection

@section('content')
<div class="block-header">
	<h2 id="temp">State </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update State Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('state.update',$state->id) }}" method="post">
                            	@csrf
                                @method('PUT')
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" id="_name" value="{{$state->name}}" class="form-control" placeholder="Enter state name">
                                    </div>
                                </div>
                                <label for="code">Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="code" id="_code" value="{{$state->code}}" class="form-control" placeholder="Enter state code">
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection



