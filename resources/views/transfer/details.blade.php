<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Welcome To Kisankraft Limited</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css") }}" rel="stylesheet">
        <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
        <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
        <style>
        .loading {
            display: none;
            width: 16px;
            height: 16px;
            background-image: url(https://image.jimcdn.com/app/cms/image/transf/none/path/se3ec4637ab2cb8e8/image/ib5621eeba886d70d/version/1474931581/image.gif);
            vertical-align: text-bottom;
        }
        /* autocomplete adds the ui-autocomplete-loading class to the textbox when it is _busy_, use general sibling combinator ingeniously */
        #autocomplete.ui-autocomplete-loading ~ .loading {
            display: inline-block;
        }
        .ui-autocomplete .m-icon {
            float: left;
            max-width: 32px;
        }
        .ui-autocomplete .x-icon {
            float: left;
            width: 32px;
            opacity: .5;
            font-size: small;
            text-align: center;
        }
        .ui-autocomplete .m-name {
            display: block;
            margin-left: 40px;
        }
        .ui-autocomplete .m-year {
            display: block;
            margin-left: 40px;
            font-size: smaller;
        }
        .ui-autocomplete div::after {
            content: "";
            display: table;
            clear: both;
        }
        .resultItem:hover {
          background-color: #ffccb3;
          cursor: pointer;
        }
    </style>
    </head>
    @extends('layouts.content')
    @section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="background: #fff">
                    <h2>
                    <b>Review Project</b>
                    </h2>
                </div>
                @if(session()->has('msg'))
                     <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{session()->get('msg')}}
                      </div>
                @endif
                

                <div class="body">
                   <div class="row clearfix">
                      
                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="reqBranch" >Req Branch</label>
                                        <select name="reqBranch" id="reqBranch" class="form-control" required>
                                            @foreach($branches as $branch)
                                                @if($transfer->req_branch == $branch->id)
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                @else
                                                    <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                        </div>

                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="sourceBranch">Product Source Branch</label>
                                        <select name="sourceBranch" id="sourceBranch" class="form-control" required >
                                            @foreach($branches as $branch)
                                                @if($transfer->source_branch == $branch->id)
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                @else
                                                    <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                        </div>
                   </div>

                   <div class="row clearfix">
                       <div class="form-group">
                            <label for="dispatchBranch">Product Select</label>
                            <div>
                                <input type="text" id='product_search'>
                                <span class="loading"></span>
                            </div>
                        </div>
                   </div>
                   <div class="row clearfix">
                        <div class="body">
                            <div class="table-responsive" id="tableContainer">
                                <table id="tblData" class="table table-bordered table-striped table-hover js-basic-example">
                                     <thead>
                                        <tr>
                                            <th>NavNo</th>
                                            <th>Sku</th>
                                            <th>Qty</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblBody">
                                        @foreach($data as $transfer)
                                            <tr>
                                                <td>{{ $transfer->nav_no }}</td>
                                                <td>{{ $transfer->item }}</td>
                                                <td><input type="number" value="{{ $transfer->quantity }}"></td>
                                                <td><input type='button' value='remove' onclick='removeProduct(this);'></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="right">
                                TotalQty:<span id='totalQty'>0</span>
                            </div>
                        </div>
                   </div>
                   <div class="row">
                        <div class="pull-right button-demo js-modal-buttons">
                            <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect" onclick="requestTransfer();">Request Transfer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

        <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
        <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

        <script type="text/javascript">

         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function(){

      $( "#product_search" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url:"/getSearchedProducts",
            type: 'post',
            dataType: "json",
            data: {
               part: request.term
            },
            success: function( data ) {
                $('.resultItem').css({"display":"block"});
               $('.resultItem').css({"height":"100%"});
               response( data );
            }
          });
        },
        select: function (event, ui) {
                   // Set selection
           /*alert(ui);
           $('#employee_search').val(ui.item.label);*/ // display the selected text
           //$('#employeeid').val(ui.item.value); // save selected id to input
           return true;
        }
      }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var $div = $("<div class='resultItem' onclick=addProduct('"+encodeURI(item.value)+"','"+encodeURI(item.label)+"');></div>");
                $("<span class='m-name'></span>").text(item.value).appendTo($div);
                $("<span class='m-year'></span>").text(item.label).appendTo($div);
                return $("<li></li>").append($div).appendTo(ul);
            };

    });


    function addProduct(nav_no,item){
        //alert(decodeURI(item));
        if(isExist(decodeURI(nav_no))){
            alert("Product already exists.");
            return;
        }
        var row=$("<tr></tr>");
        $("<td></td>").text(decodeURI(nav_no)).appendTo(row);
        $("<td></td>").text(decodeURI(item)).appendTo(row);
        $("<td><input type='number' oninput='updateQty(this);'/></td>").appendTo(row);
        $("<td><input type='button' value='remove' onclick='removeProduct(this);'></td>").appendTo(row);
        row.appendTo($('#tblBody'));
        $( "#product_search").val("");
        $('.resultItem').css({"display":"none"});
        $('.resultItem').css({"height":"0"});

    }

    function isExist(nav_no){
        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        for (var row =1; row< rows; row++) {
            var nav=table.rows[row].cells[0].innerHTML;
            if(nav_no==nav){
                return true;
                break;
            }
        }
        return false;
    }

    function removeProduct(obj){
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;
        table.deleteRow(currentNode.rowIndex);
    }

    function updateQty(obj){
            var currentNode=obj.parentElement.closest('tr');
            var tableBody=currentNode.parentElement;
            var table=tableBody.parentElement;
            var rows=table.rows.length;
            var totalQty=0;
            for (var row =1; row< rows; row++) {
                var qty=table.rows[row].cells[2].children[0].value;
                totalQty+=parseFloat(qty);
            }
            $('#totalQty').text(totalQty);
    }

    function requestTransfer(){
        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        var totalQty=0;
        var jsonRequest=[];
        for (var row =1; row< rows; row++) {
            var rowData={};
            rowData['nav_no']=table.rows[row].cells[0].innerHTML;
            rowData['item']=table.rows[row].cells[1].innerHTML;
            rowData['quantity']=table.rows[row].cells[2].children[0].value;
            if(rowData["quantity"]>0){
                jsonRequest.push(rowData);
            }
        }
        if(jsonRequest.length==0){
            alert("No Item qty available to proceed");
            return;
        }
        let objJsonStr = JSON.stringify(jsonRequest);
        let objJsonB64 = window.btoa(objJsonStr);

       
        $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/addTransferRequest",
                  type: 'POST',
                  data: {
                      'req_branch':$('#reqBranch').val(),
                      'source_branch':$('#sourceBranch').val(),
                      'location_mode':"",
                      'transfer_details': objJsonB64
                  },
                  success: function(jsonData) {
                    if(jsonData['status']==true)
                        document.location.reload();
                    else
                        alert("Server error!!! Please try again.");
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert("Server not responding, Please check your internet connection.");
                      //alert(xhr.status);
                      //alert(thrownError);
                  }
              });
    }

        </script>