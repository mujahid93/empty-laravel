<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Welcome To Kisankraft Limited</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css") }}" rel="stylesheet">
        <link href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
        <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
        <style>
        .loading {
            display: none;
            width: 16px;
            height: 16px;
            background-image: url(https://image.jimcdn.com/app/cms/image/transf/none/path/se3ec4637ab2cb8e8/image/ib5621eeba886d70d/version/1474931581/image.gif);
            vertical-align: text-bottom;
        }

        .loader {
              border: 16px solid #f3f3f3;
              border-radius: 50%;
              border-top: 16px solid #3498db;
              width: 120px;
              height: 120px;
              left: 48%;
              top: 48%;
              margin: auto;
              position: absolute;
              display: none;
              z-index: 999;
              -webkit-animation: spin 2s linear infinite; /* Safari */
              animation: spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
              0% { -webkit-transform: rotate(0deg); }
              100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
              0% { transform: rotate(0deg); }
              100% { transform: rotate(360deg); }
            }

        /* autocomplete adds the ui-autocomplete-loading class to the textbox when it is _busy_, use general sibling combinator ingeniously */
        #autocomplete.ui-autocomplete-loading ~ .loading {
            display: inline-block;
        }
        .ui-autocomplete .m-icon {
            float: left;
            max-width: 32px;
        }
        .ui-autocomplete .x-icon {
            float: left;
            width: 32px;
            opacity: .5;
            font-size: small;
            text-align: center;
        }
        .ui-autocomplete .m-name {
            display: block;
            margin-left: 40px;
        }
        .ui-autocomplete .m-year {
            display: block;
            margin-left: 40px;
            font-size: smaller;
        }
        .ui-autocomplete div::after {
            content: "";
            display: table;
            clear: both;
        }
        .resultItem:hover {
          background-color: #ffccb3;
          cursor: pointer;
        }

        
    </style>
    </head>
    @extends('layouts.content')
    @section('content')
    <div class="loader"></div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="background: #fff">
                    <h2>
                    <b>Stock Transfer
                    @if($transfer!=null && $transfer->tracking_no!=null)
                    ( Tracking No: {{ $transfer->tracking_no }} )
                    @endif
                    </b>
                    </h2>
                </div>
                @if(session()->has('msg'))
                     <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{session()->get('msg')}}
                      </div>
                @endif
                
                <div class="body">
                   <div class="row clearfix">
                      
                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="reqBranch" >Req Branch</label>
                                        <select name="reqBranch" id="reqBranch" class="form-control" onchange="updateReqGodown()" disabled="true" required>

                                            @foreach($branches as $branch)
                                                @if($transfer!=null && $transfer->req_branch == $branch->id)
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>

                                                @elseif($transfer==null && Auth::user()->employee->branch_id==$branch->id)    
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                @else
                                                    <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="reqGodown" >Req Godown</label>
                                        
                                        <select name="reqGodown" id="reqGodown" class="form-control" required <?php if($transfer!=null && $transfer->status>=1) echo "disabled='true'"; ?>>
                                            @foreach($reqGodown as $godown)
                                                @if($transfer!=null && $transfer->req_godown == $godown->id)
                                                    <option value='{{$godown->id}}' selected>{{$godown->code}}</option>
                                                @else
                                                    <option value='{{$godown->id}}'>{{$godown->code}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                        </div>

                        <div class="col-sm-1"></div>

                        <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="sourceBranch">Product Source Branch</label>
                                        @if($transfer!=null && $transfer->status>=2)
                                        <select name="sourceBranch" id="sourceBranch" class="form-control" required onchange="updateSourceGodown()"  disabled="true">
                                            @foreach($branches as $branch)
                                                @if($transfer!=null && $transfer->source_branch == $branch->id)
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                @else
                                                    <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @else
                                        <select name="sourceBranch" id="sourceBranch" class="form-control" required onchange="updateSourceGodown()">
                                            @foreach($branches as $branch)
                                                @if($transfer!=null && $transfer->source_branch == $branch->id)
                                                    <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                @else
                                                    <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @endif
                                        

                                    </div>

                                    <div class="form-group">
                                        <label for="sourceGodown" >Source Godown</label>
                                        <select name="sourceGodown" id="sourceGodown" class="form-control" required>
                                            @foreach($sourceGodown as $godown)
                                                @if($transfer!=null && $transfer->source_godown == $godown->id)
                                                    <option value='{{$godown->id}}' selected>{{$godown->code}}</option>
                                                @else
                                                    <option value='{{$godown->id}}'>{{$godown->code}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                        </div>
                   </div>

                   <div class="row clearfix">
                    <div class="col-sm-2">
                        <select class="form-control" id="category">
                            <option value="0">Machinery</option>
                            <option value="1">Accressories</option>
                            <option value="2">Parts</option>
                        </select>
                    </div>
                       <div class="col-sm-6">
                            
                            <div>
                                <input type="text" id='product_search' class="form-control">
                                <span class="loading"></span>
                            </div>
                        </div>
                   </div>
                   <div class="row clearfix">
                        <div class="body">
                            <div class="table-responsive" id="tableContainer">
                                <table id="tblData" class="table table-bordered table-striped table-hover js-basic-example">
                                     <thead>
                                        <tr>
                                            <th>NavNo</th>
                                            <th>Sku</th>
                                            <th>Qty</th>
                                            <th>Priority</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tblBody">
                                        <?php $total=0;?>
                                         @foreach($data as $trans)
                                            <tr>
                                                <td>{{ $trans->nav_no }}</td>
                                                <td>{{ $trans->item }}</td>
                                                @if($transfer!=null && $transfer->status==1)
                                                <td><input type="number"  value="{{ $trans->quantity }}" oninput='updateQty(this);'></td>
                                                <?php $total+=$trans->quantity; ?>
                                                @else
                                                <td><input type="number" value="{{ $trans->management_quantity }}" oninput='updateQty(this);'></td>
                                                <?php $total+=$trans->management_quantity; ?>
                                                @endif
                                                <td>
                                                    <select class="form-control">
                                                        @if($trans->priority==0)
                                                            <option selected>Normal</option>
                                                        @else
                                                            <option>Normal</option>
                                                        @endif
                                                        @if($trans->priority==1)
                                                            <option selected>High</option>
                                                        @else
                                                            <option>High</option>
                                                        @endif
                                                    </select>
                                                </td>
                                                <td><input type='button' value='remove' onclick='removeProduct(this);'></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="right">
                                TotalQty:<span id='totalQty'><?php echo $total; ?></span>
                            </div>

                            
                        </div>
                   </div>
                   
                   <div class="row clearfix">
                                <div id="shipBlock" class="col-sm-6 right">
                                @if($transfer!=null && $transfer->transport_type=="Other")
                                    <input type="text" name="" id="otherShip" class="form-control" value="{{ $transfer->transport }}">
                                @else
                                 <select name="shippingCompany" id="shippingCompany" class="form-control" required>
                                    @if($shipping!=null)
                                        @foreach($shipping as $item)
                                            @if($transfer!=null && $item->agency==$transfer->transport)
                                            <option value='{{$item->id}}' selected>{{$item->agency}}</option>
                                            @else
                                            <option value='{{$item->id}}'>{{$item->agency}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                                </select>
                                </div>
                                <div class="col-sm-2 right">
                                <select name="shippingMode" id="shippingMode" class="form-control col-sm-2" required onchange="ChangeMode();">

                                    @if($transfer!=null && $transfer->transport_type=="Transport")
                                    <option value="Transport" selected>Transport</option>
                                    @else
                                    <option value="Transport">Transport</option>
                                    @endif
                                    @if($transfer!=null && $transfer->transport_type=="Courier")
                                    <option value="Courier" selected>Courier</option>
                                    @else
                                    <option value="Courier">Courier</option>
                                    @endif
                                    @if($transfer!=null && $transfer->transport_type=="By Hand")
                                    <option value="By Hand" selected>By Hand</option>
                                    @else
                                    <option value="By Hand">By Hand</option>
                                    @endif
                                    @if($transfer!=null && $transfer->transport_type=="By KKTT")
                                    <option value="By KKTT" selected>By KKTT</option>
                                    @else
                                    <option value="By KKTT">By KKTT</option>
                                    @endif
                                    @if($transfer!=null && $transfer->transport_type=="By Omni")
                                    <option value="By Omni" selected>By Omni</option>
                                    @else
                                    <option value="By Omni">By Omni</option>
                                    @endif
                                    @if($transfer!=null && $transfer->transport_type=="Other")
                                    <option value="Other" selected>Other</option>
                                    @else
                                    <option value="Other">Other</option>
                                    @endif
                                </select>
                                                                                                                                                                   
                            </div>
                                
                   </div>
                   @if($transfer!=null && $transfer->status==2)
                   <div class="row" style="margin-bottom: 10px;">
                       <div class="pull-right">
                           Vehicle Number: <input type="text" name="" id="vehicle" placeholder="Vehicle Number">
                       </div>
                   </div>
                   @endif
                   <div class="row">
                   
                        <div class="pull-right button-demo js-modal-buttons">
                            <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect" onclick="requestTransfer();">
                                @if($transfer!=null && $transfer->status==1)
                                <span>Approve Transfer</span>
                                @elseif($transfer!=null && $transfer->status==2)
                                    <span>Approve Dispatch</span>
                                @else
                                    <span>Request Transfer</span>
                                @endif
                            </button>
                        </div>

                         @if($transfer!=null && $transfer->status==1)
                    <div class="pull-right button-demo js-modal-buttons">
                            <button id="btnApprove" type="button" data-color="red" name="" class="btn bg-red waves-effect" onclick="rejectTransfer();">
                                
                                <span>Reject Transfer</span>
                           
                            </button>
                        </div>
                         @elseif($transfer!=null && $transfer->status==2)
                         <div class="pull-right button-demo js-modal-buttons">
                            <button id="btnApprove" type="button" data-color="red" name="" class="btn bg-red waves-effect" onclick="rejectTransfer();">
                                <span>Reject Dispatch</span>
                           
                            </button>
                            </div>    
                         @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

        <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
        <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>

        <script type="text/javascript">

        var isEdited=false;

         var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

         document.addEventListener("wheel", function(event){
            if(document.activeElement.type === "number"){
                document.activeElement.blur();
            }
        });

    $(document).ready(function(){



      $( "#product_search" ).autocomplete({
        source: function( request, response ) {
          // Fetch data
          $.ajax({
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url:"/getSearchedProducts",
            type: 'post',
            dataType: "json",
            data: {
               part: request.term,
               type:$('#category').val()
            },
            success: function( data ) {
                $('.resultItem').css({"display":"block"});
               $('.resultItem').css({"height":"100%"});
               //alert(data);
               response( data );
            }
          });
        },
        select: function (event, ui) {
                   // Set selection
           /*alert(ui);
           $('#employee_search').val(ui.item.label);*/ // display the selected text
           //$('#employeeid').val(ui.item.value); // save selected id to input
           return true;
        }
      }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var $div = $("<div class='resultItem' onclick=addProduct('"+encodeURI(item.value)+"','"+encodeURI(item.label)+"');></div>");
                $("<span class='m-name'></span>").text(item.value).appendTo($div);
                $("<span class='m-year'></span>").text(item.label).appendTo($div);
                return $("<li></li>").append($div).appendTo(ul);
            };

    });


    function ChangeMode(){
        var branch_id=$('#sourceBranch').val();
        var mode=$('#shippingMode').val();


        var transfer={!! json_encode($transfer, JSON_HEX_TAG) !!};
        if(!jQuery.isEmptyObject(transfer))
            branch_id=transfer['source_branch'];


        $('.loader').css({"display":"block"});
        if(mode!="Other"){
            $("#loaderBody").css("display","block");
              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/GetShippingCompanies",
                  type: 'POST',
                  data: {
                      'shippingMode': mode,
                      'dispatchBranch': branch_id
                  },
                  success: function(jsonData) {
                    $('.loader').css({"display":"none"});
                      $('#shipBlock').empty();
                         $('#shipBlock').html("<select name='shippingCompany' id='shippingCompany' class='form-control' required>");
                      $.each(jsonData, function(i, item) {
                          $('#shippingCompany').append($('<option>', {
                              value: item.id,
                              text: item.agency
                          }));
                      });
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert(xhr.status);
                      alert(thrownError);
                      $('.loader').css({"display":"none"});
                  }
              });
        }
        else{
            $('.loader').css({"display":"none"});
            $('#shipBlock').empty();
            $('#shipBlock').html("<input type='text' id='otherShip' class='form-control' name='otherShipping'>");
        }

   }



    function updateReqGodown(){
        $.ajax({
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url:"/getGodownList",
            type: 'post',
            dataType: "json",
            data: {
               branch_id: $('#reqBranch').val()
            },
            success: function( data ) {
                isEdited=true;
                $('#reqGodown').empty();
                    $.each(data['data'], function(i, item) {
                          $('#reqGodown').append($('<option>', {
                              value: item.id,
                              text: item.code
                          }));
                });
            }
          });
    }

    function rejectTransfer(){

        var res=confirm("Do you really want to reject the transfer");
        if(res==false)
            return;

        var transfer={!! json_encode($transfer, JSON_HEX_TAG) !!};
        var transfer_id=-1;
        if(!jQuery.isEmptyObject(transfer))
            transfer_id=transfer['id'];

        $('.loader').css({"display":"block"});

        $.ajax({
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url:"/rejectTransfer",
            type: 'post',
            dataType: "json",
            data: {
               transfer_id: transfer_id
            },
            success: function( data ) {
                $('.loader').css({"display":"none"});
                if(data['status']==true)
                    location.replace("/viewTransferRequest"); 
                else
                    alert("Something went wrong!!!");
            }
          });
    }

    function updateSourceGodown(){
         $.ajax({
             headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
            url:"/getGodownList",
            type: 'post',
            dataType: "json",
            data: {
               branch_id: $('#sourceBranch').val()
            },
            success: function( data ) {
                isEdited=true;
                $('#sourceGodown').empty();
                    $.each(data['data'], function(i, item) {
                          $('#sourceGodown').append($('<option>', {
                              value: item.id,
                              text: item.code
                          }));
                });
                ChangeMode();
            }
          });
    }


    function addProduct(nav_no,item){
        //alert(decodeURI(item));
        if(isExist(decodeURI(nav_no))){
            alert("Product already exists.");
            return;
        }

        var res = decodeURI(item).substring(0, 2);
        if(res!="P:" && isPartExists()){
            alert("Cannot Combine parts and Machineries/Accressories");
            return;
        }
        if(res=="P:" && isMachineExists()){
            alert("Cannot Combine parts and Machineries/Accressories");
            return;
        }

        var row=$("<tr></tr>");
        $("<td></td>").text(decodeURI(nav_no)).appendTo(row);
        $("<td></td>").text(decodeURI(item)).appendTo(row);
        $("<td><input type='number' oninput='updateQty(this);'/></td>").appendTo(row);
        $("<td><select class='form-control' ><option value='0'>Normal</option><option value='1'>High</option></select></td>").appendTo(row);
        $("<td><input type='button' value='remove' onclick='removeProduct(this);'></td>").appendTo(row);
        row.appendTo($('#tblBody'));
        $( "#product_search").val("");
        $('.resultItem').css({"display":"none"});
        $('.resultItem').css({"height":"0"});
        isEdited=true;

    }

    function isExist(nav_no){
        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        for (var row =1; row < rows; row++) {
            var nav=table.rows[row].cells[0].innerHTML;
            if(nav_no==nav){
                return true;
                break;
            }
        }
        return false;
    }

    function isPartExists(){
        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        for (var row =1; row < rows; row++) {
            var item=table.rows[row].cells[1].innerHTML;
            var res = item.substring(0, 2);
            if(res=='P:'){
                return true;
                break;
            }
        }
        return false;
    }

    function isMachineExists(){
        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        for (var row =1; row < rows; row++) {
            var item=table.rows[row].cells[1].innerHTML;
            var res = item.substring(0, 2);
            if(res=='M:' || res=='A:'){
                return true;
                break;
            }
        }
        return false;
    }



    function removeProduct(obj){
        var currentNode=obj.parentElement.closest('tr');
        var tableBody=currentNode.parentElement;
        var table=tableBody.parentElement;
        table.deleteRow(currentNode.rowIndex);
        isEdited=true;
    }

    function updateQty(obj){
            var currentNode=obj.parentElement.closest('tr');
            var tableBody=currentNode.parentElement;
            var table=tableBody.parentElement;
            var rows=table.rows.length;
            var totalQty=0;
            for (var row =1; row< rows; row++) {
                var qty=table.rows[row].cells[2].children[0].value;
                totalQty+=parseFloat(qty);
            }
            $('#totalQty').text(totalQty);
            isEdited=true;
    }

    function requestTransfer(){
        if($('#reqGodown').val()==$('#sourceGodown').val()){
            alert("Cannot Transfer to same godown");
            return;
        }

        

        var transfer={!! json_encode($transfer, JSON_HEX_TAG) !!};
        var transfer_id=-1;
        if(!jQuery.isEmptyObject(transfer))
            transfer_id=transfer['id'];

        var transport="";

        if($('#shippingMode').val()=='Other')
            transport=$('#otherShip').val();
        else
            transport=$("#shippingCompany option:selected").text();

        if(transport.length==0){
            alert("Transport is not selected");
            return;
        }


        var table=document.getElementById('tblData');
        var rows=table.rows.length;
        var totalQty=0;
        var jsonRequest=[];
        for (var row =1; row< rows; row++) {
            var rowData={};
            rowData['nav_no']=table.rows[row].cells[0].innerHTML;
            rowData['item']=table.rows[row].cells[1].innerHTML;
            rowData['quantity']=table.rows[row].cells[2].children[0].value;
            rowData['priority']=table.rows[row].cells[3].children[0].value;
            if(rowData['quantity']==''){
                alert("Quantity is not added");
                return;
            }
            if(rowData["quantity"]>0){
                jsonRequest.push(rowData);
            }
        }
        if(jsonRequest.length==0){
            alert("No Item qty available to proceed");
            return;
        }

        $('.loader').css({"display":"block"});
        let objJsonStr = JSON.stringify(jsonRequest);
        let objJsonB64 = window.btoa(objJsonStr);

       
        $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/addTransferRequest",
                  type: 'POST',
                  data: {
                      'req_branch':$('#reqBranch').val(),
                      'source_branch':$('#sourceBranch').val(),
                      'req_godown':$('#reqGodown').val(),
                      'source_godown':$('#sourceGodown').val(),
                      'vehicle':$('#vehicle').val(),
                      'location_mode':"",
                      'isEdited':isEdited,
                      'transfer_id':transfer_id,
                      'transport':transport,
                      'transport_type':$("#shippingMode option:selected").text(),
                      'transfer_details': objJsonB64
                  },
                  success: function(jsonData) {
                    $('.loader').css({"display":"none"});
                    if(jsonData['status']==true)
                        if(transfer_id==-1){
                            alert("Transfer Request Added Successfully");
                            document.location.reload();
                        }
                        else
                            window.location.href='/viewTransferRequest';
                    else
                        alert("Server error!!! Please try again.");
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                    $('.loader').css({"display":"none"});
                      alert("Server not responding, Please check your internet connection.");
                      //alert(xhr.status);
                      //alert(thrownError);
                  }
              });
    }

        </script>