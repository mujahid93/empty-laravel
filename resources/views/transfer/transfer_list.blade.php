@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    @if(session()->has('pending_parts_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('pending_parts_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('pending_parts_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('pending_parts_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif
                        </div>

                      

                        

                        
                        <div class="body">
                            <div class="table-responsive" id="tbl_aio">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            @if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20)
                                                <th>Dispatch Date</th>
                                            @else
                                                <th>Req Date</th>
                                            @endif
                                            <th>Tracking No</th>
                                            <th>Employee Name</th>
                                            <th>Req Branch</th>
                                            <th>Source Branch</th>
                                            <th>Status</th>
                                            @if(!$isNonEditable && Auth::user()->employee->role_id!=6)
                                            <th width="40">View/Edit</th>
                                            @endif
                                            <th width="40">Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $transfer)
                                                <tr>
                                                    @if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20)
                                                        <td>{{date('d-m-Y h:i A', strtotime($transfer->updated_at))}}
                                                        </td>
                                                    @else
                                                        <td>{{date('d-m-Y h:i A', strtotime($transfer->created_at))}}
                                                        </td>
                                                    @endif
                                                    <td>{{$transfer->tracking_no}}</td>
                                                    <td>{{$transfer->employee->name}}</td>
                                                    <td>
                                                       {{$transfer->reqBranch->name}}
                                                    </td>

                                                    <td>
                                                       {{$transfer->sourceBranch->name}}
                                                    </td>

                                                    <td>
                                                        @if($transfer->status==1)
                                                            <span>Request Submitted</span>
                                                        @elseif($transfer->status==2)
                                                            <span>Management Approved</span>

                                                        @elseif($transfer->status==3)
                                                            <span>Request Dispatched</span>
                                                        @else
                                                            <span>Rejected</span>
                                                        @endif

                                                    </td>
                                                    @if(!$isNonEditable && Auth::user()->employee->role_id!=6)
                                                    <td>
                                                        <form action="/getTransferDetails" method="post">
                                                            @csrf
                                                            <input name="transfer_id" type="hidden" value="{{$transfer->id}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                    @endif

                                                    @if(Auth::user()->employee->role_id==6 || Auth::user()->employee->role_id==20)
                                                    <td>
                                                        <form action="/printTransferBill" method="post">
                                                            @csrf
                                                            <input name="transfer_id" type="hidden" value="{{$transfer->id}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">print</i></button>
                                                        </form>
                                                    </td>
                                                    @else
                                                    <td>
                                                        <form action="/printTransferReq" method="post">
                                                            @csrf
                                                            <input name="transfer_id" type="hidden" value="{{$transfer->id}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">print</i></button>
                                                        </form>
                                                    </td>
                                                    @endif
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $data->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterPendingPartsOrder",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script> 
@endSection
