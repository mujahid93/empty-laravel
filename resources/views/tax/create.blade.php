@extends('layouts.app')

@section('css_links')

@endsection

@section('content')
<div class="block-header">
	<h2 id="temp">New Tax </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Tax Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('tax.store') }}" method="post">
                            	@csrf
                                <label for="name">HSN Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="hsn" id="" class="form-control" placeholder="Enter hsn code">
                                    </div>
                                </div>
                                <label for="code">GST Number</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="gst" id="" class="form-control" placeholder="Enter gst percentage">
                                    </div>
                                </div>

                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('scripts')
	
@endSection