@extends('layouts.header')

<meta name="csrf-token" content="{{ csrf_token() }}">

@extends('layouts.content')

@section('content')

<div class="block-header row  clearfix">

            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">

                         @if(session()->has('result'))

                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">

                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                {{ Session::get('result') }}

                         </div>

                         @endif

                         <div class="body">

                            <div class="row">

                            </div>

                        </div>

                        <div class="body">

                            <div class="table-responsive">

                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>
                                        <tr>
                                            <th>Message</th>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Order Date</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($orders as $order)
                                                <tr>
                                                    <td width="400">{{$order->message}}</td>
                                                    <td width="100">{{$order->Order->tracking_no}}</td>
                                                    <td width="300">{{$order->Order->dealer->name}}</td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                                        @if($order->Order->OrderDetail)
                                                            @if(Auth::user()->employee->role->id == 4 && !($order->Order->OrderDetail[0]->order_status >= 4))
                                                            <td>
                                                                <form action="/ViewPayApprovedOrders" method="post">
                                                                    @csrf
                                                                    <input name="tracking_no" type="hidden" value="{{$order->Order->tracking_no}}">
                                                                    <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                                </form>
                                                            </td>
                                                            @elseif(Auth::user()->employee->role->id == 5 && $order->Order->OrderDetail[0]->order_status == 2)
                                                                <td>
                                                                    <form action="/ViewExecOrderDetails" method="post">
                                                                        @csrf
                                                                        <input name="tracking_no" type="hidden" value="{{$order->Order->tracking_no}}">
                                                                        <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                                    </form>
                                                                </td>
                                                            @elseif($order->Order->OrderDetail[0]->order_status == 7)
                                                                <td width="300">
                                                                    <form action="/ViewDealerOrderDetails" method="post">
                                                                        @csrf
                                                                        <input name="tracking_no" type="hidden" value="{{$order->Order->tracking_no}}">
                                                                        <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                                    </form>
                                                                </td>
                                                            @else
                                                                <td>
                                                                    <form action="/ViewMgmtApprOrderDetail" method="post">
                                                                        @csrf
                                                                        <input type="hidden" name="tracking_no" value="{{$order->Order->OrderDetail[0]->branch_tracking_no}}">
                                                                        <button type="submit" class="btn btn-default"><i class="col-orange material-icons">search</i></button>
                                                                    </form>
                                                                </td>
                                                            @endif
                                                        @else
                                                                <td width="300">
                                                                    <form action="/ViewDealerOrderDetails" method="post">
                                                                        @csrf
                                                                        <input name="tracking_no" type="hidden" value="{{$order->Order->tracking_no}}">
                                                                        <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                                    </form>
                                                                </td>


                                                        @endif
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $orders->links() }}   
                            </div>

                        </div>

                    </div>

                </div>

            </div>            

@endsection

@extends('layouts.footer')



@section('scripts')



<script type="text/javascript">

    $(document).ready(function(){


        $('#fromDate').change(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterByFromAndToDate",

            type: 'GET',

            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val()},

            success: function(jsonData)
            {

               // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);

                    var date = new Date(jsonData.data[i].created_at);

                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';

                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");

                    $("#_table").append(row); 

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 

            {

               alert(xhr.status);

               alert(thrownError);

            }

            });      

        });



        $('#toDate').change(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterByFromAndToDate",

            type: 'GET',

            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val()},

            success: function(jsonData)

            {

               // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);

                    var date = new Date(jsonData.data[i].created_at);

                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';

                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");

                    $("#_table").append(row); 

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }

            });      

        });



        $('#_order_status_filter').change(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterByOrderStatus",

            type: 'GET',

            data: {'queryFilter':this.value},

            success: function(jsonData)

            {

               // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);

                    var date = new Date(jsonData.data[i].created_at);

                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';

                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");

                    $("#_table").append(row); 

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 

            {

               alert(xhr.status);

               alert(thrownError);

            }

            });      

        });



        $('#_search').keyup(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterPurchaseNumber",

            type: 'GET',

            data: {'queryFilter':this.value},

            success: function(jsonData)

            {

               // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);

                    var date = new Date(jsonData.data[i].created_at);

                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';

                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");

                    $("#_table").append(row); 

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 

            {

               alert(xhr.status);

               alert(thrownError);

            }

            });      

        });



        $('#_dealer_search').keyup(function(){

            $.ajax({

            headers: {

                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                 },    

            url: "/FilterByDealer",

            type: 'GET',

            data: {'queryFilter':this.value},

            success: function(jsonData)

            {

               // alert(JSON.stringify(jsonData));

               $("#_table tbody tr").remove();

               for(var i=0;i<jsonData.data.length;i++)

               {

                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);

                    var date = new Date(jsonData.data[i].created_at);

                    var vBtn = '<form method="get"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';

                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");

                    $("#_table").append(row);

               }

            },

            error: function (xhr, ajaxOptions, thrownError) 

            {

               alert(xhr.status);

               alert(thrownError);

            }

            });      

        });





    });



    function GetOrderStatus(status)

        {

            switch(status)

                    {

                        case 1:

                            return "Dealer Placed";

                            break;



                        case 2:

                            return "Executive Placed";

                            break;



                        case 3:

                            return "Payment Verified";

                            break;



                        case 4:

                            return "Management Approved";

                            break;



                        case 5:

                            return "Invoice Raised";

                            break;



                        case 6:

                            return "Dispatched";

                            break;



                        case 7:

                            return "Rejected";

                            break;

                    }

        }





</script>   

  

@endSection