@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
	<h2 id="temp">Dealer </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Dealer Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('dealer.update',$dealer->id) }}" method="post">
                                @csrf
                                @method('PUT')
                                <label for="name">Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" value="{{ $dealer->name}}" class="form-control" name="name" placeholder="Enter your name">
                                    </div>
                                </div>
                                <label for="name">Contact Person</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" class="form-control" value="{{ $dealer->contact_person}}" name="cPerson" placeholder="Enter contact person">
                                    </div>
                                </div>

                               <label for="address">Address Line 1</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address1" rows="4" class="form-control no-resize" text="" placeholder="Type your Address..">{{ $dealer->address1 }}</textarea>
                                        </div>
                                </div>

                                <label for="address">Address Line 1</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address2" rows="4" class="form-control no-resize" placeholder="Type your Address..">{{ $dealer->address1 }}</textarea>
                                        </div>
                                </div>
                                <label for="state">State</label>
                                <div class="form-group">
                                  <select  name="state" id="state" class="form-control" onchange="FetchDistByState(this.value);" >
                                          <?php
                                              $state_arr = array($dealer->state_id);

                                          ?>
                                           @foreach($states as $state)
                                                <option value="{{ $state->id }}" {{ in_array($state->id, $state_arr ) == 1 ? 'selected' : '' }}>{{ $state->name }}</option>
                                           @endforeach
                                  </select>
                                </div>

                                <label for="_districtList" id="lblDistrict">Districts</label>
                                <div class="form-group" id="districtGroup">
                                       <select name="districts" class="form-control" id = "districts" >
                                         <?php
                                              $district_arr = array($dealer->district_id);
                                          ?>
                                             @foreach($alldistricts as $district)
                                             @if(in_array($district->id, $district_arr)){ 
                                              <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                            }
                                            @endif
                                             @endforeach
                                       </select>
                                </div>

                                <label for="dealer_since">Dealer Since</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="dealer_since" min="1900" max="2099" step="1" value="{{$dealer->dealer_since}}" />
                                    </div>
                                </div>
                                <label for="gstn">Gstn</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="gstn" class="form-control" value="{{$dealer->gstn}}" name="gstn" placeholder="Enter gstn number">
                                    </div>
                                </div>
                                <label for="landline">Landline</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="landline" class="form-control" value="{{$dealer->landline}}" name="landline" placeholder="Enter landline number">
                                    </div>
                                </div>
                                <label for="mobile">Mobile</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="mobile" name="mobile" value="{{$dealer->mobile}}" class="form-control" placeholder="Enter your mobile number">
                                    </div>
                                </div>
                                <label for="website">Website</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="website" name="website" value="{{$dealer->website}}" class="form-control" placeholder="Enter your website">
                                    </div>
                                </div>
                               
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')

@section('scripts')
<script type="text/javascript">
    function FetchDistByState(stateId)
    {
        var stateId = $('#state').val();

        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
              // alert(JSON.stringify(jsonData[0]['name']));
             $('#districtGroup').remove();
             $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select name="district" id = "districts" class="form-control" id = "districts"></select></div>');

             for(var i=0;i<jsonData.length;i++){
                  $('#districts').append($('<option>', { 
                      value: jsonData[i]['id'],
                      text : jsonData[i]['name'] 
                  }));
             }
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script>
  
@endSection

