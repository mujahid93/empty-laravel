@extends('layouts.header')
@extends('layouts.content')
@section('content')
<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Dealer Visits
                            </h2>
                        </div>

                        <div class="header">

                            <input type="button" id="btnget" value="Search"  onclick="FilterAllOrders();" />

                                    @if(session()->has('dealerVisit_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('dealerVisit_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('dealerVisit_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('dealerVisit_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif

                                    <div class="col-sm-3">
                                    <div class="form-group ">
                                            <select id="emp_filter"  class="form-control" multiple="multiple">
                                                    @if(session()->has('dealerVisitEmp'))
                                                      <option value="0">All</option>
                                                    @else
                                                      <option value="0" selected>All</option>
                                                    @endif
                                                    @foreach($employees as $employee)
                                                      @if(session()->has('dealerVisitEmp') && in_array($employee->id, session()->get('dealerVisitEmp')))
                                                          <option value="{{$employee->id}}" selected>{{$employee->name}}</option>                                     
                                                      @else
                                                          <option value="{{$employee->id}}">{{$employee->name}}</option>
                                                      @endif
                                                  @endforeach

                                            </select>
                                    </div>
                                </div>

                                    {!! link_to_route('admin.visit.excel', 
                                          'Export to Excel', null, 
                                          ['class' => 'btn btn-info']) 
                                    !!}

                                    
                        </div>

                        

                    


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
											<th>CheckIn</th>
											<th>CheckOut</th>
                                            <th>Employee</th>
                                            <th>District</th>
                                            <th>Dealer</th>
                                            <th>Front Store</th>
                                            <th>Inside Store</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dealerVisits as $dealerVisit)
                                            <tr>
                                                <td>{{date('d-m-Y',strtotime($dealerVisit->created_at))}}</td>
												<td>{{date('d-m-Y',strtotime($dealerVisit->check_in))}}</td>
												<td>{{date('d-m-Y',strtotime($dealerVisit->check_out))}}</td>
                                                <td>{{$dealerVisit->employee->name}}</td>
                                                <td>{{$dealerVisit->district_name}}</td>
                                                <td>{{$dealerVisit->dealer_name}}</td>
                                                <td><a href="{{$dealerVisit->image_url1}}" download> Front Img </a> </td>
                                                <td><a href="{{$dealerVisit->image_url2}}" download> Inside Img </a> </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $dealerVisits->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
     $('#chkveg').multiselect({

              includeSelectAllOption: true
          });

          $('#btnget').click(function(){

              alert($('#chkveg').val());
    });


   function  FilterAllOrders()
    {
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterDealerVisited",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'emp_id': $('#emp_filter').val()},
            success: function(jsonData)
            {
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

  function RemoveItem(rowId)
  {
    $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/RemoveImageFromFolder",
          type: 'POST',
          data: {
              'id': rowId
          },
          success: function(jsonData) {
              window.location.reload();
          }
      });
  }
  
    
</script>   
  
@endSection