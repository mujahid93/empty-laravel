@extends('layouts.header')
@extends('layouts.content')
@section('content')
<div class="block-header">
    <h2 id="temp">New Dealer </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Dealer Details
                            </h2>
                        </div>
                        <div class="body">
                            <form action="{{ route('dealer.store') }}" method="post">
                                @csrf
                                <label for="email_address">Email Address</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="email_address" name="email" class="form-control" placeholder="Enter your email address" required>
                                    </div>
                                </div>
                                <label for="name">Company Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" class="form-control" name="name" placeholder="Enter your name">
                                    </div>
                                </div>
                                <label for="name">User Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="name" class="form-control" name="cPerson" placeholder="Enter contact person">
                                    </div>
                                </div>
                                <label for="name">Code</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="code" class="form-control" name="code" placeholder="Enter contact person">
                                    </div>
                                </div>
                                <label for="address">Address Line 1</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address1" rows="4" class="form-control no-resize" placeholder="Type your Address.."></textarea>
                                        </div>
                                </div>
                                <label for="address">Address Line 1</label>
                                <div class="form-group">
                                        <div class="form-line">
                                            <textarea name="address2" rows="4" class="form-control no-resize" placeholder="Type your Address.."></textarea>
                                        </div>
                                </div>
                                <label for="gstn">Gstn</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="gstn" class="form-control" name="gstn" placeholder="Enter gstn number">
                                    </div>
                                </div>
                                <label for="state">State</label>
                                
                                <div class="form-group">
                                  <select  name="state" id="state" class="form-control" onchange="FetchDistByState(this.value);">
                                    @foreach($states as $state)
                                      <option value='{{$state->id}}'>{{$state->name}}</option>
                                    @endforeach
                                  </select>
                                </div>

                                 <label for="_districtList" id="lblDistrict">Districts</label>
                                <div class="form-group" id="districtGroup">
                                       <select name="district" class="form-control" id = "districts" multiple>
                                             @foreach($districts as $district)
                                              <option value='{{$district->id}}'>{{$district->name}}</option>
                                             @endforeach
                                       </select>
                                </div>
                                <label for="dealer_since">Dealer Since</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" name="dealer_since" min="1900" max="2099" step="1" value="2016" />
                                    </div>
                                </div>
                                <label for="landline">Landline</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="landline" class="form-control" name="landline" placeholder="Enter landline number">
                                    </div>
                                </div>
                                <label for="mobile">Mobile</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="Enter your mobile number">
                                    </div>
                                </div>
                                <label for="website" id="lblWebsite">Website</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="website" name="website" class="form-control" placeholder="Enter your website">
                                    </div>
                                </div>
                                <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@extends('layouts.footer')
@section('scripts')
<script type="text/javascript">


    // $(document).ready(function(){
    //     FetchDistByState(1);
    // });
    

    function FetchDistByState(stateId)
    {
        var stateId = $('#state').val();

        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData[0]['name']));
             $('#districtGroup').remove();
             $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select name="district" id = "districts" class="form-control" id = "districts"></select></div>');

             for(var i=0;i<jsonData.length;i++){
                  $('#districts').append($('<option>', { 
                      value: jsonData[i]['id'],
                      text : jsonData[i]['name'] 
                  }));
             }
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script>
  
@endSection