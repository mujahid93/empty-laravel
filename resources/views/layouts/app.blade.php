<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
    @yield('css_links')
</head>

<body class="theme-blue">
   <section>
        <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">Kisankraft Private Limited &nbsp &nbsp &nbsp &nbsp &nbsp </a> 
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Tasks -->
                    <li class="dropdown">
                    </li>
                    <!-- #END# Tasks -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar" >
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <!-- <img src="images/user.png" width="48" height="48" alt="User" /> -->
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>@if(Auth::user()) {{ Auth::user()->name }} @endif</b></div>
                    <div class="email">@if(Auth::user()){{ Auth::user()->email }} @endif</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ url('/logout') }}"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <!-- <li class="header">MAIN NAVIGATION</li> -->
                    <li class="active">
                        <a href="#">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    @can('board')
                    <li>
                        <a href='{{ route('dealer.index') }}'>
                            <i class="material-icons">text_fields</i>
                            <span>Dealers</span>
                        </a>
                    </li>
                    <li>
                        <a href='{{ route('employee.index') }}'>
                            <i class="material-icons">face</i>
                            <span>Employees</span>
                        </a>
                    </li>
                    <li>
                        <a href='{{ route('role.index') }}'>
                            <i class="material-icons">verified_user</i>
                            <span>Roles</span>
                        </a>
                    </li>
                    @endcan
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">shopping_cart</i>
                            <span>Orders</span>
                        </a>
                         <ul class="ml-menu">
                            @can('create_order_gate')
                            <li>
                                <a href="{{ route('order.create') }}">New Order</a>
                            </li>
                            @endcan
                            @can('order_info_gate')
                            <li>
                                <a href="{{ route('order.index') }}">Pending</a>
                            </li>
                            <li>
                                <a href="/ApprovedOrders">Approved</a>
                            </li>
                            <li>
                                <a href="/InvoiceRaisedOrders">Invoice Raised</a>
                            </li>
                            <li>
                                <a href="/DispatchedOrders">Dispatched</a>
                            </li>
                            <li>
                                <a href="/ReceivedOrders">Orders Received</a>
                            </li>
                            <li>
                                <a href="/RejectedOrders">Rejected</a>
                            </li>
                            @endcan
                        </ul>
                    </li>
                    @can('tour_gate')
                    <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">swap_calls</i>
                            <span>Tours</span>
                        </a>
                         <ul class="ml-menu">
                            <li>
                                <a href="#">New Tour</a>
                            </li>
                            <li>
                                <a href="#">Pending</a>
                            </li>
                            <li>
                                <a href="#">Approved</a>
                            </li>
                            <li>
                                <a href="#">Rejected</a>
                            </li>
                        </ul>
                    </li>
                    @endcan
                    @can('board')
                    <li>
                        <a href="#" class="menu-toggle">
                            <i class="material-icons">all_inclusive</i>
                            <span>Machinery</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href='{{ route('machineGroup.index') }}'>Machinery Group</a>
                            </li>
                            <li>
                                <a href=''>Machinery Subgroup</a>
                            </li>
                            <li>
                                <a href=''>Machinery SKU</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">linear_scale</i>
                            <span>Accessories</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href=''>Accessories Group</a>
                            </li>
                            <li>
                                <a href=''>Accessories Subgorup</a>
                            </li>
                            <li>
                                <a href=''>Accessories SKU</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">watch</i>
                            <span>Parts</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href=''>Parts Group</a>
                            </li>
                            <li>
                                <a href=''>Parts SKU</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <li>
                        <a href='{{ route('state.index') }}'>
                            <i class="material-icons">text_fields</i>
                            <span>States</span>
                        </a>
                        </li>
                        <li>
                            <a href='{{ route('district.index') }}'>
                                <i class="material-icons">face</i>
                                <span>Districts</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('tax.index') }}'>
                                <i class="material-icons">verified_user</i>
                                <span>Taxes</span>
                            </a>
                        </li>
                        <li>
                            <a href='{{ route('branch.index') }}'>
                                <i class="material-icons">verified_user</i>
                                <span>Branches</span>
                            </a>
                        </li>
                        @endcan
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">

                <div class="copyright">
                    &copy; 2018 - 2019 <a href="https://www.kisankraft.com">Kisankraft Pvt Ltd</a>
                </div>
                <div class="version">
                    <b>Version: </b> 1.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

        <section class="content">
        <div class="container-fluid">
            
            @yield('content')
            
        </div>
        </section>
    </div>
</body>

</html>
    <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-select/js/bootstrap-select.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
    @yield('scripts')



    