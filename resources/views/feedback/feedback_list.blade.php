@extends('layouts.header')
@extends('layouts.content')
@section('content')
<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Feedback List
                            </h2>
                        </div>

          

                        

                    


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>CheckIn</th>
                                            <th>CheckOut</th>
                                            <th>Employee</th>
                                            <th>Demo</th>
                                            <th>Image1</th>
                                            <th>Image2</th>
                                            <th>Download</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($feedback as $feed)
                                            <tr>
                                                <td>{{date('d-m-Y',strtotime($feed->created_at))}}</td>
                                                <td>{{date('d-m-Y',strtotime($feed->check_in))}}</td>
                                                <td>{{date('d-m-Y',strtotime($feed->check_out))}}</td>
                                                <td>{{$feed->employee->name}}</td>
                                                <td>{{$feed->demo->subCatName}}</td>
                                                <td><a href="{{$feed->check_in_img}}" download> CheckIn </a> </td>
                                                <td><a href="{{$feed->check_out_img}}" download> CheckOut </a> </td>
												<td><a href="{{ 'exportSingleFeedbackReport/'.$feed->id }}" download> Download </a> </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $feedback->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    
    
</script>   
  
@endSection