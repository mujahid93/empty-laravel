@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }

    #map {
        height: 60%;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
            <div class="row clearfix" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Demo Feedback Reports
                            </h2>
                        </div>

                        <div class="header">

                                        <div class="row">
                                                <input type="date" id="fromDate" name="fromDate"  value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onchange="setHref()">

                                                <input type="date" id="toDate" name="toDate"  value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onchange="setHref()">

                                                 <select id="category" name="category" onchange="setFilter()">
                                                        @foreach($category as $cat)
                                                              <option value="{{$cat->cat_id}}">{{$cat->catName}}</option>
                                                        @endforeach
                                                </select>

                                                <select id="sub_category" name="sub_category" onchange="setHref()">
                                                        @foreach($subcat as $sub)
                                                             @if($sub->cat_id == $category[0]->cat_id)
                                                              <option value="{{$sub->id}}">{{$sub->subCatName}}</option>
                                                             @endif
                                                        @endforeach
                                                </select>

                                                <a id="btnDownload" href="{{ '/exportFeedbackReport'.'/'.Carbon\Carbon::now()->format('Y-m-d').'/'.Carbon\Carbon::now()->format('Y-m-d').'/'.$subcat[0]->id }}" class="btn btn-info"> DOWNLOAD </a>
                                            
                                        </div>


                              

                                    
                        </div>

                        

          
                        <div class="body">
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')
<script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
<script type="text/javascript">
  function setFilter(){


    var subcat = <?php print_r(json_encode($subcat)) ?>;
    $('#sub_category').empty();



    for (var i = 0; i < subcat.length; i++) {
        if($('#category').val()==parseInt(subcat[i]['cat_id']))
            $('#sub_category').append("<option value='"+subcat[i]['id']+"'>"+subcat[i]['subCatName']+"</option>");
    }

    setHref();

  }

  function setHref(){
    var val="/exportFeedbackReport/"+$('#fromDate').val()+"/"+$('#toDate').val()+"/"+$('#sub_category').val();
    $("#btnDownload").attr("href", val);
    //alert(val);
  }
 
  
    
</script>  
  
@endSection