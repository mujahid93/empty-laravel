<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<div class="container">
		<div class="row">
			<div class="text-xs-center">
				<i class="fa fa-search-plus float-xs-left icon"></i>
				<h2>Purchase Request</h2>
			</div>
			<hr>
			<div class="row">
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4">
					<h1 style="font-family: 'Monotype Corsiva';"><i>KisanKraft<sup class="font-20"> &reg;</sup></i></h1>
					<h5>ISO 9001:2015 Certified</h5>
					<h6 style="font-family:comic sans MS">Krushaka Mantram - Krushi Yantram</h6>
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
				</div>
				<div class="col-xs-4 col-md-4 col-lg-4 col-sm-4" style="margin-top:25px;">
					<h5>DATE : {{ date('d-m-Y h:i A',strtotime($placedOrders[0]->order->created_at)) }}</h5>
				</div>
			</div>
			<div class="row" style="margin-top:30px;">
				<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
					<div class="card height">
						<div class="card-header"><strong>Dealer Name</strong></div>
						<div class="card-block">
							{{ $placedOrders[0]->order->dealer->name }}<br>
						</div>
						</br>
						<div class="card-header"><strong>Address Details</strong></div>
						<div class="card-block">
							{{ $placedOrders[0]->order->dealer->address }}<br>
						</div>
						</br>
						<div class="card-header"><strong>Tracking No</strong> : {{ $placedOrders[0]->branch_tracking_no }}</div>
						</br>
						<div class="card-header"><strong>Dispatch Branch</strong> : {{ $placedOrders[0]->branch_name->name }}</div>
						</br>
					</div>
				</div>
				<div class="col-xs-6 col-md-6 col-lg-6 col-sm-6">
					<div class="card height">
						<div class="card-header"><strong>Shipping Type</strong> :
						@if($placedOrders[0]->shipping_id!=null)
							 {{ $placedOrders[0]->shipping->name }}
						@endif
						</div>
						</br>
						</br>
						<div class="card-header"><strong>Shipping Name</strong> :
						@if($placedOrders[0]->shipping_id==null)
							 {{ $placedOrders[0]->other_ship }}
						@elseif($placedOrders[0]->shipping->agency=='Other')
						 	{{ $placedOrders[0]->other_ship }}
						@else
							{{ $placedOrders[0]->shipping->agency }}
						@endif
						 </div>
						</br>
						<div class="card-header"><strong>Ordered By</strong> : {{ $placedOrders[0]->order->employee->name }}</div>
						</br>
						<div class="card-header"><strong>Destination</strong> : {{ $placedOrders[0]->destination }}</div>
						</br>
						<div class="card-header"><strong>Terms of payment</strong> : {{ $placedOrders[0]->order->p_mode }}</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card ">
					<div class="card-header">
						<h3 class="text-xs-center">Order summary</h3>
					</div>
					<div class="card-block">
						<div class="table-responsive">
							<table class="table table-sm">
								<thead>
									<tr>
										<td><strong>#</strong></td>
										<td><strong>Item Name</strong></td>
										<td class="text-xs-center"><strong>Rate Type</strong></td>
										<td align="right" class="text-xs-right"><strong>Unit Price</strong></td>
										<td align="right" class="text-xs-center"><strong>GST</strong></td>
										<td align="right" class="text-xs-center"><strong>Qty</strong></td>
										<td align="right" class="text-xs-center"><strong>Discount</strong></td>
										<td align="right" class="text-xs-right"><strong>Amount</strong></td>
									</tr>
								</thead>
								<tbody>
									<?php unset($items); ?>
                                    @php ($items = [])
									<?php $totalPrice = 0; $index = 1; $finalRate = 0;?>
									@foreach($placedOrders as $order)
									<tr>
										<td>{{$index++}}</td>
										<td>{{ $order->nav_no.' '.substr($order->sku, 0, 55).'...' }}</td>
                                        <?php $price = 0; $rate_without_tax = 0; ?>
                                                <?php 
                                                $rate_type = "";
                                                switch ($order->rate_type) {
                                                    case '0':
                                                        $rate_type = "Dealer Price";
                                                        $price = $order->item_rate;
                                                        $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                                        $finalRate = number_format((float)($price - $order->dis_per/100 * $price) * $order->qty, 2, '.', '');
                                                        $finalRate_NC = ($finalRate*$order->gst/100) + $finalRate ;
                                                        break;
                                                    
                                                    case '1':
                                                        $rate_type = "MRP";
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    case '2':
                                                        $rate_type = "Subsidy--".$order->subsidy_dept->name;
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    default:
                                                        break;
                                                }
                                                ?>
                                        <td>{{$rate_type}}</td>        
										<td align="right" class="text-xs-right">Rs {{ IND_money_format($price) }}</td>
                                        <td align="right" class="text-xs-center">{{number_format($order->gst,0)}} %</td>
										<td align="right" class="text-xs-center">
                                                        {{$order->qty}}
                                        </td>
										<td align="right" class="text-xs-center">{{ number_format($order->dis_per,2) }} %</td>
										<td align="right" class="text-xs-right">Rs {{IND_money_format($finalRate_NC)}}</td>
									</tr>
														<?php
                                                            switch ($order->rate_type) {
                                                            case '0':
                                                                    $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                                                                    $totalPrice += $finalRate;
                                                                break;
                                                            
                                                            case '1':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            case '2':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            default:
                                                                break;
                                                        }
                                                        ?>
                                    @endforeach
                                    				<?php $finalTotal_With_Freight = 0; ?>
                                                    @foreach($items as $item)
                                                        <?php 
                                                            $finalTotal_With_Freight = $finalTotal_With_Freight + (($placedOrders[0]->freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
                                                        ?>
                                                    @endforeach

									<tr>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td align="right" class="emptyrow text-xs-center"><strong>Subtotal</strong></td>
										<td align="right" class="emptyrow text-xs-right">Rs {{IND_money_format($totalPrice + $finalTotal_With_Freight)}}</td>
									</tr>
									<tr>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td class="highrow"></td>
										<td align="right" class="highrow text-xs-center"><strong>Freight</strong></td>
										<td align="right" class="highrow text-xs-right">Rs {{IND_money_format($placedOrders[0]->freight)}}</td>
									</tr>
									<tr>
										<td class="emptyrow"><i class="fa fa-barcode iconbig"></i></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td class="emptyrow"></td>
										<td align="right" class="emptyrow text-xs-center"><strong>Total</strong></td>
										<td align="right" class="emptyrow text-xs-right">Rs {{IND_money_format($totalPrice + $placedOrders[0]->freight + $finalTotal_With_Freight)}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style>
	.height {
	min-height: 200px;
	}
	.icon {
	font-size: 47px;
	color: #5CB85C;
	}
	.iconbig {
	font-size: 77px;
	color: #5CB85C;
	}
	.table > tbody > tr > .emptyrow {
	border-top: none;
	}
	.table > thead > tr > .emptyrow {
	border-bottom: none;
	}
	.table > tbody > tr > .emptyrow {
	border-top: 1px solid;
	border-bottom: 1px solid;
	}
	.table > tbody > tr > .highrow {
	border-top: none;
	border-bottom: none;
	}
	</style>
</body>
</html>