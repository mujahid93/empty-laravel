<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
</head>

@extends('layouts.content')
@section('content')
            <div class="row clearfix">
                <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">

                    <div class="card">
                        <div class="header">
                            <h3>
                                Order Details
                            </h3>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Tracking Number</p>
                                        <h5 id="tracking_no">{{$placedOrders[0]->order->tracking_no}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Dealer Name</p>
                                        <h5>{{$placedOrders[0]->order->dealer->name}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Date</p>
                                        <h5>{{date('d-m-Y', strtotime($placedOrders[0]->order_date))}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Status</p>
                                        <h4><span id="orderStatus" class="label bg-green">
                                            Management Approved
                                            </span></h4>
                                    </blockquote>
                                </div>
                        </div>

                        <div class="body">
                            <div class="table-responsive">
                                <?php $finalAmt = 0; ?>
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Sku</th>
                                            <th>Qty</th>
                                            <th>Branch</th>
                                            <th>Rate Type</th>
                                            <th>Rate</th>
                                            <th>GST (%)</th>
                                            <th style="background: #DBEEBC">Disc %</th>
                                            <th style="background: #DBEEBC" width="30">Disc Rate</th>
                                            <th>Final Amt</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php unset($items); ?>
                                        @php ($items = [])
                                        <?php $totalPrice = 0; $freight = 0; $dis_rate = 0; $finalRate = 0;?>
                                        @foreach($placedOrders as $order)
                                                <?php $freight = $order->freight; $destination = $order->destination; $newDestFlag = 0;
                                                    if(!($order->order->dealer->address1 == $order->destination || $order->order->dealer->address2 == $order->destination || $order->order->dealer->address3 == $order->destination))
                                                        $newDestFlag = 1;
                                                    else
                                                        $newDestFlag = 0;
                                                 ?>
                                            <tr>
                                                <td>{{$order->sku}}</td>
                                                <td align="right">
                                                    {{$order->qty}}
                                                </td>
                                                <td>
                                                    <span class="custom-dropdown big">
                                                        <style type="text/css">
                                                            .custom-dropdown select {
                                                              background-color: #fff;
                                                              color: #000;
                                                              font-size: inherit;
                                                              padding: .5em;
                                                              padding-right: 3.5em; 
                                                              border: 0;
                                                              margin: 0;
                                                              border-radius: 3px;
                                                              text-indent: 0.01px;
                                                              text-overflow: '';
                                                              /*Hiding the select arrow for firefox*/
                                                              -moz-appearance: none;
                                                              /*Hiding the select arrow for chrome*/
                                                              -webkit-appearance:none;
                                                              /*Hiding the select arrow default implementation*/
                                                              appearance: none;
                                                            }
                                                            /*Hiding the select arrow for IE10*/
                                                        </style>
                                                        <select id="{{$order->id}}dispatchbranch" disabled="true">
                                                            @foreach($branches as $branch)
                                                                @if($order->branch == $branch->id)
                                                                    <option value="{{ $branch->id.':'.$order->id }}" selected>{{$branch->code}}</option>
                                                                @else
                                                                    <option value="{{ $branch->id.':'.$order->id }}">{{$branch->code}}</option>
                                                                @endif
                                                            @endforeach        
                                                        </select>
                                                    </span>
                                                </td>
                                                <?php $price = 0;  $rate_without_tax = 0; ?>
                                                <?php 
                                                $rate_type = "";
                                                switch ($order->rate_type) {
                                                    case '0':
                                                        $rate_type = "Dealer Price";
                                                        $price = $order->item_rate;
                                                        $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                                        $finalRate = number_format((float)($price - $order->dis_per/100 * $price) * $order->qty, 2, '.', '');
                                                        $finalRate_NC = ($finalRate*$order->gst/100) + $finalRate ;
                                                        break;
                                                    
                                                    case '1':
                                                        $rate_type = "MRP";
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    case '2':
                                                        $rate_type = "Subsidy";
                                                        $price = $order->item_rate;
                                                        $dis_rate = $order->item_rate;
                                                        $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                        $finalRate = $order->item_rate * $order->qty;
                                                        $finalRate_NC = $finalRate;
                                                        break;

                                                    default:
                                                        break;
                                                }
                                                ?>
                                                <td>
                                                  {{$rate_type}}
                                                </td>
                                                <td align="right">
                                                    <input id="up-{{ $order->id }}" style="width: 70px" type="text" value="{{IND_money_format($price)}}" readonly>
                                                </td>
                                                <td align="right">{{$order->gst}}</td>
                                                <td align="right">
                                                   <input id="disperc-{{ $order->id }}" style="width: 70px" type="text" value="{{$order->dis_per}}" readonly>
                                                </td>
                                                <td align="right">
                                                    <input id="disamt-{{ $order->id }}" style="width: 70px" type="text" value="{{IND_money_format($dis_rate)}}" readonly>
                                                </td>
                                                <td align="right">
                                                     <?php
                                                            switch ($order->rate_type) {
                                                            case '0':
                                                                    $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                                                                    $totalPrice += $finalRate;
                                                                break;
                                                            
                                                            case '1':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            case '2':
                                                                    $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                    $totalPrice += $rate_without_tax;
                                                                break;

                                                            default:
                                                                break;
                                                        }
                                                        ?>
                                                        {{IND_money_format($finalRate_NC)}}
                                                </td>
                                            </tr>
                                        @endforeach
                                                <tr>
                                                    <td colspan="7"></td>
                                                    <td colspan="1">Freight</td>
                                                    <td align="right" colspan="1">{{IND_money_format($freight)}}</td>
                                                </tr>
                                                    <?php $finalTotal_With_Freight = 0; ?>
                                                    @foreach($items as $item)
                                                        <?php 
                                                            $finalTotal_With_Freight = $finalTotal_With_Freight + (($freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
                                                        ?>
                                                    @endforeach
                                                <tr>
                                                    <td colspan="1"><b>Destination</b></td>
                                                    <td colspan="1">
                                                        @if($newDestFlag)
                                                            {{$destination}}<sup style="top: 0px;left: 10px"><span class="badge bg-green">Consignee address changed</span></sup>
                                                        @else
                                                            {{$destination}}
                                                        @endif
                                                    </td>
                                                    <td colspan="5"></td>
                                                    <td colspan="1">Total</td> 
                                                    <?php $finalAmt += $freight + $totalPrice + $finalTotal_With_Freight; ?>
                                                    <td align="right" colspan="1"><input id="totalPrice" style="width: 80px" type="text" value="{{IND_money_format($freight + $totalPrice + $finalTotal_With_Freight)}}" readonly></td>
                                                </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                                <div class="row">
                                    <div class="m-l-40 pull-left">
                                            <form action="{{ route('order_pdf') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="tracking_no" value="{{encrypt($placedOrders[0]->branch_tracking_no)}}">
                                                <button type="submit" class="btn btn-danger"><i class="col-white material-icons">picture_as_pdf</i> Print</button>
                                            </form>
                                    </div>

                                    <div class="m-l-40 pull-right">
                                            <form action="{{ route('order.exec_approve') }}" method="post">
							                    @csrf
							                    <input type="hidden" name="tracking_no" value="{{encrypt($placedOrders[0]->tracking_no)}}">
							                    <input type="hidden" name="orderStatus" value="7">
							                    <button type="submit" class="btn btn-link waves-effect">Cancel Order</button>
							                </form>
                                    </div>

                                </div>
                    </div>
                </div>
            </div>
@endsection
    
    <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
  
<script type="text/javascript">

    var selectedBranch = 0;

    function FetchPriceRate(rowData)
    {
        var rowData = rowData.split(':');
        branchId = rowData[0];
        orderId = rowData[1];

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/FetchPriceRate",
            type: 'POST',
            data: {
                'branchId': branchId,
                'orderId': orderId,
            },
            success: function(jsonData) {
                // alert(JSON.stringify(jsonData));
                window.location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    
$(function () {
    $('.js-modal-buttons .btn').on('click', function () {
        var orderStatus = $('#orderStatus').text();
        $('#selOrderStatus').val(orderStatus);
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-');
        $('#mdModal').modal('show');
    });
});

</script>
