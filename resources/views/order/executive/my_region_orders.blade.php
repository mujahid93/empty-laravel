@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    @if(session()->has('region_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('region_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('region_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('region_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif
                        </div>
                         <div class="body">
                            <div class="row">
                                    <div class="col-sm-1">
                                        <div class="form-group ">
                                                <select id="_order_status_filter" class="form-control" onchange="FilterAllOrders()">
                                                    @if(session()->get('region_orderStatus','-1')==0)
                                                        <option value="0" selected="selected">All</option>
                                                    @else
                                                        <option value="0" >All</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==1)
                                                        <option value="1" selected="selected">Pending</option>
                                                    @else
                                                        <option value="1" >Pending</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==2)
                                                        <option value="2" selected="selected">Executive Placed</option>
                                                    @else
                                                        <option value="2" >Executive Placed</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==3)
                                                        <option value="3" selected="selected">Mgmt Approved</option>
                                                    @else
                                                        <option value="3" >Mgmt Approved</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==4)
                                                        <option value="4" selected="selected">Payment Verified</option>
                                                    @else
                                                        <option value="4" >Payment Verified</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==5)
                                                        <option value="5" selected="selected">Invoice Raised</option>
                                                    @else
                                                        <option value="5" >Invoice Raised</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==6)
                                                        <option value="6" selected="selected">Dispatched</option>
                                                    @else
                                                        <option value="6" >Dispatched</option>
                                                    @endif


                                                    @if(session()->get('region_orderStatus','-1')==7)
                                                        <option value="7" selected="selected">Rejected</option>
                                                    @else
                                                        <option value="7" >Rejected</option>
                                                    @endif
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label id="lblDealer"></label>
                                        <div class="form-group" id="dealerGroup">
                                                <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                    <option value="0" >All</option>
                                                    @foreach($dealers as $dealer)

                                                        @if(session()->has('region_dealer') && session()->get('region_dealer')==$dealer->id)
                                                            <option value="{{$dealer->id}}" selected>{{$dealer->name}}</option>                                     
                                                        @else
                                                            <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="tbl_aio" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Placed By</th>
                                            <th>Order Date</th>
                                            <th>Order Status</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($regionOrders as $order)
                                            @if($order == $regionOrders->first())    
                                                <tr style="background: #efe">
                                                    <td style="position: relative;" width="300">{{$order->tracking_no}} &nbsp<span class="badge bg-green" style="color: white;padding:5px;position: absolute; top: 10px;">new</span></td>
                                                    <td width="300">{{$order->dealer->name}}</td>
                                                    <td width="300">
                                                        @if($order->placedBy!=null)
                                                            {{$order->placedBy->name}}
                                                        @else
                                                            {{$order->dealer->name}}
                                                        @endif
                                                    </td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))}}</td>
                                                    <td>
                                                        @switch($order->OrderDetail[0]->order_status)
                                                            @case(1)
                                                                <span>Dealer Placed</span>
                                                                @break

                                                            @case(2)
                                                                <span>Executive Placed</span>
                                                                @break

                                                            @case(3)
                                                                <span>Payment Verified</span>
                                                                @break    

                                                            @case(4)
                                                                <span>Management Approved</span>
                                                                @break 

                                                            @case(5)
                                                                <span>Invoice Uploaded</span>
                                                                @break 

                                                            @case(6)
                                                                <span>Dispatched</span>
                                                                @break

                                                            @case(7)
                                                                <span>Rejected</span>
                                                                @break
                                                        @endswitch
                                                    </td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td width="300">{{$order->tracking_no}}</td>
                                                    <td width="300">{{$order->dealer->name}}</td>
                                                    <td width="300">
                                                        @if($order->placedBy)
                                                            {{$order->placedBy->name}}
                                                        @else
                                                            {{$order->dealer->name}}
                                                        @endif
                                                    </td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                                    <td>
                                                         @switch($order->OrderDetail[0]->order_status)
                                                            @case(1)
                                                                <span>Dealer Placed</span>
                                                                @break

                                                            @case(2)
                                                                <span>Executive Placed</span>
                                                                @break

                                                            @case(3)
                                                                <span>Payment Verified</span>
                                                                @break    

                                                            @case(4)
                                                                <span>Management Approved</span>
                                                                @break 

                                                            @case(5)
                                                                <span>Invoice Uploaded</span>
                                                                @break 

                                                            @case(6)
                                                                <span>Dispatched</span>
                                                                @break

                                                            @case(7)
                                                                <span>Rejected</span>
                                                                @break
                                                        @endswitch
                                                    </td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $regionOrders->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')

@section('scripts')
<script type="text/javascript">

    function  FilterAllOrders()
    {
        // alert($('#_order_status_filter').val());
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterMyRegionOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'order_status': $('#_order_status_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                // window.location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }
</script>
@endSection

