@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    <input type="date" id="fromDate" name="fromDate" value="{{$todayDate}}">
                                    <input type="date" id="toDate" name="toDate" value="{{$todayDate}}">
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                         <div class="body">
                            <div class="row">
                                
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_dealer_search" class="form-control">
                                            <label class="form-label">Dealer Name</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_search" class="form-control">
                                            <label class="form-label">Purchase Number</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Order Date</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($execOrders as $order)
                                                <tr>
                                                    <td width="300">{{$order->tracking_no}}</td>
                                                    <td width="300">{{$order->dealer->name}}</td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))}}</td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $execOrders->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')

@section('scripts')

<script type="text/javascript">
    
    $(document).ready(function(){

        $('#fromDate').change(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterByFromAndToDate",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val()},
            success: function(jsonData)
            {
               // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);
                    var date = new Date(jsonData.data[i].created_at);
                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';
                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");
                    $("#_table").append(row); 
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        });

        $('#toDate').change(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterByFromAndToDate",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val()},
            success: function(jsonData)
            {
               // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);
                    var date = new Date(jsonData.data[i].created_at);
                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';
                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");
                    $("#_table").append(row); 
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        });

        $('#_order_status_filter').change(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterByOrderStatus",
            type: 'GET',
            data: {'queryFilter':this.value},
            success: function(jsonData)
            {
               // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);
                    var date = new Date(jsonData.data[i].created_at);
                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';
                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");
                    $("#_table").append(row); 
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        });

        $('#_search').keyup(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterPurchaseNumber",
            type: 'GET',
            data: {'queryFilter':this.value},
            success: function(jsonData)
            {
               // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);
                    var date = new Date(jsonData.data[i].created_at);
                    var vBtn = '<form action="/ViewDealerOrderDetails" method="post">@csrf<input name="tracking_no" type="hidden" value="'+jsonData.data[i]['tracking_no']+'"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';
                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");
                    $("#_table").append(row); 
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        });

        $('#_dealer_search').keyup(function(){
            $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterByDealer",
            type: 'GET',
            data: {'queryFilter':this.value},
            success: function(jsonData)
            {
               // alert(JSON.stringify(jsonData));
               $("#_table tbody tr").remove();
               for(var i=0;i<jsonData.data.length;i++)
               {
                    var orderStatus= GetOrderStatus(jsonData.data[i]['order_status']);
                    var date = new Date(jsonData.data[i].created_at);
                    var vBtn = '<form method="get"><button type="submit" class="btn btn-default"><i class="col-grey material-icons">search</i></button></form>';
                    var row = $("<tr><td>"+jsonData.data[i]['tracking_no']+"</td><td>"+jsonData.data[i]['dealer']['name']+"</td><td>"+date.getDate() + '-' + (date.getMonth() + 1) + '-' +  date.getFullYear()+"</td><td>"+orderStatus+"</td><td>"+vBtn+"</td><tr>");
                    $("#_table").append(row);
               }
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
            });      
        });


    });

    function GetOrderStatus(status)
        {
            switch(status)
                    {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
                    }
        }


</script>   
  
@endSection