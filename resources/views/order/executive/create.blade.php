<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Welcome To Kisankraft Limited</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css") }}" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
        <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
        <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
    </head>
    @extends('layouts.content')
    @section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header" style="background: #fff">
                    <h2>
                    <b>Place New Order</b>
                    </h2>
                </div>
                @if(session()->has('msg'))
                     <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{session()->get('msg')}}
                      </div>
                @endif
                
                <form name="orderForm" id="orderForm" action="{{ route('order.store') }}" method="post">
                    @csrf
                    <div class="body" style="background: #fff">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="Group" id="todayDate">Date</label>
                                    <input type="hidden" name="classification" id="classification" value="1">
                                    <style type="text/css">
                                    input[type="date"]::-webkit-calendar-picker-indicator {
                                    color: rgba(0, 0, 0, 0);
                                    opacity: 1;
                                    display: block;
                                    background: url(https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png) no-repeat;
                                    width: 20px;
                                    height: 20px;
                                    border-width: thin;
                                    }
                                    </style>
                                    @if(session()->has('orderDate'))
                                        <?php $orderDate = session()->get('orderDate') ?>
                                    @else
                                        <?php $orderDate = $todayDate ?>
                                    @endif
                                    <input type="date" id="todayDate" name="todayDate" class="form-control" value="{{ $orderDate }}" />
                                </div>
                            </div>
                            @can('isManagement')
                                <div class="col-sm-4">
                                    <label for="State">State</label>
                                    <div class="form-group">
                                        <select name="state" id="state" class="form-control" required onchange="FetchDealersByState()">
                                            @foreach($states as $state)
                                                @if(session()->has('state_id'))
                                                    @if(session()->get('state_id') == $state->id)
                                                        <option value='{{$state->id}}' selected>{{$state->name}}</option>
                                                    @else
                                                        <option value='{{$state->id}}'>{{$state->name}}</option>
                                                    @endif
                                                @else
                                                    <option value='{{$state->id}}'>{{$state->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                <label for="dealer">Dealer</label>
                                <div class="form-group">
                                    <select name="m_dealer" id="m_dealer" class="form-control" required onchange="FetchPriceByPriceType()">
                                        @foreach($dealers as $dealer)
                                            @if(session()->has('dealer_id'))
                                                @if(session()->get('dealer_id') == $dealer->id)
                                                    <option value='{{$dealer->id}}' selected>{{$dealer->name}}</option>
                                                @else
                                                    <option value='{{$dealer->id}}'>{{$dealer->name}}</option>
                                                @endif
                                            @else
                                                <option value='{{$dealer->id}}'>{{$dealer->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>                                
                            @endcan
                            @can('isExecutive')
                            <div class="col-sm-4">
                                <label for="dealer">Dealer</label>
                                <div class="form-group">
                                    <select name="dealer" id="dealer" class="form-control" required onchange="FetchPriceByPriceType()">
                                        @foreach($dealers as $dealer)
                                            @if(session()->has('dealer_id'))
                                                @if(session()->get('dealer_id') == $dealer->id)
                                                    <option value='{{$dealer->id}}' selected>{{$dealer->name}}</option>
                                                @else
                                                    <option value='{{$dealer->id}}'>{{$dealer->name}}</option>
                                                @endif
                                            @else
                                                <option value='{{$dealer->id}}'>{{$dealer->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endcan
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="dispatchBranch" id="lblBranch">Dispatch Branch</label>
                                    <select name="dispatchBranch" id="dispatchBranch" class="form-control" required onchange="GetPriceByBranch()">
                                        @foreach($branches as $branch)
                                        @if(session()->has('dispatchBranch'))
                                        @if(session()->get('dispatchBranch') == $branch->id)
                                        <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                        @else
                                        <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                        @endif
                                        @else
                                        <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li onclick="setClassification(1)" role="presentation" class="active" id="mchTab"><a href="#Machinery" data-toggle="tab">Machinery</a></li>
                            <li onclick="setClassification(2)" role="presentation" id="accTab"><a href="#accessory" data-toggle="tab">Accessories</a></li>
                            <li onclick="setClassification(3)" role="presentation" id="partTab"><a href="#parts" data-toggle="tab">Parts</a></li>
                        </ul>
                        
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="Machinery">
                                <div class="card p-l-20 p-t-20" style="background: #fff">
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group" id="">
                                                <label for="Group" id="lblGrp">Group</label>
                                                <div id="GrpNames">
                                                    <select name="Group1" id="Group1" class="form-control" onchange="FetchSubgrpByGrp(this.value);" required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Subgroup" id="lblSubgrp">Subgroup</label>
                                                <div id="SubgrpNames">
                                                    <select name="Subgroup1" id="Subgroup1" class="form-control" onchange="FetchSkuBySubgrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="hsnCode">HSN Code</label>
                                                <input type="text" name="hsnCode1" style="background-color: #fff;" id="hsnCode1" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label for="taxPerc">GST %</label>
                                                <input type="text" name="taxPerc1" style="background-color: #fff;" id="taxPerc1" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="priceLevel" id="lblPriceLevel">Rate Type</label>
                                                <select name="priceLevel1" id="priceLevel1" class="form-control" required onchange="FetchPriceByPriceType()">
                                                    <option value="0">Dealer Price</option>
                                                    <option value="1">MRP Price</option>
                                                    <!-- <option value="2">Subsidy</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2" id="subsidyDeptDiv1">
                                            <div class="form-group">
                                                <label for="subsidyDept" id="lblSubsidyDept">Subsidy Dept</label>
                                                <select name="subsidyDept1" id="subsidyDept1" class="form-control" required onchange="FetchSubsidyRateByDept()">
                                                    <option value="0">Dealer Price</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="discountPrice">Unit Rate</label>
                                                <input type="hidden" name="basePrice1" id="basePrice1">
                                                <input type="hidden" name="bulkQty1" id="bulkQty1">
                                                <input type="hidden" name="bulkDiscount1" id="bulkDiscount1">
                                                <input type="hidden" name="priceRate1" id="priceRate" value="1">
                                                <input type="text" name="discountPrice1"  style="background-color: #fff;" id="discountPrice1" class="form-control" placeholder="" required readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Sku" id="lblSku">Model No</label>
                                                <div id="SkuNames">
                                                    <select name="Sku1" id="Sku1" class="form-control" onchange="FetchPriceBySku(this.value);" required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="qty">Qty</label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button onclick="decQty()" class="btn btn-sm btn-default" type="button"><i class="material-icons">remove</i></button>
                                                    </span>
                                                    <input type="text" style="padding: 5px;
                                                    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                                                    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                                                    box-sizing: border-box;" name="qty1" id="qty1" onkeyup="qtyKeyup()" class="m-l-5 allownumericwithoutdecimal form-control" placeholder="1" required/>
                                                    <span class="input-group-btn">
                                                        <button onclick="incQty()" class="btn btn-sm btn-default" type="button"><i class="material-icons">add</i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="disPerc">Disc % Req</label>
                                            <input oninput="CalDiscountPrice()" type="number" step="any" name="disPerc1" id="disPerc1" class="allownumericwithdecimal form-control" placeholder="0"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <label for="disVal">Rate Req</label>
                                            <input oninput="CalDiscountPerc()" type="number" name="disVal1" step="any" id="disVal1" class="allownumericwithdecimal form-control" placeholder="0"/>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="finalPrice">Final Rate</label>
                                                <input type="text" name="finalPrice1"  min="1" required style="background-color: #fff;" id="finalPrice1" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-t-10 m-b-20">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <button type="submit" style="border-radius: 20px" name="orderSubmit" value="addToCart" class="btn bg-blue btn-block pull-right">
                                                    Add to cart
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <form class="col-sm-6" method="GET" action="{{route('order.checkout')}}">
                                                    @csrf
                                                        <button  type="submit" name="orderSubmit" value="checkoutCart"  style="border-radius: 20px;color: white" class="btn btn-block bg-blue pull-right">
                                                            Submit
                                                            <span class="badge bg-white col-black">
                                                            @if(isset($cartCount))
                                                                {{ $cartCount }}
                                                            @else
                                                                {{0}}
                                                            @endif
                                                        </span>
                                                        </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <form class="pull-right col-sm-6" method="POST" action="{{route('order.clearCart')}}">
                                                  @csrf
                                                      <button type="submit" name="orderSubmit"  style="background-color: #6EBF1C;color: white; border-radius: 20px" value="clearCart" class="btn waves-effect">
                                                            Clear Cart
                                                      </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                        </div>  
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="accessory">
                                <div class="card p-l-20 p-t-20" style="background: #fff">
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group" id="">
                                                <label for="Group" id="lblGrp">Group</label>
                                                <div id="GrpNames">
                                                    <select name="Group2" id="Group2" class="form-control" onchange="FetchSubgrpByGrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Subgroup" id="lblSubgrp">Subgroup</label>
                                                <div id="SubgrpNames">
                                                    <select name="Subgroup2" id="Subgroup2" class="form-control" onchange="FetchSkuBySubgrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="hsnCode">HSN Code</label>
                                                <input type="text" name="hsnCode2" style="background-color: #fff;" id="hsnCode2" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label for="taxPerc">GST %</label>
                                                <input type="text" name="taxPerc2" style="background-color: #fff;" id="taxPerc2" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="priceLevel" id="lblPriceLevel">Rate Type</label>
                                                <select name="priceLevel2" id="priceLevel2" class="form-control" required onchange="FetchPriceByPriceType()">
                                                    <option value="0">Dealer Price</option>
                                                    <option value="1">MRP Price</option>
                                                    <!-- <option value="2">Subsidy</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2" id="subsidyDeptDiv2">
                                            <div class="form-group">
                                                <label for="subsidyDept" id="lblSubsidyDept">Subsidy Dept</label>
                                                <select name="subsidyDept2" id="subsidyDept2" class="form-control" required onchange="FetchSubsidyRateByDept()">
                                                    <option value="0">Dealer Price</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="discountPrice">Unit Rate</label>
                                                <input type="hidden" name="basePrice2" id="basePrice2">
                                                <input type="hidden" name="bulkQty2" id="bulkQty2">
                                                <input type="hidden" name="bulkDiscount2" id="bulkDiscount2">
                                                <input type="hidden" name="priceRate2" id="priceRate2" value="1">
                                                <input type="text" name="discountPrice2"  style="background-color: #fff;" id="discountPrice2" class="form-control" placeholder="" required readonly/>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Sku" id="lblSku">Model No</label>
                                                <div id="SkuNames">
                                                    <select name="Sku2" id="Sku2" class="form-control" onchange="FetchPriceBySku(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="qty">Qty</label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button onclick="decQty()" class="btn" style="background-color: #6EBF1C" type="button"><i class="material-icons">remove</i></button>
                                                    </span>
                                                    <input type="text" style="padding: 5px;
                                                    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                                                    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                                                    box-sizing: border-box;" name="qty2" id="qty2" onkeyup="qtyKeyup()" class="m-l-5 allownumericwithoutdecimal form-control" placeholder="1" />
                                                    <span class="input-group-btn">
                                                        <button onclick="incQty()" class="btn" style="background-color: #6EBF1C" type="button"><i class="material-icons">add</i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <label for="disPerc">Disc % Req</label>
                                            <input oninput="CalDiscountPrice()" type="number" step="any" name="disPerc2" id="disPerc2" class="allownumericwithdecimal form-control" placeholder="0"  />
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <label for="disVal">Rate Req</label>
                                            <input oninput="CalDiscountPerc()" type="number" name="disVal2" step="any" id="disVal2" class="allownumericwithdecimal form-control" placeholder="0"  />
                                        </div>
                                        
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label for="finalPrice">Final Rate</label>
                                                <input type="text" name="finalPrice2" style="background-color: #fff;" id="finalPrice2" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="p-t-10 m-b-20">
                                         <div class="col-sm-2">
                                            <div class="form-group">
                                                <button type="submit" style="border-radius: 20px" name="orderAccSubmit" value="addToCart" class="btn bg-blue btn-block pull-right">
                                                    Add to cart
                                                </button>
                                            </div>
                                         </div>
                                         <div class="col-sm-3">
                                            <div class="form-group">
                                                    <button  type="submit" name="orderAccSubmit" value="checkoutCart" style="color: white; border-radius: 20px" class="btn bg-blue">Submit
                                                        <span class="badge bg-white col-black">
                                                            @if(isset($cartCount))
                                                                {{ $cartCount }}
                                                            @else
                                                                {{0}}
                                                            @endif
                                                        </span>
                                                    </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <form class="pull-right col-sm-6" method="POST" action="{{route('order.clearCart')}}">
                                                  @csrf
                                                  <button type="submit" name="orderAccSubmit"  style="background-color: #6EBF1C;color: white; border-radius: 20px" value="clearCart" class="btn waves-effect">      Clear Cart
                                                  </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                        </div>  
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="parts">
                                <div class="card p-l-20 p-t-20" style="background: #fff">
                                    <div class="row clearfix">
                                        
                                        <div class="col-sm-2">
                                            <div class="form-group" id="">
                                                <label for="Group" id="lblGrp">Category</label>
                                                <div id="GrpNames">
                                                    <select name="type" id="type" class="form-control" onchange="setClassification(3);" >
                                                        <option value="0">Machinery</option>
                                                        <option value="1">Accessories</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group" id="">
                                                <label for="Group" id="lblGrp">Group</label>
                                                <div id="GrpNames">
                                                    <select name="Group3" id="Group3" class="form-control" onchange="FetchSubgrpByGrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Subgroup" id="lblSubgrp">Subgroup</label>
                                                <div id="SubgrpNames">
                                                    <select name="Subgroup3" id="Subgroup3" class="form-control" onchange="FetchSkuBySubgrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="Sku" id="lblSku">Model No</label>
                                                <div id="SkuNames">
                                                    <select name="Sku3" id="Sku3" class="form-control" onchange="FetchPartSkuBySubgrp(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="Part" id="lblSku">Part</label>
                                                <div id="PartName">
                                                    <select name="partSku3" id="partSku3" class="form-control" onchange="FetchPriceBySku(this.value);" >
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label for="taxPerc">GST %</label>
                                                <input type="text" name="taxPerc3" style="background-color: #fff;" id="taxPerc3" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="priceLevel" id="lblPriceLevel">Rate Type</label>
                                                <select name="priceLevel3" id="priceLevel3" class="form-control" required onchange="FetchPriceByPriceType()">
                                                    <option value="0">Dealer Price</option>
                                                    <option value="1">MRP Price</option>
                                                    <!-- <option value="2">Subsidy</option> -->
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="discountPrice">Unit Rate</label>
                                                <input type="hidden" name="basePrice3" id="basePrice3">
                                                <input type="hidden" name="bulkQty3" id="bulkQty3">
                                                <input type="hidden" name="bulkDiscount3" id="bulkDiscount3">
                                                <input type="hidden" name="priceRate3" id="priceRate3" value="1">
                                                <input type="text" name="discountPrice3"  style="background-color: #fff;" id="discountPrice3" class="form-control" placeholder="" required readonly/>
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="qty">Qty</label>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <button onclick="decQty()" class="btn" style="background-color: #6EBF1C" type="button"><i class="material-icons">remove</i></button>
                                                    </span>
                                                    <input type="text" style="padding: 5px;
                                                    -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                                                    -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                                                    box-sizing: border-box;" name="qty3" id="qty3" onkeyup="qtyKeyup()" class="m-l-5 allownumericwithoutdecimal form-control" placeholder="1" />
                                                    <span class="input-group-btn">
                                                        <button onclick="incQty()" class="btn" style="background-color: #6EBF1C" type="button"><i class="material-icons">add</i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-2">
                                            <label for="disPerc">Disc % Req</label>
                                            <input oninput="CalDiscountPrice()" type="number" step="any" name="disPerc3" id="disPerc3" class="allownumericwithdecimal form-control" placeholder="0"  />
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <label for="disVal">Rate Req</label>
                                            <input oninput="CalDiscountPerc()" type="number" name="disVal3" step="any" id="disVal3" class="allownumericwithdecimal form-control" placeholder="0"  />
                                        </div>
                                        
                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label for="finalPrice">Final Rate</label>
                                                <input type="text" name="finalPrice3" style="background-color: #fff;" id="finalPrice3" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="p-t-10 m-b-20">
                                         <div class="col-sm-2">
                                            <div class="form-group">
                                                <button type="submit" style="border-radius: 20px" name="orderPartSubmit" value="addToCart" class="btn bg-blue btn-block pull-right">
                                                    Add to cart
                                                </button>
                                            </div>
                                         </div>
                                         <div class="col-sm-3">
                                            <div class="form-group">
                                                    <button  type="submit" name="orderPartSubmit" value="checkoutCart" style="color: white; border-radius: 20px" class="btn bg-blue">Submit
                                                        <span class="badge bg-white col-black">
                                                            @if(isset($cartCount))
                                                                {{ $cartCount }}
                                                            @else
                                                                {{0}}
                                                            @endif
                                                        </span>
                                                    </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <form class="pull-right col-sm-6" method="POST" action="{{route('order.clearCart')}}">
                                                  @csrf
                                                  <button type="submit" name="orderPartSubmit"  style="background-color: #6EBF1C;color: white; border-radius: 20px" value="clearCart" class="btn waves-effect">      Clear Cart
                                                  </button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                        </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

        <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
        <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>

        <script type="text/javascript">

        var branchPriceRate = 1;
        var bulkQty = 0;
        var bulkDiscount = 0;
        var isBulkPurch = false;
        var isBulkDisGiven = false;
        var bulkDisPer = 0;
        var classification = 1;
        var branchPrice = 0;

        $(document).ready(function() {
            var defaultLoad="{!! session()->get('classification') !!}";
            if(defaultLoad!="NF"){
                switch(defaultLoad){
                    case "1":
                        break;

                    case "2":
                        $('.nav-tabs a[href="#accessory"]').tab('show');
                        setClassification(2);
                        break;

                    case "3":
                        $('.nav-tabs a[href="#parts"]').tab('show');
                        setClassification(3);
                        break;

                    default:
                        break
                }
            }
        });



        function validateAndSend() {
            // alert('hi tehre');
            // // if($('#finalPrice'+classification).val()=="NaN" || $('#finalPrice'+classification).val()<1)
            // //     alert('Please enter correct input');
            // // else
            // $('#orderForm').submit();
        }

       

        function setClassification(category) {
            classification = category;
            var type=$('#type').val();
            $('#classification').val(category);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchGroup",
                type: 'POST',
                data: {
                    'category': category,
                    'type':type
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                    $('#Group' + category).empty();
                    $.each(jsonData, function(i, item) {
                        $('#Group' + category).append($('<option>', {
                            value: item,
                            text: item
                        }));
                    });
                    $('#Group' + category).trigger('change');
                    if(category==1)
                        setMachinery();
                    else if(category==2)
                        setAccessory();
                    else if(category==3)
                        setParts();

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function setAccessory()
        {
            $("#Group2").prop('required',true);
            $("#Subgroup2").prop('required',true);
            $("#Sku2").prop('required',true);
            $("#qty2").prop('required',true);
            $("disPerc2").prop('required',true);
            $("disVal2").prop('required',true);

            $("#qty1").prop('required',false);
            $("#Group1").prop('required',false);
            $("#Subgroup1").prop('required',false);
            $("#Sku1").prop('required',false);
            $("disPerc1").prop('required',false);
            $("disVal1").prop('required',false);

            $("#qty3").prop('required',false);
            $("#Group3").prop('required',false);
            $("#Subgroup3").prop('required',false);
            $("#Sku3").prop('required',false);
            $("disPerc3").prop('required',false);
            $("disVal3").prop('required',false);
        }

        function setMachinery()
        {

            $("#qty1").prop('required',true);
            $("#Group1").prop('required',true);
            $("#Subgroup1").prop('required',true);
            $("#Sku1").prop('required',true);
            $("disPerc1").prop('required',true);
            $("disVal1").prop('required',true);

            $("#Group2").prop('required',false);
            $("#Subgroup2").prop('required',false);
            $("#Sku2").prop('required',false);
            $("#qty2").prop('required',false);
            $("disPerc2").prop('required',false);
            $("disVal2").prop('required',false);

            $("#Group3").prop('required',false);
            $("#Subgroup3").prop('required',false);
            $("#Sku3").prop('required',false);
            $("#qty3").prop('required',false);
            $("disPerc3").prop('required',false);
            $("disVal3").prop('required',false);
        }

        function setParts()
        {
            $("#qty3").prop('required',true);
            $("#Group3").prop('required',true);
            $("#Subgroup3").prop('required',true);
            $("#Sku3").prop('required',true);
            $("disPerc3").prop('required',true);
            $("disVal3").prop('required',true);

            $("#Group2").prop('required',false);
            $("#Subgroup2").prop('required',false);
            $("#Sku2").prop('required',false);
            $("#qty2").prop('required',false);
            $("disPerc2").prop('required',false);
            $("disVal2").prop('required',false);

            $("#qty1").prop('required',false);
            $("#Group1").prop('required',false);
            $("#Subgroup1").prop('required',false);
            $("#Sku1").prop('required',false);
            $("disPerc1").prop('required',false);
            $("disVal1").prop('required',false);
        }

        function SetItemPrice(isDisPercSet) {
            // $('#disPerc').trigger('change');
            //alert("Need to set finalPrice");
            var disPerc = 0;
            var disVal = 0;
            
            if($('#priceLevel'+classification).val()==1 || $('#priceLevel'+classification).val()==2)
                branchPriceRate = 1.0;
            branchPrice = branchPriceRate * $('#basePrice' + classification).val();
            if (isDisPercSet) {
                disVal = branchPrice - $('#disPerc' + classification).val() / 100 * branchPrice;
                $('#disVal' + classification).val(parseFloat(disVal).toFixed(2));
            } else {
                disVal = $('#disVal' + classification).val();
                disPerc = (branchPrice - disVal) * 100 / branchPrice;
                $('#disPerc' + classification).val(parseFloat(disPerc).toFixed(2));
            }
            $('#discountPrice' + classification).val(parseFloat(branchPrice).toFixed(2));
            var disPrice = parseFloat(disVal).toFixed(2);
            var taxPer = parseFloat($('#taxPerc' + classification).val());
            var qty = parseFloat($('#qty' + classification).val());
            if($('#priceLevel'+classification).val()==1 || $('#priceLevel'+classification).val()==2)
                $('#finalPrice' + classification).val(parseFloat(disPrice * qty).toFixed(2));
            else
                $('#finalPrice' + classification).val(parseFloat(disPrice * qty + disPrice * qty * taxPer / 100).toFixed(2));
        }

        function CalDiscountPrice() 
        {
            var isDisPercSet = true;
            SetItemPrice(isDisPercSet);
        }

        function CalDiscountPerc() {
            var isDisPercSet = false;
            SetItemPrice(isDisPercSet);
        }

        function GetPriceByBranch() {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchPriceRateByBranch",
                type: 'POST',
                data: {
                    'branchId': $('#dispatchBranch').val()
                },
                success: function(jsonData) {
                    $('#Sku'+classification).trigger('change');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function decQty() {
            if ($('#qty' + classification).val() >= 1)
                $('#qty' + classification).val(parseFloat($('#qty' + classification).val()) - 1);
            else
                $('#qty' + classification).val(1);
            SetItemPrice();
        }

        function incQty() {
            if ($('#qty' + classification).val() >= 0)
                $('#qty' + classification).val(parseFloat($('#qty' + classification).val()) + 1);
            else
                $('#qty' + classification).val(1);
            SetItemPrice();
        }

        function qtyKeyup()
        {
                currentQty = $('#qty'+classification).val();
                SetItemPrice();
        }

        $(document).ready(function() {
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchDealersByState",
                type: 'POST',
                data: {
                    'stateId': $('#state').val()
                },
                success: function(jsonData) {
                    // alert(jsonData['dealerId']);
                    $('#m_dealer').empty();
                    $.each(jsonData['dealers'], function(i, item) {
                        $('#m_dealer').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    });
                    if(jsonData['dealerId'] != 0)
                    {
                        $('#m_dealer').val(jsonData['dealerId']);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchGroup",
                type: 'POST',
                data: {
                    'category': 1
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                    $('#Group' + classification).empty();
                    $.each(jsonData, function(i, item) {
                        $('#Group' + classification).append($('<option>', {
                            value: item,
                            text: item
                        }));
                    });
                    $('#Group' + classification).trigger('change');
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });

            $('#dispatchBranch').change(function() {
                GetPriceByBranch();
            });

            $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
                var that = this;
                //paste event
                if (event.type === "paste") {
                    setTimeout(function() {
                        $(that).val($(that).val().replace(/[^\d].+/, ""));
                    }, 100);
                } else {
                    if (event.which < 48 || event.which > 57) {
                        event.preventDefault();
                    } else {
                        $(this).val($(this).val().replace(/[^\d].+/, ""));
                    }
                }
            });

            

        })

        function FetchDealersByState(){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchDealersByState",
                type: 'POST',
                data: {
                    'stateId': $('#state').val()
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                    $('#m_dealer').empty();
                    $.each(jsonData['dealers'], function(i, item) {
                        $('#m_dealer').append($('<option>', {
                            value: item.id,
                            text: item.name
                        }));
                    });
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        //Subgroup List
        function FetchSubgrpByGrp(grp) {
            var type=$('#type').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchSubgrpByGrp",
                type: 'POST',
                data: {
                    'category': classification,
                    'grp': grp,
                    'type':type
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                    $('#Subgroup' + classification).empty();
                    $.each(jsonData, function(i, item) {
                        $('#Subgroup' + classification).append($('<option>', {
                            value: item,
                            text: item
                        }));
                    });
                    $('#Subgroup' + classification).trigger('change');
                    $('#priceLevel'+classification).val(0);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        //Sku List
        function FetchSkuBySubgrp(subgrp) {
            var type=$('#type').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchSkuBySubgrp",
                type: 'POST',
                data: {
                    'category': classification,
                    'subgrp': subgrp,
                    'grp': $('#Group' + classification).val(),
                    'type':type
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                    $('#Sku' + classification).empty();
                    $.each(jsonData, function(i, item) {
                        $('#Sku' + classification).append($('<option>', {
                            value: item,
                            text: item
                        }));
                    });
                    $('#Sku' + classification).trigger('change');
                    $('#priceLevel'+classification).val(0);
                    
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }


        function FetchPartSkuBySubgrp(subgrp) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchPartBySubgrp",
                type: 'POST',
                data: {
                    'sku':$('#Sku3').val(),
                    'type':$('#type').val()
                },
                success: function(jsonData) {
                    //alert(JSON.stringify(jsonData));
                    $('#partSku' + classification).empty();
                    $.each(jsonData, function(i, item) {
                        $('#partSku' + classification).append($('<option>', {
                            value: item['id'],
                            text: item['sku']
                        }));
                    });
                    $('#partSku' + classification).trigger('change');
                    $('#priceLevel'+classification).val(0);
                    
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function FetchPriceByPriceType() {
            $('#Sku'+classification).trigger('change');
        }
        
        //Price Info
        function FetchPriceBySku(sku) {

            var selected_dealer = 0;
                            if($('#dealer').val() == null)
                            {
                                selected_dealer = $('#m_dealer').val();
                            }
                            else
                            {
                                selected_dealer = $('#dealer').val();
                            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchPriceBySku",
                type: 'POST',
                data: {
                    'category': classification,
                    'sku': sku,
                    'dealer': selected_dealer
                },
                success: function(jsonData) {
                     //alert(JSON.stringify(jsonData));
                    switch($('#priceLevel'+classification).val())
                    {
                        case '0':
                            if(jsonData['subsidy_applicable']==1)
                            {
                                $('#priceLevel'+classification).val(2);
                                $('#Sku'+classification).trigger('change');
                            }
                            else
                            {
                                $('#subsidyDeptDiv'+classification).css('visibility','hidden');
                                var branchRatePer = 1.0;

                                if(jsonData['branchRatePer'] != null)
                                    branchRatePer = jsonData['branchRatePer'];
                                $('#basePrice' + classification).val(parseFloat(jsonData['itemInfo'][0].dp*branchRatePer).toFixed(2));
                                $('#discountPrice' + classification).val(parseFloat(jsonData['itemInfo'][0].dp*branchRatePer).toFixed(2));
                                $('#disVal' + classification).val(parseFloat(jsonData['itemInfo'][0].dp*branchRatePer).toFixed(2));
                                $('#disPerc'+classification).prop('readonly', false);
                                $('#disVal'+classification).prop('readonly', false);
                            }
                            break;

                        case '1':
                            $('#subsidyDeptDiv'+classification).css('visibility','hidden');
                            $('#basePrice' + classification).val(jsonData['itemInfo'][0].mrp);
                            $('#discountPrice' + classification).val(jsonData['itemInfo'][0].mrp);
                            $('#disPerc'+classification).val(0);
                            $('#disVal'+classification).val(jsonData['itemInfo'][0].mrp);
                            $('#disPerc'+classification).prop('readonly', true);
                            $('#disVal'+classification).prop('readonly', true);
                            break;

                        case '2':
                            var selected_dealer = 0;
                            if($('#dealer').val() == null)
                            {
                                selected_dealer = $('#m_dealer').val();
                            }
                            else
                            {
                                selected_dealer = $('#dealer').val();
                            }
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url: "/FetchSubsidyRateByDealer",
                                type: 'POST',
                                data: {
                                    'category': classification,
                                    'mch_id': jsonData['itemInfo'][0].id,
                                    'dealer': selected_dealer
                                },
                                success: function(jsonData) {
                                    // alert(JSON.stringify(jsonData));
                                    $('#basePrice' + classification).val(jsonData['itemPrice']);
                                    $('#discountPrice' + classification).val(jsonData['itemPrice']);
                                    $('#disVal' + classification).val(jsonData['itemPrice']);
                                    $('#disPerc'+classification).val(0);
                                    $('#disVal'+classification).val(jsonData['itemPrice']);
                                    $('#disPerc'+classification).prop('readonly', true);
                                    $('#disVal'+classification).prop('readonly', true);
                                    if(jsonData['itemPrice'] == '-1')
                                    {
                                        alert('Selected item not approved for subsidy');
                                        $('#priceLevel'+classification).val(1);
                                        $('#Sku'+classification).trigger('change');
                                    }
                                    else
                                    {
                                        $('#subsidyDeptDiv'+classification).css('visibility','visible');
                                        $('#subsidyDept' + classification).empty();
                                        $.each(jsonData['itemData'], function(i, item) {
                                            $('#subsidyDept' + classification).append($('<option>', {
                                                value: item,
                                                text: item
                                            }));
                                        });
                                        $('#subsidyDept' + classification).val(jsonData['subsidy_dept']);
                                    }
                                    SetItemPrice();
                                },
                                error: function(xhr, ajaxOptions, thrownError) 
                                {
                                    alert(xhr.status);
                                    alert(thrownError);
                                }
                            });
                            break;
                    }
                    $('#taxPerc' + classification).val(jsonData['itemInfo'][0].gst);
                    $('#hsnCode' + classification).val(jsonData['itemInfo'][0].hsn);
                    $('#qty' + classification).val(1);
                    SetItemPrice();
                },
                error: function(xhr, ajaxOptions, thrownError) 
                {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        //Fetch Price 
        function FetchSubsidyRateByDept()
        {
            var selected_dealer = 0;
                            if($('#dealer').val() == null)
                            {
                                selected_dealer = $('#m_dealer').val();
                            }
                            else
                            {
                                selected_dealer = $('#dealer').val();
                            }

             $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/FetchSubsidyRateByDept",
                type: 'POST',
                data: {
                    'category': classification,
                    'sku': $('#Sku'+classification).val(),
                    'subsidy_dept': $('#subsidyDept'+classification).val(),
                    'dealer': selected_dealer
                },
                success: function(jsonData) {
                    // alert(JSON.stringify(jsonData));
                                    if(jsonData == '-1')
                                    {
                                        alert('Subsidy rate not available for the particular department');
                                        $('#priceLevel'+classification).val(2);
                                        $('#Sku'+classification).trigger('change');
                                    }
                                    else
                                    {
                                        $('#basePrice' + classification).val(jsonData);
                                        $('#discountPrice' + classification).val(jsonData);
                                        $('#disVal' + classification).val(jsonData);
                                        $('#disPerc'+classification).val(0);
                                        $('#disVal'+classification).val(jsonData);
                                        $('#disPerc'+classification).prop('readonly', true);
                                        $('#disVal'+classification).prop('readonly', true);
                                    }
                                    SetItemPrice();
                },
                error: function(xhr, ajaxOptions, thrownError) 
                {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

                    </script>
                    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
                    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
                    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
                    <script>
                    // Retrieve an instance of Firebase Messaging so that it can handle background
                    // messages.
                    var config = {
                        apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",
                        authDomain: "krishi-6d123.firebaseapp.com",
                        databaseURL: "https://krishi-6d123.firebaseio.com",
                        projectId: "krishi-6d123",
                        storageBucket: "krishi-6d123.appspot.com",
                        messagingSenderId: "988261907749"
                    };
                    firebase.initializeApp(config);
                    const messaging = firebase.messaging();
                    messaging.usePublicVapidKey('BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY');
                    messaging.requestPermission().then(function() {
                        console.log('Notification permission granted.');
                        var user_email = "null";
                        //Get current user email id
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "/getCurrentUserEmail",
                            type: 'POST',
                            data: {
                                'data': "null"
                            },
                            success: function(jsonData) {
                                messaging.getToken().then(function(currentToken) {
                                    if (currentToken) {
                                        sendTokenToServer(jsonData, currentToken);
                                        // updateUIForPushEnabled(currentToken);
                                    } else {
                                        // Show permission request.
                                        console.log('No Instance ID token available. Request permission to generate one.');
                                        // Show permission UI.
                                        updateUIForPushPermissionRequired();
                                        setTokenSentToServer(false);
                                    }
                                }).catch(function(err) {
                                    console.log('An error occurred while retrieving token. ', err);
                                    showToken('Error retrieving Instance ID token. ', err);
                                    setTokenSentToServer(false);
                                });
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        });
                        // TODO(developer): Retrieve an Instance ID token for use with FCM.
                        // alert("permission granted");
                    }).catch(function(err) {
                        console.log('Unable to get permission to notify.', err);
                    });
                    
                    messaging.onMessage(function(payload) {
                        console.log('Message received in home. ', payload['data']);
                        // window.location.reload();
                    });
                    
                    $(document).ready(function() {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "/GetNotificationCount",
                            type: 'POST',
                            data: {
                                'data': "null"
                            },
                            success: function(jsonData) {
                                $('#notify_count').text(jsonData);
                            },
                            error: function(xhr, ajaxOptions, thrownError) {

                            }
                        });
                    })

                    function sendTokenToServer(email, currentToken) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "/sendTokenToServer",
                            type: 'POST',
                            data: {
                                'email': email,
                                'token': currentToken
                            },
                            success: function(jsonData) {
                                // alert(jsonData);
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                            }
                        });
                    }

                    </script>