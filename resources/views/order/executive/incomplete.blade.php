@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Incomplete Orders 
                            </h2>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Dealer Name</th>
                                            <th>Order Date</th>
                                            <th>Total Items</th>
                                            <th width="40">View</th>
                                        </tr>
                                    </thead>
                                  	<tbody>
                                  		@foreach($cartItems as $order)
                                  		<tr>
                                  			<td>{{$order->dealer->name}}</td>
                                  			<td>{{$order->order_date}}</td>
                                  			<td>{{$order->items}}</td>
                                  			<td width="300">
                                              <form action="/ViewCheckoutPage" method="post">
                                                  @csrf
                                                  <input name="dealer_id" type="hidden" value="{{$order->dealer_id}}">
                                                  <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                              </form>
                                        </td>
                                  		</tr>
                                  		@endforeach
                                  	</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
