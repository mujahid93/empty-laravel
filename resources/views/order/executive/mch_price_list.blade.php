@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            Machinery
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                         <div class="body">
                            <div class="row">
                                    <div class="col-sm-3">
                                        <form id="branchRatesForm" action="{{route('order.mch_price_list')}}" method="GET">
                                                <div class="form-group">
                                                    <label for="dispatchBranch" id="lblBranch">Dispatch Branch</label>
                                                    <?php $branchRate = 1.0; ?>
                                                    <select name="dispatchBranch" id="dispatchBranch" class="form-control" onchange="filterResult()">
                                                            @foreach($branches as $branch)
                                                                    @if($branch->id == session()->get('mch_branch_id'))
                                                                        <?php $branchRate = $branch->rate; ?>
                                                                        <option value='{{$branch->id}}' selected>{{$branch->name}}</option>
                                                                    @else
                                                                        <option value='{{$branch->id}}'>{{$branch->name}}</option>
                                                                    @endif
                                                            @endforeach
                                                    </select>
                                                </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group form-float">
                                                <label for="dispatchBranch">Group</label>
                                                <select name="_grp_search" id="_grp_search" class="form-control" onchange="filterResult()">
                                                        @foreach($groups as $grp)
                                                                    @if(session()->get('mch_grp_name')==$grp)
                                                                    <option selected>{{$grp}}</option>
                                                                    @else
                                                                    <option>{{$grp}}</option>
                                                                    @endif
                                                        @endforeach

                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                         <div class="form-group form-float">
                                                <label for="dispatchBranch" >Subgroup</label>
                                                <select name="" id="subgrp2" class="form-control" onchange="filterResult()">
                                                    @foreach($subgrp as $grp)
                                                                    @if(session()->get('mch_subgrp_name')==$grp)
                                                                    <option selected>{{$grp}}</option>
                                                                    @else
                                                                    <option>{{$grp}}</option>
                                                                    @endif
                                                        @endforeach
                                                </select>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Group</th>
                                            <th>Subgroup</th>
                                            <th>Sku</th>
                                            <th>DP</th>
                                            <th>MRP</th>
                                            <th>SUBSIDY</th>
                                            <th>HSN</th>
                                            <th>GST</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->grp}}</td>
                                            <td>{{$item->subgrp}}</td>
                                            <td>{{$item->sku}}</td>
                                            <td align="right">&#8377; {{ number_format($item->dp * $branchRate, 0, '.', ',') }}</td>
                                            <td align="right">&#8377;  {{ number_format($item->mrp, 0, '.', ',') }}</td>
                                            <td align="right">
                                                @if($item->subsidy_rate != NULL)
                                                    @if($item->subsidy_rate->branch_id ==  session()->get('mch_branch_id'))
                                                        &#8377; {{$item->subsidy_rate->subsidy_price}}
                                                    @else
                                                        {{"NA"}}
                                                    @endif
                                                @else
                                                    {{"NA"}}
                                                @endif
                                            </td>
                                            <td>{{$item->hsn}}</td>
                                            <td>{{number_format($item->gst, 0)}}%</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$items->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')

@section('scripts')
<script type="text/javascript">
    
        
        
    function filterResult(){
        var branch=$("#dispatchBranch option:selected").val();
        var group=document.getElementById("_grp_search").value;
        var subgroup=document.getElementById("subgrp2").value;

        $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/FetchMchPriceList",
                    type: 'GET',
                    data: {
                        'category': 1,
                        'branch_id':branch,
                        'grp': group,
                        'subgrp':subgroup
                    },
                    success: function(jsonData) {
                        //alert(JSON.stringify(jsonData));
                        /*$("#_table tbody tr").remove();
                           for(var i=0;i<jsonData.items.data.length;i++)
                           {
                                var orderStatus;
                                var row = $("<tr><td>"+jsonData.items.data[i]['grp']+"</td><td>"+jsonData.items.data[i]['subgrp']+"</td><td>"+jsonData.items.data[i]['sku']+"</td><td>"+jsonData.items.data[i]['dp'] * jsonData.sel_branch+"</td><td>"+jsonData.items.data[i]['mrp']+"</td><td>"+jsonData.items.data[i]['hsn']+"</td><td>"+jsonData.items.data[i]['gst']+"</td><tr>");
                                $("#_table").append(row); 
                           }*/
                           window.location.reload();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });


    }

    function GetPriceByBranch(priceRate)
    {
        $('#branchRatesForm').submit(); 
    }

    
</script>
@endSection