@extends('layouts.header')

<meta name="csrf-token" content="{{ csrf_token() }}">

@extends('layouts.content')

@section('content')



<div class="block-header row  clearfix">

            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">



                    <div class="card">

                        <div class="header">
                            <h2>
                                Pending Orders
                            </h2>
                        </div>

                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif

                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>

                                        <tr>

                                            <th>Tracking No</th>

                                            <th>Dealer Name</th>

                                            <th>Placed By</th>

                                            <th>Order Date</th>

                                            <th width="40">View/Edit</th>

                                            <th width="40">Delete</th>
                                        </tr>

                                    </thead>

                                    <tbody>

                                        @foreach($orders as $order)
                                                <tr>
                                                    <td width="300">{{$order->tracking_no}}</td>
                                                    <td width="300">{{$order->dealer->name}}</td>
                                                    <td width="300">
                                                        @if($order->placedBy)
                                                            {{$order->placedBy->user->name}}
                                                        @else
                                                            {{$order->dealer->name}}
                                                        @endif
                                                    </td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                    <td width="300">
                                                        <form action="/DeleteOrder" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-maroon material-icons">delete</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        @endforeach

                                    </tbody>

                                </table>

                                {{ $orders->links() }}   

                            </div>

                        </div>

                    </div>

                </div>

            </div>            

@endsection

@extends('layouts.footer')

