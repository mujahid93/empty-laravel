<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
  </head>
  @extends('layouts.content')
  @section('css_links')
  <link  href="{{ asset("/css/checkout.css") }}" rel="stylesheet" />
  @endsection
  @section('content')
  <body>
    <div class="block-header">
      <!-- <h2 id="temp">Order </h2> -->
    </div>
    <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card" >
          
          <div class="header">
            <h2>
              @if(sizeof($cart)>0)
               <b>Dealer</b>&nbsp &nbsp &nbsp{{$cart[0]->dealer->name}}
              @endif
            </h2>
          </div>
          @if(session()->has('result'))
          <div class="alert bg-green alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('result') }}
          </div>
          @endif
          <form action='/ConfirmOrder' method="post">
            @csrf
            <div class="body">
              <?php $cartItems = array() ; ?>
              @foreach($branches as $branch)
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                    <div class="header">
                                            @switch($branch)
                                                @case(1)
                                                    <span>BANGALORE(KA)</span>
                                                    @break

                                                @case(2)
                                                    <span>DODDABALLAPUR(KA)</span>
                                                    @break

                                                @case(3)
                                                    <span>PUNE(MH)</span>
                                                    @break    

                                                @case(4)
                                                    <span>BHUBANESWAR(OR)</span>
                                                    @break 

                                                @case(5)
                                                    <span>COIMBATORE(TN)</span>
                                                    @break 

                                                @case(6)
                                                    <span>RAIPUR(CG)</span>
                                                    @break

                                                @case(7)
                                                    <span>JAIPUR(RJ)</span>
                                                    @break
                                                @case(8)
                                                    <span>KARNAL(HR)</span>
                                                    @break 
                                                @case(9)
                                                    <span>GUWAHATI(AS)</span>
                                                    @break 
                                                @case(10)
                                                    <span>LUCKNOW(UP)</span>
                                                    @break 
                                                @case(11)
                                                    <span>HOWRAH(WB)</span>
                                                    @break 
                                                @case(12)
                                                    <span>AHMEDABAD(GJ)</span>
                                                    @break 
                                                @case(13)
                                                    <span>HYDERABAD(TS)</span>
                                                    @break 
                                                @case(14)
                                                    <span>HINDUPUR(AP)</span>
                                                    @break  
                                                @case(15)
                                                    <span>HUBLI(KA)</span>
                                                    @break  
                                                @case(16)
                                                    <span>BHOPAL</span>
                                                    @break  
                                                @case(17)
                                                    <span>SHIMLA(HP)</span>
                                                    @break      
                                            @endswitch
                    </div>
                    <div class="body">
                      <table id="_tblCart" class="table table-hover js-basic-example">
                            <thead>
                                <tr>
                                    <th width="200"> SKU</th>
                                    <th> QTY</th>
                                   
                                      <th style="text-align:center;"> SPL. DISCOUNT</th>

                                    <th style="text-align:center;"> DIS % REQ.</th>
                                    <th style="text-align:center"> RATE REQ.</th>
                                    <th> RATE TYPE</th>
                                    <th> FINAL RATE</th>
                                    <th> REMOVE</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($cart as $order)
                                @if($branch != $order['branch'])
                                  @continue
                                @endif
                                <tr>
                                  <td value =""><b><input readonly style="border-width: 0;" name="sku-{{$order->id}}" value="{{ $order->data->sku }}"/></b></td>
                                  <div style="display: none;">Rs <input readonly name="up-{{$order->id}}" id="up-{{$order->id}}" value="{{$order->item_rate}}"/></div>
                                  <td width="300">
                                        <span class="input-group">
                                            <span class="input-group-btn">
                                                <button id="decQty-{{$order->id}}" onclick="DecQty(this.id)" class="btn btn-sm" style="background-color: #6EBF1C" type="button"><i class="material-icons">remove</i></button>
                                            </span>
                                            <input type="text" 
                                            style="background-color: #eee;padding: 5px;
                                            -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
                                            -moz-box-sizing: border-box;    /* Firefox, other Gecko */
                                            box-sizing: border-box; min-width: 50px;"
                                             name="qty-{{$order->id}}" onkeyup="OnQtyChange(this.id)" id="qty-{{$order->id}}" value="{{$order->qty}}" class="m-l-5 allownumericwithoutdecimal form-control" placeholder="1" required/>
                                            <span class="input-group-btn">
                                                <button id="incQty-{{$order->id}}" onclick="IncQty(this.id)" class="btn btn-sm" style="background-color: #6EBF1C" type="button"><i class="material-icons">add</i></button>
                                            </span>
                                        </span>
                                  </td>
                                  <?php 
                                  array_push($cartItems,$order->id); 
                                  $rate_type = '';
                                  switch ($order->rate_type) {
                                    case '0':
                                      $rate_type = "Dealer Price";
                                      ?>

                                      <td align="right"><input readonly  style="width: 130px;" type="number" step="any" name="spl_disc-{{$order->id}}" id="spl_disc-{{$order->id}}" value="{{ $order->spl_disc }}" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>

                                        <td align="right"><input oninput="CalDisPer(this.id)" style="width: 130px;" type="number" step="any" name="disPer-{{$order->id}}" id="disPer-{{$order->id}}" value="{{ $order->dis_per }}" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>
                                        <td align="center"><input oninput="CalDisPrice(this.id)" style="width: 120px;" type="number" step="any" name="disPrice-{{$order->id}}" id="disPrice-{{$order->id}}" value="{{ number_format($order->item_rate - $order->item_rate*$order->dis_per/100, 2, '.', '') }}" class="allownumericwithdecimal form-control" placeholder="0"/></td>
                                        <td><input type="hidden"  id="rateType-{{$order->id}}" name="rateType-{{$order->id}}" value="{{$order->rate_type}}" readonly/>{{$rate_type}}</td>
                                        <td value ="">
                                            @if($order->dis_per > 0)
                                            <input style="border-width: 0;" readonly name="finalPrice-{{$order->id}}" id="finalPrice-{{$order->id}}" value="{{ ( $order->item_rate - $order->item_rate*$order->dis_per/100) * $order->qty   +  ( $order->item_rate - $order->item_rate*$order->dis_per/100) * $order->qty * $order->data->gst/100  }}">
                                            @else
                                             <input style="border-width: 0;" readonly name="finalPrice-{{$order->id}}" id="finalPrice-{{$order->id}}" value="{{ (($order->item_rate) * $order->qty) + (($order->item_rate) * $order->qty *  $order->data->gst/100 )  }}">
                                            @endif
                                          </input>
                                        </td>
                                      <?php
                                      break;
                                    case '1':
                                      $rate_type = "MRP";
                                      ?>

                                      <td align="right"><input readonly  style="width: 130px;" type="number" step="any" name="spl_disc-{{$order->id}}" id="spl_disc-{{$order->id}}" value="{{ $order->spl_disc }}" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>

                                      <td align="right"><input readonly oninput="CalDisPer(this.id)" style="width: 130px;" type="number" step="any" name="disPer-{{$order->id}}" id="disPer-{{$order->id}}" value="{{ $order->dis_per }}" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>
                                        <td align="center"><input readonly oninput="CalDisPrice(this.id)" style="width: 120px;" type="number" step="any" name="disPrice-{{$order->id}}" id="disPrice-{{$order->id}}" value="{{ number_format($order->item_rate, 2, '.', '') }}" class="allownumericwithdecimal form-control" placeholder="0"/></td>
                                        <td><input type="hidden"  id="rateType-{{$order->id}}" name="rateType-{{$order->id}}" value="{{$order->rate_type}}" readonly/> {{$rate_type}}</td>
                                        <td value ="">
                                            <input style="border-width: 0;" readonly name="finalPrice-{{$order->id}}" id="finalPrice-{{$order->id}}" value="{{ (($order->item_rate) * $order->qty)  }}">
                                            </input>
                                        </td>
                                      <?php
                                      break;
                                    case '2':
                                      $rate_type = "Subsidy";
                                      ?>

                                       <td align="right"><input  style="width: 130px;" type="number" step="any" name="spl_disc-{{$order->id}}" id="spl_disc-{{$order->id}}" value="0" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>

                                      <td align="right"><input readonly oninput="CalDisPer(this.id)" style="width: 130px;" type="number" step="any" name="disPer-{{$order->id}}" id="disPer-{{$order->id}}" value="{{ $order->dis_per }}" class="allownumericwithdecimal form-control" placeholder="0"  />  </td>
                                        <td align="center"><input readonly oninput="CalDisPrice(this.id)" style="width: 120px;" type="number" step="any" name="disPrice-{{$order->id}}" id="disPrice-{{$order->id}}" value="{{  number_format($order->item_rate - $order->item_rate*5/100, 2, '.', '')  }}" class="allownumericwithdecimal form-control" placeholder="0"/></td>
                                        <td><input type="hidden" id="rateType-{{$order->id}}" name="rateType-{{$order->id}}" value="{{$order->rate_type}}" readonly/><input type="hidden" name="subsidyDept-{{$order->id}}" value="{{$order->subsidy_dept_id}}" readonly/>{{$rate_type}}</td>
                                        <td value ="">
                                            <input style="border-width: 0;" readonly name="finalPrice-{{$order->id}}" id="finalPrice-{{$order->id}}" value="{{ (($order->item_rate - $order->item_rate*5/100) * $order->qty)  }}">
                                            </input>
                                        </td>
                                      <?php
                                      break;
                                    default:
                                      break;
                                  }
                                ?>
                                  <td>
                                        <input type="hidden" id="gst-{{$order->id}}" value="{{$order->data->gst}}" name="gst-{{$order->id}}">
                                        <input type="hidden" value="{{$order->id}}" name="orderId-{{$order->id}}">
                                        <input type="hidden" value="{{$order->branch}}" name="branchId-{{$order->id}}">
                                        <input type="hidden" value="{{$order->classification}}" name="category-{{$order->id}}">
                                        <input type="hidden" value="{{session()->get('current_dealer')}}" name="dealer">
                                        <button type="button" onclick="RemoveItem(this.value)" name="checkoutSubmit" style="border-radius: 55%;background-color: #eee" value="{{$order->id}}" class="btn btn-sm bg-red">
                                            <i class="col-red material-icons">clear</i>
                                        </button>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                      </table>
                      <div class="card p-t-20 p-l-20" >
                        <div class="row clearfix">
                                  <div class="col-sm-2">
                                    <input  type="text" value="Freight" style="border-width: 0" readonly class="form-control font-bold" required />
                                  </div>
                                  <div class="col-sm-2">
                                    <input  type="text" name="freight_{{$branch}}" id="freight_{{$branch}}" class="form-control" required />
                                  </div>
                                  <div class="col-sm-2">
                                    <input  type="text" value="Destination" style="border-width: 0" readonly class="form-control font-bold" required />
                                  </div>
                                  <div class="col-sm-2">
                                    <select  name="destination_{{$branch}}" id="destination_{{$branch}}" class="form-control">
                                      <option value="{{$destination[0]->address1}}">{{$destination[0]->address1}}</option>  
                                      <option value="{{$destination[0]->address2}}">{{$destination[0]->address2}}</option>  
                                      <option value="{{$destination[0]->address3}}">{{$destination[0]->address3}}</option>  
                                    </select>
                                  </div>
                                  <div class="col-sm-2">
                                    <input type="text" name="new_dest_{{$branch}}" id="new_dest_{{$branch}}" placeholder="New Destination" class="form-control" />
                                  </div>
                        </div>
                        <div class="row clearfix">
                                  <div class="col-sm-2">
                                    <input  type="text" value="Ship Mode" style="border-width: 0" readonly class="form-control font-bold" required />
                                  </div>
                                  <div class="col-sm-3">
                                    <select name="shippingMode_{{$branch}}" id="shippingMode_{{$branch}}" class="form-control" required onchange="GetShippingCompanies(this.id)">
                                      <option style="background: #eee">Select Shipping Mode</option>
                                      <option value="Transport">Transport</option>
                                      <option value="Courier">Courier</option>
                                      <option value="By Hand">By Hand</option>
                                      <option value="By KKTT">By KKTT</option>
                                      <option value="By Omni">By Omni</option>
                                      <option value="Other">Other</option>
                                    </select>
                                  </div>
                                  <div class="col-sm-4">
                                    <select name="shippingCompany_{{$branch}}" id="shippingCompany_{{$branch}}" class="form-control" required>
                                      @if(session()->has('ShippingDetails'))
                                        @foreach($ShippingDetails as $shipping)
                                          <option value='{{$shipping->id}}'>{{$shipping->agency}}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                  </div>
                        </div>
                        <div class="row clearfix" id="otherShipPanel_{{$branch}}" style="display: none;">
                                  <div class="col-sm-2">
                                    <input  type="text" value="New Shipping" style="border-width: 0" readonly class="form-control font-bold" required />
                                  </div>
                                  <div class="col-sm-2">
                                    <input type="text" name="otherShip_{{$branch}}" id="otherShip_{{$branch}}" class="form-control">
                                  </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            <div class="body">
              <button type="submit" style="border-radius: 20px" name="placeOrder" value="placeOrder" class="btn bg-green waves-effect">Place Order</button>
            </div>
          </form>
          <form class="m-l-120" style="position: relative; margin-top: -50px;padding-bottom: 20px;" method="GET" action="{{route('order.create')}}">
            @csrf
            <button type="submit" style="border-radius: 20px" class="btn bg-blue waves-effect">Continue shopping</button>
          </form>
        </div>
      </div>
    </div>
              <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Shipping Form</h3>
                        </div>
                        <div class="modal-body">
                            <p>Add New Shipping Agency</p>
                        </div>
                        <div class="modal-footer">
                            <form id="newShippingForm" method="post" action="{{ route('shipping.store')}}">
                              @csrf
                              <div class="row clearfix m-b-10">
                                <div class="col-sm-6">
                                  <div class="form-line">
                                      <select name="shippingMode" id="shippingMode" class="form-control" required>
                                        <option style="background: #eee">Select Shipping Mode</option>
                                        <option value="Transport">Transport</option>
                                        <option value="Courier">Courier</option>
                                        <option value="By Hand">By Hand</option>
                                        <option value="By KKTT">By KKTT</option>
                                        <option value="By Omni">By Omni</option>
                                      </select>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="form-line">
                                    <input type="hidden" name="branchId" id="branchId">
                                    <input type="text" name="shippingCompany" id="shippingCompany" class="form-control" required />
                                  </div>
                                </div>
                              </div>
                              <button type="submit" name="saveShipping" value="saveShipping" class="btn btn-link waves-effect">Yes</button>
                              <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>  
  </body>
  <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
  <script type="text/javascript">

    function showNewShipForm(inputId)
    {
            var rowData = inputId.split('-');
            rowId = rowData[1];
            $('#branchId').val(rowId);
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-blue');
            $('#mdModal').modal('show');

    }

    function OnQtyChange(inputId)
    {
      var rowData = inputId.split('-');
      rowId = rowData[1];
      var subTotal = parseFloat($('#qty-'+rowId).val()) * ( parseFloat($('#disPrice-'+rowId).val())) ;
      var finalPrice = 0;
      var tempVariable = 0;
      switch($('#rateType-'+rowId).val())
      {
        case '0':
          finalPrice = subTotal + subTotal*parseFloat($('#gst-'+rowId).val())/100;
          break;

        case '1':
          finalPrice = subTotal;
          break;

        case '2':
          finalPrice = subTotal;
          break;
      }
      $('#finalPrice-'+rowId).val(finalPrice);
    }

  function CalDisPrice(inputId)
  {
      var rowData = inputId.split('-');
      rowId = rowData[1];
      var disPrice = $('#'+inputId).val();
      var disPer = (1 -  (disPrice / parseFloat($('#up-'+rowId).val()))) * 100;
      $('#disPer-'+rowId).val(disPer.toFixed(2));
      var subTotal = parseFloat($('#qty-'+rowId).val()) * ( parseFloat($('#disPrice-'+rowId).val())) ;
      var finalPrice = subTotal + subTotal*parseFloat($('#gst-'+rowId).val())/100;
      $('#finalPrice-'+rowId).val(finalPrice);
  }

  function CalDisPer(inputId)
  {
      var rowData = inputId.split('-');
      rowId = rowData[1];
      var disPer     = $('#'+inputId).val();
      var disPrice   = $('#up-'+rowId).val() - disPer * parseFloat($('#up-'+rowId).val()) / 100;
      $('#disPrice-'+rowId).val(parseFloat(disPrice).toFixed(2));
      var subTotal   = parseFloat($('#qty-'+rowId).val()) * (parseFloat($('#disPrice-'+rowId).val())) ;
      var finalPrice = subTotal + subTotal*parseFloat($('#gst-'+rowId).val())/100;
      $('#finalPrice-'+rowId).val(parseFloat(finalPrice).toFixed(2));
  }

  function DecQty(inputId)
  {
    var rowData = inputId.split('-');
    rowId = rowData[1];
    if($('#qty-'+rowId).val()>1)
    {
      $('#qty-'+rowId).val(parseFloat($('#qty-'+rowId).val())-1);
      var subTotal = parseFloat($('#qty-'+rowId).val()) * ( parseFloat($('#disPrice-'+rowId).val())) ;
      var finalPrice = 0;
      switch($('#rateType-'+rowId).val())
      {
        case '0':
          finalPrice = subTotal + subTotal*parseFloat($('#gst-'+rowId).val())/100;
          break;

        case '1':
          finalPrice = subTotal;
          break;

        case '2':
          finalPrice = subTotal;
          break;
      }
      $('#finalPrice-'+rowId).val(finalPrice);
    }
  }

  function IncQty(inputId)
  {
      var rowData = inputId.split('-');
      rowId = rowData[1];
      $('#qty-'+rowId).val(parseFloat($('#qty-'+rowId).val())+1);
      var subTotal = parseFloat($('#qty-'+rowId).val()) * ( parseFloat($('#disPrice-'+rowId).val())) ;
      var finalPrice = 0;
      switch($('#rateType-'+rowId).val())
      {
        case '0':
          finalPrice = subTotal + subTotal*parseFloat($('#gst-'+rowId).val())/100;
          break;

        case '1':
          finalPrice = subTotal;
          break;

        case '2':
          finalPrice = subTotal;
          break;
      }
      $('#finalPrice-'+rowId).val(finalPrice);
  }

  function RemoveItem(rowId)
  {
    $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/RemoveItemFromCart",
          type: 'POST',
          data: {
              'rowId': rowId
          },
          success: function(jsonData) {
              window.location.reload();
          }
      });
  }

  function addShipping() {
      $('#newShippingForm').css('display', 'block');
  }

  function GetShippingCompanies(rowData) {
      var rowData = rowData.split('_');
      branchId = rowData[1];
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/GetShippingCompanies",
          type: 'POST',
          data: {
              'shippingMode': $('#shippingMode_' + branchId).val(),
              'dispatchBranch': branchId
          },
          success: function(jsonData) {
              $('#shippingCompany_' + branchId).empty();
              $.each(jsonData, function(i, item) {
                  $('#shippingCompany_' + branchId).append($('<option>', {
                      value: item.id,
                      text: item.agency
                  }));
              });
              if($('#shippingMode_' + branchId).val()=="Other"){
                  $('#otherShipPanel_' + branchId).show();
                  $('#otherShip_' + branchId).prop('required',true);
              }
              else{
                  $('#otherShipPanel_' + branchId).hide();
                  $('#otherShip_' + branchId).prop('required',false);
              }
          }
      });
  }

  $(document).ready(function() {
      $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/LoadShipMode",
          type: 'POST',
          success: function(jsonData) {

              var transOption = '';
              var courierOption = '';

              $('#selTrans').remove();
              $('#rbShipMode').after('<div class="form-group" id="selTrans" name="selTrans"><select style="width:250px" name="transType" id="transType" class="form-control"></select></div>');
              $('#rbShipMode').after('<div class="form-group" id="selCourier" name="selCourier"><select  style="width:250px" name="courierType" id="courierType" class="form-control"></select></div>');

              for (var i = 0; i < jsonData.length; i++) {
                  if (jsonData[i]['name'] == "Transport")
                      transOption += '<option value="' + jsonData[i]['id'] + '">' + jsonData[i]['agency'] + '</option>';
                  else if (jsonData[i]['name'] == "Courier") {
                      courierOption += '<option value="' + jsonData[i]['id'] + '">' + jsonData[i]['agency'] + '</option>';
                  }
              }
              $('#transType').append(transOption);
              $('#courierType').append(courierOption);
              // alert(JSON.stringify(jsonData));
          },
          error: function(xhr, ajaxOptions, thrownError) {
              alert(xhr.status);
              alert(thrownError);
          }
      });
  });

  function SelShipMode(shipMode) {

      switch (shipMode) {
          case 'transport':
              $('#selCourier').css('display', 'none');
              $('#selTrans').css('display', 'block');
              break;
          case 'courier':
              $('#selCourier').css('display', 'block');
              $('#selTrans').css('display', 'none');
              break;
          case 'byhand':
              $('#selCourier').css('display', 'none');
              $('#selTrans').css('display', 'none');
              break;
      }
  }
    </script>
    @endSection