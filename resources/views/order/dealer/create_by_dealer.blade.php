<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap-select/css/bootstrap-select.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/plugins/node-waves/waves.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/plugins/animate-css/animate.css") }}" rel="stylesheet" />
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />

</head>
@extends('layouts.content')
@section('content')
<div class="block-header">
	<h2 id="temp">New Order </h2>
</div>
<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Fill Order Details &nbsp 
                                <span>&nbsp &nbsp &nbsp<i style="position: relative; margin-top:10px;color: green" class="material-icons">
                                    shopping_cart
                                    </i>
                                    <button class="btn-circle bg-green" style="position: absolute; top:15px;color: black; width: 25px; height: 25px;" id="cart_count">
                                        @if(session()->has('CartCount'))
                                            {{ session()->get('CartCount') }} 
                                        @endif
                                    </button>
                                </span>
                            </h2>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert bg-green alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <form action="{{ route('order.store') }}" method="post">
                            	@csrf
                                <div class="row clearfix">
                                      <div class="col-sm-9">
                                          <label for="dealer">Select Dealer</label>
                                          <div class="form-group">
                                              <div class="form-line">
                                            <select name="dealer" class="form-control" required>
                                                @if(session()->has('sel_dealer'))
                                                     @if(session()->get('sel_dealer') == $dealers->id)
                                                        <option value='{{$dealers->id}}' selected>{{$dealers->name}}</option>
                                                     @else
                                                        <option value='{{$dealers->id}}'>{{$dealers->name}}</option>
                                                     @endif
                                                @else
                                                    <option value='{{$dealers->id}}'>{{$dealers->name}}</option>
                                                @endif
                                            </select>
                                          </div>
                                          </div>
                                      </div>
                                    <div class="col-sm-3">
                                        <label for="todayDate">Date</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="date" id="todayDate" class="form-control" value="{{ $todayDate }}" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="classification">Classification</label>
                                            <div class="form-line">
                                              <select  name="classification" id="classification" class="form-control" onchange="FetchGroup(this.value)" required>
                                                    <option value="0">Selection Category</option>
                                                    <option value='1'>Machinery</option>
                                                    <option value='2'>Accessories</option>
                                                    <option value='3'>Parts</option>
                                              </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group" id="">
                                            <label for="Group" id="lblGrp">Group</label>
                                            <div id="GrpNames">
                                            <div class="form-line">

                                                <select name="Group" id="Group" class="form-control" onchange="FetchSubgrpByGrp(this.value);" required>
                                                </select>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="Subgroup" id="lblSubgrp">Subgroup</label>
                                            <div id="SubgrpNames">
                                            <div class="form-line">

                                                <select name="Subgroup" id="Subgroup" class="form-control" onchange="FetchSkuBySubgrp(this.value);" >
                                                </select>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-sm-2">
                                          <div class="form-group">
                                              <label for="dispatchBranch" id="lblBranch">Dispatch Branch</label>
                                              <div class="form-line">
                                                  <select style="
                                                        -webkit-appearance: none;
                                                        -moz-appearance: none;
                                                        text-indent: 1px;
                                                        text-overflow: '';" name="dispatchBranch" id="dispatchBranch" class="form-control" onchange="GetPriceByBranch();" required>
                                                    <option value="{{$branch[0]->id}}" >{{$branch[0]->name}}</option>
                                                  </select>
                                              </div>
                                          </div>
                                    </div>
                                   
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="hsnCode">HSN Code</label>
                                            <div class="form-line">
                                                <input type="text" name="hsnCode" id="hsnCode" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="Sku" id="lblSku">Sku Name</label>
                                            <div id="SkuNames">
                                            <div class="form-line">

                                                <select name="Sku" id="Sku" class="form-control" onchange="FetchPriceBySku(this.value);" required>
                                                </select>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="discountPrice">Unit Price</label>
                                            <div class="form-line">
                                                <input type="hidden" name="basePrice" id="basePrice">
                                                <input type="hidden" name="bulkQty" id="bulkQty">
                                                <input type="hidden" name="priceRate" id="priceRate" value="{{$branch[0]->rate}}">
                                                <input type="text" name="discountPrice" id="discountPrice" class="form-control" placeholder="" required readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="qty">Quantity</label>
                                            <div class="form-line">
                                                <input type="text" name="qty" id="qty" class="allownumericwithoutdecimal form-control" placeholder="1" required />
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="taxPerc">Tax %</label>
                                            <div class="form-line">
                                                <input type="text" name="taxPerc" id="taxPerc" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-2" style="display:  none;">
                                        <div class="form-group">
                                            <label for="taxAmt">Unit Price * Tax %</label>
                                            <div class="form-line">
                                                <input type="text" name="taxAmt" id="taxAmt" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="finalPrice">Final Price</label>
                                            <div class="form-line">
                                                <input type="text" name="finalPrice" id="finalPrice" class="form-control" placeholder="" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" name="orderSubmit" value="addToCart" class="btn bg-green waves-effect">Add To Cart</button>
                                    

                            </form>

                                <form class="pull-right col-sm-6" method="POST" action="{{route('order.clearCart')}}">
                                      @csrf
                                      <button type="" name="orderSubmit" value="clearCart" class="btn bg-red waves-effect">Clear Cart</button>
                                </form>
                                <form class="m-t-20" method="GET" action="{{route('order.checkout')}}">
                                  @csrf
                                  <button type="" name="orderSubmit" value="checkoutCart" class="btn btn-primary waves-effect">Proceed to Checkout</button>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
<script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
  
	<script type="text/javascript">
        var branchPriceRate = 1;
        var bulkQty = 0;
        var isBulkPurch = false;
        
        function SetItemPrice(isDisPercSet) {
                var branchPrice = branchPriceRate * $('#basePrice').val();
                $('#discountPrice').val(branchPrice); 
                var branchDP = parseFloat($('#discountPrice').val());
                var taxPer = parseFloat($('#taxPerc').val());
                var qty = parseFloat($('#qty').val());
                $('#finalPrice').val(parseFloat(branchDP*qty + branchDP*qty*taxPer/100).toFixed(2));
        }

        $(document).ready(function(){

           $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
                var that = this;

                //paste event 
                if (event.type === "paste") {
                    setTimeout(function() {
                        $(that).val($(that).val().replace(/[^\d].+/, ""));
                    }, 100);
                } else {

                    if (event.which < 48 || event.which > 57) {
                        event.preventDefault();
                    } else {
                        $(this).val($(this).val().replace(/[^\d].+/, ""));
                    }
                }

            });

           $('#qty').val(1);
           $('#qty').keyup(function(){
                SetItemPrice();
         });
        })

        //Group List
        function FetchGroup(category)
        {
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "/FetchGroup",
              type: 'POST',
              data: {'category': category},
              success: function(jsonData)
              {
                 // alert(JSON.stringify(jsonData));
                    $('#Group').empty();
                    $.each(jsonData, function (i, item) {
                        $('#Group').append($('<option>', { 
                            value: item.id,
                            text : item.name
                        }));
                    });
                    $('#Group').trigger('change');

                    // Set Branch Price Rate
                    branchPriceRate = $('#priceRate').val();
              },
              error: function (xhr, ajaxOptions, thrownError) 
              {
                 alert(xhr.status);
                 alert(thrownError);
              }
            });
        }

        //Subgroup List
        function FetchSubgrpByGrp(grpId)
        {
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "/FetchSubgrpByGrp",
              type: 'POST',
              data: {'category': $('#classification').val(),'grpId': grpId},
              success: function(jsonData)
              {
                 // alert(JSON.stringify(jsonData));
                 $('#Subgroup').empty();
                 $.each(jsonData, function (i, item) {
                    $('#Subgroup').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                    }));
                });
                $('#Subgroup').trigger('change');
              },
              error: function (xhr, ajaxOptions, thrownError) 
              {
                 alert(xhr.status);
                 alert(thrownError);
              }
            });
        }   

        //Sku List
        function FetchSkuBySubgrp(subgrpId)
        {
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "/FetchSkuBySubgrp",
              type: 'POST',
              data: {'category': $('#classification').val(),'grpId': $('#Group').val(), 'subgrpId': subgrpId},
              success: function(jsonData)
              {
                 // alert(JSON.stringify(jsonData));
                 $('#Sku').empty();
                 $.each(jsonData, function (i, item) {
                    $('#Sku').append($('<option>', { 
                        value: item.id,
                        text : item.name 
                    }));
                });
                $('#Sku').trigger('change');
                 
              },
              error: function (xhr, ajaxOptions, thrownError) 
              {
                 alert(xhr.status);
                 alert(thrownError);
              }
            });
        }   

        //Price Info
        function FetchPriceBySku(skuId)
        {
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "/FetchPriceBySku",
              type: 'POST',
              data: {'category': $('#classification').val(),'grpId': $('#Group').val(), 'subgrpId': $('#Subgroup').val(), 'skuId':skuId},
              success: function(jsonData)
              {
                 // alert(JSON.stringify(jsonData));
                 $('#basePrice').val(jsonData[0].base_price);
                 $('#discountPrice').val(jsonData[0].base_price);
                 $('#taxPerc').val(jsonData[0].gst['gst']);
                 $('#hsnCode').val(jsonData[0].gst['hsn']);

                 bulkQty = jsonData[0].bulk_qty;
                 $('#bulkQty').val(bulkQty);
                 $('#lblBulkQty').text(bulkQty);
                 $('#spBulkQty').text(bulkQty);
                 SetItemPrice();       
              },
              error: function (xhr, ajaxOptions, thrownError) 
              {
                 alert(xhr.status);
                 alert(thrownError);
              }
            });
        }

    </script>

    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase-messaging.js"></script>
    <script>

        // Retrieve an instance of Firebase Messaging so that it can handle background
        // messages.
        var config = {
            apiKey: "AIzaSyCIw2yRWospao4QiaUcPY5jFEKdIvTpU74",
            authDomain: "krishi-6d123.firebaseapp.com",
            databaseURL: "https://krishi-6d123.firebaseio.com",
            projectId: "krishi-6d123",
            storageBucket: "krishi-6d123.appspot.com",
            messagingSenderId: "988261907749"
          };
          firebase.initializeApp(config);
         const messaging = firebase.messaging();
         messaging.usePublicVapidKey('BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY');
       messaging.requestPermission().then(function() {
                          console.log('Notification permission granted.');
                          var user_email = "null";    
                              //Get current user email id
                              $.ajax({
                                    headers: {
                                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                      },
                                    url: "/getCurrentUserEmail",
                                    type: 'POST',
                                    data: {'data': "null"},
                                    success: function(jsonData)
                                    {   
                                       messaging.getToken().then(function(currentToken) {
                                            if (currentToken) {
                                                sendTokenToServer(jsonData,currentToken);
                                                // updateUIForPushEnabled(currentToken);
                                            } else {
                                                // Show permission request.
                                                console.log('No Instance ID token available. Request permission to generate one.');
                                                // Show permission UI.
                                                updateUIForPushPermissionRequired();
                                                setTokenSentToServer(false);
                                            }
                                        }).catch(function(err) {
                                            console.log('An error occurred while retrieving token. ', err);
                                            showToken('Error retrieving Instance ID token. ', err);
                                            setTokenSentToServer(false);
                                        });
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) 
                                    {
                                              
                                    }
                              });

                          // TODO(developer): Retrieve an Instance ID token for use with FCM.
                          // alert("permission granted");


                        }).catch(function(err) {
                          console.log('Unable to get permission to notify.', err);
                        });

          messaging.onMessage(function(payload) {
            console.log('Message received in home. ', payload['data']);
            // // alert();
            // $("#notify_header").prepend('<li> <a href="javascript:void(0);"><div class="menu-info"><h4>'+payload['data']['message']+'</h4><p><i class="material-icons">priority_high</i>order status</p></div></a></li>');
            window.location.reload();
          });

          $(document).ready(function(){
            // $.ajax({
            //         headers: {
            //              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //           },
            //         url: "/fetchNotifications",
            //         type: 'POST',
            //         data: {'data': "null"},
            //         success: function(jsonData)
            //         {   
            //             $('#notify_count').text(jsonData.length);
            //             for(var i=0; i<jsonData.length; i++)
            //             {
            //               $("#notify_header").prepend('<li> <a href="javascript:void(0);"><div class="menu-info"><h4>'+jsonData[i]['message']+'</h4><p><i class="material-icons">priority_high</i>order status</p></div></a></li>');
            //             }
            //         },
            //         error: function (xhr, ajaxOptions, thrownError) 
            //         {
                              
            //         }
            //   });
          })

         function sendTokenToServer(email, currentToken)
         {
              $.ajax({
                    headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      },
                    url: "/sendTokenToServer",
                    type: 'POST',
                    data: {'email': email, 'token': currentToken},
                    success: function(jsonData)
                    {   
                        // alert(jsonData);
                    },
                    error: function (xhr, ajaxOptions, thrownError) 
                    {
                              
                    }
              });
         }   

    </script>
