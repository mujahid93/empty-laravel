@extends('layouts.header')
@extends('layouts.content')
@section('content')
<script type="text/javascript">
function back_block() {
    history.pushState(null, null, document.title);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.title);
    });
}
</script>
<body onload="javascript:back_block()" class="four-zero-four">
	<center>
	    <div class="four-zero-four-container m-t-100">
	        <div class="error-code"><h2>Order has been sent to your sales executive</h2></div>
	        <div class="error-message"><h5>Please contact your regional sales executive for further information.</h5></div>
	        <div class="button-place m-t-70">
	            <a href="{{ route('home') }}" class="btn btn-default btn-lg waves-effect">View order status</a>
	        </div>
	    </div>
	</center>
</body>

@endsection
@extends('layouts.footer')