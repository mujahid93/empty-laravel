<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
</head>

@extends('layouts.content')
@section('content')
            <div class="row clearfix">
                <div class="col-lg-13 col-md-13 col-sm-13 col-xs-13">

                    <div class="card">
                        <div class="header">
                            <h3>
                                Order Details
                            </h3>
                        </div>
                        <div class="body">

                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Tracking Number</p>
                                        <h5 id="purchaseNo">{{$placedOrders[0]->tracking_no}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Dealer Name</p>
                                        <h5>{{$placedOrders[0]->order->dealer->name}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Date</p>
                                        <h5>{{date('d-m-Y', strtotime($placedOrders[0]->order_date))}}</h5>
                                    </blockquote>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-13 col-xs-3">
                                    <blockquote>
                                        <p>Order Status</p>
                                        <h4><span id="orderStatus" class="label bg-pink">
                                            Pending                                            
                                            </span></h4>
                                    </blockquote>
                                </div>
                            </div>

                        <div class="body">
                                <div class="table-responsive">
                                        <input type="hidden" name="orderStatus" value="2">
                                        
                                        <?php $finalAmt = 0; $count = 0 ; $curr_order = null;?>
                                        @foreach($dispatchBranches as $dispBranch)
                                            <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                                <thead>
                                                    <tr>
                                                        <th>Classification</th>
                                                        <th>SKU Name</th>
                                                        <th>Quantity</th>
                                                        <th>Branch</th>
                                                        <th>Rate</th>
                                                        <th>GST (%)</th>
                                                        <th style="background: #DBEEBC">Dis %</th>
                                                        <th style="background: #DBEEBC" width="30">Dis Req</th>
                                                        <th>Final Rate</th>
                                                        <th>Split Order</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    @php ($items = [])
                                                    <?php $totalPrice = 0; $freight = 0; $finalRate = 0; ?>
                                                    @foreach($placedOrders as $order)
                                                        @if($dispBranch != $order->branch)
                                                            @continue
                                                        @endif
                                                        @php($curr_order  = $order)
                                                        <?php $count++; $freight = $order->freight; $destination = $order->destination; $newDestFlag = 0; ?>
                                                        <tr>
                                                          <form class="pull-right col-sm-6" method="POST" action="{{route('order.split')}}">
                                                          @csrf
                                                                <td>{{$order->classification}}</td>
                                                                <td>{{$order->sku}}</td>
                                                                <td><input id="qty-{{ $order->id }}" style="width: 70px" type="text" value="{{$order->qty}}" oninput="ChangeQty(this.id)"></td>
                                                                <td>
                                                                    <span class="custom-dropdown big">
                                                                        <style type="text/css">
                                                                            .custom-dropdown select {
                                                                              background-color: #1abc9c;
                                                                              color: #fff;
                                                                              font-size: inherit;
                                                                              padding: .5em;
                                                                              padding-right: 3.5em; 
                                                                              border: 0;
                                                                              margin: 0;
                                                                              border-radius: 3px;
                                                                              text-indent: 0.01px;
                                                                              text-overflow: '';
                                                                              /*Hiding the select arrow for firefox*/
                                                                              -moz-appearance: none;
                                                                              /*Hiding the select arrow for chrome*/
                                                                              -webkit-appearance:none;
                                                                              /*Hiding the select arrow default implementation*/
                                                                              appearance: none;
                                                                            }
                                                                            /*Hiding the select arrow for IE10*/
                                                                        </style>
                                                                        <select id="{{$order->id}}dispatchbranch" onchange="FetchPriceRate(this.value)">
                                                                            @foreach($branches as $branch)
                                                                                @if($order->branch == $branch->id)
                                                                                    <option value="{{ $branch->id.':'.$order->id }}" selected>{{$branch->code}}</option>
                                                                                @else
                                                                                    <option value="{{ $branch->id.':'.$order->id }}">{{$branch->code}}</option>
                                                                                @endif
                                                                            @endforeach        
                                                                        </select>
                                                                    </span>
                                                                </td>
                                                                <?php $price = 0;$rate_without_tax = 0; ?>
                                                                <?php 
                                                                  $rate_type = "";
                                                                  switch ($order->rate_type) {
                                                                      case '0':
                                                                          $rate_type = "Dealer Price";
                                                                          $price = $order->item_rate;
                                                                          $dis_rate = number_format($price - (float)$order->dis_per/100 * $price, 2, '.', '');
                                                                          $finalRate = number_format((float)(($price - $order->dis_per/100 * $price)) * $order->qty, 2, '.', '');
                                                                          break;
                                                                      
                                                                      case '1':
                                                                          $rate_type = "MRP";
                                                                          $price = $order->item_rate;
                                                                          $dis_rate = $order->item_rate;
                                                                          $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                                          $finalRate = $order->item_rate * $order->qty;
                                                                          break;

                                                                      case '2':
                                                                          $rate_type = "Subsidy";
                                                                          $price = $order->item_rate;
                                                                          $dis_rate = $order->item_rate;
                                                                          $rate_without_tax = (($order->item_rate * 100)/($order->gst + 100)) * $order->qty;
                                                                          $finalRate = $order->item_rate * $order->qty;
                                                                          break;

                                                                      default:
                                                                          break;
                                                                  }
                                                                ?>
                                                                <td>
                                                                    <input id="up-{{ $order->id }}" style="width: 70px" type="text" value="{{number_format((float)$price, 2, '.', '')}}" readonly>
                                                                </td>
                                                                <td>{{$order->gst}}</td>
                                                                <td>
                                                                  @if($order->rate_type == 0)
                                                                    <input id="disperc-{{ $order->id }}" style="width: 70px" type="text" value="{{$order->discountPer}}" oninput="CalDiscount(this.id)">
                                                                  @elseif($order->discountPer == null)
                                                                    0
                                                                  @else
                                                                    {{$order->discountPer}}
                                                                  @endif
                                                                </td>
                                                                <td>
                                                                  @if($order->rate_type == 0)
                                                                    <input id="disamt-{{ $order->id }}" style="width: 70px" type="text" value="{{number_format((float)$price, 2, '.', '')}}" oninput="CalDiscount(this.id)">
                                                                  @else
                                                                    {{$order->item_rate - $order->discountPer*$order->item_rate/100}}
                                                                  @endif
                                                                </td>
                                                                <td>
                                                                  <?php
                                                                      switch ($order->rate_type) {
                                                                      case '0':
                                                                              $items[] = ['rate'=>$finalRate,'gst'=>$order->gst];
                                                                              $totalPrice += $finalRate;
                                                                          break;
                                                                      
                                                                      case '1':
                                                                              $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                              $totalPrice += $rate_without_tax;
                                                                          break;

                                                                      case '2':
                                                                              $items[] = ['rate'=>$rate_without_tax,'gst'=>$order->gst];
                                                                              $totalPrice += $rate_without_tax;
                                                                          break;

                                                                      default:
                                                                          break;
                                                                  }
                                                                  ?>
                                                                    <input id="finalAmt-{{ $order->id }}" style="width: 70px" type="text" value="{{number_format((float)($price * $order->taxPerc/100 + $price) * $order->qty, 2, '.', '')}}" readonly>
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" name="order_pin" value="{{$order->id}}">
                                                                    <button type="submit" name="splitOrder" class="btn bg-green waves-effect">
                                                                        Split Order
                                                                    </button>    
                                                                </td>
                                                            </form>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                             <div class="card">
                                                <div class="header">
                                                  <div class="form-group">
                                                    <div class="row clearfix">
                                                      <div class="col-sm-6">
                                                        <label for="freight">Freight Charges</label>
                                                        <div class="form-line">
                                                          <input type="text" name="freight_{{$dispBranch}}" id="freight_{{$dispBranch}}" value="{{$curr_order->freight}}" class="form-control" oninput="calcTotal(this.id)" />
                                                        </div>
                                                      </div>
                                                      <?php $finalTotal_With_Freight = 0; ?>
                                                      @foreach($items as $item)
                                                          <?php 
                                                              $finalTotal_With_Freight = $finalTotal_With_Freight + (($freight * $item['rate']/$totalPrice) + $item['rate']) * $item['gst']/100 ;
                                                          ?>
                                                      @endforeach
                                                    <div class="col-sm-6">
                                                        <label for="destination">Destination</label>
                                                        <div class="form-line">
                                                          <select  name="destination_{{$dispBranch}}" id="destination_{{$dispBranch}}" class="form-control">
                                                            <option value="{{$placedOrders[0]->order->dealer->address1}}">{{$curr_order->order->dealer->address1}}</option>
                                                            <option value="{{$placedOrders[0]->order->dealer->address2}}">{{$curr_order->order->dealer->address2}}</option>
                                                            <option value="{{$placedOrders[0]->order->dealer->address3}}">{{$curr_order->order->dealer->address3}}</option>
                                                          </select>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    
                                                    <div class="row clearfix" id="defaultShipping_{{$dispBranch}}">
                                                      <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label for="shippingMode" id="lblShipMode">Shipping Mode</label>
                                                          <div class="form-line">
                                                            <input type="text" name="shipMode" id="shipMode" value="{{$curr_order->shipping['name']}}" class="form-control"/>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label for="shippingCompany" id="lblShipCompany">Shipping Company</label>
                                                          <div class="form-line">
                                                            <input type="text" name="shipAgency" id="shipAgency" value="{{$curr_order->shipping['agency']}}" class="form-control"/>
                                                          </div>
                                                        </div>
                                                      </div>
                                                       @if($curr_order->shipping['name']=='Other')
                                                        <div class="row clearfix" style="margin-left: 0px;">
                                                          <div class="col-sm-6">
                                                            <div class="form-group">
                                                              <label for="shippingCompany" id="lblShipCompany">Other Shipping</label>
                                                              <div class="form-line">
                                                                <input type="text" name="other_ship" id="shipAgency" value="{{$curr_order->other_ship}}" class="form-control"/>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      @endif
                                                    </div>
                                                    <div class="row clearfix" id="modifyShipping_{{$dispBranch}}" style="display: none;">
                                                      <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label for="shippingMode" id="lblShipMode">Shipping Mode</label>
                                                          <div class="form-line">
                                                            <select name="shippingMode_{{$dispBranch}}" id="shippingMode_{{$dispBranch}}" class="form-control" required onchange="GetShippingCompanies(this.id)">
                                                              <option style="background: #eee" value="0">Select Shipping Mode</option>
                                                              <option value="Transport">Transport</option>
                                                              <option value="Courier">Courier</option>
                                                              <option value="By Hand">By Hand</option>
                                                              <option value="By KKTT">By KKTT</option>
                                                              <option value="By Omni">By Omni</option>
                                                              <option value="Other">Other</option>
                                                            </select>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label for="shippingCompany" id="lblShipCompany">Shipping Company</label>
                                                          <select name="shippingCompany_{{$dispBranch}}" id="shippingCompany_{{$dispBranch}}" onchange="SaveShippingData(this.id)" class="form-control" required>
                                                          </select>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="row clearfix" id="otherShipPanel_{{$dispBranch}}" style="display: none;">
                                                         <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label for="shippingCompany" id="lblShipCompany">Other Shipping</label>
                                                          <div class="form-line">
                                                            <input type="text" name="shipAgency" id="otherShipping_{{$dispBranch}}" oninput="SaveOtherShipping(this.id)" value="" class="form-control"/>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>

                                                    <div class="pull-left button-demo js-modal-buttons">
                                                     <button type="button" id="nShipping_{{$dispBranch}}" onclick="ChangeShipping(this.id)" data-color="blue" name="" class="btn bg-blue waves-effect">
                                                        Change Shipping
                                                     </button>
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>
                                              <?php $finalAmt += $freight + $totalPrice + $finalTotal_With_Freight; ?>
                                        @endforeach
                                        </form>
                                </div>
                            <div class="row">
                                  <div class="pull-right">
                                      <button type="" data-color="purple" name="" class="btn btn-success waves-effect"><span id="tFinalAmt">{{$finalAmt}}</span></button>
                                  </div>
                                  <div class="pull-right button-demo js-modal-buttons">
                                      <button type="button" onclick="SubmitOrder()" data-color="purple" name="" class="btn bg-purple waves-effect">Get Confirmation By Dealer</button>
                                  </div>
                            </div>
                        </div>
                </div>
            </div>

              <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Order Status</h3>
                        </div>
                        <div class="modal-body">
                            <p>Submit order for management approval ?</p>
                        </div>
                          <form action="/GetDealerConfirmation" method="post">
                            @csrf
                            <div class="modal-footer">
                                    <input type="hidden" name="tracking_no" id="tracking_no" value="{{encrypt($placedOrders[0]->tracking_no)}}">
                                    <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </div>
                          </form>
                    </div>
                </div>
              </div>  
@endsection
    
<script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
    <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>
  
<script type="text/javascript">

    function calcTotal(data)
    {
                  var rowData = data.split('_');
                  branchId = rowData[1];

                  var freight = $('#freight_'+branchId).val();
                  if (freight == null || freight == "") {
                    return;
                  }

                  $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/ChangeFreight",
                    type: 'POST',
                    data: {
                      'freight': freight,
                      'branch' : branchId,
                      'tracking_no': $('#tracking_no').val()
                    },
                    success: function(jsonData)
                    {
                         // alert(JSON.stringify(jsonData));
                         $('#tFinalAmt').text(jsonData['tfinal']);
                    }
                  });
    }

    function SaveOtherShipping(data){
        var rowData = data.split('_');
        branchId = rowData[1];
        var otherShipping = $('#otherShipping_'+branchId).val();
        if (otherShipping == null || otherShipping == "") {
          return;
        }
         $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/UpdateOtherShip",
                    type: 'POST',
                    data: {
                      'otherShipping': otherShipping,
                      'branch' : branchId,
                      'tracking_no': $('#tracking_no').val()
                    },
                    success: function(jsonData)
                    {
                         // alert(JSON.stringify(jsonData));
                         //$('#tFinalAmt').text(jsonData['tfinal']);
                    }
        });
    }

    function SaveShippingData(rowData)
    {
              var rowData = rowData.split('_');
              branchId = rowData[1];

              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/SaveShippingData",
                  type: 'POST',
                  data: {
                      'tracking_no': $('#tracking_no').val(),
                      'dispatchBranch': branchId,
                      'shipping': $('#shippingCompany_'+branchId).val()
                  },
                  success: function(jsonData) {
                      
                  }
              });
    }

    function postShippingData()
    {
        // $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         url: "/ChangeOrderStatus",
        //         type: 'POST',
        //         data: $('#ShippingForm').serialize(),
        //         success: function(jsonData)
        //         {
        //             window.open("http://127.0.0.1:8000/home","_self")
        //         }
        //     });
    }

    function GetShippingCompanies(rowData)
        {
              var rowData = rowData.split('_');
              branchId = rowData[1];

              $.ajax({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url: "/GetShippingCompanies",
                  type: 'POST',
                  data: {
                      'tracking_no': $('#tracking_no').val(),
                      'shippingMode': $('#shippingMode_' + branchId).val(),
                      'dispatchBranch': branchId
                  },
                  success: function(jsonData) {
                      $('#shippingCompany_' + branchId).empty();
                      $.each(jsonData, function(i, item) {
                          $('#shippingCompany_' + branchId).append($('<option>', {
                              value: item.id,
                              text: item.agency
                          }));
                      });

                      if($('#shippingMode_' + branchId).val()=='Other'){
                         $('#otherShipPanel_' + branchId).show();
                         $('#otherShipping_' + branchId).prop('required',true);
                      }
                      else{
                         $('#otherShipPanel_' + branchId).hide();
                         $('#otherShipping_' + branchId).prop('required',false);
                      }
                  }
              });
        }

    function ChangeShipping(rowData)
    {
              var rowData = rowData.split('_');
              branchId = rowData[1];
              $('#defaultShipping_' + branchId).css('display','none');
              $('#modifyShipping_' + branchId).css('display','block');
              $('#shippingMode_' + branchId).val(0);
    }

    var selectedBranch = 0;

    function FetchPriceRate(rowData)
    {
        var rowData = rowData.split(':');
        branchId = rowData[0];
        orderId = rowData[1];
        // alert(orderId);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/FetchPriceRate",
            type: 'POST',
            data: {
                'branchId': branchId,
                'orderId': orderId,
            },
            success: function(jsonData) {
                // alert(JSON.stringify(jsonData));
                window.location.href = window.location.href;
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function ChangeQty(orderId) {
        var newQty = 1;
        var orderId = orderId.split('-');
        if($('#qty-'+orderId[1]).val() > 0)
        {
            newQty = $('#qty-'+orderId[1]).val();
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ChangeItemQtyByExec",
            type: 'POST',
            data: {
                'orderId': orderId[1],
                'qty': newQty,
            },
            success: function(jsonData) {
                // $('#up-'+jsonData['orderId']).val(jsonData['price']);
                $('#gstAmt-'+jsonData['orderId']).val(jsonData['taxAmt']);
                $('#finalAmt-'+jsonData['orderId']).val(jsonData['finalPrice']);
                $('#tFinalAmt').text(jsonData['tfinal']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function CalDiscount(orderId)
    {
        var orderId = orderId.split('-');

        if($('#disperc-'+orderId[1]).is(":focus"))
        {
            if($('#disperc-'+orderId[1]).val() <= 0)
            {
                $('#disamt-'+orderId[1]).val(0);
                $('#disperc-'+orderId[1]).val(0);
            }
            var disAmt = $('#up-'+orderId[1]).val() - $('#disperc-' + orderId[1]).val() / 100 * $('#up-'+orderId[1]).val();
            $('#disamt-'+orderId[1]).val(disAmt.toFixed(2));
        }
        else
        if($('#disamt-'+orderId[1]).is(":focus"))
        {
            if($('#disamt-'+orderId[1]).val() <= 0 )
            {
                 $('#disamt-'+orderId[1]).val(0);
                 $('#disperc-'+orderId[1]).val(0);
            }
            var disPerc = ($('#up-'+orderId[1]).val() - $('#disamt-'+orderId[1]).val()) * 100 / $('#up-'+orderId[1]).val();
            $('#disperc-'+orderId[1]).val(disPerc.toFixed(2));
        }
        var discountPerc = $('#disperc-'+orderId[1]).val();


        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/ApplyDiscountByExecutive",
            type: 'POST',
            data: {
                'orderId': orderId[1],
                'discountPerc': discountPerc,
            },
            success: function(jsonData) {
                // alert(JSON.stringify(jsonData['price']));
                // $('#up-'+jsonData['orderId']).val(jsonData['price']);
                $('#gstAmt-'+jsonData['orderId']).val(jsonData['taxAmt']);
                $('#finalAmt-'+jsonData['orderId']).val(jsonData['finalPrice']);
                $('#tFinalAmt').text(jsonData['tfinal']);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function SubmitOrder()
    {
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
    }

</script>
