<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Kisankraft Private Limited</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link  href="{{ asset("/MaterialTheme/plugins/bootstrap/css/bootstrap.css") }}" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/style.css") }}" href="css/style.css" rel="stylesheet">
    <link  href="{{ asset("/MaterialTheme/css/themes/all-themes.css") }}" rel="stylesheet" />
  </head>
  @extends('layouts.content')
  @section('css_links')
  <link  href="{{ asset("/css/checkout.css") }}" rel="stylesheet" />
  @endsection
  @section('content')
  <body>
    <div class="block-header">
      <!-- <h2 id="temp">Order </h2> -->
    </div>
    <div class="row clearfix">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card" >
          
          <div class="header">
            <h2>
            <b>Dealer</b>&nbsp &nbsp &nbsp
            @if(session()->has('dealerName'))
            {{ Session::get('dealerName') }}
            @endif
            </h2>
          </div>
          @if(Session::has('cartItems'))
          @if(session()->has('result'))
          <div class="alert bg-green alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('result') }}
          </div>
          @endif
          <form action='/ConfirmOrder' method="post">
            @csrf
            <div class="body">
              @foreach($branches as $branch)
              <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="card">
                    <div class="header">
                    </div>
                    <div class="body">
                      <table id="_tblCart" class="table table-bordered table-striped table-hover js-basic-example">
                        <thead>
                          <tr>
                            <th> SKU</th>
                            <th> Qty</th>
                            <th> Price</th>
                            @can('isExecutive')
                            <th> Discount Per%</th>
                            @endcan
                            <th> Tax%</th>
                            <th> Final Price</th>
                            <th> Dispatch Branch</th>
                            <th> Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($orderedItems as $order)
                          @if($branch != $order['branchId'])
                          @continue
                          @endif
                          <tr>
                            <td value ="{{ $order['sku'][0]->id }}"> {{ $order['sku'][0]->name }} </td>
                            <td>{{$order['qty']}}</td>
                            <td>{{ $order['price'] }}</td>
                            @can('isExecutive')
                            <td>{{ $order['discountPerc'] }}</td>
                            @endcan
                            <td>{{ $order['taxPerc'] }} </td>
                            <td>{{ $order['finalPrice'] }}</td>
                            <td>{{ $order['dispatchBranch'] }}</td>
                            <td>
                              <form action="{{ route('order.store') }}" method="post">
                                @csrf
                                <input type="hidden" name="orderIndex" value="{{ $order['index'] }}">
                                <button name="orderSubmit" value="removeItem" class="btn btn-default"><i class="col-red material-icons">delete</i></button>
                              </form>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            
            <div class="body">
              <button type="submit" name="placeOrder" value="placeOrder" class="btn bg-green waves-effect">Place Order</button>
            </div>
          </form>
          <form class="m-l-120" style="position: relative; margin-top: -50px;padding-bottom: 20px;" method="GET" action="{{route('order.create')}}">
            @csrf
            <button type="submit" class="btn bg-blue waves-effect">Continue shopping</button>
          </form>
        </div>
      </div>
    </div>
  </body>
  @endif
  <script src="{{ asset("/MaterialTheme/plugins/jquery/jquery.min.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/bootstrap/js/bootstrap.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/admin.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/jquery-slimscroll/jquery.slimscroll.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/node-waves/waves.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/jquery-countto/jquery.countTo.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/demo.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/plugins/bootstrap-notify/bootstrap-notify.js") }}"></script>
  <script src="{{ asset("/MaterialTheme/js/pages/ui/notifications.js") }}"></script>

 @endSection