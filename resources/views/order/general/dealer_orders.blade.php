@extends('layouts.header')
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
                <h2 class="col-lg-10 col-md-10" > Orders </h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                My Orders 
                            </h2>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">

                                    <thead>
                                        <tr>
                                            <th>Purchase No</th>
                                            <th>Dealer Name</th>
                                            <th>Placed By</th>
                                            <th>Order Date</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dealerOrders as $order)
                                            @if($order == $dealerOrders->first())    
                                                <tr style="background: #efe">
                                                    <td style="position: relative;" width="300">{{$order->purchase_no}} &nbsp<span class="badge bg-green" style="color: white;padding:5px;position: absolute; top: 10px;">new</span></td>
                                                    <td width="300">{{$order->dealer->user->name}}</td>
                                                    <td width="300">
                                                        @if($order->placedBy)
                                                            {{$order->placedBy->user->name}}
                                                        @else
                                                            {{$order->dealer->user->name}}
                                                        @endif
                                                    </td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="purchaseNo" type="hidden" value="{{$order->purchase_no}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td width="300">{{$order->purchase_no}}</td>
                                                    <td width="300">{{$order->dealer->user->name}}</td>
                                                    <td width="300">
                                                        @if($order->placedBy)
                                                            {{$order->placedBy->user->name}}
                                                        @else
                                                            {{$order->dealer->user->name}}
                                                        @endif
                                                    </td>
                                                    <td width="300">{{date('d-m-Y', strtotime($order->created_at))}}</td>
                                                    <td width="300">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="purchaseNo" type="hidden" value="{{$order->purchase_no}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>

                                {{ $dealerOrders->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')

@section('scripts')


<script type="text/javascript">

    

</script>   
  
@endSection