@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    @if(session()->has('rejected_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('rejected_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('rejected_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('rejected_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif
                        </div>

                         <div class="body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                @foreach($states as $state)
                                                    @if(session()->has('rejected_state') && session()->get('rejected_state')==$state->id)
                                                        <option value="{{$state->id}}" selected>{{$state->name}}</option>                                     
                                                    @else
                                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <input type="hidden" id="selectedDist" value="{{Session::get('e_mo_district')}}"  />
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                @foreach($districtList as $district)
                                                    @if(session()->has('rejected_district') && session()->get('rejected_district')==$district->id)
                                                        <option value="{{$district->id}}" selected>{{$district->name}}</option>                                     
                                                    @else
                                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                @foreach($dealerList as $dealer)
                                                    @if(session()->has('rejected_dealer') && session()->get('rejected_dealer')==$dealer->id)
                                                        <option value="{{$dealer->id}}" selected>{{$dealer->name}}</option>                                     
                                                    @else
                                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" id="_tracking_no_filter" class="form-control" value="{{session()->get('rejected_tracking','')}}">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="body">
                            <div class="table-responsive" id="tbl_aio">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Sales Exec</th>
                                            <th>Order Date</th>
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($RejectedOrders as $order)
                                                <tr>
                                                    <td width="50">{{$order->tracking_no}}</td>
                                                    <td width="700">{{$order->dealer->name}}</td>
                                                    <td width="400">
                                                            {{$order->placedBy->name}}
                                                    </td>
                                                    <td width="50">{{date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))}}</td>
                                                    <td width="50">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{$order->tracking_no}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $RejectedOrders->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterRejectedOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script> 
@endSection
