
@extends('layouts.header')
@extends('layouts.content')
@section('content')
<script type="text/javascript">
function back_block() {
    history.pushState(null, null, document.title);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, document.title);
    });
}
</script>

<body onload="javascript:back_block()"  class="four-zero-four"">

	<center>
	    <div class="four-zero-four-container m-t-100">
	        <div class="error-code"><h2>Order Status</h2></div>
	        <div class="error-message"><h5>
	        	@if(session()->has('result'))
	        		{{session()->get('result')}}
	        	@endif
	        </h5></div>
	        <div class="button-place m-t-70">
	            <a href="{{ route('home') }}" class="btn btn-default btn-lg waves-effect">Go to home</a>
	        </div>
	    </div>
	</center>
	
</body>

@endsection
@extends('layouts.footer')