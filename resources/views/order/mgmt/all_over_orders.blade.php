@extends('layouts.header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@extends('layouts.content')
@section('content')

<div class="block-header row  clearfix">
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                                    @if(session()->has('e_mo_fromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('e_mo_fromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('e_mo_toDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('e_mo_toDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif
                        </div>

                         <div class="body">
                            <div class="row">
                                @if(!isset($order_rejected_flag))
                                    <div class="col-sm-1">
                                        <div class="form-group ">
                                                <select id="_order_status_filter" class="form-control" onchange="FilterAllOrders()">
                                                    @if(session()->get('e_mo_orderStatus','-1')==0)
                                                        <option value="0" selected="selected">All</option>
                                                    @else
                                                        <option value="0" >All</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==1)
                                                        <option value="1" selected="selected">Pending</option>
                                                    @else
                                                        <option value="1" >Pending</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==2)
                                                        <option value="2" selected="selected">Executive Placed</option>
                                                    @else
                                                        <option value="2" >Executive Placed</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==3)
                                                        <option value="3" selected="selected">Mgmt Approved</option>
                                                    @else
                                                        <option value="3" >Mgmt Approved</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==4)
                                                        <option value="4" selected="selected">Payment Verified</option>
                                                    @else
                                                        <option value="4" >Payment Verified</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==5)
                                                        <option value="5" selected="selected">Invoice Raised</option>
                                                    @else
                                                        <option value="5" >Invoice Raised</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==6)
                                                        <option value="6" selected="selected">Dispatched</option>
                                                    @else
                                                        <option value="6" >Dispatched</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==7)
                                                        <option value="7" selected="selected">Rejected</option>
                                                    @else
                                                        <option value="7" >Rejected</option>
                                                    @endif


                                                    @if(session()->get('e_mo_orderStatus','-1')==9)
                                                        <option value="9" selected="selected">Parts Approval Pending</option>
                                                    @else
                                                        <option value="9" >Parts Approval Pending</option>
                                                    @endif

                                                </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-2">
                                    <div class="form-group ">
                                            <select id="_state_filter"  class="form-control" onchange="FetchDistByOneState(this.value)">
                                                    <option value="0">All</option>
                                                @foreach($states as $state)
                                                    @if(session()->has('e_mo_state') && session()->get('e_mo_state')==$state->id)
                                                        <option value="{{$state->id}}" selected>{{$state->name}}</option>                                     
                                                    @else
                                                        <option value="{{$state->id}}">{{$state->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <label id="lblDistrict"></label>
                                    <div class="form-group" id="districtGroup">
                                            <select id="_district_filter"  class="form-control"  onchange="FetchDealers()">
                                                <option value="0">All</option>
                                                @foreach($districtList as $district)
                                                    @if(session()->has('e_mo_district') && session()->get('e_mo_district')==$district->id)
                                                        <option value="{{$district->id}}" selected>{{$district->name}}</option>                                     
                                                    @else
                                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label id="lblDealer"></label>
                                    <div class="form-group" id="dealerGroup">
                                            <select id="_dealer_filter" class="form-control" onchange="FilterAllOrders()">
                                                <option value="0">All</option>
                                                @foreach($dealerList as $dealer)
                                                    @if(session()->has('e_mo_dealer') && session()->get('e_mo_dealer')==$dealer->id)
                                                        <option value="{{$dealer->id}}" selected>{{$dealer->name}}</option>                                     
                                                    @else
                                                        <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                     <div class="form-group form-float">
                                        <div class="form-line">

                                            <input type="text" id="_tracking_no_filter" class="form-control" value="{{session()->get('e_mo_tracking','')}}">
                                            <label class="form-label">Tracking Number</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-1">
                                     <div class="form-group form-float">
                                            <button type="button" class="btn btn-default" onclick="FilterAllOrders()"><i class="col-green material-icons">search</i></button>
                                </div>
                                </div>
                            </div>
                        </div>

                        
                        <div class="body">
                            <div class="table-responsive" id="tbl_aio">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Tracking No</th>
                                            <th>Dealer Name</th>
                                            <th>Sales Exec</th>
                                            <th>Order Date</th>
                                            @if(!isset($order_rejected_flag))
                                                <th>Order Status</th>
                                            @endif
                                            <th width="40">View/Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($allIndiaOrders as $order)
                                                <tr>
                                                    <td width="50">{{$order->tracking_no}}</td>
                                                    <td width="700">{{$order->dealer->name}}</td>
                                                    <td width="400">
                                                            {{$order->placedBy->name}}
                                                    </td>
                                                    <td width="50">{{date('d-m-Y', strtotime($order->OrderDetail[0]->order_date))}}</td>
                                                    @if(!isset($order_rejected_flag))
                                                        <td>
                                                            @switch($order->OrderDetail[0]->order_status)
                                                                @case(1)
                                                                    <span> Pending</span>
                                                                    @break

                                                                @case(2)
                                                                    <span>Executive Placed</span>
                                                                    @break

                                                                @case(3)
                                                                    <span>Payment Verified</span>
                                                                    @break    

                                                                @case(4)
                                                                    <span>Management Approved by</span>
                                                                    {{$order->OrderDetail[0]->mgmt_approver->name}}
                                                                    @break 

                                                                @case(5)
                                                                    <span>Invoice Raised</span>
                                                                    @break 

                                                                @case(6)
                                                                    <span>Order Dispatched</span>
                                                                    @break

                                                                @case(9)
                                                                    <span>Parts Order Placed</span>
                                                                    @break
                                                            @endswitch
                                                        </td>
                                                    @endif
                                                    <td width="50">
                                                        <form action="/ViewDealerOrderDetails" method="post">
                                                            @csrf
                                                            <input name="tracking_no" type="hidden" value="{{encrypt($order->tracking_no)}}">
                                                            <button type="submit" class="btn btn-default"><i class="col-green material-icons">search</i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $allIndiaOrders->links() }}   
                            </div>
                        </div>
                    </div>
                </div>
            </div>            

@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    function  FilterAllOrders()
    {
        if(document.getElementById("_state_filter").selectedIndex==0){
            document.getElementById("_district_filter").selectedIndex="0";
            document.getElementById("_dealer_filter").selectedIndex="0";
        }
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterExePlacedOrders",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'order_status': $('#_order_status_filter').val(), 'state':$('#_state_filter').val(), 'district':$('#_district_filter').val(), 'tracking_no':$('#_tracking_no_filter').val(), 'dealer':$('#_dealer_filter').val()},
            success: function(jsonData)
            {
                //$("#tbl_aio").load(location.href + " #tbl_aio");
                //location.reload();
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

    function FetchDealers(){
        document.getElementById("_dealer_filter").selectedIndex = "0";
        $.ajax({
          url: "/FetchDealersByDistricts",
          type: 'GET',
          data: {'districtId': $('#_district_filter').val()},
          success: function(jsonData)
          {
             // alert(JSON.stringify(jsonData));
             $('#dealerGroup').remove();
             $('#lblDealer').after('<div class="form-group" id="dealerGroup"><select id="_dealer_filter" class="form-control"  onchange="FilterAllOrders()"><option value="0">All</option></select></div>');
             $.each(jsonData, function (i, item) {
                $('#_dealer_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
             });
             FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

    $(document).ready(function(){

         FilterAllOrders();
         FetchDistByOneState($('#_state_filter').val());

        

        $('#_state_filter').change(function(){
            FetchDistByOneState($('#_state_filter').val());
        });

        $('#_order_status_filter').change(function(){
            FilterAllOrders(); 
        });

    });

    function GetOrderStatus(status)
        {
            switch(status)
            {
                        case 1:
                            return "Pending";
                            break;

                        case 2:
                            return "Executive Placed";
                            break;

                        case 3:
                            return "Mgmt Approved";
                            break;

                        case 4:
                            return "Payment Verified";
                            break;

                        case 5:
                            return "Invoice Raised";
                            break;

                        case 6:
                            return "Dispatched";
                            break;

                        case 7:
                            return "Rejected";
                            break;
            }
        }

    

    function FetchDistByOneState(stateId)
    {
        $.ajax({
          url: "/FetchDistByOneState",
          type: 'GET',
          data: {'stateId': stateId},
          success: function(jsonData)
          {
            $('#districtGroup').remove();
            $('#lblDistrict').after('<div class="form-group" id="districtGroup"><select id="_district_filter"  class="form-control"  onchange="FetchDealers()"><option value="0">All</option></select></div>');
            var selectedDist=$('#_district_filter').val()
            $.each(jsonData, function (i, item) {

                $('#_district_filter').append($('<option>', { 
                    value: item.id,
                    text : item.name 
                }));
            });
            FilterAllOrders();
          },
          error: function (xhr, ajaxOptions, thrownError) 
          {
            alert("sadsad");
             alert(xhr.status);
             alert(thrownError);
          }
        });
    }

</script> 
@endSection
