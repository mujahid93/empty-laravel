@extends('layouts.header')
@extends('layouts.content')
@section('content')
<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Final Expenses
                            </h2>
                        </div>

                        <div class="header">

                                  

                                    
                        </div>

                        

                    


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Tracking_no</th>
                                            <th>Employee</th>
                                            <th>Expense</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($expenses as $exp)
                                            <tr>
                                                <td>{{date('d-m-Y',strtotime($exp->startDate)).'--'.date('d-m-Y',strtotime($exp->endDate))}}</td>
                                                <td>{{ $exp->transaction_code }}</td>
                                                <td>{{$exp->employee->name}}</td>
                                                <td>{{ $exp->expense }}</td>
                                                <td><a href="{{ '/getExpenseDetails/'.$exp->transaction_code }}"> View </a></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $expenses->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    


   function  FilterAllOrders()
    {
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterExpenses",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'emp_id': $('#emp_filter').val()},
            success: function(jsonData)
            {
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

  function blockRow(rowId)
  {
    /*$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/RemoveImageFromFolder",
          type: 'POST',
          data: {
              'id': rowId
          },
          success: function(jsonData) {
              window.location.reload();
          }
      });*/
  }
  
    
</script>   
  
@endSection