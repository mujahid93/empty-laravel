@extends('layouts.header')
@extends('layouts.content')
@section('content')

<style type="text/css">
    .btn{
        margin-right: 10px;
    }
</style>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>
                                Expense Information
                            </h2>
                            <br/>
                            <table>
                                <td>Employee Name: </td><td><span> {{$expense[0]->employee->name}},  </span></td>
                                @foreach($expense[0]->otherEmp($expense[0]->other_emp) as $emp)
                                    <td><span> {{$emp}} </span></td>
                                @endforeach
                            </table>
                        </div>
                         @if(session()->has('result'))
                         <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{ Session::get('result') }}
                         </div>
                         @endif
                        <div class="body">
                            <div class="row">
                                <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>State</th>
                                            <th>District</th>
                                            <th>City</th>
                                            <th>Outstation</th>
                                            <th>Local</th>
                                            <th>Lodging</th>
                                            <th>Meal</th>
                                            <th>Misc.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <form method="POST" action="{{ route('expense.approve') }}" id="expenseForm">
                                            {{ csrf_field() }}
                                            <?php $total=0; $index=0; ?>
                                            @foreach($expense as $item)
                                                <?php $total+=$item->outstationExps+$item->localExps+$item->lodgingExps+$item->mealExps+$item->miscExps; ?>
                                                <tr>
                                                    <td>{{ $item->date }}</td>
                                                    <td>{{ $item->state->name }}</td>
                                                    <td>{{ $item->district_name }}</td>
                                                    <td>{{ $item->city }}</td>
                                                    <td><input type="number" name="{{$item->id.'-outstationExps'}}" value="{{ $item->outstationExps }}"> </td>
                                                    <td> <input type="number" name="{{$item->id.'-localExps'}}" value="{{ $item->localExps }}"></td>
                                                    <td> <input type="number" name="{{$item->id.'-lodgingExps'}}" value="{{ $item->lodgingExps }}"></td>
                                                    <td><input type="number" name="{{$item->id.'-mealExps'}}" value="{{ $item->mealExps }}"></td>
                                                    <td><input type="number" name="{{$item->id.'-miscExps'}}" value="{{ $item->miscExps }}"></td>
                                                </tr> 
                                                <?php  $index++; ?>
                                            @endforeach
                                            <input type="hidden" name="transaction_code" id="transaction_code" value="{{ $expense[0]->transaction_code }}">
                                            <input type="hidden" name="amount" id="amount" value="{{ $total }}">
                                            <input type="hidden" name="rowCount" id="rowCount" value="{{ $index }}">
                                        </form>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                             <div class="row">
                                 <div class="pull-right button-demo btn-reject-order">
                                          <button type="button" class="btn btn-default">Expenses Total Rs {{IND_money_format($total)}}</button>
                                    </div>
                             </div>
                              <div class="row">
                                    <div class="pull-right button-demo btn-reject-order" onclick="showRejectDialog();">
                                          <button type="button" data-color="red" name="" class="btn bg-purple waves-effect">Reject Tour</button>
                                    </div>
                                    <div class="pull-right button-demo js-modal-buttons" onclick="javaScript:showConfirmDialog();">
                                          <button id="btnApprove" type="button" data-color="green" name="" class="btn bg-green waves-effect">Approve</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     

             <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Approve Tour</h3>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to approve Expense ?</p>
                        </div>
                        <div class="modal-footer">
                            
                                <button type="button" class="btn btn-link waves-effect" onclick="submitForm();">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                                
                        </div>
                    </div>
                </div>
              </div>  
              <div class="modal fade" id="rejectTourModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content  bg-red">
                        <div class="modal-header">
                            <h3 class="modal-title" id="defaultModalLabel">Reject Tour</h3>
                        </div>
                        <div class="modal-body">
                            <p>Do you want to reject this Expense?</p>
                        </div>
                        <div class="modal-footer">
                            <form action="{{ route('expense.reject') }}" method="post">
                                @csrf
                                <input type="hidden" name="transaction_code" value="{{$expense[0]->transaction_code}}">
                                <button type="submit" class="btn btn-link waves-effect">Yes</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
              </div>       
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    

        function showConfirmDialog(){
            var orderStatus = $('#orderStatus').text();
            $('#selOrderStatus').val(orderStatus);
            var color = $(this).data('color');
            $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
            $('#mdModal').modal('show');
        }

        function showRejectDialog(){
                var orderStatus = $('#orderStatus').text();
                $('#selOrderStatus').val(orderStatus);
                var color = $(this).data('color');
                $('#rejectTourModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
                $('#rejectTourModal').modal('show');
        }

        function submitForm(){
            var x = document.getElementById("expenseForm");
            x.submit();
        }
   
    
</script>   
  
@endSection