@extends('layouts.header')
@extends('layouts.content')
@section('content')
<style type="text/css">
    .btn
    {
        margin-right: 10px;
    }
</style>
            <div class="row clearfix" onload="test();">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Final Expenses
                            </h2>
                        </div>

                        <div class="header">

                            <input type="button" id="btnget" value="Search"  onclick="FilterAllOrders();" />

                                    @if(session()->has('expenseFromDate'))
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('expenseFromDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="fromDate" name="fromDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->startOfWeek()->format('Y-m-d')}}">
                                    @endif


                                    @if(session()->has('expenseToDate'))
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::parse(session()->get('expenseToDate'))->format('Y-m-d')}}">
                                    @else
                                        <input type="date" id="toDate" name="toDate" onchange="FilterAllOrders()" value="{{Carbon\Carbon::now()->endOfWeek()->format('Y-m-d')}}">
                                    @endif

                                    <div class="col-sm-3">
                                    <div class="form-group ">
                                            <select id="emp_filter"  class="form-control" onchange="FilterAllOrders()">
                                                    @if(session()->has('expenseEmp'))
                                                      <option value="0">All</option>
                                                    @else
                                                      <option value="0" selected>All</option>
                                                    @endif
                                                    @foreach($employees as $employee)
                                                      @if(session()->has('expenseEmp') && $employee->id==session()->get('expenseEmp'))
                                                          <option value="{{$employee->id}}" selected>{{$employee->name}}</option>                                     
                                                      @else
                                                          <option value="{{$employee->id}}">{{$employee->name}}</option>
                                                      @endif
                                                  @endforeach

                                            </select>
                                    </div>
                                </div>

                                  

                                    
                        </div>

                        

                    


                        @if(session()->has('result'))
                             <div class="alert {{Session::get('color')}} alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    {{ Session::get('result') }}
                             </div>
                        @endif
                        <div class="body">
                            <div class="table-responsive">
                                <table id="_table" class="table table-bordered table-striped table-hover js-basic-example">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Tracking_no</th>
                                            <th>Employee</th>
                                            <th>Expense</th>
                                            <th>Print</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($expenses as $exp)
                                            <tr>
                                                <td>{{date('d-m-Y',strtotime($exp->startDate)).'--'.date('d-m-Y',strtotime($exp->endDate))}}</td>
                                                <td>{{ $exp->transaction_code }}</td>
                                                <td>{{$exp->employee->name}}</td>
                                                <td>{{ $exp->expense }}</td>
                                                @if($exp->is_deleted==0)
                                                <td><a href="{{ '/printExpense/'.$exp->emp_id.'/'.$exp->token }}"> Print </a></td>
                                                @else
                                                  <td><a class="btn disabled"> Print </a></td>
                                                @endif
                                                @if($exp->is_deleted==0)
                                                  <td><a href="{{ '/blockExpense/'.$exp->emp_id.'/'.$exp->token }}"> Block </a></td>
                                                @else
                                                  <td><a href="{{ '/unblockExpense/'.$exp->emp_id.'/'.$exp->token }}"> Unblock </a></td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $expenses->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
@endsection
@extends('layouts.footer')
@section('scripts')

<script type="text/javascript">
    
    


   function  FilterAllOrders()
    {
        $.ajax({
            headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },    
            url: "/FilterExpenses",
            type: 'GET',
            data: {'fromDate':$('#fromDate').val(), 'toDate':$('#toDate').val(), 'emp_id': $('#emp_filter').val()},
            success: function(jsonData)
            {
                window.location = window.location.pathname;
            },
            error: function (xhr, ajaxOptions, thrownError) 
            {
               alert(xhr.status);
               alert(thrownError);
            }
        });      
    }

  function blockRow(rowId)
  {
    /*$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: "/RemoveImageFromFolder",
          type: 'POST',
          data: {
              'id': rowId
          },
          success: function(jsonData) {
              window.location.reload();
          }
      });*/
  }
  
    
</script>   
  
@endSection