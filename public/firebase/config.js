/**
 * @return {!Object} The FirebaseUI config.
 */
 

      const messaging = firebase.messaging();

      messaging.usePublicVapidKey("BM63P2n43lfmfORb6FcAokseF1IFS56yDLj4tU6Ir_fB4QpoSBFLEvlsq2YA5tM2YZ-yausLQyQgj7gSzTQS5GY");

      // Initialize the FirebaseUI Widget using Firebase.

      var ui = new firebaseui.auth.AuthUI(firebase.auth());

      // FirebaseUI config.

      var uiConfig = {

        callbacks: {

        // Called when the user has been successfully signed in.

        'signInSuccess': function(user, credential, redirectUrl) {

          document.getElementById("loader").style.display = "block";

          CheckIfUserAuthorized(user,credential);

          return false;

          }

        },

        signInFlow : 'popup',

        signInOptions: [

          // Leave the lines as is for the providers you want to offer your users.
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        ],

        // Terms of service url.

        tosUrl: '<your-tos-url>'

      };

      

      // The start method will wait until the DOM is loaded.

      ui.start('#firebaseui-auth-container', uiConfig);



      var CheckIfUserAuthorized = function(user,credential){

           firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {

              //Verify user on server
              $.ajax({
                headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },

                url: "CheckUser",

                type: 'POST',

                data: {'user_email': user.email , 'token':idToken},

                success: function(jsonData)

                {

                  document.getElementById("loader").style.display = "none";

                  if(jsonData == "failure"){

                    alert("You are not a verified user")

                    firebase.auth().currentUser.delete();

                  }

                  else{

                    window.location.href = '/home';

                  }

                },

                error: function (xhr, ajaxOptions, thrownError) 

                {

                  

                }

                });



            }).catch(function(error) {

              // Handle error

            });

      };



      

