<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

use App\Models\Branch;
use App\Models\MachineGroup;
use App\Models\MachineSubgroup;
use App\Models\MachineSku;
use App\Models\MachineSubsidy;
use App\Models\AccessoriesGroup;
use App\Models\AccessoriesSubgroup;
use App\Models\AccessoriesSku;
use App\Models\AccSubsidy;
use App\Models\Part;
use App\Models\Tax;
use App\Models\Version;
use App\Models\Order;
use App\Models\Shipping;
use App\Models\Dealer;
use App\Models\CallLog;
use App\Models\OrderDetail;
use App\Models\ApprovedOrder;
use App\Models\EmployeeState;
use App\Models\Email;
use App\Models\EmpDistrict;
use App\Models\District;
use App\Models\State;
use App\Models\SubsidyDept;
use App\Models\Employee;
use App\Models\DealerVisit;
use App\Models\DemoCategory;
use App\Models\FeedbackQuestion;
use Rap2hpoutre\FastExcel\FastExcel;

use App\Http\Resources\Branch as BranchResource;
use App\Http\Resources\District as DistrictResource;
use App\Http\Resources\State as StateResource;
use App\Http\Resources\MachineGroup as MchGrpResource;
use App\Http\Resources\MachineSku as MchSkuResource;
use App\Http\Resources\MchSubsidy as MchSubsidyResource;
use App\Http\Resources\MachineSubgroup as MchSubgrpResource;
use App\Http\Resources\AccGrp as AccGrpResource;
use App\Http\Resources\AccSubgrp as AccSubgrpResource;
use App\Http\Resources\AccSku as AccSkuResource;
use App\Http\Resources\AccSubsidy as AccSubsidyResource;
use App\Http\Resources\Tax as TaxResource;
use App\Http\Resources\Version as VersionResource;
use App\Http\Resources\Shipping as ShippingResource;
use App\Http\Resources\Dealer as DealerResource;
use App\Http\Resources\DealerOrder as DealerOrderResource;
use App\Http\Resources\Order as OrderResource;
use App\Http\Resources\RejectedOrders;
use App\Http\Resources\OrderDetail as OrderDetailResource;
use App\Http\Resources\ApprovedOrder as ApprovedOrderResource;
use App\Http\Resources\MgmtPlacedOrder as MgmtApprOrderResource;
use App\Http\Resources\MchPriceList as MchPriceListResource;
use App\Http\Resources\AccPriceList as AccPriceListResource;
use App\Http\Resources\SubsidyDept as SubsidyDeptResource;
use App\Http\Resources\Employee as EmployeeResource;


Route::group(['middleware' => 'auth:api'], function(){


	        //Api for Dealer Visit
Route::post('saveDealerVisit','DealerVisitController@saveDealerVisit');



    //Sync Data Api for android
    Route::post('getSyncMeta',function(){

        $data=Input::all();
            //Log::info($data);
        $max_branch_updated=$data['branch_update_at'];
        $max_dealer_updated=$data['dealer_update_at'];
        $max_shipping_updated=$data['shipping_update_at'];
        $max_mch_updated=$data['machineSKU_update_at'];
        $max_mchsub_updated=$data['machineSubsidy_update_at'];
        $max_acc_updated=$data['accList_update_at'];
        $max_accsub_updated=$data['accSubsidyList_update_at'];
        $max_subdept_updated=$data['subsidyDept_update_at'];
        $max_district_updated=$data['district_update_at'];
        $max_state_updated=$data['state_update_at'];
        $max_demo_categories_updated=$data['demo_categories_updated_at'];
        $max_feedback_questions_updated=$data['feedback_questions_updated_at'];
        $emp_id=$data['emp_id'];
        $role_id=$data['role_id'];
        $date=Carbon::now()->format('Y-m-d');
        $total=0;

        if($role_id==4 || $role_id==14){
          $info=DB::select("SELECT (SELECT COALESCE(COUNT(*),0) FROM branches WHERE updated_at>'$max_branch_updated') as brcount,(SELECT COALESCE(COUNT(*),0) FROM dealers WHERE updated_at>'$max_dealer_updated') as dlcount, (SELECT COALESCE(COUNT(*),0) FROM shippings WHERE updated_at>'$max_shipping_updated') as spcount,(SELECT COALESCE(COUNT(*),0) FROM machine_skus WHERE updated_at>'$max_mch_updated') as mchcount,(SELECT COALESCE(COUNT(*),0) FROM machine_subsidies WHERE updated_at>'$max_mchsub_updated') as mchsubcount,(SELECT COALESCE(COUNT(*),0) FROM accessories_skus WHERE updated_at>'$max_acc_updated') as account,(SELECT COALESCE(COUNT(*),0) FROM acc_subsidies WHERE updated_at>'$max_accsub_updated') as acsubcount,(SELECT COALESCE(COUNT(*),0) FROM subsidy_dept WHERE updated_at>'$max_subdept_updated') as subdeptcount,(SELECT COALESCE(COUNT(*),0) FROM districts WHERE updated_at>'$max_district_updated') as districtcount,(SELECT COALESCE(COUNT(*),0) FROM states WHERE updated_at>'$max_state_updated') as statecount,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `tour_plan` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as tourcount,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `demo_plans` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as democount");
          $total=$info[0]->brcount+$info[0]->dlcount+$info[0]->spcount+$info[0]->mchcount+$info[0]->mchsubcount+$info[0]->account+$info[0]->acsubcount+$info[0]->subdeptcount+$info[0]->districtcount+$info[0]->statecount+$info[0]->tourcount+$info[0]->democount;
          $res= array('branch'=>$info[0]->brcount,
           'dealer'=>$info[0]->dlcount,
           'shipping'=>$info[0]->spcount,
           'machine'=>$info[0]->mchcount,
           'mch_subsidy'=>$info[0]->mchsubcount,
           'accessory'=>$info[0]->account,
           'acc_subsidy'=>$info[0]->acsubcount,
           'sub_dept'=>$info[0]->subdeptcount,
           'district'=>$info[0]->districtcount,
           'state'=>$info[0]->statecount,
           'tour'=>$info[0]->tourcount,
           'demo_categories'=>0,
           'feedback_questions'=>0,
           'demo'=>$info[0]->democount,
           'total'=>$total);
          Log::info($res);
          return $res;
        }

        if($role_id==2){
          $info=DB::select("SELECT (SELECT COALESCE(COUNT(*),0) FROM branches WHERE updated_at>'$max_branch_updated') as brcount,(SELECT COALESCE(COUNT(*),0) FROM shippings WHERE updated_at>'$max_shipping_updated') as spcount,(SELECT COALESCE(COUNT(*),0) FROM machine_skus WHERE updated_at>'$max_mch_updated') as mchcount,(SELECT COALESCE(COUNT(*),0) FROM machine_subsidies WHERE updated_at>'$max_mchsub_updated') as mchsubcount,(SELECT COALESCE(COUNT(*),0) FROM accessories_skus WHERE updated_at>'$max_acc_updated') as account,(SELECT COALESCE(COUNT(*),0) FROM acc_subsidies WHERE updated_at>'$max_accsub_updated') as acsubcount,(SELECT COALESCE(COUNT(*),0) FROM subsidy_dept WHERE updated_at>'$max_subdept_updated') as subdeptcount");
          $total=$info[0]->brcount+$info[0]->spcount+$info[0]->mchcount+$info[0]->mchsubcount+$info[0]->account+$info[0]->acsubcount+$info[0]->subdeptcount;
          return array('branch'=>$info[0]->brcount,
           'dealer'=>0,
           'shipping'=>$info[0]->spcount,
           'machine'=>$info[0]->mchcount,
           'mch_subsidy'=>$info[0]->mchsubcount,
           'accessory'=>$info[0]->account,
           'acc_subsidy'=>$info[0]->acsubcount,
           'sub_dept'=>$info[0]->subdeptcount,
           'district'=>0,
           'state'=>0,
           'tour'=>0,
           'demo_categories'=>0,
           'feedback_questions'=>0,
           'demo'=>0,
           'total'=>$total);
        }

        if($role_id==3 || $role_id==8 || $role_id==9 || $role_id==10 || $role_id==11){
          $query="SELECT (SELECT COALESCE(COUNT(*),0) FROM branches WHERE updated_at>'$max_branch_updated') as brcount,(SELECT COALESCE(COUNT(*),0) FROM shippings WHERE updated_at>'$max_shipping_updated') as spcount,(SELECT COALESCE(COUNT(*),0) FROM machine_skus WHERE updated_at>'$max_mch_updated') as mchcount,(SELECT COALESCE(COUNT(*),0) FROM machine_subsidies WHERE updated_at>'$max_mchsub_updated') as mchsubcount,(SELECT COALESCE(COUNT(*),0) FROM accessories_skus WHERE updated_at>'$max_acc_updated') as account,(SELECT COALESCE(COUNT(*),0) FROM acc_subsidies WHERE updated_at>'$max_accsub_updated') as acsubcount,(SELECT COALESCE(COUNT(*),0) FROM subsidy_dept WHERE updated_at>'$max_subdept_updated') as subdeptcount,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `tour_plan` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as tourcount,(SELECT COALESCE(COUNT(*),0) FROM employee_district LEFT JOIN districts ON districts.id=employee_district.district_id WHERE emp_id=$emp_id AND districts.updated_at>'$max_district_updated') as districtcount,(SELECT COUNT(*) FROM states WHERE id IN (SELECT DISTINCT(state_id) FROM districts WHERE id IN (SELECT district_id FROM employee_district WHERE emp_id=$emp_id)) AND updated_at>'$max_state_updated') as statecount,(SELECT COALESCE(COUNT(*),0) FROM demo_categories WHERE updated_at>'$max_demo_categories_updated' ) as demo_categories,(SELECT COALESCE(COUNT(*),2) FROM feedback_questions WHERE updated_at>'$max_feedback_questions_updated' ) as feedback_questions,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `demo_plans` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as democount";
          $info=DB::select($query);

              //Log::info($query);

          $dealersCount=DB::select("SELECT COUNT(*) as dlcount FROM dealers LEFT JOIN employee_district ON employee_district.district_id=dealers.district_id WHERE employee_district.emp_id=$emp_id AND dealers.updated_at>'$max_dealer_updated'");

          $total=$info[0]->brcount+$info[0]->spcount+$info[0]->mchcount+$info[0]->mchsubcount+$info[0]->account+$info[0]->acsubcount+$info[0]->subdeptcount+$dealersCount[0]->dlcount+$info[0]->tourcount+$info[0]->districtcount+$info[0]->statecount+$info[0]->demo_categories+$info[0]->feedback_questions+$info[0]->democount;
          return array('branch'=>$info[0]->brcount,
           'dealer'=>$dealersCount[0]->dlcount,
           'shipping'=>$info[0]->spcount,
           'machine'=>$info[0]->mchcount,
           'mch_subsidy'=>$info[0]->mchsubcount,
           'accessory'=>$info[0]->account,
           'acc_subsidy'=>$info[0]->acsubcount,
           'sub_dept'=>$info[0]->subdeptcount,
           'district'=>$info[0]->districtcount,
           'state'=>$info[0]->statecount,
           'tour'=>$info[0]->tourcount,
           'demo_categories'=>$info[0]->demo_categories,
           'feedback_questions'=>$info[0]->feedback_questions,
           'demo'=>$info[0]->democount,
           'total'=>$total);
        }

        if($role_id==17 || $role_id==18){

          $info=DB::select("SELECT (SELECT COALESCE(COUNT(*),0) FROM machine_skus WHERE updated_at>'$max_mch_updated') as mchcount,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `tour_plan` plan LEFT JOIN tour_plan_items item ON plan.id=item.tour_id WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as tourcount,(SELECT COALESCE(COUNT(*),0) FROM employee_district LEFT JOIN districts ON districts.id=employee_district.district_id WHERE emp_id=$emp_id AND districts.updated_at>'$max_district_updated') as districtcount,(SELECT COUNT(*) FROM states WHERE id IN (SELECT DISTINCT(state_id) FROM districts WHERE id IN (SELECT district_id FROM employee_district WHERE emp_id=$emp_id)) AND updated_at>'$max_state_updated') as statecount,(SELECT COALESCE(COUNT(*),0) FROM demo_categories WHERE updated_at>'$max_demo_categories_updated' ) as demo_categories,(SELECT COALESCE(COUNT(*),2) FROM feedback_questions WHERE updated_at>'$max_feedback_questions_updated' ) as feedback_questions,(SELECT COALESCE(COUNT(DISTINCT(token)),0) FROM `demo_plans` plan WHERE plan.date>='$date' AND plan.emp_id='$emp_id') as democount ");

          //Log::info($info);

          $dealersCount=DB::select("SELECT COUNT(*) as dlcount FROM dealers LEFT JOIN employee_district ON employee_district.district_id=dealers.district_id WHERE employee_district.emp_id=$emp_id AND dealers.updated_at>'$max_dealer_updated'");

          $total=$info[0]->mchcount+$dealersCount[0]->dlcount+$info[0]->tourcount+$info[0]->districtcount+$info[0]->statecount+$info[0]->demo_categories+$info[0]->feedback_questions+$info[0]->democount;

          return array('branch'=>0,
           'dealer'=>$dealersCount[0]->dlcount,
           'shipping'=>0,
           'machine'=>$info[0]->mchcount,
           'mch_subsidy'=>0,
           'accessory'=>0,
           'acc_subsidy'=>0,
           'sub_dept'=>0,
           'district'=>$info[0]->districtcount,
           'state'=>$info[0]->statecount,
           'tour'=>$info[0]->tourcount,
           'demo_categories'=>$info[0]->demo_categories,
           'feedback_questions'=>$info[0]->feedback_questions,
           'demo'=>$info[0]->democount,
           'total'=>$total);
        }
      });





         //Branch List Sync
    Route::post('getBranchList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];

      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $branch=Branch::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(250)->get();
      return BranchResource::collection($branch);
    });


         //Dealer List Sync
    Route::post('getDealerList', function () {
      $data = Input::all();
                    //Log::info($data);
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';
      $emp_id=$data['emp_id'];    
      $emp=Employee::where('id',$emp_id)->first();
      if($emp->role_id==4 || $emp->role_id==14){
        $dealers=Dealer::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(500)->get();                        
      }
      else{
        $dealers=Dealer::where('updated_at','>',$updated_at)->whereIn('district_id',EmpDistrict::where('emp_id',$emp_id)->pluck('district_id'))->orderBy('updated_at','ASC')->skip($nextChunck)->take(500)->get();
      }
      return DealerResource::collection($dealers);
    });    


         //Shipping List Sync
    Route::post('getShippingList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $shipping=Shipping::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(250)->get();
      return ShippingResource::collection($shipping);
    });

         //Machine SKU list
    Route::post('getMchSkuList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $machine=MachineSku::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(250)->get();
      return MchSkuResource::collection($machine);
    });

         //Machine Subsidy List
    Route::post('getMchSubsidyList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $mchsubsidy=MachineSubsidy::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(400)->get();
      return MchSubsidyResource::collection($mchsubsidy);
    });

         //Accessories SKU List
    Route::post('getAccSkuList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $sku=AccessoriesSku::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(250)->get();
      return AccSkuResource::collection($sku);
    });

          //Accessories Subsidy list
    Route::post('getAccSubsidyList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $accsubsidy=AccSubsidy::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(250)->get();
      return AccSubsidyResource::collection($accsubsidy);
    });



           //Subsidy department list
    Route::post('getSubsidyDeptList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $subdept=SubsidyDept::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(50)->get();
      return SubsidyDeptResource::collection($subdept);
    });


          //District list
    Route::post('getDistrictList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      $emp_id=$data['emp_id'];
      $emp=Employee::where('id',$emp_id)->first();
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';
      if($emp->role_id==4 || $emp->role_id==14)
        $district=District::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(150)->get();
      else
        $district=District::where('updated_at','>',$updated_at)->whereIn('id',EmpDistrict::where('emp_id',$emp_id)->pluck('district_id'))->orderBy('updated_at','ASC')->skip($nextChunck)->take(150)->get();
      return DistrictResource::collection($district);
    }); 

           //State list
    Route::post('getStateList', function () {
      $data = Input::all();
      $nextChunck=$data['nextChunck'];
      $emp_id=$data['emp_id'];
      $emp=Employee::where('id',$emp_id)->first();
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      if($emp->role_id==4 || $emp->role_id==14){
        $state=State::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(50)->get();
      }
      else{
        $district_ids=EmpDistrict::where('emp_id',$emp_id)->pluck('district_id');
        $sel_state_ids = District::whereIn('id',$district_ids)->pluck('state_id');
        $state=State::where('updated_at','>',$updated_at)->whereIn('id',$sel_state_ids)->orderBy('updated_at','ASC')->skip($nextChunck)->take(50)->get();
      }
      return StateResource::collection($state);
    }); 


           //Category List
    Route::post('getDemoCategories',function(){
      $data=Input::all();
              //Log::info($data);
      $nextChunck=$data['nextChunck'];
      $emp_id=$data['emp_id'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $demoCategories=DemoCategory::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(100)->get();
      return $demoCategories;
    });


           //Feedback Questions
    Route::post('getFeedbackQuestions',function(){
      $data=Input::all();
      $nextChunck=$data['nextChunck'];
      $emp_id=$data['emp_id'];
      if($data['updated_at']!=null)
        $updated_at=$data['updated_at'];
      else
        $updated_at='0000-00-00 00:00:00';

      $questions=FeedbackQuestion::where('updated_at','>',$updated_at)->orderBy('updated_at','ASC')->skip($nextChunck)->take(100)->get();

      return $questions;
    });



            // Refresh Order Status
    Route::get('refreshOrderStatus/{purchaseNo}/{emp_id}/{order_token}',function($purchaseNo,$emp_id,$order_token){
     if($purchaseNo=="NULL"){
       $order=Order::where('emp_id',$emp_id)->where('order_token',$order_token)->first();
       if($order==null){
         return array('isSuccess'=>false,
          'orderToken'=>$order_token,
          'tracking_no'=>null,
          'status'=>-1);
       }
       else{
         $OrderDetail=OrderDetail::where('tracking_no',$order->tracking_no)->first();
         return array('isSuccess'=>true,
          'orderToken'=>$order_token,
          'tracking_no'=>$order->tracking_no,
          'status'=>$OrderDetail->order_status);
       }
     }
     else
     {
      $OrderDetail=OrderDetail::where('tracking_no',$purchaseNo)->first();
      if($OrderDetail!=null){
        return array('isSuccess'=>true,
          'orderToken'=>$order_token,
          'tracking_no'=>$purchaseNo,
          'status'=>$OrderDetail->order_status);
      }
      else{
       return array('isSuccess'=>false,
        'orderToken'=>$order_token,
        'tracking_no'=>null,
        'status'=>-1);                  
     }
    }
    });


             //Get Order Detail
    Route::get('getOrderDetailsByTrackingNo/{tracking_no}', function ($tracking_no) {
      return OrderDetailResource::collection(OrderDetail::where('tracking_no',$tracking_no)->get());
    });


    Route::get('getDealerOrderDetailsByTrackingNo/{tracking_no}', function ($tracking_no) {
      return OrderDetail::where('tracking_no',$tracking_no)->get();
    });


    Route::get('getOrderDetails/{orderId}', function ($orderId) {
      return OrderDetailResource::collection(OrderDetail::where('order_id',$orderId)->get());
    });

             //Place Order Api
    Route::post('placeOrder','ApiController@api_placeOrder');


            //Management Api
    Route::post('applyDiscount','ApiController@api_applyDiscount');
    Route::post('approveOrder','ApiController@api_approveOrder');
    Route::post('rejectOrder','ApiController@api_rejectOrder');
    Route::get('getManagmentSummary','ApiController@FetchManagmentSummary');


            //New shipping from app
    Route::post('newShipping','ApiController@api_newShipping');


            //Executive Api
    Route::get('getDashboardSummary','OrderController@FetchDashboardSummary');


    Route::get('getDealerApprovalList/{page}',function($page){
      $emp_id=Auth::user()->employee->id;
      return DB::select("SELECT dealers_data.id as id, firm,districts.name as district FROM dealers_data LEFT JOIN districts ON dealers_data.district_id=districts.id WHERE dealers_data.district_id IN (SELECT district_id FROM employee_district WHERE emp_id=$emp_id) AND dealers_data.status=1 LIMIT 10 OFFSET $page");
    });


    Route::post('approveDealer',function(){
      $data=Input::all();
      $mobile=$data["nameValuePairs"]["mobile"];
      $dealer_id=$data["nameValuePairs"]["dealer_id"];
      $id=$data["nameValuePairs"]["id"];

                //Check Mobile already Present
      $dealerCount=Dealer::where('mobile1',$mobile)->count();
      if($dealerCount>0){
        return array("status"=>false);
      }

                //Check already approved
      $statusCount=DB::table('dealers_data')->select('status')->where('id',$id)->where('status',1)->count();

      if($statusCount==0){
        return array("status"=>false);
      }
      Dealer::where('id',$dealer_id)->update(["mobile1"=>$mobile]);
      DB::table('dealers_data')->where('id',$id)->update(['status'=>2]);
      return array("status"=>true);
    });


    Route::post('rejectDealer',function(){
      $data=Input::all();
      $id=$data["nameValuePairs"]["id"];


                //Check already approved
      $statusCount=DB::table('dealers_data')->select('status')->where('id',$id)->where('status',1)->count();

      if($statusCount==0){
        return array("status"=>false);
      }

      DB::table('dealers_data')->where('id',$id)->update(['status'=>3]);
      return array("status"=>true);
    });


    Route::get('rejectOrderByDealer/{tracking_no}', function ($tracking_no) {
      $orderDetail=OrderDetail::where('tracking_no',$tracking_no)->first();
      if($orderDetail==null){
        return array('isSuccess'=>false,
         'message'=>'Order does not exist');
      }
      if($orderDetail->order_status==8){
        $rowsAffected=OrderDetail::where('tracking_no',$tracking_no)->update(['order_status'=>7]);
        if($rowsAffected>0){
          $order=Order::where('tracking_no',$tracking_no)->first();
          $emp=Employee::where('id',$order->emp_id)->first();
          $data= array('tracking_no' =>$tracking_no,
           'message'=>'Your order Rejected by dealer',
           'date'=>Carbon::now()->format('d-m-Y g:i a'),
           'fileurl'=>'',
           'name'=>'',
           'status'=>'7',
           'messagetype'=>7);
          $fcm_tokens = Email::where('id',$emp->email_id)->get();
          notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);
          return array('isSuccess'=>true,
            'message'=>'success');
        }
        else
          return array('isSuccess'=>false,
            'message'=>'failed to reject order');
      }
      else{
        return array('isSuccess'=>false,
         'message'=>'Order Already Rejected');
      }
    });

    Route::get('confirmOrderByDealer/{tracking_no}', function ($tracking_no) {
      $orderDetail=OrderDetail::where('tracking_no',$tracking_no)->first();
      if($orderDetail==null){
        return array('isSuccess'=>false,
         'message'=>'Order does not exist');
      }
      if($orderDetail->order_status==8){
        $status=2;
        if($orderDetail->classification="Parts"){
          $rowsAffected=OrderDetail::where('tracking_no',$tracking_no)->update(['order_status'=>9]);
          $status=9;
        }
        else
          $rowsAffected=OrderDetail::where('tracking_no',$tracking_no)->update(['order_status'=>2]);
        if($rowsAffected>0){
          $order=Order::where('tracking_no',$tracking_no)->first();
          $emp=Employee::where('id',$order->emp_id)->first();
          $data= array('tracking_no'=>$tracking_no,
           'message'=>'Your order confirmed by dealer',
           'date'=>Carbon::now()->format('d-m-Y g:i a'),
           'fileurl'=>'',
           'name'=>'',
           'status'=>$status,
           'messagetype'=>2);
          $fcm_tokens = Email::where('id',$emp->email_id)->get();
          notifyUser(array($fcm_tokens[0]['fcm_mobile']),$data);

          return array('isSuccess'=>true,
            'message'=>'success');
        }
        else
          return array('isSuccess'=>false,
            'message'=>'failed to confirm the order');
      }
      else{
        return array('isSuccess'=>false,
         'message'=>'Order Already Confirmed');
      }
    });




            //Management Pending Orders
    Route::get('getPayApprovOrders/{page}', function ($page) {
      return OrderResource::collection(Order::whereHas('OrderDetail', function ($query) {
        $query->where('order_status',3);
      })->where('status',1)->orderBy('created_at', 'desc')->skip($page*10)->take(10)->get());
    });


            //Management All Orders
    Route::post('getAllOrders/{page}', function ($page) {
     $data = Input::all();
     Log:info($data);
     $Status='';
     $branch_id='';
     $FromDate=$data['StartDate'];
     $ToDate=$data['EndDate'];

     if($data['Status']!='-1')
      $Status=$data['Status'];

    if($data['branch_id']!='-1')
      $branch_id=$data['branch_id'];

    return OrderResource::collection(Order::whereHas('OrderDetail', function ($query) use($Status,$branch_id) {
      $query->where('order_status','LIKE','%'.$Status.'%')->where('branch','LIKE','%'.$branch_id.'%');
    })->where('status',1)->where('created_at','>=',$FromDate)->where('created_at','<=',$ToDate)->orderBy('created_at', 'desc')->skip($page*10)->take(10)->get());
    });


            //Search Order by tracking No(Management only api)
    Route::get('searchOrder/{tracking_no}/{page}', function ($tracking_no,$page) {
      Log::info($page);
      return OrderResource::collection(Order::whereHas('Dealer', function ($query) use($tracking_no) {
        $query->where('name','LIKE','%'.$tracking_no.'%');
      })->orWhere('tracking_no','LIKE','%'.$tracking_no.'%')->orderBy('created_at', 'desc')->skip($page*10)->take(10)->get());
    });




            //Block App User Remotely
    Route::get('BlockUser',function(){
      $data= array('purchase_no' =>'3214-B',
       'message'=>'Your account is block due to suspicious activity observed by authorities',
       'date'=>Carbon::now()->format('d-m-Y'),
       'fileurl'=>'',
       'status'=>'1',
       'messagetype'=>6);

      $fcm_tokens = array('c83rwd8XlZQ:APA91bEXz1fvZu_w0jMxZkAbHxPRod-QN8C3N1D354bJawjygf9hf1_kvU_HEiQ0GN9kSBRETR0Dom2yGpJDsPLS4ZD98PXpIsxZHpuDpJGQJtBtt3fyqXR4CBLjRAPkpLYJkHdl7eja');
      notifyUser($fcm_tokens,$data);
    });

    Route::get('UnBlockUser',function(){
      $data= array('purchase_no' =>'3214-B',
       'message'=>'Your account is unblocked and data is restored',
       'date'=>Carbon::now()->format('d-m-Y'),
       'fileurl'=>'',
       'status'=>'1',
       'messagetype'=>7);

      $fcm_tokens = array('c83rwd8XlZQ:APA91bEXz1fvZu_w0jMxZkAbHxPRod-QN8C3N1D354bJawjygf9hf1_kvU_HEiQ0GN9kSBRETR0Dom2yGpJDsPLS4ZD98PXpIsxZHpuDpJGQJtBtt3fyqXR4CBLjRAPkpLYJkHdl7eja');
      notifyUser($fcm_tokens,$data);
    });


            //Get dealer placed orders
    Route::get('getDealerPlacedOrders/{page}', function ($page) {
      $emp_id=Auth::user()->employee->id;
      $offset=$page*10;
      $query="SELECT ordr.tracking_no,dealers.name as dealerName,dealers.id as dealer_id,items.order_date as orderDate,SUM(CASE WHEN items.rate_type=0 THEN (items.qty*items.item_rate)+(((items.qty*items.item_rate)/100)*items.gst) ELSE items.qty*items.item_rate END) as amount FROM orders ordr LEFT JOIN order_details items ON ordr.tracking_no=items.tracking_no LEFT JOIN dealers ON ordr.dealer_id=dealers.id WHERE items.order_status=1 AND dealers.district_id IN (SELECT district_id FROM employee_district dist WHERE emp_id='$emp_id') GROUP BY items.tracking_no ORDER BY ordr.created_at DESC LIMIT 10 OFFSET $offset";
                //Log::info($query);
                    //return DealerOrderResource::collection(DB::select($query));
      return DB::select($query);
    });


    Route::post('approveDealerOrder',function(){
      $data=Input::all();
      Log::info($data);
      $emp_id=Auth::user()->employee->id;
      $count=COUNT($data);
      $branchTrackingNo = array();
                //Check Status Before placing order
      $detail=OrderDetail::where('id',$data[0]['id'])->first();
      if($detail->order_status>1){
        return array('dealer_id'=>-1,
          'tracking_no'=>'',
          'order_token'=>'00000',
          'isSuccess'=>false,
          'isPlaced'=>true);  
      }
      DB::beginTransaction();
      $tracking_no="";
      try{
        $char = 'A';
        $type=$data[0]['rate_type'];
        for($i=0;   $i<$count;  $i++){
          $orderDetail =OrderDetail::where('id',$data[$i]['id'])->first();
          $orderDetail->destination=$data[$i]['destination'];
          $orderDetail->dis_per=$data[$i]['dis_per'];
          $orderDetail->spl_disc=$data[$i]['spl_disc'];
          $orderDetail->other_ship=$data[$i]['other_ship'];
          $orderDetail->freight=$data[$i]['freight'];
          $orderDetail->item_rate=$data[$i]['item_rate'];
          $orderDetail->qty=$data[$i]['qty'];
          $orderDetail->shipping_id=$data[$i]['shipping_id'];
          $orderDetail->branch=$data[$i]['branch'];

          if($type!=$data[$i]['rate_type']){
            $char++;
            $type=$data[$i]['rate_type'];
            $branchTrackingNo_Code = $char;
            $branchTrackingNo[$data[$i]['branch']] = $branchTrackingNo_Code;
          }

          if($branchTrackingNo)
          {
            if (array_key_exists($data[$i]['branch'],$branchTrackingNo))
            {
              $branchTrackingNo_Code = $branchTrackingNo[$data[$i]['branch']];
            }
            else
            {
              $char++;
              $branchTrackingNo_Code = $char;
            }
          }
          else
          {
            $branchTrackingNo_Code = 'A';
          }
          $branchTrackingNo[$data[$i]['branch']] = $branchTrackingNo_Code;
          $orderDetail->branch_tracking_no = $orderDetail->tracking_no."-".$branchTrackingNo_Code;
          $orderDetail->order_status=8;
          $tracking_no=$orderDetail->tracking_no;
          $orderDetail->save();
        }

        $order=Order::where('tracking_no',$tracking_no)->first();
        Order::where('tracking_no',$order->tracking_no)->update(['emp_id'=>$emp_id]);
      }
      catch(Exception $e){
        DB::rollback();
        Log::info("Exception Occurred in dealer order approve ".$e);
        return array('dealer_id'=>-1,
          'tracking_no'=>'',
          'order_token'=>'00000',
          'isSuccess'=>false,
          'isPlaced'=>false);
      }
      DB::commit();
      $res= array('dealer_id'=>$order->dealer_id,
        'tracking_no'=>$tracking_no,
        'order_token'=>$order->order_token,
        'isSuccess'=>true,
        'isPlaced'=>true);

      Log::info($res);

      $dealer=Dealer::where('id',$order->dealer_id)->first();
      $dataLoad=array('tracking_no' =>$tracking_no,
       'message'=>'Your order processed by executive, Order waiting for your confirmation.',
       'date'=>Carbon::now()->format('d-m-Y g:i a'),
       'fileurl'=>'',
       'name'=>'',
       'status'=>'8',
       'messagetype'=>9);

      notifyUser(array($dealer->fcm_mobile),$dataLoad);
      return $res;

    });


    Route::post('getOrderMeta',function(){
     $data=Input::all();
     Log::info($data);
     $trackingList=$data['trackingList'];
     $fromDate=$data['fromDate'];
     $toDate=$data['toDate'].' 23:59:59';
     if($trackingList==null)
      $trackingList="''";

    if($data['dealer_id']==-1){
      $query="SELECT tracking_no FROM orders WHERE tracking_no NOT IN ($trackingList) AND emp_id=".$data['emp_id']." AND created_at BETWEEN '$fromDate' AND '$toDate'";
    }
    else{
      $query="SELECT tracking_no FROM orders WHERE tracking_no NOT IN ($trackingList) AND dealer_id=".$data['dealer_id']." AND created_at BETWEEN '$fromDate' AND '$toDate'";
    }
    $result=DB::select($query);
    return array('isSuccess'=>true,
      'trackingList'=>$result);
    });


    Route::post('syncOrder',function(){
      $data=Input::all();
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,order_details.branch_tracking_no as branch_tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,orignal_qty as originalQty,dis_per,spl_disc,gst,shipping_id,other_ship,destination,freight,order_status as status,order_details.order_date as orderDate,classification,orders.is_dirty as is_dirty FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN (".$data['trackingList'].")  ORDER BY order_details.tracking_no";
      return DB::select($query);
    });



    Route::post('orderNotifManager','NotifController@orderNotifManager');
    Route::post('orderNotifManagment','NotifController@orderNotifManagment');


            //Api For Tour Planning
    Route::post('saveTourPlan','TourPlanningController@saveTourPlan');
    Route::post('saveUpdatedTourPlan','TourPlanningController@saveUpdatedTourPlan');
    Route::get('getTourPlanList','TourPlanningController@getTourPlanList');
    Route::get('getTourSummary/{emp_id}/{token}','TourPlanningController@getTourSummary');
    Route::get('getPendingTourApi/{emp_id}/{token}','TourPlanningController@getPendingTourApi');
    Route::get('getPendingTourDetailsApi/{emp_id}/{token}','TourPlanningController@getPendingTourDetailsApi');
    Route::get('setTourAction/{emp_id}/{token}/{action}','TourPlanningController@setTourAction');
    Route::get('getTourStatus/{emp_id}','TourPlanningController@getTourStatus');
    Route::get('getRecentTours/{emp_id}/{page}','TourPlanningController@getRecentTours');
    Route::get('getTourDetails/{emp_id}/{token}/{type}','TourPlanningController@getTourDetailsApi');
    Route::get('getTourTrackList/{date}/{page}','TourPlanningController@getTourTrackList');
    Route::get('getTourTrackDetails/{date}/{emp_id}','TourPlanningController@getTourTrackDetails');
    Route::get('getTourListManager/{page}',function($page){
      $user=Auth::user()->employee;
      $offset=15*$page;
      if($user->role_id==4 || $user->role_id==14){
        $data=DB::select("SELECT employees.name as empName,CONCAT(MIN(date),'--', MAX(date)) as date, SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,emp_id,token,plan.status FROM tour_plan plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE emp_id=$user->id OR emp_id IN (SELECT id FROM employees WHERE emp_id is NULL) GROUP BY token ORDER BY date LIMIT 15 OFFSET $offset");
      }
      else{
        $data=DB::select("SELECT employees.name as empName,CONCAT(MIN(date),'--', MAX(date)) as date, SUM(localExps+lodgingExps+mealExps+miscExps+outstationExps) as cost,emp_id,token,plan.status FROM tour_plan plan LEFT JOIN employees ON employees.id=plan.emp_id WHERE emp_id=$user->id OR  emp_id IN (SELECT id FROM employees WHERE manager_id='$user->id') GROUP BY token ORDER BY date LIMIT 15 OFFSET $offset");
      }
      return $data;
    });



            //Demo plan
    Route::post('saveDemoPlan','DemoPlanController@saveDemoPlan');
    Route::get('getPendingDemoPlanList/{emp_id}','DemoPlanController@getPendingDemoPlanList');
    Route::get('getDemoPlanDetails/{emp_id}/{token}','DemoPlanController@getDemoPlanDetails');
    Route::get('setDemoAction/{emp_id}/{token}/{action}','DemoPlanController@setDemoAction');
    Route::get('getDemoStatus/{emp_id}','DemoPlanController@getDemoStatus');

            //Feedback
    Route::post('saveFeedback','FeedbackController@saveFeedback');


            //Expenses
    Route::post('saveTourExpense','TourPlanningController@saveTourExpense');
    Route::get('getPendingExpenseListApi/{page}','TourPlanningController@getPendingExpenseListApi');
    Route::get('getExpenseDetailsApi/{transaction_code}','TourPlanningController@getExpenseDetailsApi');
    Route::get('rejectExpense/{transaction_code}','TourPlanningController@rejectExpense');
    Route::post('expenseUpdateAndApproveApi','TourPlanningController@expenseUpdateAndApproveApi');
    Route::get('getExpenseListHistoryApi/{page}','TourPlanningController@getExpenseListHistoryApi');
	
	
	    //Parts Sync api
	Route::post('getParts',function(){
	 /*if(Auth::user()->employee->id=='1007' || Auth::user()->employee->id=='170' || Auth::user()->employee->id=='177' || Auth::user()->employee->id=='711' || Auth::user()->employee->id=='1175' || Auth::user()->employee->id=='583'){*/
		 $headers = array('Content-Type: text/csv');
		 $data=Input::all();
		 //Log::info($data);
		 if($data['updated_at']==null)
			$data['updated_at']="0000-00-00 00:00:00";
		 $parts=Part::where('updated_at','>',$data['updated_at'])->select(DB::raw("id,nav_no,REPLACE(name1,',',';') as name1,REPLACE(name2,',',';') as name2,REPLACE(sku,',',';') as sku,hsn,gst,dp,mrp,subsidy,REPLACE(mch_ids,',',';') as mch_ids,REPLACE(acc_ids,',',';') as acc_ids,status,updated_at"))->get();
		 return (new FastExcel($parts))->download('parts.csv');
	 //}
	 /*else{
		 $headers = array('Content-Type: text/csv');
		 $data=Input::all();
		 Log::info($data);
		 if($data['updated_at']==null)
			$data['updated_at']="0000-00-00 00:00:00";
		 $parts=Part::where('updated_at','>',$data['updated_at'])->select(DB::raw("id,nav_no,REPLACE(name1,',',';') as name1,REPLACE(name2,',',';') as name2,REPLACE(sku,',',';') as sku,hsn,gst,dp,mrp,subsidy,REPLACE(mch_ids,',',';') as mch_ids,REPLACE(acc_ids,',',';') as acc_ids,status,updated_at"))->take(0)->get();
		 return (new FastExcel($parts))->download('parts.csv');
	 }*/
	});
});
Route::post('saveLiveTrack','LiveTrackController@saveLiveTrack');



Route::post('saveCallLog',function(){

    $data=Input::all();
    DB::beginTransaction();

    $response=array();

    //Adding all controlls in try catch block 
    try{
      for ($i=0; $i < COUNT($data); $i++) { 
        $log=new CallLog();
        $log->emp_id=$data[$i]['emp_id'];
        $log->dealer_id=$data[$i]['dealer_id'];
        $log->dealer_name=$data[$i]['dealer_name'];
        $log->mobile=$data[$i]['mobile'];
        $log->duration=$data[$i]['duration'];
        $log->call_time=$data[$i]['call_time'];
        $log->status=1;
        $log->save();
      }
    }
    catch(Exeption $e){
        //There is some error occured while placing order and need rollback
        DB::rollback();
        $response=array('isSuccess' => false,'message'=>'Error in saving call log ');
        return $response;
    }

    //Order is successfully added in database and now we can commit to finalize the changes.
    DB::commit();
    $response=array('isSuccess' => true,'message'=>'Call Log Synced');
    return $response;

});




//Check Login Email
Route::post('verifyEmail',function(){
  $data = Input::all();
  Log::info($data);
  $email=$data['nameValuePairs']['email'];
  $imei=$data['nameValuePairs']['imei'];
  $fcm_app=$data['nameValuePairs']['fcm_token'];
  $newEmail= Email::where('email',$email)->where('status',1)->first();
  if($newEmail==null){
    return array('status' => false);
  }
  try{
    $version=$data['nameValuePairs']['version'];
    $newEmail->version=$version;
  }
  catch(Exception $e){
    Log::info("Version OLD");
  }
  $newEmail->fcm_mobile=$fcm_app;
  $newEmail->save();
  if($newEmail->imei==null){
    //$newEmail->imei=$imei;
    $newEmail->imei=null;
    $newEmail->save();
    return array('status' => true);
  }
  else if($newEmail->imei!=$imei)
    return array('status' => false);
  else
    return array('status' => true);
});


//Login Api
Route::post('getEmailAccessToken','LoginController@getEmailAccessToken');
Route::post('getOTPAccessTokenDealer','LoginController@getOTPAccessTokenDealer');
Route::post('getOTPAccessTokenAgronomy','LoginController@getOTPAccessTokenAgronomy');




        //Api for order status sync
Route::post('getRefreshStatus',function(){
  $data=Input::all();
  $tracking_no=$data['tracking_no'];
  return DB::select("SELECT tracking_no,MAX(order_status) as status FROM order_details WHERE tracking_no IN ($tracking_no) GROUP BY tracking_no");
});

Route::get('getDealerData/{id}',function($id){
  $data= DB::select("SELECT dealers_data.id,dealers_data.name,firm,mobile,gst_no,states.name as state,districts.name as district ,(CASE WHEN dealers.id is NULL THEN 0 ELSE dealers.id END) as dealer_id,dealers_data.status FROM `dealers_data` LEFT JOIN dealers ON dealers.id=dealers_data.dealer_id LEFT JOIN districts ON districts.id=dealers_data.district_id LEFT JOIN states ON states.id=dealers_data.state_id WHERE dealers_data.id=$id");
  $data=json_encode($data);
  $data=json_decode($data,true);
  return $data[0];
});

//getPeriodicOrders
    Route::post('getPeriodicOrders',function(){
      $data=Input::all();
      Log::info($data);
      $trackingList=array();
      $trackingArray=array();
      for ($i=0; $i < COUNT($data); $i++) { 
        $trackingList[]=$data[$i]['tracking_no'];
        $trackingArray[$data[$i]['tracking_no']]=$data[$i]['is_dirty'];
      }
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,order_details.branch_tracking_no as branch_tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,orignal_qty as originalQty,dis_per,spl_disc,gst,shipping_id,other_ship,destination,freight,order_status as status,order_details.order_date as orderDate,classification,orders.is_dirty as is_dirty FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN ('".implode("','", $trackingList)."')  ORDER BY order_details.tracking_no";
      $result=DB::select($query);
      $response=array();
      for ($i=0; $i < COUNT($result); $i++) { 
        if($result[$i]->is_dirty!=$trackingArray[$result[$i]->tracking_no])
          array_push($response, $result[$i]);
        else{
          $orderItem=new stdClass();
          $orderItem->tracking_no=$result[$i]->tracking_no;
          $orderItem->is_dirty=$result[$i]->is_dirty;
          $orderItem->branch_tracking_no=$result[$i]->branch_tracking_no;
          $orderItem->status=$result[$i]->status;
          array_push($response, $orderItem);
        }
      }
      return $response;
    });




        //Route::get('testNotif','TourPlanningController@testNotif');



function notifyUser($firebaseIds,$data){
 $url = 'https://fcm.googleapis.com/fcm/send';
 $fields = array (
  'registration_ids' => $firebaseIds,
  'data'=>$data
);
 $fields = json_encode ( $fields );
 $headers = array (
  'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
  'Content-Type: application/json'
);
 $ch = curl_init ();
 curl_setopt ( $ch, CURLOPT_URL, $url );
 curl_setopt ( $ch, CURLOPT_POST, true );
 curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
 curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
 curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
 $result = curl_exec ( $ch );
 Log::info($result);
           // echo $result;
 curl_close ( $ch );
}

/*Route::get('getEmpList/{page}/{filter}',function($page,$filter){
 $offset=$page*15;
           //Log::info("Offset:".$offset);
 if($filter=="Alpha")
  $EmpData=DB::select("SELECT employees.id as id,employees.name as name,SUM(GetTotal(od.qty,od.item_rate,od.dis_per,od.gst,od.rate_type)) as sale FROM orders LEFT JOIN order_details od ON od.order_id=orders.id LEFT JOIN employees ON employees.id=orders.emp_id WHERE od.order_date BETWEEN '2019-03-01' AND '2019-04-30' GROUP BY orders.emp_id ORDER BY name ASC LIMIT 15 OFFSET $offset");
else if($filter=="ASC")
  $EmpData=DB::select("SELECT employees.id as id,employees.name as name,SUM(GetTotal(od.qty,od.item_rate,od.dis_per,od.gst,od.rate_type)) as sale FROM orders LEFT JOIN order_details od ON od.order_id=orders.id LEFT JOIN employees ON employees.id=orders.emp_id WHERE od.order_date BETWEEN '2019-03-01' AND '2019-04-30' GROUP BY orders.emp_id ORDER BY sale ASC LIMIT 15 OFFSET $offset");
else if($filter=="DSC")
  $EmpData=DB::select("SELECT employees.id as id,employees.name as name,SUM(GetTotal(od.qty,od.item_rate,od.dis_per,od.gst,od.rate_type)) as sale FROM orders LEFT JOIN order_details od ON od.order_id=orders.id LEFT JOIN employees ON employees.id=orders.emp_id WHERE od.order_date BETWEEN '2019-03-01' AND '2019-04-30' GROUP BY orders.emp_id ORDER BY sale DESC LIMIT 15 OFFSET $offset");
return $EmpData;
           //return EmployeeResource::collection($EmpData);

});*/

        //Sync Order list
Route::post('syncMyOrders', function () {
  $data = Input::all();
                //Log::info($data);
  $nextChunck=$data['nextChunck'];
  $tracking_no=$data['tracking_no'];
  $emp_id=$data['emp_id'];    
  $sync_till=$data['sync_till_tracking_no'];
  $offset=$nextChunck*4;

  if($sync_till!="NULL"){
    $order=Order::where('tracking_no',$tracking_no)->first();
    $last_order=Order::where('tracking_no',$sync_till)->first();
    if($order!=null)
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,dis_per,gst,shipping_id,destination,freight,order_status as status,order_details.order_date as orderDate,classification FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN (SELECT * FROM( SELECT tracking_no FROM orders WHERE emp_id='$emp_id' AND created_at>'$order->created_at' AND id<$last_order->id order by id LIMIT 4 OFFSET $offset) a)";
    else
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,dis_per,gst,shipping_id,destination,freight,order_status as status,order_details.order_date as orderDate,classification FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN (SELECT * FROM( SELECT tracking_no FROM orders WHERE emp_id='$emp_id' AND id<$last_order->id order by id LIMIT 4 OFFSET $offset) a)";
  }
  else{
    $order=Order::where('tracking_no',$tracking_no)->first();
    if($order!=null)
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,dis_per,gst,shipping_id,destination,freight,order_status as status,order_details.order_date as orderDate,classification FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN (SELECT * FROM( SELECT tracking_no FROM orders WHERE emp_id='$emp_id' AND created_at>'$order->created_at' order by id LIMIT 4 OFFSET $offset) a)";
    else
      $query="SELECT orders.dealer_id as dealer_id,order_details.tracking_no as tracking_no,branch as branch_id,grp,subgrp,sku,rate_type,subsidy_dept_id,item_rate,qty,dis_per,gst,shipping_id,destination,freight,order_status as status,order_details.order_date as orderDate,classification FROM `order_details` LEFT JOIN orders ON order_details.tracking_no=orders.tracking_no WHERE  order_details.tracking_no IN (SELECT * FROM( SELECT tracking_no FROM orders WHERE emp_id='$emp_id' order by id LIMIT 4 OFFSET $offset) a)";                    
  }

  $OrderDetails=DB::select($query);
  return $OrderDetails;
}); 





Route::get('deleteOrder/{o_no}',function($o_no){
  DB::beginTransaction();
  try{
    $orderDetail=OrderDetail::whereIn('order_status',[2,1,9])->where('tracking_no',$o_no)->first();
    if($orderDetail==null){
      DB::commit();
      return array('status'=>false);
    }
    OrderDetail::where('tracking_no',$o_no)->delete();
    Order::where('tracking_no',$o_no)->delete();

  }
  catch(Exeption $e){
    DB::rollback();
    return array('status'=>false);
  }
  DB::commit();
  $data= array('tracking_no' =>$o_no,
   'message'=>'Order with tracking no '.$o_no." deleted by the executive",
   'date'=>Carbon::now()->format('d-m-Y g:i a'),
   'fileurl'=>'',
   'name'=>"",
   'status'=>'8',
   'messagetype'=>8);
  $url = 'https://fcm.googleapis.com/fcm/send';
  $fields = array (
    'to' => '/topics/Payment_Approver',
    'data' => $data
  );
  $fields = json_encode ( $fields );
  $headers = array (
    'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
    'Content-Type: application/json'
  );
  $ch = curl_init ();
  curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt ( $ch, CURLOPT_POST, true );
  curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
  curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
  $result = curl_exec ( $ch );
  curl_close ( $ch );
  return array('status'=>true);
});

Route::get('getEmpList',function(){
  return Employee::where('status',1)->take(20)->get(['id','name','date_of_join']);
});


Route::get('getAppVersion/{appVersion}/{type}',function($appVersion,$type){
  $res= array("status"=>true,"app_version"=>$appVersion);
  return $res;

      //Checking Latest Version
  $ver=Version::where('type',0)->orderBy('id','DESC')->first();
  if($ver->code==$appVersion){
    $test= array("status"=>true,"app_version"=>$ver->code);
  }
  $version=Version::where('type',$type)->orderBy('id','DESC')->first();
  if($version->code==$appVersion)
    $test= array("status"=>true,"app_version"=>$version->code);
  else
    $test= array("status"=>false,"app_version"=>$version->code);
		  Log::info($test);
  return $test;

});



function notifByDeveloper($data){
 $url = 'https://fcm.googleapis.com/fcm/send';
 $fields = array (
  'to' => '/topics/Developer',
  'data' => $data
);
 $fields = json_encode ( $fields );
 $headers = array (
  'Authorization: key=' . "AAAA5hj__SU:APA91bFWPyt6j25UdcipzQgGl-noRzJUOOn5_L5UovRwAnHoJK2ltH7KwhvP9_fwwHE-1UEcIyW0E1B7fm_0z6gekZxEnshCk3YR1UDyOY6534GbtQqwUbpP0YYmcNLjOxbqbAcH_v8ZrWKqHuxeItMEDYhFk4Tx_g",
  'Content-Type: application/json'
);
 $ch = curl_init ();
 curl_setopt ( $ch, CURLOPT_URL, $url );
 curl_setopt ( $ch, CURLOPT_POST, true );
 curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
 curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
 curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
 $result = curl_exec ( $ch );
 curl_close ( $ch );
}