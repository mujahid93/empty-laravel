<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//MGMT Routes
Route::group(['middleware' => ['auth','isUserMgmt']], function () {
        Route::get('PayApprovedOrders','OrderController@PayApprovedOrders')->name('order.pay_verified');
        Route::post('ApplyDiscount','OrderController@ApplyDiscount');
        Route::post('ViewPayApprovedOrders','OrderController@ViewPayApprovedOrders');
        Route::get('FilterPurchaseNumber','OrderController@FilterPurchaseNumber');
        Route::get('rejectedOrders','OrderController@rejectedOrders')->name('order.rejected');
        Route::get('FilterRejectedOrders','FilterController@FilterRejectedOrders');
        Route::get('FilterInvoiceOrders','FilterController@FilterInvoiceOrders');
        Route::get('FilterDispatchOrders','FilterController@FilterDispatchOrders');
        Route::get('gmaps', 'LiveTrackController@gmaps');

});

//Sales Routes
Route::group(['middleware' => ['auth','isUserSales']], function () {
        Route::post('ApplyDiscountByExecutive','OrderController@ApplyDiscountByExecutive');
        Route::get('DealerPlacedOrders', 'OrderController@DealerPlacedOrders')->name('order.dealerOrderList');;
        Route::get('ViewOrderDetailsPlacedByDealer', 'OrderController@ViewOrderDetailsPlacedByDealer')->name('order.dealerPlaced');
        Route::resource('order', 'OrderController');
        Route::post('FetchDealersByState','OrderController@FetchDealersByState');
        Route::post('FetchGroup', 'OrderController@FetchGroup');
        Route::post('FetchPriceRateByBranch', 'OrderController@FetchPriceRateByBranch');
        Route::post('FetchSubgrpByGrp', 'OrderController@FetchSubgrpByGrp');
        Route::post('FetchSubgroupByGroup','MachineSkuController@FetchSubgrpByGrp');
        Route::post('FetchSkuBySubgrp', 'OrderController@FetchSkuBySubgrp');
        Route::post('FetchPriceBySku', 'OrderController@FetchPriceBySku');
        Route::post('FetchSubsidyRateByDealer', 'OrderController@FetchSubsidyRateByDealer');
        Route::post('FetchSubsidyRateByDept', 'OrderController@FetchSubsidyRateByDept');
        Route::get('IncompleteOrders', 'OrderController@IncompleteOrders')->name('order.incomplete_orders');
        Route::post('ViewCheckoutPage', 'OrderController@ViewCheckoutPage');
        Route::get('CheckoutCart', 'OrderController@CheckoutCart')->name('order.checkout');
        Route::post('ConfirmOrder', 'OrderController@ConfirmOrder');
        Route::post('ClearCart', 'OrderController@ClearCart')->name('order.clearCart');
        Route::post('RemoveItem', 'OrderController@RemoveItem')->name('order.remove_item');
        Route::post('AmendOrder','OrderController@AmendOrder');
        Route::get('Cart_AmendOrder','OrderController@Cart_AmendOrder')->name('order.cart_amend');
        Route::get('Executive__Placed_orders','OrderController@Executive__Placed_orders');
        Route::get('Executive__rejected_orders','OrderController@Executive__rejected_orders');
        Route::get('Executive__Myregion_orders','OrderController@Executive__Myregion_orders');
        Route::post('SplitOrder', 'OrderController@SplitOrder')->name('order.split');
        Route::get('PendingOrders','OrderController@PendingOrders');
        Route::post('DeleteOrder','OrderController@DeleteOrderByExecutive');
        Route::post('ChangeFreight','OrderController@ChangeBranchFreight');
        Route::post('UpdateOtherShip','OrderController@UpdateOtherShip');
        Route::post('SaveShippingData','OrderController@SaveShippingData');
        Route::post('GetDealerConfirmation','OrderController@GetDealerConfirmation');        
});

//Payment Approver Routes
Route::group(['middleware' => ['auth','isUserPayAppr']], function () {
        Route::get('ExecApprovedOrders','OrderController@ExecApprovedOrders')->name('order.exec_placed');
        Route::get('FilterPayPendingOrders','FilterController@FilterPayPendingOrders');
});

//Sales Coordinator Routes
Route::group(['middleware' => ['auth','isUserSC']], function () {
        Route::get('MgmtApprovedOrders','OrderController@MgmtApprovedOrders');
        Route::get('FilterMgmtApprovedOrders','FilterController@FilterMgmtApprovedOrders');
        Route::post('new_invoice', 'InvoiceController@NewInvoice')->name('invoice.new_invoice');
        Route::post('ViewMgmtApprOrderDetail','OrderController@ViewMgmtApprOrderDetail');
});



//Can user view invoice
Route::group(['middleware' => ['auth','CanUserViewInvoice']], function () {
        Route::resource('invoice', 'InvoiceController');
        Route::post('file', 'InvoiceController@getFile');
});

//Visible to all routes
Route::group(['middleware' => ['auth']], function () {

    //Tour Planning
        Route::get('pendingTour','TourPlanningController@pendingTour')->name('pendingTour');
        Route::get('pendingAllTour','TourPlanningController@pendingAllTour');
        Route::get('approvedTour','TourPlanningController@approvedTour')->name('approvedTour');
        Route::get('tourPrint/{emp_id}/{token}','TourPlanningController@tourPrint');
        Route::get('tourDelete/{emp_id}/{token}','TourPlanningController@tourDelete');
        Route::get('tourDetails/{emp_id}/{token}','TourPlanningController@tourDetails');
        Route::post('tourApprove','TourPlanningController@tourApprove')->name('tour.approve');
        Route::post('tourReject','TourPlanningController@tourReject')->name('tour.reject');
        Route::post('tourUpdate','TourPlanningController@tourUpdate')->name('tour.update');
        Route::get('tourEdit/{tour_id}','TourPlanningController@tourEdit');

    //Demo Plan
        Route::get('getWebPendingDemoList','DemoPlanController@getWebPendingDemoList')->name('pendingDemo');
        Route::get('pendingDemoDetails/{emp_id}/{token}','DemoPlanController@pendingDemoDetails');
        Route::get('getAllDemoDetails/{emp_id}/{token}','DemoPlanController@getAllDemoDetails');
        Route::post('demoApprove','DemoPlanController@demoApprove')->name('demo.approve');
        Route::post('demoReject','DemoPlanController@demoReject')->name('demo.reject');
        Route::get('approvedDemo','DemoPlanController@getWebApprovedDemoList')->name('approvedDemos');
        Route::get('demoPrint/{emp_id}/{token}','DemoPlanController@demoPrint');
        Route::get('demoPlanExcel/{emp_id}/{token}','DemoPlanController@excel');

    //Feedback Reports
        Route::get('getDemoFeedbackReports','FeedbackController@getDemoFeedbackReports');
        Route::get('getDemoFeedbackList','FeedbackController@getDemoFeedbackList');
        Route::get('exportFeedbackReport/{fromDate}/{toDate}/{subcat_id}','FeedbackController@exportFeedbackReport');
		Route::get('exportSingleFeedbackReport/{feedback_id}','FeedbackController@exportSingleFeedbackReport');
        
        Route::get('FetchMchPriceList','OrderController@FetchMchPriceList');
        Route::get('FetchAccPriceList','OrderController@FetchAccPriceList');

    //Common Routes

        //Order Routes
        Route::get('GetAllNotifications','OrderController@GetAllNotifications');
        Route::post('FetchMchGrpList','OrderController@FetchMchGrpList');
        Route::post('ViewOrderDetails','OrderController@ViewOrderDetails');
        Route::post('ChangeItemQtyByExec','OrderController@ChangeItemQtyByExec');

        //Machinery & Accessories Price List
        Route::get('GetMchPriceList','OrderController@GetMchPriceList')->name('order.mch_price_list');
        Route::get('GetAccPriceList','OrderController@GetAccPriceList')->name('order.acc_price_list');

        //Firebase Messaging Token Routes
        Route::post('sendTokenToServer','TestController@sendTokenToServer');
        Route::post('getCurrentUserEmail','TestController@getCurrentUserEmail');
        Route::get('ShowSuccessPage', 'OrderController@ShowSuccessPage')->name('order.success');
        
        //Filters
        Route::get('FilterExePlacedOrders','FilterController@FilterExePlacedOrders');
        Route::get('FilterAllOrdersByMgmt','OrderController@FilterAllOrdersByMgmt');
        Route::get('FilterAllOrdersByExec','OrderController@FilterAllOrdersByExec');
        Route::get('FilterAllByPurchaseNumber','OrderController@FilterAllByPurchaseNumber');
        Route::get('FilterByDealer','OrderController@FilterByDealer');
        Route::get('FilterAllByDealer','OrderController@FilterAllByDealer');
        Route::get('FilterByOrderStatus','OrderController@FilterByOrderStatus');
        Route::get('FilterAllByOrderStatus','OrderController@FilterAllByOrderStatus');
        Route::get('FilterByFromAndToDate','OrderController@FilterByFromAndToDate');
        Route::get('FilterAllByFromAndToDate','OrderController@FilterAllByFromAndToDate');
        Route::get('FilterByGrpName','OrderController@FilterByGrpName');
        Route::get('FilterBySubgrpName','OrderController@FilterBySubgrpName');
        Route::get('FilterBySkuName','OrderController@FilterBySkuName');
        Route::get('FilterByHsn','OrderController@FilterByHsn');
        Route::get('FilterMyRegionOrders','FilterController@FilterMyRegionOrders');
        Route::get('FilterPayApprovedOrders','FilterController@FilterPayApprovedOrders');
        Route::get('FilterDealerVisited','FilterController@FilterDealerVisited');
        Route::get('FilterEmployeeTracking','FilterController@FilterEmployeeTracking');

        //Executive
        Route::post('ViewExecOrderDetails','OrderController@ViewExecOrderDetails');
        Route::post('ChangeOrderStatus','OrderController@ChangeOrderStatus')->name('order.exec_approve');
        Route::post('ViewOrderPdf','OrderController@ViewOrderPdf')->name('order_pdf');
        Route::post('LoadShipMode','OrderController@LoadShipMode');
        Route::post('FetchPriceRate','OrderController@FetchPriceRate');
        Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
        
        Route::get('cart','OrderController@cart')->name('order.cart');
        Route::post('RemoveItemFromCart','OrderController@RemoveItemFromCart');

        //Dispatch Routes
        Route::resource('dispatch', 'DispatchController');
        Route::post('new_dispatch', 'DispatchController@NewDispatch')->name('dispatch.new_dispatch');
        Route::post('GetShippingCompanies', 'OrderController@GetShippingCompanies');

        //Shipping Routes
        Route::resource('shipping', 'ShippingController');

        //Home Routes
        Route::post('FetchDashboardSummary', 'OrderController@FetchDashboardSummary');
        Route::get('AllIndiaOrders','OrderController@Executive__Placed_orders');

        //Notification
        Route::post('GetNotificationCount','OrderController@GetNotificationCount');
        Route::post('ViewDealerOrderDetails','OrderController@ViewDealerOrderDetails');

        Route::get('FetchDistByOneState', 'DealerController@FetchDistByOneState');
        Route::get('FetchDealersByDistricts', 'DealerController@FetchDealersByDistricts');

        Route::resource('dealerVisit', 'DealerVisitController');
        Route::post('RemoveImageFromFolder','DealerVisitController@RemoveImageFromFolder');
        Route::get('/dealerVisits/excel',[
              'as' => 'admin.visit.excel',
              'uses' => 'DealerVisitController@excel'
        ]);
        Route::get('getImageZip','DealerVisitController@zipFile');

        //Employee routes
        Route::resource('employee', 'EmployeeController');
        Route::post('importEmployees', 'EmployeeController@importEmployees')->name('employee.import');
        Route::get('FetchDistByState', 'EmployeeController@FetchDistByState');
});

//Software Developer Routes
Route::group(['middleware' => ['auth','isUserSD']], function () {
        //New Shipping Requests
        Route::get('NewShippingRequest', 'OrderController@GetNewShippingRequest');
        Route::get('NewShippingDetail', 'OrderController@GetNewShippingDetail');
        Route::post('UpdateOrderShipping', 'OrderController@DoUpdateOrderShipping');
        Route::post('KeepNewShipping', 'OrderController@DoKeepNewShipping');


        //Role Routes
        Route::resource('role', 'RoleController');
        Route::get('FilterRole','RoleController@FilterRole');
        
        //Dept Routes
        Route::resource('dept', 'DeptController');
        
        //State Routes
        Route::resource('state', 'StateController');
        
        //Branch Routes
        Route::resource('branch', 'BranchController');
        
        //District Routes
        Route::resource('district', 'DistrictController');
        Route::post('importDistricts', 'DistrictController@importDistricts')->name('district.import');

        //Dealer routes
        Route::resource('dealer', 'DealerController');
        Route::post('importDealers', 'DealerController@importDealers')->name('dealer.import');

        

        //Controller methods for use by dealer
        // Route::get('FilterDealer','DealerController@FilterDealer');

        Route::get('GetDealerOrders', 'OrderController@GetDealerOrders')->name('order.dealer_orders');
        

        //Tax Routes
        Route::resource('tax', 'TaxController');
        
        //Machine Routes
        Route::post('importMachinerySku', 'MachineSkuController@importMachinerySku')->name('machineGroup.import');
        Route::resource('machineSku', 'MachineSkuController');
        
        //Accessory Routes
        Route::post('importAccessories', 'AccessoriesSkuController@importAccessories')->name('accGroup.import');
        Route::resource('accSku', 'AccessoriesSkuController');
});
        //Login routes
        Route::post('CheckUser','TestController@CheckUser');

        //User verification for android
        Route::post('verifyEmail','TestController@verifyEmail');
        Route::post('getAccessToken','TestController@getAccessToken');
        

       